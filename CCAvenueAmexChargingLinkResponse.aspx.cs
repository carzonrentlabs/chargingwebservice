﻿using System;
using System.Collections.Specialized;
using CCA.Util;
using ChargingWebService;
using System.Data;

public partial class CCAvenueAmexChargingLinkResponse : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string order_id = "", tracking_id = "", bank_ref_no = ""
            , order_status = "", amount = ""
            , payment_mode = "", card_name = "", status_message = "";

        if (!IsPostBack)
        {
            string workingKey = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueAmexCCAvenueWorkingKey");
            CCACrypto ccaCrypto = new CCACrypto();

            string encResp = Request.Form["encResp"];
            //string encResp = "d6d28fb24983a5ffdce539face70f8f090c77970677312f4d8de104fb03c867291b821087a90b4c4df063aa0356cab33dc04a9327773b91141d67a9e4892b09d050bbaceba4e44d26f9b9f41ab0f83d2d99376320ff51cdb94bc459db1d88be49a730017f983e9d9400710a82b922549aed9f33730cc0bbf0902779daf51b400c219ad975c324f42ba75b3238e055d0c64974134ec0429fbb5fb3271fbc16aae802c74b8ac0c0281c815f4e659c073c9055f60cb9eca2262fea255496650929638069da848b6ad693ebe9c87b3ce0e62d2680a1aa9f26fce6fe2cc3913fa149fb3273e129847194adc670ac4bc5051db78393b12cece03447f307b232bc418d816b4e3f241684ef4cce24c76b03f19f7c0edb5b3589f82855b2bfde2ad3c34bac5bc5e44a7317fae157daa3d95def2725ab9dbd61013fb1df7c9cf03702f66404bbec17b02b6d643da0665fe99fe8e654e3e992740482881570c36343375402c9647bf7bb302e87385cc58274d53e432f8ef9af65c9050481cb36605649275041d3b5501fbe23d19d5fa7c5a734696bf961a012c08cc031b4d9ed5868c5214f45626e002be26fdf922c25053ebf4c3ac8976da4ef05e33ef0c4b976d1d05bab58038af3f6f780ff9830bfd472296db2754880a22cbd77a784b7055df7caf8ff47a842166a107b6269113894f2bde62fa3b8cae101fee46739b7fee5fcf9692d352cbcb663cebd87dead3c01099da2ffbab557e89d6aad46dce17e9b4ed742129cced7ad97b1faf0b718bceb131d4c24024300d3a93c04903c2c0f0fdd2643b434e9320afe922c3a0ea175cdd923405ff810fcf0eabcf3e98f31ff01a4e9e497e65a59ac8a9121c494d3707a5e19b310e233caa7c602d4f9b2e4d77db44838e6cbdda01d0599833b142c5ea07045fcee6f3e5e27b9ed0fc619fb2824d32f64b720688c74753530469d27839a311684870679ba0589a714344b493bdf4a98f6b00d8549e6acf834bfa49f6ba43f70f49e35904741a0117d8ab3c95c814b37dd965e294436e84bb0e0a2a72d07620bcaaa6386b438608debe80bab6edf5793c6cc1fb90db6c33382834b200f17e9534b08d119f98b0758ab05f39ccee4696d264eb79bf35e027c448737469cabea832f47125ebdc6664437caa7f49c800e8255e28e0e86922f8058b0af714e20e418477a3f8a83271bdf6aca1a849a9976028821a6d8b76c8096605b9f914dd84f8d28f99fdb1b73fe90e7ec89b81a6cff5cd39cc01d3a3c18324f16c3660ff04271f537945cdc6a3db85b5ebaf5857e30c5845a4ef3502d12b5c02aef42989f342946d1561101a7a79de43fb69142e70b1b5fdfcf1577041305320cab70f4f27f6248c8b9c1289f3a6d8cc4a833e035765b133c1";
            ErrorLog.LoginfoToLogFile(encResp, "CCAvenueAmex Response before enc");

            //string encResponse = ccaCrypto.Decrypt(Request.Form["encResp"], workingKey);
            string encResponse = ccaCrypto.Decrypt(encResp, workingKey);

            ErrorLog.LoginfoToLogFile(Convert.ToString(Request.Form["encResp"])
                , "HDFC Response Raw");

            ErrorLog.LoginfoToLogFile(encResponse
                , "CCAvenueAmex Response decrupt");

            NameValueCollection Params = new NameValueCollection();
            string[] segments = encResponse.Split('&');
            foreach (string seg in segments)
            {
                string[] parts = seg.Split('=');
                if (parts.Length > 0)
                {
                    string Key = parts[0].Trim();
                    string Value = parts[1].Trim();
                    Params.Add(Key, Value);
                }
            }

            for (int i = 0; i < Params.Count; i++)
            {
                //Response.Write(Params.Keys[i] + " = " + Params[i] + "<br/>");

                //ErrorLog.LoginfoToLogFile(Params.Keys[i] + " = " + Params[i], "HDFC Response");

                if (Convert.ToString(Params.Keys[i]) == "order_id")
                {
                    order_id = Convert.ToString(Params[i]);
                }

                if (Convert.ToString(Params.Keys[i]) == "amount")
                {
                    amount = Convert.ToString(Params[i]);
                }
                if (Convert.ToString(Params.Keys[i]) == "tracking_id")
                {
                    tracking_id = Convert.ToString(Params[i]);
                }
                if (Convert.ToString(Params.Keys[i]) == "bank_ref_no")
                {
                    bank_ref_no = Convert.ToString(Params[i]);
                }
                if (Convert.ToString(Params.Keys[i]) == "order_status")
                {
                    order_status = Convert.ToString(Params[i]);
                }
                if (Convert.ToString(Params.Keys[i]) == "payment_mode")
                {
                    payment_mode = Convert.ToString(Params[i]);
                }
                if (Convert.ToString(Params.Keys[i]) == "card_name")
                {
                    card_name = Convert.ToString(Params[i]);
                }
                if (Convert.ToString(Params.Keys[i]) == "status_message")
                {
                    status_message = Convert.ToString(Params[i]);
                }
            }
            AmexCardCls objAmex = new AmexCardCls();

            DataSet ds = new DataSet();
            ds = objAmex.GetHDFCOrderDetails(order_id);
            OrderDetails bookingDetails = new OrderDetails();

            if (ds.Tables[0].Rows.Count > 0)
            {
                bookingDetails.bookingId = Convert.ToInt32(ds.Tables[0].Rows[0]["BookingId"]);
                bookingDetails.resultIndicator = ds.Tables[0].Rows[0]["AmexResultIndicator"].ToString();
                bookingDetails.chargedyn = Convert.ToBoolean(ds.Tables[0].Rows[0]["chargedyn"]);
                bookingDetails.approvalNo = ds.Tables[0].Rows[0]["ApprovalNo"].ToString();
            }
            else
            {
                Session["ResponseText"] = "No match found for the order : " + order_id;
                Response.Redirect("Message.aspx");
            }
            //bool ChargedYN = false;
            //string chargingString = "";
            //if (order_status == "Success" && status_message == "Y")
            //if (order_status == "Success" 
            //    && order_status == "Successful")
            //{
            //    ChargedYN = true;
            //}
            //else
            //{
            //    ChargedYN = false;
            //}

            //chargingString = "Order ID : " + order_id
            //    + "<br/>" + "amount : " + amount
            //    + "<br/>" + "Order Status : " + order_status
            //    + "<br/>" + "BookingID : " + bookingDetails.bookingId;

            //int resultCode = objAmex.UpdateHDFCChargingDetails
            //    (bookingDetails.bookingId, order_id, tracking_id
            //    , bank_ref_no, order_status, payment_mode, card_name
            //    , status_message, ChargedYN);

            CCAvenueAPI responseapi = new CCAvenueAPI();
            string returnstatus = responseapi.GetCCAvenueAmexChargingStatus(
                order_id, bookingDetails.bookingId);

            //Session["ResponseText"] = chargingString;
            Session["ResponseText"] = returnstatus;
            Response.Redirect("Message.aspx");
        }
    }
}