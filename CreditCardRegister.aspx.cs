﻿using System;
using ChargingWebService;

public partial class CreditCardRegister : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["ClientCoIndivID"])))
        {
            clientcoindivid.Value = Request.QueryString["ClientCoIndivID"].ToString();

            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["DeRegister"])))
                {
                    if (Convert.ToBoolean(Request.QueryString["DeRegister"]))
                    {
                        DeRegister(Convert.ToInt32(clientcoindivid.Value));
                    }
                }
            }
        }
        else
        {
            lblderegister.ForeColor = System.Drawing.Color.Red;
            lblderegister.Font.Bold = true;
            lblderegister.Text = "Invalid Request.";
        }
    }

    public void DeRegister(int ClientCoIndivID)
    {
        DB db = new DB();
        string returnstatus = "";
        returnstatus = db.UnRegisterCard(ClientCoIndivID);

        lblderegister.ForeColor = System.Drawing.Color.Red;
        lblderegister.Font.Bold = true;
        lblderegister.Text = returnstatus;
    }
}