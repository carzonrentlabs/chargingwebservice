﻿using System;
using System.Data;
using System.Text;
using CCA.Util;
using ChargingWebService;

public partial class CCAvenueCardRegisterMV : System.Web.UI.Page
{
    CCACrypto ccaCrypto = new CCACrypto();
    string ccaRequest = "";
    public string strEncRequest = "";
    string workingKey = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueWorkingKeyMV");
    public string strAccessCode = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueAccessCodeMV");
    public string strmerchant_id = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueMVMerchant_id");
    public string CORRedirectURL = GuestRegistrationErrorDesc.GetConfigurationValue("CORRedirectURLMV");
    public string CORCancelURL = GuestRegistrationErrorDesc.GetConfigurationValue("CORCancelURLMV");
    public string CCAvenueRegisterURL = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueRegisterURL");

    public string CorCCAvenuePayments_option = GuestRegistrationErrorDesc.GetConfigurationValue("CorCCAvenuePayments_option1");
    public string CorCCAvenuecard_type = GuestRegistrationErrorDesc.GetConfigurationValue("CorCCAvenuecard_type1");
    public string CorCCAvenuecard_name = GuestRegistrationErrorDesc.GetConfigurationValue("CorCCAvenuecard_name1");
    //string workingKey = "A33287ACE56CA4C8A04DFD6171152303";//put in the 32bit alpha numeric key in the quotes provided here 	
    //string workingKey = "4F606421D9285053DBD933700AF9793E";//put in the 32bit alpha numeric key in the quotes provided here 	
    //public string strAccessCode = "AVCS01KH12BR17SCRB";// put the access key in the quotes provided here.
    //public string strAccessCode = "MPZXOJZ3MASM8P5A";// put the access key in the quotes provided here.

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        //{

        string ClientCoIndivID, FirstName, LastName
            , EmailId, MobileNo, TravellerId, CityName, StateName, BillAddress;
        //ClientCoIndivID = "4186564";//"2358093"; //"977643";//"4189839";//
        ClientCoIndivID = Request.QueryString["ClientCoIndivID"].ToString();
        AmexCardCls objAmex = new AmexCardCls();
        DataSet ds = new DataSet();
        ds = objAmex.GetGuestDetailsForCCAvenue(ClientCoIndivID);

        if (ds.Tables[0].Rows.Count > 0)
        {
            FirstName = ds.Tables[0].Rows[0]["FName"].ToString();
            LastName = ds.Tables[0].Rows[0]["LName"].ToString();
            if (LastName == "")
                LastName = FirstName;
            EmailId = ds.Tables[0].Rows[0]["EmailID"].ToString();
            MobileNo = ds.Tables[0].Rows[0]["Phone1"].ToString();
            TravellerId = ds.Tables[0].Rows[0]["TravellerId"].ToString();
            CityName = ds.Tables[0].Rows[0]["CityName"].ToString();
            StateName = ds.Tables[0].Rows[0]["StateName"].ToString();
            BillAddress = ds.Tables[0].Rows[0]["BillAddress"].ToString();

            if (ds.Tables[0].Rows[0]["CCRegisteredYN"].ToString() == "True")
            {
                Session["ResponseText"] = "Your card is already registered.";
                Response.Redirect("Message.aspx");
            }
        }
        else
        {
            FirstName = "";
            LastName = "";
            EmailId = "";
            MobileNo = "";
            TravellerId = "";
            CityName = "";
            StateName = "";
            BillAddress = "";

            Session["ResponseText"] = "Invalid Request.";
            Response.Redirect("Message.aspx");
        }
        //tid : 1691150257806
        //merchant_id : 320
        //order_id : bookingid
        //amount : 1.00
        //currency : INR
        //redirect_url : https://corapi.carzonrent.com/chargingwebservice/CCAvenueCardResponse.aspx
        //cancel_url : https://corapi.carzonrent.com/chargingwebservice/CCAvenueCardResponse.aspx
        //billing_name : Optional
        //billing_address : Optional
        //billing_city : Optional
        //billing_state : Optional
        //billing_zip : Optional
        //billing_country : Optional
        //billing_tel : Optional
        //billing_email : Optional
        //delivery_name : Optional
        //delivery_address : Optional
        //delivery_city : Optional
        //delivery_state : Optional
        //delivery_zip : Optional
        //delivery_country : Optional
        //delivery_tel : Optional
        //merchant_param1 : Optional
        //merchant_param2 : Optional
        //merchant_param3 : Optional
        //merchant_param4 : Optional
        //merchant_param5 : Optional
        //promo_code : Optional
        //customer_identifier : Optional
        string orderid = TravellerId;

        ccaRequest = "tid=" + orderid + "&merchant_id=" + strmerchant_id + "&order_id="
            + orderid + "&amount=1.00&currency=INR" + "&redirect_url=" + CORRedirectURL
            + "&cancel_url=" + CORCancelURL + "&billing_name=" + FirstName + " " + LastName
            + "&billing_address=" + BillAddress + "&billing_city=" + CityName
            + "&billing_state=" + StateName + "&billing_zip=425001"
            + "&billing_country=India&billing_tel=" + MobileNo + "&billing_email=" + EmailId
            + "&delivery_name=" + FirstName + " " + LastName
            + "&delivery_address=" + BillAddress
            + "&delivery_city=" + CityName + "&delivery_state=" + StateName
            + "&delivery_zip=425001&delivery_country=India&delivery_tel="
            + MobileNo + "&merchant_param1=" + ClientCoIndivID
            + "&merchant_param2=" + ClientCoIndivID
            + "&merchant_param3=" + ClientCoIndivID
            + "&merchant_param4=" + ClientCoIndivID
            + "&merchant_param5=" + ClientCoIndivID
            + "&si_type=ONDEMAND&si_mer_ref_no=" + orderid
            + "&si_setup_amount=1.00"
            + "&promo_code=&customer_identifier="
            //+ "&Payments_option=OPTCRDC&card_type=CRDC&card_name=Visa&";
            //+ "&Payments_option=OPTCRDC&card_type=CRDC&card_name=Amex&";
            + "&Payments_option=" + CorCCAvenuePayments_option
            + "&card_type=" + CorCCAvenuecard_type
            + "&card_name=" + CorCCAvenuecard_name + "&";
        //+ "&ignore_payment_option=OPTMOBP|OPTNBK|OPTCASHC|OPTEMI|OPTIVRS|OPTWLT|OPTNEFT|OPTUPI|OPTPL|OPTRWRD|OPTER&";
        strEncRequest = ccaCrypto.Encrypt(ccaRequest, workingKey);
        //encRequest.Value = strEncRequest;
        //access_code.Value = strAccessCode;
        //ErrorLog.LoginfoToLogFile(ccaRequest, "CC Avenue Request");
        //ErrorLog.LoginfoToLogFile(strEncRequest, "CC Avenue Request");
        CCAvenueRedirect(strAccessCode, strEncRequest, ccaRequest, workingKey);
        //}
    }

    public void CCAvenueRedirect(string access_code, string strEncRequest, string WithOutencrypt, string workingKey)
    {
        CCACrypto ccaCrypto = new CCACrypto();

        ErrorLog.LoginfoToLogFile("CCAvenueRedirect : access_code=" + access_code +
            "&WorkingKey=" + workingKey
            + "&strEncRequest=" + strEncRequest, ""); //log file 

        ErrorLog.LoginfoToLogFile("CCAvenueRedirect : access_code=" + access_code +
            "&WorkingKey=" + workingKey
            + "&WithOutencrypt=" + WithOutencrypt, ""); //log file 
        //string EncryptedParameter = "";
        //EncryptedParameter = "";

        //ErrorLog.LoginfoToLogFile("CCAvenueRedirect: " + EncryptedParameter, "ChargingTransaction"); //log file 

        StringBuilder sb = new StringBuilder();
        sb.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
        sb.Append("<html xmlns='https://www.w3.org/1999/xhtml'><head id='Head1' runat='server'>");
        sb.Append("<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>");
        sb.Append("<script type='text/javascript'>");
        sb.Append("$(document).ready(function() {");
        sb.Append("$('#nonseamless').submit();");
        sb.Append("});");
        sb.Append("</script>");
        sb.Append("<title></title>");

        sb.Append("</head><body>");
        sb.Append("<form id='nonseamless' method='post' name='redirect' action='" + CCAvenueRegisterURL + "'>");
        sb.Append("<input type='hidden' id='encRequest' name='encRequest' value='" + strEncRequest + "'>");
        sb.Append("<input type='hidden' id='access_code' name='access_code' value='" + access_code + "'>");
        sb.Append("</form>");
        sb.Append("</body></html>");
        Response.Write(sb.ToString());
        Response.End();
    }
}