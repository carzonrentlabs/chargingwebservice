﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AmexcardRegister.aspx.cs" Inherits="AmexcardRegister" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<body>
    <input type="hidden" runat="server" id="hdmandateToken" name="key" />
    <input type="hidden" runat="server" id="hdAuthID" name="salt" />
    <div class="container">
        <div class="jumbotron mt-3">
            <h1>Carzonrent India Pvt. Ltd.</h1>
            <a class="btn btn-lg btn-primary" href="#" onclick="callBilldeskSdk()" role="button">Register Card »</a>
        </div>
        <div id="spinner" style="display: none;" class="mt-3 text-center">
            <div class="spinner-border" role="status" style="width: 5rem; height: 5rem;">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <div id="result" class="jumbotron mt-3">
        </div>
    </div>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"
        integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"
        integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous">
    </script>

 <%--   <script type="module" src="https://uat.billdesk.com/jssdk/v1/dist/billdesksdk/billdesksdk.esm.js"></script>
    <script nomodule="" src="https://uat.billdesk.com/jssdk/v1/dist/billdesksdk.js "></script>
    <link href="https://uat.billdesk.com/jssdk/v1/dist/billdesksdk/billdesksdk.css" rel="stylesheet" />--%>

    <script type="module" src="https://pay.billdesk.com/jssdk/v1/dist/billdesksdk/billdesksdk.esm.js"></script>
    <script nomodule="" src="https://pay.billdesk.com/jssdk/v1/dist/billdesksdk.js"></script>
    <link href="https://pay.billdesk.com/jssdk/v1/dist/billdesksdk/billdesksdk.css" rel="stylesheet" />

    <script type="text/javascript">

        function callBilldeskSdk() {
            var Mtoken = $('#hdmandateToken').val();
            var appMrchnt = '<%=ConfigurationManager.AppSettings["BilldeskMerchantID"].ToString() %>';
            var appRUrl = '<%=ConfigurationManager.AppSettings["BilldeskCardResponseUrl"].ToString() %>';
            var MAuthID = $('#hdAuthID').val();
            var mandate_flow_config = {              
                merchantId: appMrchnt,//"CARZON2UAT",
                mandateTokenId: Mtoken,//"NA1F0000197690",
                authToken: MAuthID,
                childWindow: false,
                returnUrl: appRUrl,
                prefs: { "payment_categories": ["card", "emi"] }
            }

            var config = {
                flowConfig: mandate_flow_config,
                flowType: "emandate"
            }
            window.loadBillDeskSdk(config)
        }



    </script>
</body>

</html>
