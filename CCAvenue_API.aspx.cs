﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using CCA.Util;
using System.Collections.Specialized;
using System.Xml;

namespace CCAvenue_API_Kit
{
    public partial class CCAvenue_API : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string accessCode = "AVJX28JI73CE69XJEC"; //from avenues
                string workingKey = "281FE314EBBDEEA20CBA9539D61D2CB1";// from avenues

                //string orderStatusQueryJson = "{ \"reference_no\":\"103001198924\", \"order_no\":\"77141516\" }"; //Ex. { "reference_no":"CCAvenue_Reference_No" , "order_no":"123456"} 

                string redirecturl = "", cancel_url = "", language = "", merchant_id = "", si_amount = ""
                    , si_start_date = "", billing_email = "", billing_tel = "", order_id="", currency="", amount="", si_type="";

                redirecturl = "http://instatest.carzonrent.com/chargingwebservice/HDFCRedirect.aspx";
                cancel_url = "http://instatest.carzonrent.com/chargingwebservice/HDFCRedirect.aspx";
                language = "EN";
                merchant_id = "1381993";
                si_amount = "15000";
                si_start_date = "30-09-2022";
                billing_email = "rahul.jain@carzonrent.com";
                billing_tel = "9818107670";
                merchant_id = "1381993";
                order_id = "123456";
                currency = "INR";
                amount = "1";
                si_type = "Ondemand";

                string orderStatusQueryJson = "{ \"merchant_id\":\""+ merchant_id 
                    + "\",\"order_id\":\""+ order_id 
                    + "\",\"currency\":\""+ currency + "\",\"amount\":\""+ amount 
                    + "\",\"redirecturl\":\"" + redirecturl + "\",\"cancel_url\":\"" + cancel_url
                    + "\",\"language\":\"" + language + "\", \"si_type\":\""+ si_type 
                    + "\", \"si_amount\":\"" + si_amount
                    + "\", \"si_setup_amount\":\"Y\", \"si_start_date\":\"" + si_start_date
                    + "\", \"billing_email\":\"" + billing_email
                    + "\", \"billing_tel\":\"" + billing_tel + "\"}";

                string encJson = "";

                //string queryUrl = "https://login.ccavenue.com/apis/servlet/DoWebTrans";
                //string queryUrl = "https://apitest.ccavenue.com/apis/servlet/DoWebTrans";
                string queryUrl = "https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction";

                CCACrypto ccaCrypto = new CCACrypto();
                encJson = ccaCrypto.Encrypt(orderStatusQueryJson, workingKey);

                // make query for the status of the order to ccAvenues change the command param as per your need
                string authQueryUrlParam = "enc_request=" + encJson + "&access_code=" + accessCode + "&command=orderStatusTracker&request_type=JSON&response_type=JSON";

                // Url Connection
                String message = postPaymentRequestToGateway(queryUrl, authQueryUrlParam);
                //Response.Write(message);
                NameValueCollection param = getResponseMap(message);
                String status = "";
                String encResJson = "";
                if (param != null && param.Count == 2)
                {
                    for (int i = 0; i < param.Count; i++)
                    {
                        if ("status".Equals(param.Keys[i]))
                        {
                            status = param[i];
                        }
                        if ("enc_response".Equals(param.Keys[i]))
                        {
                            encResJson = param[i];
                            //Response.Write(encResXML);
                        }
                    }
                    if (!"".Equals(status) && status.Equals("0"))
                    {
                        String ResJson = ccaCrypto.Decrypt(encResJson, workingKey);
                        Response.Write(ResJson);
                    }
                    else if (!"".Equals(status) && status.Equals("1"))
                    {
                        Console.WriteLine("failure response from ccAvenues: " + encResJson);
                    }

                }

            }
            catch (Exception exp)
            {
                Response.Write("Exception " + exp);

            }
        }

        private string postPaymentRequestToGateway(String queryUrl, String urlParam)
        {

            String message = "";
            try
            {
                StreamWriter myWriter = null;// it will open a http connection with provided url
                WebRequest objRequest = WebRequest.Create(queryUrl);//send data using objxmlhttp object
                objRequest.Method = "POST";
                //objRequest.ContentLength = TranRequest.Length;
                objRequest.ContentType = "application/x-www-form-urlencoded";//to set content type
                myWriter = new System.IO.StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(urlParam);//send data
                myWriter.Close();//closed the myWriter object

                // Getting Response
                System.Net.HttpWebResponse objResponse = (System.Net.HttpWebResponse)objRequest.GetResponse();//receive the responce from objxmlhttp object 
                using (System.IO.StreamReader sr = new System.IO.StreamReader(objResponse.GetResponseStream()))
                {
                    message = sr.ReadToEnd();
                    //Response.Write(message);
                }
            }
            catch (Exception exception)
            {
                Console.Write("Exception occured while connection." + exception);
                ErrorLog.LogErrorToLogFile(exception, "SIRegistration");
            }
            return message;

        }

        private NameValueCollection getResponseMap(String message)
        {
            NameValueCollection Params = new NameValueCollection();
            if (message != null || !"".Equals(message))
            {
                string[] segments = message.Split('&');
                foreach (string seg in segments)
                {
                    string[] parts = seg.Split('=');
                    if (parts.Length > 0)
                    {
                        string Key = parts[0].Trim();
                        string Value = parts[1].Trim();
                        Params.Add(Key, Value);
                    }
                }
            }
            return Params;
        }
    }
}