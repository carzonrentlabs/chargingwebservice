﻿using ChargingWebService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebHook : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        PayUCallbackResponse callbackData = new PayUCallbackResponse();

        callbackData.mihpayid = Request["mihpayid"];
        callbackData.mode = Request["mode"];
        callbackData.key = Request["key"];
        callbackData.txnid = Request["txnid"];
        callbackData.amount = Request["amount"];
        callbackData.addedon = Request["addedon"];
        callbackData.productinfo = Request["productinfo"];
        callbackData.firstname = Request["firstname"];
        callbackData.lastname= Request["lastname"];
        callbackData.address1= Request["address1"];
        callbackData.address2= Request["address2"];
        callbackData.city = Request["city"];
        callbackData.state = Request["state"];
        callbackData.country = Request["country"];
        callbackData.zipcode = Request["zipcode"];
        callbackData.email = Request["email"];
        callbackData.phone = Request["phone"];
        callbackData.udf1 = Request["udf1"];
        callbackData.udf2 = Request["udf2"];
        callbackData.udf3 = Request["udf3"];
        callbackData.udf4 = Request["udf4"];
        callbackData.udf5 = Request["udf5"];
        callbackData.card_token = Request["card_token"];
        callbackData.card_no = Request["card_no"];
        callbackData.field0 = Request["field0"];
        callbackData.field1 = Request["field1"];
        callbackData.field2 = Request["field2"];
        callbackData.field3 = Request["field3"];
        callbackData.field4 = Request["field4"];
        callbackData.field5 = Request["field5"];
        callbackData.field6 = Request["field6"];
        callbackData.field7 = Request["field7"];
        callbackData.field8 = Request["field8"];
        callbackData.field9 = Request["field9"];
        callbackData.payment_source = Request["payment_source"];
        callbackData.PG_TYPE = Request["PG_TYPE"];
        callbackData.error = Request["error"];
        callbackData.error_Message = Request["error_Message"];
        callbackData.net_amount_debit = Request["net_amount_debit"];
        callbackData.unmappedstatus = Request["unmappedstatus"];
        callbackData.hash = Request["hash"];
        callbackData.bank_ref_num = Request["bank_ref_num"];
        callbackData.bankcode = Request["bankcode"];
        callbackData.status = Request["status"];
        
        //callbackData.surl = Request["surl"];
        //callbackData.curl = Request["curl"];
        //callbackData.furl = Request["furl"];
        //callbackData.card_hash = Request["card_hash"];

        #region
        clsAdmin objadmin = new clsAdmin();
       

        objadmin.UpdateQRPaymentCallBackRespose(callbackData);

        
        #endregion
    }
    
}