﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Specialized;
using CCA.Util;
using ChargingWebService;

public partial class CCAvenueCardResponse : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string order_id = "", tracking_id = "", bank_ref_no = "", order_status = ""
            , payment_mode = "", card_name = "", status_message = "", si_ref_no = ""
            , si_created = "", si_status = "", si_mer_ref_no = "";

        if (!IsPostBack)
        {
            //string workingKey = "A33287ACE56CA4C8A04DFD6171152303";//put in the 32bit alpha numeric key in the quotes provided here
            //string workingKey = "4F606421D9285053DBD933700AF9793E";//put in the 32bit alpha numeric key in the quotes provided here
            string workingKey = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueWorkingKey");
            CCACrypto ccaCrypto = new CCACrypto();
            string encResponse = ccaCrypto.Decrypt(Request.Form["encResp"], workingKey);
            NameValueCollection Params = new NameValueCollection();
            string[] segments = encResponse.Split('&');
            foreach (string seg in segments)
            {
                string[] parts = seg.Split('=');
                if (parts.Length > 0)
                {
                    string Key = parts[0].Trim();
                    string Value = parts[1].Trim();
                    Params.Add(Key, Value);
                }
            }

            for (int i = 0; i < Params.Count; i++)
            {
                //Response.Write(Params.Keys[i] + " = " + Params[i] + "<br/>");

                ErrorLog.LoginfoToLogFile(Params.Keys[i] + " = " + Params[i], "CC Avenue Response");

                if (Convert.ToString(Params.Keys[i]) == "order_id")
                {
                    order_id = Convert.ToString(Params[i]);
                }

                if (Convert.ToString(Params.Keys[i]) == "order_id")
                {
                    order_id = Convert.ToString(Params[i]);
                }
                if (Convert.ToString(Params.Keys[i]) == "tracking_id")
                {
                    tracking_id = Convert.ToString(Params[i]);
                }

                if (Convert.ToString(Params.Keys[i]) == "reference_no")
                {
                    tracking_id = Convert.ToString(Params[i]);
                }
                if (Convert.ToString(Params.Keys[i]) == "bank_ref_no")
                {
                    bank_ref_no = Convert.ToString(Params[i]);
                }
                if (Convert.ToString(Params.Keys[i]) == "order_status")
                {
                    order_status = Convert.ToString(Params[i]);
                }
                if (Convert.ToString(Params.Keys[i]) == "payment_mode")
                {
                    payment_mode = Convert.ToString(Params[i]);
                }
                if (Convert.ToString(Params.Keys[i]) == "card_name")
                {
                    card_name = Convert.ToString(Params[i]);
                }
                if (Convert.ToString(Params.Keys[i]) == "status_message")
                {
                    status_message = Convert.ToString(Params[i]);
                }
                if (Convert.ToString(Params.Keys[i]) == "si_ref_no")
                {
                    si_ref_no = Convert.ToString(Params[i]);
                }
                if (Convert.ToString(Params.Keys[i]) == "si_created")
                {
                    si_created = Convert.ToString(Params[i]);
                }
                if (Convert.ToString(Params.Keys[i]) == "si_status")
                {
                    si_status = Convert.ToString(Params[i]);
                }
                if (Convert.ToString(Params.Keys[i]) == "si_mer_ref_no")
                {
                    si_mer_ref_no = Convert.ToString(Params[i]);
                }
            }

            AmexCardCls objAmex = new AmexCardCls();

            int resultCode = objAmex.UpdateCCAvenueSaveCardDetails(
                order_id, tracking_id
                , bank_ref_no, order_status, payment_mode, card_name
                , status_message, si_ref_no, si_created, si_status, si_mer_ref_no);

            if (status_message == "SUCCESS" 
                || status_message == "PROCEED" 
                || status_message == "token_awaited")
            {
                Session["ResponseText"] = "The card registration request is successfully received and will get the processed in next 30 minutes.";
            }
            else if (status_message == "AUTHORIZED-NA-00")
            {
                Session["ResponseText"] = "Only Amex is acceptable, please don't use Master / Visa card.";
            }
            else
            {
                Session["ResponseText"] = status_message;
            }
            Response.Redirect("Message.aspx");
        }
    }
}