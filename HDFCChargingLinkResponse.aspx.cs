﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Specialized;
using CCA.Util;
using ChargingWebService;
using System.Data;

public partial class HDFCChargingLinkResponse : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string order_id = "", tracking_id = "", bank_ref_no = ""
            , order_status = "", amount = ""
            , payment_mode = "", card_name = "", status_message = "";

        if (!IsPostBack)
        {
            string workingKey = GuestRegistrationErrorDesc.GetConfigurationValue("HDFCCCAvenueWorkingKey");
            CCACrypto ccaCrypto = new CCACrypto();

            //string encResp = "a812aa36f111469cb851e70228768b57b52604c9dcbd7f5feac482bd82e686bce4eccd57ee1ed9693753257f4b22531e9a8e30c47ed1f55199d259ffc9c6b760a0c24c83b55ac83215be9547c116f47c7dbcf94847389feb3b5ef60ecedb4628e19c862347f07892db072af6a986d2e5ddcb3c0f73e6f36453d540cae95c789cdcccde1b8dc655ff3fe4a5a055bffa704e4f22f73031577ff9791a6c6f4d36030dfa2995c37a19830eccb2b04faa366bd81b54d40da462e68cab953c7167133655ac2bfeee5539b0c00828ac7266e1537e564e3eb16dcdf7be54f3043c4baf4adf14183cf1972b24ee03878c7fced21a8637d63c15f8bfcf290db8588d37e1a982f0079825e9d9399d5f652780b99f30c6a82573b6b710d14336958641b6a399efef943e3a70ea834adce5fad7b50413560b8cc01c11f8ea3179e576511bc5e8422840537e59dc341db4da737d4fdc1ce3b763c9e3d24cb94ae4cb9d8aa2ef0c5b994aa2fb88a182d29a9fc5dd343ed0635859d4433346b0d645d24d36b945340cd090dfd95d8ae5a1ff78b9843745fa024f156e203753b5b3df163dfbf9d6e7942e0761df1dd0c8df48f4604aea4daf36660d728acd0960adc76c8e7e7b0018bddd8ce44ef1f80e32b5e946f22473d13e2e69dedc50631e544193a77462478efa573bfe3fec94a63781434bcb2baba31b7e4295e717e33d292c3d7f6e431c7133551d9e82a883981d95bf9e1b831e34246af7196d0e795d8c8812a279a099e810d7d4978ae6a1d8b06bd654b10a2c3d6b5e77e91ec01146e8cff13e7ac03d83790c08ed4958bdcb7d475b0913e02e5f10ae7057b6f8b2d601296f80945edfc7c30adcf07646640f34c1993f632e76c2b3f128e134f940d55dabf198adab888f6274f772f0d2936c47e94b4b77235424cb4167be0cb201bb20e17d4232f1c6386eb4e8517cae31983476284b268200f09cda2bcc112571f120f8105cfb6a2f22594c8c5323b62dc0ba2566e267d9b26b97818b3b85662830abc787dc6ebd2ce0dbfcd494e9b03df7f48c78cba59ddabd06751f9583b3882902b17a31c335d35e1a42094de513df018a5b71864ec5d83fa3c31e0e4e70ee631072ecf9bd1d9e1e85fca2c1eaa5c6b77bb9ddb75f32ffb85b4a791f5231be797039563202dcc56297beffc5099573397d85c6c458ff3fa858e89a7616729cef099c16d5f99827ac863b1c4ba807630602ab087642b26264ab01808880fe44fce59afb0a081b79f4bf0e22e9327eaa71455d254d241909d74001d077b002993a32a23dd63730e83f2b7d61d9e203ba863c45d8bcffb15dbc05b657394afe9ef0e611febf0f1a79cb6bafadd08a4cbc3059ac1e21863af860f51a383468e5f5d8bba70fffcd53b25428703f6a8bbe05ab48a046fa9e397a08f7084dae3dd3ccb0712b1737a8f13227ff9ae61e2e1aabf488d2ec9838c06e76ed18a40b7367f7e3e51e498283d0862e";
            string encResp = Request.Form["encResp"];

            ErrorLog.LoginfoToLogFile(encResp, "HDFC Response before enc");

            //string encResponse = ccaCrypto.Decrypt(Request.Form["encResp"], workingKey);
            string encResponse = ccaCrypto.Decrypt(encResp, workingKey);

            ErrorLog.LoginfoToLogFile(Convert.ToString(Request.Form["encResp"])
                , "HDFC Response Raw");

            ErrorLog.LoginfoToLogFile(encResponse, "HDFC Response decrupt");

            NameValueCollection Params = new NameValueCollection();
            string[] segments = encResponse.Split('&');
            foreach (string seg in segments)
            {
                string[] parts = seg.Split('=');
                if (parts.Length > 0)
                {
                    string Key = parts[0].Trim();
                    string Value = parts[1].Trim();
                    Params.Add(Key, Value);
                }
            }

            for (int i = 0; i < Params.Count; i++)
            {
                //Response.Write(Params.Keys[i] + " = " + Params[i] + "<br/>");

                //ErrorLog.LoginfoToLogFile(Params.Keys[i] + " = " + Params[i], "HDFC Response");

                if (Convert.ToString(Params.Keys[i]) == "order_id")
                {
                    order_id = Convert.ToString(Params[i]);
                }

                if (Convert.ToString(Params.Keys[i]) == "amount")
                {
                    amount = Convert.ToString(Params[i]);
                }
                if (Convert.ToString(Params.Keys[i]) == "tracking_id")
                {
                    tracking_id = Convert.ToString(Params[i]);
                }
                if (Convert.ToString(Params.Keys[i]) == "bank_ref_no")
                {
                    bank_ref_no = Convert.ToString(Params[i]);
                }
                if (Convert.ToString(Params.Keys[i]) == "order_status")
                {
                    order_status = Convert.ToString(Params[i]);
                }
                if (Convert.ToString(Params.Keys[i]) == "payment_mode")
                {
                    payment_mode = Convert.ToString(Params[i]);
                }
                if (Convert.ToString(Params.Keys[i]) == "card_name")
                {
                    card_name = Convert.ToString(Params[i]);
                }
                if (Convert.ToString(Params.Keys[i]) == "status_message")
                {
                    status_message = Convert.ToString(Params[i]);
                }
            }
            AmexCardCls objAmex = new AmexCardCls();

            DataSet ds = new DataSet();
            ds = objAmex.GetHDFCOrderDetails(order_id);
            OrderDetails bookingDetails = new OrderDetails();

            if (ds.Tables[0].Rows.Count > 0)
            {
                bookingDetails.bookingId = Convert.ToInt32(ds.Tables[0].Rows[0]["BookingId"]);
                bookingDetails.resultIndicator = ds.Tables[0].Rows[0]["AmexResultIndicator"].ToString();
                bookingDetails.chargedyn = Convert.ToBoolean(ds.Tables[0].Rows[0]["chargedyn"]);
                bookingDetails.approvalNo = ds.Tables[0].Rows[0]["ApprovalNo"].ToString();
            }
            else
            {
                Session["ResponseText"] = "No match found for the order : " + order_id;
                Response.Redirect("Message.aspx");
            }
            bool ChargedYN = false;
            string chargingString = "";
            //if (order_status == "Success" && status_message == "Y")
            if (order_status == "Success" && order_status == "Successful")
            {
                ChargedYN = true;
            }
            else
            {
                ChargedYN = false;
            }

            //chargingString = "Order ID : " + order_id
            //    + "<br/>" + "amount : " + amount
            //    + "<br/>" + "Order Status : " + order_status
            //    + "<br/>" + "BookingID : " + bookingDetails.bookingId;

            //int resultCode = objAmex.UpdateHDFCChargingDetails
            //    (bookingDetails.bookingId, order_id, tracking_id
            //    , bank_ref_no, order_status, payment_mode, card_name
            //    , status_message, ChargedYN);

            CCAvenueAPI responseapi = new CCAvenueAPI();
            string returnstatus = responseapi.GetHDFCChargingStatus(
                order_id, bookingDetails.bookingId);

            //Session["ResponseText"] = chargingString;
            Session["ResponseText"] = returnstatus;
            Response.Redirect("Message.aspx");
        }
    }
}