﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using ChargingWebService;
using System.Data;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Linq;

public partial class MVCharging : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Request.QueryString.HasKeys())
        {
            Session["ResponseText"] = "Unable to Process the Request.";
            Response.Redirect("Message.aspx");
        }
        //changes to do only bookingid required and then get all the details from the db
        string OrderID, InvoiceNo, RentalDate, PickupCity, CustomerName, amount
            , TravellerID, BookingStatus, TransactionMode;

        int ClientCoIndivID;
        //OrderID = "10437169";
        string TranTrackid = Request.QueryString["BookingID"].ToString();

        //TransactionMode = Request.QueryString["TransactionMode"].ToString();//"C" for Charging, A for Preauth

        string[] words = TranTrackid.Split(':');

        OrderID = words[0];
        TransactionMode = words[1];

        InvoiceNo = OrderID;

        //update orderid

        clsAdmin objadmin = new clsAdmin();
        DataSet ds = new DataSet();

        string EmailID, PhoneNo, StateCode;
        ds = objadmin.GetChargingData(Convert.ToInt32(OrderID));

        if (ds.Tables[0].Rows.Count > 0)
        {
            ClientCoIndivID = Convert.ToInt32(ds.Tables[0].Rows[0]["ClientCoIndivID"]);
            amount = ds.Tables[0].Rows[0]["TotalCost"].ToString();
            RentalDate = ds.Tables[0].Rows[0]["RentalDate"].ToString();
            PickupCity = ds.Tables[0].Rows[0]["CityName"].ToString();
            CustomerName = ds.Tables[0].Rows[0]["GuestName"].ToString();
            TravellerID = ds.Tables[0].Rows[0]["TravellerId"].ToString();
            EmailID = ds.Tables[0].Rows[0]["EmailID"].ToString();
            PhoneNo = ds.Tables[0].Rows[0]["Phone1"].ToString();
            StateCode = ds.Tables[0].Rows[0]["StateCode"].ToString();

            BookingStatus = ds.Tables[0].Rows[0]["Status"].ToString();
        }
        else
        {
            ClientCoIndivID = 0;
            amount = "0";
            RentalDate = "24/06/2020";//Convert.ToString(System.DateTime.Now.ToString("dd/mmm/yyyy"));
            PickupCity = "";
            CustomerName = "test";
            TravellerID = "";
            EmailID = "";
            PhoneNo = "";
            StateCode = "";
            BookingStatus = "";
        }

        //InvoiceNo = "123454";
        //OldBookingID = "";
        //SendSMSLater = "";
        //ClientCoID = "";
        //SMSMobiles = "";
        //SMSMobiles1 = "";
        //bitIsVIP = "";
        //hdnAcceleration_No = "";
        //SendMailLater = "";
        //LimoStatus = "";
        //amount = "100";
        //OrderID = "123454";
        //PageID = "CallTaker";
        //PickupCity , RentalDate, CustomerName --to    get from db

        ErrorLog.LoginfoToLogFile("MVPreChargingBeforeRedirect : " + "OrderID=" + OrderID + "&" + "amount=" + amount
            + "&" + "RentalDate=" + RentalDate + "&" + "PickupCity=" + PickupCity + "&" + "CustomerName=" + CustomerName
            + "&" + "ClientCoIndivID=" + ClientCoIndivID + "&" + "EmailID=" + EmailID
            + "&" + "PhoneNo=" + PhoneNo + "&" + "StateCode=" + StateCode, "MVCharing"); //log file 

        if (string.IsNullOrEmpty(amount))
        {
            Session["ResponseText"] = "Cannot Process to Charge, as Invoice is not Processed.";
            Response.Redirect("Message.aspx");
        }
        else
        {
            if (BookingStatus == "O")
            {
                MVChargingRedirect(OrderID, InvoiceNo, Convert.ToDouble(amount), RentalDate, PickupCity, CustomerName
                    , Convert.ToInt32(ClientCoIndivID), EmailID, PhoneNo, StateCode, TravellerID, TransactionMode);
            }
            else if (BookingStatus == "C")
            {
                //redirect // page to create
                Session["ResponseText"] = "Booking Already Closed, cannot Charge.";
                Response.Redirect("Message.aspx");
            }
            else if (BookingStatus == "Y")
            {
                //redirect // page to create
                Session["ResponseText"] = "Booking Already Closed in No-Show, cannot Charge.";
                Response.Redirect("Message.aspx");
            }
            else if (BookingStatus == "N")
            {
                Session["ResponseText"] = "Booking is not even dispatched.";
                Response.Redirect("Message.aspx");
                //redirect // page to create
            }
            else if (BookingStatus == "M")
            {
                Session["ResponseText"] = "Booking is not even dispatched.";
                Response.Redirect("Message.aspx");
                //redirect // page to create
            }
            else if (BookingStatus == "X")
            {
                Session["ResponseText"] = "Booking is not confirmed.";
                Response.Redirect("Message.aspx");
                //redirect // page to create
            }
        }
    }

    public void MVChargingRedirect(string ApprovalNo, string InvoiceNo, double Amount, string RentalDate
        , string PickupCity, string CustomerName, int ClientCoIndivID, string EmailID, string PhoneNo
        , string StateCode, string TravellerID, string TransactionMode)
    {
        ErrorLog.LoginfoToLogFile("MVChargingRedirect : ApprovalNo=" + ApprovalNo + "&InvoiceNo=" + InvoiceNo
            + "&Amount=" + Amount + "&RentalDate=" + RentalDate + "&PickupCity=" + PickupCity
            + "&CustomerName=" + CustomerName + "&ClientCoIndivID=" + ClientCoIndivID + "&EmailID=" + EmailID
            + "&PhoneNo=" + PhoneNo + "&StateCode=" + StateCode + "&TravellerID=" + TravellerID, "MVCharing1"); //log file 

        string url = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessURL");

        string EncryptedParameter = "", AuthUserName = "", AuthPassword = "", AuthCode = "", HashKey = "", EncKey = "";
        ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

        AuthUserName = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessAuthUserName");
        AuthPassword = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessAuthPassword");
        AuthCode = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessAuthCode");
        HashKey = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessHashKey");
        EncKey = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessEncKey");

        string PartnerName = "NA";
        string Address1 = "", Address2 = "";
        //string ReturnURL = "https://corapi.carzonrent.com/chargingwebservice/MVChargingReturn.aspx";
        string ReturnURL = "https://instatest.carzonrent.com/chargingwebservice/MVChargingReturn.aspx";

        //string TransactionMode = "C"; //C for Direct Capture


        string[,] RequestArray = new string[20, 2];

        int i = -1;
        RequestArray[++i, 0] = "TravellerId";
        RequestArray[i, 1] = TravellerID;
        RequestArray[++i, 0] = "TransactionMode";
        RequestArray[i, 1] = TransactionMode;
        RequestArray[++i, 0] = "InvoiceNo";
        RequestArray[i, 1] = ApprovalNo;
        RequestArray[++i, 0] = "Amount";
        RequestArray[i, 1] = Convert.ToString(Amount);
        RequestArray[++i, 0] = "FirstName";
        RequestArray[i, 1] = CustomerName;
        RequestArray[++i, 0] = "LastName";
        RequestArray[i, 1] = CustomerName;
        RequestArray[++i, 0] = "EmailId";
        RequestArray[i, 1] = EmailID;
        RequestArray[++i, 0] = "MobileNo";
        RequestArray[i, 1] = PhoneNo;
        RequestArray[++i, 0] = "Address1";
        RequestArray[i, 1] = Address1;
        RequestArray[++i, 0] = "Address2";
        RequestArray[i, 1] = Address2;
        RequestArray[++i, 0] = "StateCode";
        RequestArray[i, 1] = StateCode;
        RequestArray[++i, 0] = "City";
        RequestArray[i, 1] = PickupCity.Replace(" ", "");
        RequestArray[++i, 0] = "PartnerName";
        RequestArray[i, 1] = PartnerName;
        RequestArray[++i, 0] = "ReturnURL";
        RequestArray[i, 1] = ReturnURL;

        //ErrorLog.LoginfoToLogFile("MVRegisterRedirect : " + "TravellerId~~" + ClientCoIndivID + "::" + "TransactionMode~~" + TransactionMode
        //    + "::" + "InvoiceNo~~" + ApprovalNo + "::" + "Amount~~" + Amount + "::" + "FirstName~~" + CustomerName + "::" + "LastName~~" + CustomerName
        //   + "::" + "EmailId~~" + EmailID + "::" + "MobileNo~~" + PhoneNo + "::" + "Address1~~" + Address1 + "::" + "Address2~~" + Address2
        //   + "::" + "StateCode~~" + StateCode + "::" + "City~~" + PickupCity + "::" + "PartnerName~~" + PartnerName + "::" + "ReturnURL~~" + ReturnURL
        //   , "SavePreauthdata"); //log file 

        //EncryptedParameter = URLEncrypt("TravellerId~~" + ClientCoIndivID + "::" + "TransactionMode~~" + TransactionMode
        //    + "::" + "InvoiceNo~~" + ApprovalNo + "::" + "Amount~~" + Amount + "::" + "FirstName~~" + CustomerName + "::" + "LastName~~" + CustomerName
        //   + "::" + "EmailId~~" + EmailID + "::" + "MobileNo~~" + PhoneNo + "::" + "Address1~~" + Address1 + "::" + "Address2~~" + Address2
        //   + "::" + "StateCode~~" + StateCode + "::" + "City~~" + PickupCity + "::" + "PartnerName~~" + PartnerName + "::" + "ReturnURL~~" + ReturnURL);
        EncryptedParameter = Encryption.WrapRequestData(HashKey, EncKey, ref RequestArray);

        ErrorLog.LoginfoToLogFile("MVCharging: " + EncryptedParameter, "ChargingTransaction"); //log file 

        StringBuilder sb = new StringBuilder();
        sb.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
        sb.Append("<html><head></head><body>");
        sb.Append("<form name='frm' method='POST' action='" + url + "'>");
        sb.Append("<input type='hidden' name='AuthUserName' value='" + AuthUserName + "'>");
        sb.Append("<input type='hidden' name='AuthPassword' value='" + AuthPassword + "'>");
        sb.Append("<input type='hidden' name='AuthCode' value='" + AuthCode + "'>");
        sb.Append("<input type='hidden' name='EncData' value='" + EncryptedParameter + "'>");
        sb.Append("</form>");
        sb.Append("<script language='javascript'>document.frm.submit();</script>");
        sb.Append("</body></html>");
        Response.Write(sb.ToString());
        Response.End();
    }

    //public string URLEncrypt(string strencrypt)
    //{
    //    string cipher = "";
    //    DESCryptoServiceProvider des = new DESCryptoServiceProvider();
    //    byte[] key = ASCIIEncoding.ASCII.GetBytes("Paymate1");
    //    byte[] iv = ASCIIEncoding.ASCII.GetBytes("Paymate1");
    //    try
    //    {
    //        MemoryStream ms = new MemoryStream();
    //        CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, iv),
    //        CryptoStreamMode.Write);
    //        StreamWriter sw = new StreamWriter(cs);
    //        sw.Write(strencrypt);
    //        sw.Flush();
    //        cs.FlushFinalBlock();
    //        sw.Flush();
    //        byte[] hash = ms.GetBuffer();
    //        for (int i = 0; i < ms.Length; i++)
    //            cipher += BitConverter.ToString(hash, i, 1);
    //        sw.Close();
    //        cs.Close();
    //        ms.Close();
    //    }
    //    catch (Exception ex)
    //    {
    //        cipher = ex.Message;
    //    }
    //    return cipher;
    //}
}