﻿using System;
using System.Web.UI;

public partial class Message : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        lbl_message.Text = Session["ResponseText"].ToString();
    }
    protected void btnClose_Click(object sender, EventArgs e)
    {
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "closePage", "closeWindow();", true);
    }
}