﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using ChargingWebService;
using Jose;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

public partial class AmexcardRegister : System.Web.UI.Page
{
    clsRegistration objRegistrationAmex_OL;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            generateToken();
            //Response.Redirect("https://corapi.carzonrent.com/chargingwebservice/CCAvenueCardRegister.aspx?ClientCoIndivID=" + Request.QueryString["ClientCoIndivID"].ToString());
        }
    }
    public void generateToken()
    {
        try
        {
            string ClientCoIndivID, FirstName, LastName, EmailId, MobileNo
                , StartDate, Endate;
            //ClientCoIndivID = "4186564";//"2358093"; //"977643";//"4189839";//
            ClientCoIndivID = Request.QueryString["ClientCoIndivID"].ToString();
            AmexCardCls objAmex = new AmexCardCls();
            objRegistrationAmex_OL = new clsRegistration();
            DataSet ds = new DataSet();
            ds = objAmex.GetGuestDetails(ClientCoIndivID);

            if (ds.Tables[0].Rows.Count > 0)
            {
                FirstName = ds.Tables[0].Rows[0]["FName"].ToString();
                LastName = ds.Tables[0].Rows[0]["LName"].ToString();
                if (LastName == "")
                    LastName = FirstName;
                EmailId = ds.Tables[0].Rows[0]["EmailID"].ToString();
                MobileNo = ds.Tables[0].Rows[0]["Phone1"].ToString();
                StartDate = ds.Tables[0].Rows[0]["startDate"].ToString();
                Endate = ds.Tables[0].Rows[0]["EndDate"].ToString();
                //EmailId = "alkamanav86@gmail.com";
                //MobileNo = "9650829129";
                if (ds.Tables[0].Rows[0]["CCRegisteredYN"].ToString() == "True")
                {
                    Session["ResponseText"] = "Your card is already registered.";
                    Response.Redirect("Message.aspx");

                }
            }
            else
            {
                FirstName = "";
                LastName = "";
                EmailId = "";
                MobileNo = "";

            }
            var responseUrl = ConfigurationManager.AppSettings["BilldeskCardResponseUrl"];
            var mandateUrl = ConfigurationManager.AppSettings["BilldeskMandateUrl"];
            var merchantKey = ConfigurationManager.AppSettings["BilldeskMerchantID"];
            var clientID = ConfigurationManager.AppSettings["BilldeskClientID"];
            string hostName = Dns.GetHostName(); // Retrive the Name of HOST           
            string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();
            objRegistrationAmex_OL.mercid = merchantKey;//ConfigurationManager.ConnectionStrings["mercId"].ConnectionString;
            //objRegistrationAmex_OL.customer_refid = "Cus" + ClientCoIndivID;
            //objRegistrationAmex_OL.subscription_refid = "Sub" + ClientCoIndivID;
            objRegistrationAmex_OL.customer_refid = "Cus" + ds.Tables[0].Rows[0]["TravellerId"].ToString();
            objRegistrationAmex_OL.subscription_refid = "Sub" + ds.Tables[0].Rows[0]["TravellerId"].ToString();

            objRegistrationAmex_OL.subscription_desc = "Carzonrent happy customer";
            objRegistrationAmex_OL.currency = "356";
            objRegistrationAmex_OL.frequency = "adho";
            objRegistrationAmex_OL.amount_type = "max";
            objRegistrationAmex_OL.start_date = DateTime.Now.ToString("yyyy'-'MM'-'dd''");
            objRegistrationAmex_OL.end_date = DateTime.Now.AddYears(10).ToString("yyyy'-'MM'-'dd''");
            objRegistrationAmex_OL.amount = "15000.00";
            objRegistrationAmex_OL.customer = new Customer();
            objRegistrationAmex_OL.customer.first_name = FirstName;
            objRegistrationAmex_OL.customer.last_name = LastName;
            objRegistrationAmex_OL.customer.mobile = MobileNo;
            objRegistrationAmex_OL.customer.mobile_alt = MobileNo;
            objRegistrationAmex_OL.customer.email = EmailId;
            objRegistrationAmex_OL.customer.email_alt = EmailId;
            objRegistrationAmex_OL.additional_info = new AdditionalInfo();
            objRegistrationAmex_OL.additional_info.additional_info1 = "NA";
            objRegistrationAmex_OL.additional_info.additional_info2 = "NA";
            objRegistrationAmex_OL.additional_info.additional_info3 = "NA";
            objRegistrationAmex_OL.device = new Device();
            objRegistrationAmex_OL.device.init_channel = "internet";
            objRegistrationAmex_OL.device.ip = myIP;
            //objRegistrationAmex_OL.device.mac = "NA";
            //objRegistrationAmex_OL.device.imei = "NA";
            objRegistrationAmex_OL.device.user_agent = "Mozilla/5.0";
            //objRegistrationAmex_OL.device.accept_header = "NA";
            //objRegistrationAmex_OL.device.fingerprintid = "NA";            
            objRegistrationAmex_OL.ru = responseUrl;
            //objRegistrationAmex_OL.ru = "www.billdesk.com/web";

            string postData = JsonConvert.SerializeObject(objRegistrationAmex_OL);

            var payload = new
            {
                postData = JsonConvert.SerializeObject(objRegistrationAmex_OL)

            };
            Int32 unixTimestamp = (Int32)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

            var headers = new Dictionary<string, object>()
            {
                { "alg", "HS256"},
                {"clientid", clientID},


            };


            var secretKey = ConfigurationManager.AppSettings["BilldeskEncryptionkey"];
            Jwk key = new Jwk(Encoding.UTF8.GetBytes(secretKey));
            var token = Jose.JWT.Encode(postData, key, JwsAlgorithm.HS256, headers);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var bdTimestamp = unixTimestamp.ToString();
            var traceid = Guid.NewGuid().ToString("N").Substring(0, 15);
            var client = new RestClient(mandateUrl);

            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Accept", "application/jose");
            request.AddHeader("Content-Type", "application/jose");
            request.AddHeader("BD-Traceid", traceid);
            request.AddHeader("BD-Timestamp", bdTimestamp);
            //var body = @"eyJhbGciOiJIUzI1NiIsImNsaWVudGlkIjoiY2Fyem9uMnVhdCIsIkNvbnRlbnQtVHlwZSI6ImFwcGxpY2F0aW9uL2pvc2UiLCJBY2NlcHQiOiJhcHBsaWNhdGlvbi9qb3NlIiwiQkQtVHJhY2VpZCI6IjA3ZTlkZThhMzBiNDQ0ZGNiZmQxNzhiNWVkNWY1OGFlIiwiQkQtVGltZXN0YW1wIjoiMTY2NDY2NTE0NyJ9.eyJtZXJjaWQiOiJDQVJaT04yVUFUIiwiY3VzdG9tZXJfcmVmaWQiOiJDdXM0MTg5ODM5Iiwic3Vic2NyaXB0aW9uX3JlZmlkIjoiU3ViNDE4OTgzOSIsInN1YnNjcmlwdGlvbl9kZXNjIjoiQ2Fyem9ucmVudCBoYXBweSBjdXN0b21lciIsImN1cnJlbmN5IjoiMzU2IiwiZnJlcXVlbmN5IjoiYWRob2MiLCJhbW91bnRfdHlwZSI6Im1heCIsImFtb3VudCI6IjE1MDAwLjAwIiwic3RhcnRfZGF0ZSI6IjIwMjItMTAtMDEiLCJlbmRfZGF0ZSI6IjIwMzItMTAtMDEiLCJjdXN0b21lciI6eyJmaXJzdF9uYW1lIjoiRGV2ZWxvcGVycyIsImxhc3RfbmFtZSI6Ikt1bWFyIiwibW9iaWxlIjoiOTE5NTYwMjA3NzUwIiwibW9iaWxlX2FsdCI6IiIsImVtYWlsIjoiYmhhcmR3YWpuN0BnbWFpbC5jb20iLCJlbWFpbF9hbHQiOiIifSwiYWRkaXRpb25hbF9pbmZvIjp7ImFkZGl0aW9uYWxfaW5mbzEiOiIiLCJhZGRpdGlvbmFsX2luZm8yIjoiIiwiYWRkaXRpb25hbF9pbmZvMyI6IiJ9LCJkZXZpY2UiOnsiaW5pdF9jaGFubmVsIjoiaW50ZXJuZXQiLCJpcCI6IjE3LjIzMy4xMDcuOTIiLCJtYWMiOiIxMS1BQy01OC0yMS0xQi1BQSIsImltZWkiOiI5OTAwMDAxMTIyMzM0NDUiLCJ1c2VyX2FnZW50IjoiTW96aWxsYS81LjAiLCJhY2NlcHRfaGVhZGVyIjoidGV4dC9odG1sIiwiZmluZ2VycHJpbnRpZCI6IjYxYjEyYzE4YjVkMGNmOTAxYmUzNGEyM2NhNjRiYjE5In0sInJ1IjoiaHR0cHM6Ly9pbnN0YXRlc3QuY2Fyem9ucmVudC5jb20vY2hhcmdpbmd3ZWJzZXJ2aWNlL0JpbGxEZXNrUmVzcG9uc2UuYXNweCJ9.tIAoXCQyOaGrL5QnbxgydTK-duchQ76YCt1zbpMOD8I";
            var body = token;
            request.AddParameter("application/jose", body, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            string result = response.Content;
            // Console.WriteLine(response.Content);
            var pgresponse = Jose.JWT.Decode(result, key, JwsAlgorithm.HS256);
            ErrorLog.LoginfoToLogFile("Request Data", "&&\n");
            ErrorLog.LoginfoToLogFile("Encrypted data for card requeste initiate for ClientID " + ClientCoIndivID 
             + " trackid " + ds.Tables[0].Rows[0]["TravellerId"].ToString() + " headers are TracID " + traceid + " and timeStamp " 
             + bdTimestamp + " ", token);
            ErrorLog.LoginfoToLogFile("Request Data end", "&&\n");
            ErrorLog.LoginfoToLogFile("Response Data", "&&\n");
            ErrorLog.LoginfoToLogFile("Encrypted data for card Response initiate for ClientID " + ClientCoIndivID
             + " trackid " + ds.Tables[0].Rows[0]["TravellerId"].ToString() + " headers are TracID " + traceid + " and timeStamp " 
             + bdTimestamp + " ", result);
            ErrorLog.LoginfoToLogFile("Decrypted data for card Response initiate for ClientID " + ClientCoIndivID
                + " trackid " + ds.Tables[0].Rows[0]["TravellerId"].ToString() + " headers are TracID " + traceid + " and timeStamp " 
                + bdTimestamp + " ", pgresponse);
            ErrorLog.LoginfoToLogFile("Response Data End", "&&\n");
            JObject jObj = JObject.Parse(pgresponse);
            if (Convert.ToString(jObj["status"]) == "initiated")
            {
                string mandateToken = (string)jObj["mandate_tokenid"];
                JToken parameterLinks = jObj["links"][1];
                JObject jObj1 = JObject.Parse(parameterLinks.ToString());
                string authorizationCode = (string)jObj1["headers"]["authorization"];
                hdmandateToken.Value = mandateToken;
                hdAuthID.Value = authorizationCode;
                //int resultCode = objAmex.UpdateMandateToken(Convert.ToInt32(ClientCoIndivID), mandateToken, authorizationCode);

                int resultCode = objAmex.UpdateMandateToken(ds.Tables[0].Rows[0]["TravellerId"].ToString(), mandateToken, authorizationCode);
                //if(ds.Tables[0].Rows[0]["MandateYN"].ToString()== "True")
                //{
                //    int resultCode = objAmex.UpdateMandateToken(Convert.ToInt32(ClientCoIndivID), mandateToken, authorizationCode);
                //}
                ////

            }
            else
            {
                Session["ResponseText"] = (string)jObj["message"];
                Response.Redirect("Message.aspx");
            }

        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
            ErrorLog.LoginfoToLogFile(ex.Message, "Error Message");
        }

    }
}



