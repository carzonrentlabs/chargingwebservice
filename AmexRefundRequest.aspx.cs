﻿using ChargingWebService;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class AmexRefundRequest : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var trackId = Request.QueryString["TrackId"].ToString();

        // Get and set booking details
        AmexCardCls objAmex = new AmexCardCls();
        DataSet ds = new DataSet();
        //ds = objAmex.GetAmexOrderDetailsRefund("", Convert.ToInt32(trackId));
        ds = objAmex.GetAmexOrderDetailsRefund(trackId.ToString());
        OrderDetails bookingDetails = new OrderDetails();

        if (ds.Tables[0].Rows.Count > 0)
        {
            bookingDetails.bookingId = Convert.ToInt32(ds.Tables[0].Rows[0]["BookingId"]);
            bookingDetails.resultIndicator = ds.Tables[0].Rows[0]["AmexResultIndicator"].ToString();
            bookingDetails.chargedyn = Convert.ToBoolean(ds.Tables[0].Rows[0]["chargedyn"]);
            bookingDetails.approvalNo = ds.Tables[0].Rows[0]["ApprovalNo"].ToString();
            bookingDetails.transactionid = ds.Tables[0].Rows[0]["transactionid"].ToString();
            bookingDetails.totalCost = ds.Tables[0].Rows[0]["TotalCost"].ToString();

            GetAmexRefund(bookingDetails).Wait();
            Response.Redirect("Message.aspx");
        }
        else
        {
            Session["ResponseText"] = "No match found for the Booking ID : " + trackId;
            Response.Redirect("Message.aspx");
            // Handle the case where there are no booking details
        }
    }
    private async Task GetAmexRefund(OrderDetails bookingDetails)
    {
        try
        {
            bool result = false;
            AmexCardCls objAmex = new AmexCardCls();
            string transactionId = "";

            var amex_get_order_status_api_url = ConfigurationManager.AppSettings["GetOrderStatusApiUrl"];
            var amex_gateway_host = ConfigurationManager.AppSettings["GatewayHost"];
            var amex_auth_value = ConfigurationManager.AppSettings["AmexAuthValue"];
            var merchant_name = ConfigurationManager.AppSettings["MerchantName"];
            var intraction_operation = ConfigurationManager.AppSettings["IntractionOperationType"];
            var api_operation = ConfigurationManager.AppSettings["ApiOperation"];
            var username = ConfigurationManager.AppSettings["Username"];
            var password = ConfigurationManager.AppSettings["Password"];

            var apiUrl = amex_gateway_host + amex_get_order_status_api_url + bookingDetails.approvalNo + "/transaction/" + bookingDetails.transactionid;

            // Set the TLS version to TLS 1.2
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            var transaction_OL = new RefundTransactionDetails_OL
            {
                currency = "INR",
                amount = bookingDetails.totalCost
            };
            var requestBody = new AmexRefundRequestBody
            {
                apiOperation = "REFUND",
                transaction = transaction_OL,
            };

            // Set the TLS version to TLS 1.2
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            // Set the Authorization header
            var client = new HttpClient();
            var authValue = Convert.ToBase64String(Encoding.ASCII.GetBytes(username + ":" + password));// amex_auth_value;
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + authValue);

            var jsonBody = Newtonsoft.Json.JsonConvert.SerializeObject(requestBody);

            var content = new StringContent(jsonBody, Encoding.UTF8, "application/json");
            var response = await client.PutAsync(apiUrl, content);

            if (response != null)
            {
                if (response.IsSuccessStatusCode)
                {
                    var responseContent = await response.Content.ReadAsStringAsync();
                    RefundResponse responseObj = JsonConvert.DeserializeObject<RefundResponse>(responseContent);
                    if (responseObj.result == "SUCCESS")
                    {
                        if (responseObj.response.gatewayCode == "APPROVED" && (responseObj.order.status == "PARTIALLY_REFUNDED" || responseObj.order.status == "REFUNDED"))
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }

                    }
                    else
                    {
                        result = false;
                    }
                    objAmex.UpdatRefundStatus(bookingDetails, responseObj, result, bookingDetails.transactionid);
                }
                else
                {
                    var responseContent = await response.Content.ReadAsStringAsync();
                    AmexGetOrderStatusResponse responseObj = JsonConvert.DeserializeObject<AmexGetOrderStatusResponse>(responseContent);
                    Session["ResponseText"] = "Approval Number: "+ bookingDetails.approvalNo+". Error: API request failed.";
                }
               
            }
            else
            {
                Session["ResponseText"] = "Approval Number: " + bookingDetails.approvalNo + ". No response.";
            }

        }
        catch (Exception ex)
        {
            Session["ResponseText"] = "Error in refund process. BookingID : "
                + bookingDetails.bookingId + "<br/>Error : " + ex.Message;
        }
    }
}