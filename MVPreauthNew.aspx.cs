﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using ChargingWebService;
using System.Data;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MVPreauthNew : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //changes to do only bookingid required and then get all the details from the db
        string OrderID, AgentEmailId, InvoiceNo, RentalDate, RentalType, PickupCity, CustomerName, amount
            , ClientCoIndivID, SysRegCode, BookingStatus;

        OrderID = Request.Form["OrderID"].ToString();
        InvoiceNo = OrderID;

        clsAdmin objadmin = new clsAdmin();
        DataSet ds = new DataSet();

        string EmailID, PhoneNo, StateCode;
        ds = objadmin.GetChargingDetails(Convert.ToInt32(OrderID));

        if (ds.Tables[0].Rows.Count > 0)
        {
            amount = ds.Tables[0].Rows[0]["TotalCost"].ToString();
            RentalDate = ds.Tables[0].Rows[0]["RentalDate"].ToString();
            PickupCity = ds.Tables[0].Rows[0]["CityName"].ToString();
            CustomerName = ds.Tables[0].Rows[0]["GuestName"].ToString();
            ClientCoIndivID = ds.Tables[0].Rows[0]["ClientCoIndivID"].ToString();
            SysRegCode = ds.Tables[0].Rows[0]["PaymateSysRegCode"].ToString();
            EmailID = ds.Tables[0].Rows[0]["EmailID"].ToString();
            PhoneNo = ds.Tables[0].Rows[0]["Phone1"].ToString();
            StateCode = ds.Tables[0].Rows[0]["StateCode"].ToString();

            BookingStatus = ds.Tables[0].Rows[0]["Status"].ToString();
        }
        else
        {
            amount = "0";
            RentalDate = "24/06/2020";//Convert.ToString(System.DateTime.Now.ToString("dd/mmm/yyyy"));
            PickupCity = "Delhi";
            CustomerName = "test";
            ClientCoIndivID = "0";
            SysRegCode = "";

            EmailID = "";
            PhoneNo = "";
            StateCode = "";
            BookingStatus = "";
        }

        AgentEmailId = "rahul.jain@carzonrent.com";
        RentalType = "self";
        //InvoiceNo = "123454";
        //OldBookingID = "";
        //SendSMSLater = "";
        //ClientCoID = "";
        //SMSMobiles = "";
        //SMSMobiles1 = "";
        //bitIsVIP = "";
        //hdnAcceleration_No = "";
        //SendMailLater = "";
        //LimoStatus = "";
        //amount = "100";
        //OrderID = "123454";
        //PageID = "CallTaker";
        //PickupCity , RentalDate, CustomerName --to    get from db

        ErrorLog.LoginfoToLogFile("MVPreAuthBeforeRedirect : " + "OrderID=" + OrderID + "&" + "amount=" + amount
            + "&" + "RentalDate=" + RentalDate + "&" + "PickupCity=" + PickupCity + "&" + "CustomerName=" + CustomerName
            + "&" + "ClientCoIndivID=" + ClientCoIndivID + "&" + "SysRegCode=" + SysRegCode + "&" + "EmailID=" + EmailID
            + "&" + "PhoneNo=" + PhoneNo + "&" + "StateCode=" + StateCode, "MVPreAuth"); //log file 

        if (string.IsNullOrEmpty(amount))
        {
            Session["ResponseText"] = "Cannot Process to Charge, as Invoice is not Processed.";
            Response.Redirect("Message.aspx");
        }
        else
        {
            if (BookingStatus == "O")
            {
                MVChargingRedirect(OrderID, AgentEmailId, InvoiceNo, Convert.ToDouble(amount), RentalDate, RentalType, PickupCity, CustomerName, SysRegCode
                    , Convert.ToInt32(ClientCoIndivID), EmailID, PhoneNo, StateCode);
            }
            else if (BookingStatus == "C")
            {
                //redirect // page to create
                Session["ResponseText"] = "Booking Already Closed, cannot Charge.";
                Response.Redirect("Message.aspx");
            }
            else if (BookingStatus == "Y")
            {
                //redirect // page to create
                Session["ResponseText"] = "Booking Already Closed in No-Show, cannot Charge.";
                Response.Redirect("Message.aspx");
            }
            else if (BookingStatus == "N")
            {
                Session["ResponseText"] = "Booking is not even dispatched.";
                Response.Redirect("Message.aspx");
                //redirect // page to create
            }
            else if (BookingStatus == "M")
            {
                Session["ResponseText"] = "Booking is not even dispatched.";
                Response.Redirect("Message.aspx");
                //redirect // page to create
            }
            else if (BookingStatus == "X")
            {
                Session["ResponseText"] = "Booking is not confirmed.";
                Response.Redirect("Message.aspx");
                //redirect // page to create
            }
        }
    }

    public void MVChargingRedirect(string ApprovalNo, string AgentEmailId, string InvoiceNo,
           double Amount, string RentalDate, string RentalType, string PickupCity, string CustomerName
        , string SysRegCode, int ClientCoIndivID, string EmailID, string PhoneNo, string StateCode)
    {
        string url = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessURL");

        string EncryptedParameter = "";

        string AuthUserName = "", AuthPassword = "", AuthCode = "";

        ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

        AuthUserName = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessAuthUserName");
        AuthPassword = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessAuthPassword");
        AuthCode = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessAuthCode");

        string PartnerName = "STL";
        string Address1 = "", Address2 = "";
        string ReturnURL = "https://corapi.carzonrent.com/chargingwebservice/MVChargingReturn.aspx";
        string TransactionMode = "A";

        ErrorLog.LoginfoToLogFile("MVRegisterRedirect : " + "TravellerId~~" + ClientCoIndivID + "::" + "TransactionMode~~" + TransactionMode
            + "::" + "InvoiceNo~~" + ApprovalNo + "::" + "Amount~~" + Amount + "::" + "FirstName~~" + CustomerName + "::" + "LastName~~" + CustomerName
           + "::" + "EmailId~~" + EmailID + "::" + "MobileNo~~" + PhoneNo + "::" + "Address1~~" + Address1 + "::" + "Address2~~" + Address2
           + "::" + "StateCode~~" + StateCode + "::" + "City~~" + PickupCity + "::" + "PartnerName~~" + PartnerName + "::" + "ReturnURL~~" + ReturnURL
           , "SavePreauthdata"); //log file 

        EncryptedParameter = URLEncrypt("TravellerId~~" + ClientCoIndivID + "::" + "TransactionMode~~" + TransactionMode
            + "::" + "InvoiceNo~~" + ApprovalNo + "::" + "Amount~~" + Amount + "::" + "FirstName~~" + CustomerName + "::" + "LastName~~" + CustomerName
           + "::" + "EmailId~~" + EmailID + "::" + "MobileNo~~" + PhoneNo + "::" + "Address1~~" + Address1 + "::" + "Address2~~" + Address2
           + "::" + "StateCode~~" + StateCode + "::" + "City~~" + PickupCity + "::" + "PartnerName~~" + PartnerName + "::" + "ReturnURL~~" + ReturnURL);


        ErrorLog.LoginfoToLogFile("MVChargingRedirect : " + EncryptedParameter, "ChargingTransaction"); //log file 

        StringBuilder sb = new StringBuilder();
        sb.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
        sb.Append("<html><head></head><body>");
        sb.Append("<form name='frm' method='POST' action='" + url + "'>");
        sb.Append("<input type='hidden' name='AuthUserName' value='" + AuthUserName + "'>");
        sb.Append("<input type='hidden' name='AuthPassword' value='" + AuthPassword + "'>");
        sb.Append("<input type='hidden' name='AuthCode' value='" + AuthCode + "'>");
        sb.Append("<input type='hidden' name='EncData' value='" + EncryptedParameter + "'>");
        sb.Append("</form>");
        sb.Append("<script language='javascript'>document.form1.submit();</script>");
        sb.Append("</body></html>");
        Response.Write(sb.ToString());
        Response.End();
    }

    public string URLEncrypt(string strencrypt)
    {
        string cipher = "";
        DESCryptoServiceProvider des = new DESCryptoServiceProvider();
        byte[] key = ASCIIEncoding.ASCII.GetBytes("Paymate1");
        byte[] iv = ASCIIEncoding.ASCII.GetBytes("Paymate1");
        try
        {
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, iv),
            CryptoStreamMode.Write);
            StreamWriter sw = new StreamWriter(cs);
            sw.Write(strencrypt);
            sw.Flush();
            cs.FlushFinalBlock();
            sw.Flush();
            byte[] hash = ms.GetBuffer();
            for (int i = 0; i < ms.Length; i++)
                cipher += BitConverter.ToString(hash, i, 1);
            sw.Close();
            cs.Close();
            ms.Close();
        }
        catch (Exception ex)
        {
            cipher = ex.Message;
        }
        return cipher;
    }
}