﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ChargingWebService;
using System.Data;
using System.Net;

public partial class MVRegister : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        //changes to do only bookingid required and then get all the details from the db

        string ClientCoIndivID, FirstName, LastName, EmailId, MobileNo
        , Address1, Address2, StateCode, City, TransactionMode, TravellerId;

        ClientCoIndivID = Request.QueryString["ClientCoIndivID"].ToString();

        clsAdmin objadmin = new clsAdmin();

        DataSet ds = new DataSet();

        ds = objadmin.GetGuestDetails(ClientCoIndivID);

        if (ds.Tables[0].Rows.Count > 0)
        {
            FirstName = ds.Tables[0].Rows[0]["FName"].ToString();
            LastName = ds.Tables[0].Rows[0]["LName"].ToString();
            EmailId = ds.Tables[0].Rows[0]["EmailID"].ToString();
            MobileNo = ds.Tables[0].Rows[0]["Phone1"].ToString();
            Address1 = ds.Tables[0].Rows[0]["Address1"].ToString();
            Address2 = ds.Tables[0].Rows[0]["Address2"].ToString();
            StateCode = ds.Tables[0].Rows[0]["StateCode"].ToString();
            City = ds.Tables[0].Rows[0]["CityName"].ToString();
            TransactionMode = ds.Tables[0].Rows[0]["TransactionMode"].ToString();
            TravellerId = ds.Tables[0].Rows[0]["TravellerId"].ToString();
        }
        else
        {
            FirstName = "";
            LastName = "";
            EmailId = "";
            MobileNo = "";
            Address1 = "";
            Address2 = "";
            StateCode = "";
            City = "";
            TransactionMode = "";
            TravellerId = "";
        }

        ErrorLog.LoginfoToLogFile("MVPreAuthBeforeRedirect : " + "ClientCoIndivID=" + ClientCoIndivID
            + "&" + "TravellerId=" + TravellerId
            + "&" + "FirstName=" + FirstName + "&" + "LastName=" + LastName + "&" + "EmailId=" + EmailId
            + "&" + "MobileNo=" + MobileNo + "&" + "Address1=" + Address1 + "&" + "Address2=" + Address2
            + "&" + "StateCode=" + StateCode + "&" + "City=" + City, "MVRegister"); //log file 

        MVRegisterRedirect(TravellerId, FirstName, LastName, EmailId, MobileNo, Address1, Address2, StateCode, City, TransactionMode);
    }

    public void MVRegisterRedirect(string TravellerId, string FirstName, string LastName, string EmailId, string MobileNo
        , string Address1, string Address2, string StateCode, string City, string TransactionMode)
    {
        string url = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewRegistrationProcessURL");

        string EncryptedParameter = "", ValueToHash = "";

        string AuthUserName = "", AuthPassword = "", AuthCode = "", HashKey = "", EncKey = "";

        ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

        AuthUserName = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessAuthUserName");
        AuthPassword = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessAuthPassword");
        AuthCode = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessAuthCode");
        HashKey = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessHashKey");
        EncKey = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessEncKey");
        string PartnerName;
        PartnerName = "NA";
        //string ReturnURL = "https://corapi.carzonrent.com/chargingwebservice/MVRegisterReturn.aspx";
        string ReturnURL = "https://instatest.carzonrent.com/chargingwebservice/MVRegisterReturn.aspx";
        //string TransactionMode = "A";

        //ErrorLog.LoginfoToLogFile("MVRegisterRedirect : " + "TravellerId~~" + TravellerId + "::" + "Action~~" + TransactionMode
        //    + "::" + "FirstName~~" + FirstName + "::" + "LastName~~" + LastName + "::" + "EmailId~~" + EmailId + "::" + "MobileNo~~" + MobileNo
        //   + "::" + "Address1~~" + Address1 + "::" + "Address2~~" + Address2 + "::" + "StateCode~~" + StateCode + "::" + "City~~" + City
        //   + "::" + "PartnerName~~" + PartnerName + "::" + "ReturnURL~~" + ReturnURL
        //   , "SavePreauthdata"); //log file 

        //ErrorLog.LoginfoToLogFile("MVRegisterRedirect : " + HashKey + TransactionMode + Address1 + Address2 + City + EmailId
        //    + FirstName + LastName + MobileNo + PartnerName + ReturnURL + StateCode + TravellerId
        //   , "ValueToHash"); //log file 



        string[,] RequestArray = new string[20, 2];

        int i = -1;
        RequestArray[++i, 0] = "TravellerId";
        RequestArray[i, 1] = TravellerId;
        RequestArray[++i, 0] = "Action";
        RequestArray[i, 1] = TransactionMode;
        RequestArray[++i, 0] = "FirstName";
        RequestArray[i, 1] = FirstName;
        RequestArray[++i, 0] = "LastName";
        RequestArray[i, 1] = LastName;
        RequestArray[++i, 0] = "EmailId";
        RequestArray[i, 1] = EmailId;
        RequestArray[++i, 0] = "MobileNo";
        RequestArray[i, 1] = MobileNo;
        RequestArray[++i, 0] = "Address1";
        RequestArray[i, 1] = Address1;
        RequestArray[++i, 0] = "Address2";
        RequestArray[i, 1] = Address2;
        RequestArray[++i, 0] = "StateCode";
        RequestArray[i, 1] = StateCode;
        RequestArray[++i, 0] = "City";
        RequestArray[i, 1] = City;
        RequestArray[++i, 0] = "PartnerName";
        RequestArray[i, 1] = PartnerName;
        RequestArray[++i, 0] = "ReturnURL";
        RequestArray[i, 1] = ReturnURL;


        //Encryption exep = new Encryption();
        //(string HashKey, string EncKey, ref string[,] RequestArray)
        EncryptedParameter = Encryption.WrapRequestData(HashKey, EncKey, ref RequestArray);


        //ValueToHash = URLEncrypt(HashKey + TransactionMode + Address1 + Address2 + City + EmailId
        //    + FirstName + LastName + MobileNo + PartnerName + ReturnURL + StateCode + TravellerId);

        //EncryptedParameter = URLEncrypt("TravellerId~~" + TravellerId + "::" + "Action~~" + TransactionMode
        //    + "::" + "FirstName~~" + FirstName + "::" + "LastName~~" + LastName + "::" + "EmailId~~" + EmailId + "::" + "MobileNo~~" + MobileNo
        //   + "::" + "Address1~~" + Address1 + "::" + "Address2~~" + Address2 + "::" + "StateCode~~" + StateCode + "::" + "City~~" + City
        //   + "::" + "PartnerName~~" + PartnerName + "::" + "ReturnURL~~" + ReturnURL);


        ErrorLog.LoginfoToLogFile("MVRegisterRedirect : " + EncryptedParameter, "SaveCarddata"); //log file 

        StringBuilder sb = new StringBuilder();
        sb.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
        sb.Append("<html><head></head><body>");
        sb.Append("<form name='frm' method='POST' action='" + url + "'>");
        sb.Append("<input type='hidden' name='AuthUserName' value='" + AuthUserName + "'>");
        sb.Append("<input type='hidden' name='AuthPassword' value='" + AuthPassword + "'>");
        sb.Append("<input type='hidden' name='AuthCode' value='" + AuthCode + "'>");
        sb.Append("<input type='hidden' name='EncData' value='" + EncryptedParameter + "'>");
        sb.Append("</form>");
        sb.Append("<script language='javascript'>document.frm.submit();</script>");
        sb.Append("</body></html>");
        Response.Write(sb.ToString());
        Response.End();
    }

    //public string URLEncrypt(string strencrypt)
    //{
    //    string cipher = "";
    //    DESCryptoServiceProvider des = new DESCryptoServiceProvider();
    //    byte[] key = ASCIIEncoding.ASCII.GetBytes("Paymate1");
    //    byte[] iv = ASCIIEncoding.ASCII.GetBytes("Paymate1");
    //    try
    //    {
    //        MemoryStream ms = new MemoryStream();
    //        CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, iv),
    //        CryptoStreamMode.Write);
    //        StreamWriter sw = new StreamWriter(cs);
    //        sw.Write(strencrypt);
    //        sw.Flush();
    //        cs.FlushFinalBlock();
    //        sw.Flush();
    //        byte[] hash = ms.GetBuffer();
    //        for (int i = 0; i < ms.Length; i++)
    //            cipher += BitConverter.ToString(hash, i, 1);
    //        sw.Close();
    //        cs.Close();
    //        ms.Close();
    //    }
    //    catch (Exception ex)
    //    {
    //        cipher = ex.Message;
    //    }
    //    return cipher;
    //}
}