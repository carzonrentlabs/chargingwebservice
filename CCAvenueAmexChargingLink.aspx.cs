﻿using System;
using System.Data;
using System.Text;
//using ChargingWebService;
using CCA.Util;
using ChargingWebService;

public partial class CCAvenueAmexChargingLink : System.Web.UI.Page
{
    CCACrypto ccaCrypto = new CCACrypto();
    string ccaRequest = "";
    public string strEncRequest = "";
    string workingKey = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueAmexCCAvenueWorkingKey");
    public string strAccessCode = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueAmexCCAvenueAccessCode");
    public string strmerchant_id = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueAmexCCAvenueMerchant_id");
    public string CORRedirectURL = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueAmexCORRedirectURL");
    public string CORCancelURL = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueAmexCORCancelURL");
    public string CCAvenueRegisterURL = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueAmexCCAvenueRegisterURL");


    public string HDFPayments_option = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueAmexPayments_option");
    public string HDFcard_type = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueAmexcard_type");
    public string HDFCcard_name = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueAmexcard_name");
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        //{
        AmexCardCls objAmex = new AmexCardCls();

        string FirstName, LastName
            , EmailId, MobileNo, ApprovalNo, CityName, StateName, BillAddress;
        double TotalCost = 0;
        string encRequestParam = Request.QueryString["MTrackid"].ToString().Trim();
        if (!encRequestParam.Contains(":"))
        {
            Session["ResponseText"] = "Error:Invalid MTrackid parameter.";
            Response.Redirect("Message.aspx");
            return;
        }

        string[] words = encRequestParam.Split(':');
        string bookingId = words[0];
        string amount = words[1];

        if (string.IsNullOrEmpty(encRequestParam) || words.Length > 2)
        {
            // Handle the case where the query parameter is missing or empty
            //Response.Write("Error:Invalid MTrackid parameter.");
            Session["ResponseText"] = "Error:Invalid MTrackid parameter.";
            Response.Redirect("Message.aspx");
            return;
        }

        DataSet EncryptDecrypt = objAmex.EncryptDecryptNew(bookingId, amount, 0);
        if (EncryptDecrypt.Tables[0].Rows.Count > 0)
        {
            string decryptedParamStr = EncryptDecrypt.Tables[0].Rows[0]["BookingID"].ToString();
            string[] decryptedParamArray = decryptedParamStr.Split(':');
            bookingId = decryptedParamArray[0];
            amount = decryptedParamArray[1];
        }

        // Get and set booking details
        DataSet ds = objAmex.BookingDetailsHDFCDirectCharge(Convert.ToInt32(bookingId));
        if (ds.Tables[0].Rows.Count > 0)
        {
            if (Convert.ToBoolean(ds.Tables[0].Rows[0]["BookingChargedYN"]))
            {
                Session["ResponseText"] = "Your booking " + bookingId + " is already charged.";
                Response.Redirect("Message.aspx");
                return;
            }

            if (Convert.ToBoolean(ds.Tables[0].Rows[0]["AwaitedYN"]))
            {
                Session["ResponseText"] = "Your transaction against bookingid " 
                    + bookingId 
                    + " is pending at Bank end, please wait for 30 min before trying again.";
                Response.Redirect("Message.aspx");
                return;
            }
        }

        if (ds.Tables[0].Rows.Count > 0)
        {
            FirstName = ds.Tables[0].Rows[0]["FName"].ToString();
            LastName = ds.Tables[0].Rows[0]["LName"].ToString();
            if (LastName == "")
                LastName = FirstName;
            EmailId = ds.Tables[0].Rows[0]["EmailID"].ToString();
            MobileNo = ds.Tables[0].Rows[0]["Phone1"].ToString();
            ApprovalNo = ds.Tables[0].Rows[0]["ApprovalNo"].ToString();
            CityName = ds.Tables[0].Rows[0]["CityName"].ToString();
            StateName = ds.Tables[0].Rows[0]["StateName"].ToString();
            BillAddress = ds.Tables[0].Rows[0]["BillAddress"].ToString();
            TotalCost = Convert.ToDouble(ds.Tables[0].Rows[0]["TotalCost"]);
            objAmex.SaveAmexSession(Convert.ToInt32(bookingId)
                , ApprovalNo);
        }
        else
        {
            TotalCost = 0;
            FirstName = "";
            LastName = "";
            EmailId = "";
            MobileNo = "";
            ApprovalNo = "";
            CityName = "";
            StateName = "";
            BillAddress = "";

            Session["ResponseText"] = "Invalid Request.";
            Response.Redirect("Message.aspx");
        }
        //tid : 1691150257806
        //merchant_id : 320
        //order_id : bookingid
        //amount : 1.00
        //currency : INR
        //redirect_url : https://corapi.carzonrent.com/chargingwebservice/CCAvenueCardResponse.aspx
        //cancel_url : https://corapi.carzonrent.com/chargingwebservice/CCAvenueCardResponse.aspx
        //billing_name : Optional
        //billing_address : Optional
        //billing_city : Optional
        //billing_state : Optional
        //billing_zip : Optional
        //billing_country : Optional
        //billing_tel : Optional
        //billing_email : Optional
        //delivery_name : Optional
        //delivery_address : Optional
        //delivery_city : Optional
        //delivery_state : Optional
        //delivery_zip : Optional
        //delivery_country : Optional
        //delivery_tel : Optional
        //merchant_param1 : Optional
        //merchant_param2 : Optional
        //merchant_param3 : Optional
        //merchant_param4 : Optional
        //merchant_param5 : Optional
        //promo_code : Optional
        //customer_identifier : Optional
        string orderid = ApprovalNo;

        ccaRequest = "tid=" + orderid + "&merchant_id=" + strmerchant_id + "&order_id="
            + orderid + "&amount=" + TotalCost
            + "&currency=INR" + "&redirect_url=" + CORRedirectURL
            + "&cancel_url=" + CORCancelURL
            + "&billing_name=" + FirstName + " " + LastName
            + "&billing_address=" + BillAddress + "&billing_city=" + CityName
            + "&billing_state=" + StateName + "&billing_zip=425001"
            + "&billing_country=India&billing_tel=" + MobileNo
            + "&billing_email=" + EmailId
            + "&delivery_name=" + FirstName + " " + LastName
            + "&delivery_address=" + BillAddress
            + "&language=EN"
            + "&delivery_city=" + CityName + "&delivery_state=" + StateName
            + "&delivery_zip=425001&delivery_country=India&delivery_tel="
            + MobileNo + "&merchant_param1=" + bookingId
            + "&merchant_param2=" + bookingId
            + "&merchant_param3=" + bookingId
            + "&merchant_param4=" + bookingId
            + "&merchant_param5=" + bookingId
            //+ "&si_type=ONDEMAND&si_mer_ref_no=" + orderid
            //+ "&si_setup_amount=1.00"
            + "&promo_code=&customer_identifier="
            //+ "&Payments_option=OPTCRDC&card_type=CRDC&card_name=Visa"

            + "&payment_option=" + HDFPayments_option //OPTCRDC|OPTDBCRD|OPTNBK|OPTCASHC|OPTMOBP
            + "&card_type=" + HDFcard_type  //CRDC|DBCRD|NBK|CASHC|MOBP
            + "&card_name=" + HDFCcard_name //Visa|Master
            + "&";
        //+ "&ignore_payment_option=OPTMOBP|OPTNBK|OPTCASHC|OPTEMI|OPTIVRS|OPTWLT|OPTNEFT|OPTUPI|OPTPL|OPTRWRD|OPTER&";

        strEncRequest = ccaCrypto.Encrypt(ccaRequest, workingKey);

        AmexCCAvenueRedirect(strAccessCode, strEncRequest, ccaRequest, workingKey);
    }

    public void AmexCCAvenueRedirect(string access_code, string strEncRequest
        , string WithOutencrypt, string workingKey)
    {
        CCACrypto ccaCrypto = new CCACrypto();

        ErrorLog.LoginfoToLogFile("AmexCCAvenueRedirect : access_code="
            + access_code +
            "&WorkingKey=" + workingKey
            + "&strEncRequest=" + strEncRequest, ""); //log file 

        ErrorLog.LoginfoToLogFile("AmexCCAvenueRedirect : access_code="
            + access_code +
            "&WorkingKey=" + workingKey
            + "&WithOutencrypt=" + WithOutencrypt, ""); //log file 
        //string EncryptedParameter = "";
        //EncryptedParameter = "";

        StringBuilder sb = new StringBuilder();
        sb.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
        sb.Append("<html xmlns='https://www.w3.org/1999/xhtml'><head id='Head1' runat='server'>");
        sb.Append("<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>");
        sb.Append("<script type='text/javascript'>");
        sb.Append("$(document).ready(function() {");
        sb.Append("$('#nonseamless').submit();");
        sb.Append("});");
        sb.Append("</script>");
        sb.Append("<title></title>");

        sb.Append("</head><body>");
        sb.Append("<form id='nonseamless' method='post' name='redirect' action='" + CCAvenueRegisterURL + "'>");
        sb.Append("<input type='hidden' id='encRequest' name='encRequest' value='" + strEncRequest + "'>");
        sb.Append("<input type='hidden' id='access_code' name='access_code' value='" + access_code + "'>");
        sb.Append("</form>");
        sb.Append("</body></html>");
        Response.Write(sb.ToString());
        Response.End();
    }
}