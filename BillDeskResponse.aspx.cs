﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ChargingWebService;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Data;
using System.Net;
using System.Configuration;
using Jose;

public partial class BillDeskResponse : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AmexCardCls objAmex = new AmexCardCls();
        int flag = 0;
        try
        {
            string mandate_response = "";
            if (Request.Form["encrypted_response"] == null || string.IsNullOrWhiteSpace(Request.Form["encrypted_response"]))
            {
                mandate_response = Request.Form["mandate_response"].ToString();
            }
            else
            {
                mandate_response = Request.Form["encrypted_response"].ToString();
            }
            ErrorLog.LoginfoToLogFile("BillDeskResponse : " + mandate_response, "");
            //mandate_response = "eyJhbGciOiJIUzI1NiIsImNsaWVudGlkIjoiY2Fyem9ucmVuMiIsImtpZCI6IkhNQUMifQ.eyJtZXJjaWQiOiJDQVJaT05SRU4yIiwiZW5kX2RhdGUiOiIyMDMyLTEwLTI2IiwicGF5bWVudF9tZXRob2RfdHlwZSI6ImNhcmQiLCJhbW91bnQiOiIxNTAwMC4wMCIsInJ1IjoiaHR0cHM6Ly9jb3JhcGkuY2Fyem9ucmVudC5jb20vQ2hhcmdpbmdXZWJTZXJ2aWNlL0JpbGxEZXNrUmVzcG9uc2UuYXNweCIsInN1YnNjcmlwdGlvbl9kZXNjIjoiQ2Fyem9ucmVudCBoYXBweSBjdXN0b21lciIsInZlcmlmaWNhdGlvbl9lcnJvcl90eXBlIjoic3VjY2VzcyIsImNyZWF0ZWRvbiI6IjIwMjItMTAtMjZUMTE6MTQ6NTIrMDU6MzAiLCJmcmVxdWVuY3kiOiJhZGhvIiwiY3VzdG9tZXJfcmVmaWQiOiJDdXM0NTM0Mjc2IiwiYW1vdW50X3R5cGUiOiJtYXhpbXVtIiwiYWRkaXRpb25hbF9pbmZvIjp7ImFkZGl0aW9uYWxfaW5mbzEiOiJOQSIsImFkZGl0aW9uYWxfaW5mbzMiOiJOQSIsImFkZGl0aW9uYWxfaW5mbzIiOiJOQSJ9LCJtYW5kYXRlaWQiOiJNTjA0ODk2MzQwMzMzNyIsInZlcmlmaWNhdGlvbl9lcnJvcl9jb2RlIjoiVFJTMDAwMCIsInN1YnNjcmlwdGlvbl9yZWZpZCI6IlN1YjQ1MzQyNzYiLCJjdXJyZW5jeSI6IjM1NiIsIm9iamVjdGlkIjoibWFuZGF0ZSIsImNhcmQiOnsicGF5bWVudGFjY291bnRpZCI6IlBBMTRDRTYxMzUwNDM4IiwibWFza2VkX3ZhbHVlIjoieHh4eHh4eHh4eHh4NzgyOCIsImhvbGRlcl9uYW1lIjoiWW9nZW5kZXIgVmVybWEifSwidmVyaWZpY2F0aW9uX2Vycm9yX2Rlc2MiOiJUcmFuc2FjdGlvbiBTdWNjZXNzZnVsIiwic3RhcnRfZGF0ZSI6IjIwMjItMTAtMjYiLCJzdGF0dXMiOiJhY3RpdmUiLCJjdXN0b21lciI6eyJlbWFpbF9hbHQiOiJ5b2dlbmRlci52ZXJtYUBjYXJ6b25yZW50LmNvbSIsIm1vYmlsZV9hbHQiOiI5OTcxNjk3OTEyIiwibW9iaWxlIjoiOTk3MTY5NzkxMiIsImxhc3RfbmFtZSI6IlZlcm1hIiwiZmlyc3RfbmFtZSI6IllvZ2VuZHJhIiwiZW1haWwiOiJ5b2dlbmRlci52ZXJtYUBjYXJ6b25yZW50LmNvbSJ9fQ.FTw868Yc3lD5690hTMBaRPR2pHtEAJ8YHqyiiZZTOJI";

            var secretKey = ConfigurationManager.AppSettings["BilldeskEncryptionkey"];
            Jwk key = new Jwk(Encoding.UTF8.GetBytes(secretKey));
            var pgresponse = Jose.JWT.Decode(mandate_response, key, JwsAlgorithm.HS256);
            // var pgresponse = "{\"mercid\":\"CARZONREN2\",\"end_date\":\"2032-10-26\",\"payment_method_type\":\"card\",\"amount\":\"15000.00\",\"ru\":\"https://corapi.carzonrent.com/ChargingWebService/BillDeskResponse.aspx\",\"subscription_desc\":\"Carzonrenthappycustomer\",\"verification_error_type\":\"success\",\"createdon\":\"2022-10-26T11:14:52+05:30\",\"frequency\":\"adho\",\"customer_refid\":\"Cus4534276\",\"amount_type\":\"maximum\",\"additional_info\":{\"additional_info1\":\"NA\",\"additional_info3\":\"NA\",\"additional_info2\":\"NA\"},\"mandateid\":\"MN048963403337\",\"verification_error_code\":\"TRS0000\",\"subscription_refid\":\"Sub4534276\",\"currency\":\"356\",\"objectid\":\"mandate\",\"card\":{\"paymentaccountid\":\"PA14CE61350438\",\"masked_value\":\"xxxxxxxxxxxx7828\",\"holder_name\":\"YogenderVerma\"},\"verification_error_desc\":\"TransactionSuccessful\",\"start_date\":\"2022-10-26\",\"status\":\"active\",\"customer\":{\"email_alt\":\"yogender.verma@carzonrent.com\",\"mobile_alt\":\"9971697912\",\"mobile\":\"9971697912\",\"last_name\":\"Verma\",\"first_name\":\"Yogendra\",\"email\":\"yogender.verma@carzonrent.com\"}}";

            JObject jObj = JObject.Parse(pgresponse);

            //ErrorLog.LoginfoToLogFile("***************@BillDeskResponsePage***********************", "&&\n");
            //ErrorLog.LoginfoToLogFile("Encrypted Response BillDeskResponsePage ", mandate_response);
            ErrorLog.LoginfoToLogFile("&&", "&&\n");
            ErrorLog.LoginfoToLogFile("decrypted Response BillDeskResponsePage ", pgresponse);
            ErrorLog.LoginfoToLogFile("***********BillDeskResponsePage_End************************", "&&\n");

            if (Convert.ToString(jObj["status"]) == "rejected" || Convert.ToString(jObj["status"]) == "active")
            {
                var mandateId = Convert.ToString(jObj["mandateid"]);
                var customerId = Convert.ToString(jObj["customer_refid"]);
                string TravellerId = Convert.ToString(customerId.Remove(0, 3));
                var cardStatus = Convert.ToString(jObj["status"]);
                var errorType = Convert.ToString(jObj["verification_error_type"]);
                var errorCode = Convert.ToString(jObj["verification_error_code"]);
                var errordesc = Convert.ToString(jObj["verification_error_desc"]);
                var cardMaskedValue = (string)jObj["card"]["masked_value"];
                var paymentAccountid = (string)jObj["card"]["paymentaccountid"];
                var holderName = (string)jObj["card"]["holder_name"];
                if (cardStatus == "active")
                {
                    flag = 1;
                }
                else
                    flag = 0;

                int resultCode = objAmex.UpdateSaveCardDetails(TravellerId, cardStatus, errorCode, errorType
                    , errordesc, mandateId, flag, cardMaskedValue, paymentAccountid, holderName);

                Session["ResponseText"] = Convert.ToString(jObj["verification_error_desc"]);
                //Response.Redirect("Message.aspx");
                Response.Redirect("Message.aspx", false);        //write redirect
                Context.ApplicationInstance.CompleteRequest();
            }
            else
            {

                Session["ResponseText"] = Convert.ToString(jObj["error_type"]) + " " + Convert.ToString(jObj["message"]);
                //Response.Redirect("Message.aspx");
                Response.Redirect("Message.aspx", false);        //write redirect
                Context.ApplicationInstance.CompleteRequest();

            }
        }
        catch (Exception ex)
        {
            ErrorLog.LoginfoToLogFile("The error is on BillDeskResopnse: " + ex.Message, "");
            ErrorLog.LogErrorToLogFile(ex
                , "BillDeskResponse : The error is on BillDeskResopnse: ");
            // Response.Write(ex.Message);
            Session["ResponseText"] = ex.Message.ToString();
            Response.Redirect("Message.aspx");
        }
    }
}