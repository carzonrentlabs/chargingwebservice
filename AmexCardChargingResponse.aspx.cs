﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Results;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ChargingWebService;
using Jose;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace _API
{
    public partial class AmexCardChargingResponse : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var resultIndicator = Request.QueryString["resultIndicator"].ToString();
            var sessionVersion = Request.QueryString["sessionVersion"].ToString();

            if (!String.IsNullOrEmpty(resultIndicator))
            {
                // Get and set booking details
                AmexCardCls objAmex = new AmexCardCls();
                DataSet ds = new DataSet();
                ds = objAmex.GetAmexOrderDetails(resultIndicator, 0);
                OrderDetails bookingDetails = new OrderDetails();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    bookingDetails.bookingId = Convert.ToInt32(ds.Tables[0].Rows[0]["BookingId"]);
                    bookingDetails.resultIndicator = ds.Tables[0].Rows[0]["AmexResultIndicator"].ToString();
                    bookingDetails.chargedyn = Convert.ToBoolean(ds.Tables[0].Rows[0]["chargedyn"]);
                    bookingDetails.approvalNo = ds.Tables[0].Rows[0]["ApprovalNo"].ToString();

                    CallGetAmexOrderPaymentStatus(bookingDetails).Wait();

                    // GetAmexOrderPaymentStatus(bookingDetails).Wait();
                    Response.Redirect("Message.aspx");
                }
                else
                {
                    Session["ResponseText"] = "No match found for the result.Indicator : " + resultIndicator;
                    Response.Redirect("Message.aspx");
                    // Handle the case where there are no booking details
                }
            }
            else
            {
                Session["ResponseText"] = "No Indicator found";
                Response.Redirect("Message.aspx");
            }

        }
        private async Task CallGetAmexOrderPaymentStatus(OrderDetails bookingDetails)
        {
            clsAdmin pbjCls = new clsAdmin();
            string response = await pbjCls.GetAmexOrderPaymentStatus(bookingDetails);
            Session["ResponseText"] = response;
        }


        //private async Task GetAmexOrderPaymentStatus(OrderDetails bookingDetails)
        //{
        //    try
        //    {
        //        bool result = false;
        //        AmexCardCls objAmex = new AmexCardCls();
        //        string transactionId = "";

        //        var amex_get_order_status_api_url = ConfigurationManager.AppSettings["GetOrderStatusApiUrl"];
        //        var amex_gateway_host = ConfigurationManager.AppSettings["GatewayHost"];
        //        var amex_auth_value = ConfigurationManager.AppSettings["AmexAuthValue"];
        //        var return_url = ConfigurationManager.AppSettings["ReturnUrl"];
        //        var merchant_name = ConfigurationManager.AppSettings["MerchantName"];
        //        var intraction_operation = ConfigurationManager.AppSettings["IntractionOperationType"];
        //        var api_operation = ConfigurationManager.AppSettings["ApiOperation"];
        //        var username = ConfigurationManager.AppSettings["Username"];
        //        var password = ConfigurationManager.AppSettings["Password"];

        //        var apiUrl = amex_gateway_host + amex_get_order_status_api_url + bookingDetails.approvalNo;

        //        Set the TLS version to TLS 1.2
        //        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

        //        var client = new HttpClient();
        //        var request = new HttpRequestMessage(HttpMethod.Get, apiUrl);

        //        Set the Authorization header
        //        var authValue = Convert.ToBase64String(Encoding.ASCII.GetBytes(username + ":" + password));
        //        request.Headers.Add("Authorization", "Basic " + authValue);

        //        var response = await client.SendAsync(request);
        //        var responseContent = await response.Content.ReadAsStringAsync();


        //        AmexGetOrderStatusResponse responseObj = JsonConvert.DeserializeObject<AmexGetOrderStatusResponse>(responseContent);
        //        var transaction = responseObj.
        //        if (responseObj != null)
        //        {
        //            if (responseObj.status == "CAPTURED" && responseObj.transaction.Count() > 2)
        //            {
        //                Transaction transaction3 = responseObj.transaction[2];
        //                if (transaction3.authentication.ThreeDS.acsEci == "05"
        //                    && transaction3.order.status == "CAPTURED"
        //                    && transaction3.order.authenticationStatus == "AUTHENTICATION_SUCCESSFUL"
        //                    && transaction3.response.gatewayCode == "APPROVED"
        //                    && transaction3.response.gatewayRecommendation == "NO_ACTION")
        //                {
        //                    result = true;
        //                }
        //                else
        //                {
        //                    result = false;
        //                }
        //            }
        //            else
        //            {
        //                result = false;
        //            }

        //            if (responseObj.authentication != null)
        //            {
        //                transactionId = responseObj.authentication._3ds.transactionId;
        //            }

        //            objAmex.UpdateOrderPaymentStatus(bookingDetails, responseObj.status
        //                , transactionId, result);
        //        }
        //        else
        //        {
        //            Session["ResponseText"] = "there is some issue at amex response. Amex Response : " + responseObj.status;
        //        }


        //        Session["ResponseText"] = "BookingId : " + bookingDetails.bookingId
        //            + "<br/> Status : " + responseObj.status;
        //    }
        //    catch (Exception ex)
        //    {
        //        Response.Write("Error: " + ex.Message);
        //        Session["ResponseText"] = "Error in Charging BookingID : "
        //            + bookingDetails.bookingId + "<br/>Error : " + ex.Message;
        //        Response.Redirect("Message.aspx");
        //    }
        //}
    }
}