﻿using _API;
using ChargingWebService;
using Quartz.Util;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
//using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;

/// <summary>
/// Summary description for CanaraApiCls
/// </summary>
public class CanaraApiCls
{
    public CanaraApiCls()
    {
        //
        // TODO: Add constructor logic here
        //
    }


    // Specify the data types for the DataTable columns
    private static Dictionary<string, Type> columnDataTypes = new Dictionary<string, Type>
{
    { "utr_RRN_Number", typeof(string) },
    { "TxnRefNo", typeof(string) },
    { "TxnStatus", typeof(string) },
    { "Txn_init_date", typeof(string) },
    { "Approved_By", typeof(string) },
    { "Approved_Date", typeof(string) },
    { "Error_Code", typeof(string) },
    { "Error_Msg", typeof(string) },
};

    // To save batch initiate api response
    public int SaveCanaraBatchInitiateResponse(CanSuccessResponseBodyEnryptData resBatch)
    {

        SqlParameter[] param = new SqlParameter[8];
        param[0] = new SqlParameter("@Result", resBatch.status.result);
        param[1] = new SqlParameter("@referenceNumber", resBatch.status.referenceNumber);
        param[2] = new SqlParameter("@BatchNo", resBatch.Response.Body.txnSummary.BatchNo);
        param[3] = new SqlParameter("@TotalRecords", resBatch.Response.Body.txnSummary.TotalRecords);
        param[4] = new SqlParameter("@SumAmount", resBatch.Response.Body.txnSummary.SumAmount);
        param[5] = new SqlParameter("@ErrorCode", resBatch.Response.Body.txnSummary.ErrorCode);
        param[6] = new SqlParameter("@ErrorDec", resBatch.Response.Body.txnSummary.ErrorDec);
        param[7] = new SqlParameter("@BatchRequestID", resBatch.Response.Body.txnSummary.BatchRequestID);
        var res = SqlHelper.ExecuteNonQuery("Prc_UpdateCanaraBatchInitiateResponse", param);
        return res;
    }
    public CanraRequest GetBatchTrxnDetails(string batchRequestId)
    {
        string senderAccountNo = ConfigurationManager.AppSettings["CanaraSenderAccountNo"];
        string senderAccountName = ConfigurationManager.AppSettings["CanaraSenderAccountName"];
        string customerID = ConfigurationManager.AppSettings["CanaraCustomerID"];
        string authorization = ConfigurationManager.AppSettings["CanaraAuthorization"];
        string key = "3iins2570n1pu4p52qie9c16dr";

        CanraRequest objTrakckingRemarkRS = new CanraRequest();
        List<TxnDetail> objTransactionRSList = new List<TxnDetail>();

        try
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@BatchRequestId", batchRequestId);
            //DataSet ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetTrackingRemarkActivation_WithTrackRemark", param);
            DataSet ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetBatchTrxnDetails", param);
            string BatchRequestIDPrc = "";
            double totalAmt = 0;
            if (ds.Tables[0].Rows.Count > 0)
            {
                
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    //OL_BatchTransactionData tansaction = new OL_BatchTransactionData();
                    TxnDetail tansaction = new TxnDetail();
                    tansaction.TxnRefNo = Convert.ToString(dr["TxnRefNo"]);
                    tansaction.DrAcct = senderAccountNo; //hardcode
                    tansaction.SndrNm = senderAccountName; //hardcode
                    tansaction.TxnAmt = Convert.ToString(dr["TxnAmt"]);
                    tansaction.TxnType = Convert.ToString(dr["TxnType"]);
                    tansaction.BenefIFSC = Convert.ToString(dr["BenefIFSC"]);
                    tansaction.BenefAcNo = Convert.ToString(dr["BenefAcNo"]);
                    tansaction.BenefAcNm = Convert.ToString(dr["BenefAcNm"]);
                    tansaction.Nrtv = Convert.ToString(dr["Nrtv"]);
                    if(string.IsNullOrEmpty(BatchRequestIDPrc))
                    {
                        BatchRequestIDPrc  = Convert.ToString(dr["BatchRequestId"]);
                    }
                    totalAmt = Convert.ToDouble(tansaction.TxnAmt) + totalAmt;
                    objTransactionRSList.Add(tansaction);
                }
                //txnDetailList.Txn = objTransactionRSList;


                objTrakckingRemarkRS = new CanraRequest
                {
                    Request = new CanraRequestBodyReq
                    {
                        Body = new CanraRequestBody
                        {
                            encryptData = new CanraReqEncryptData
                            {
                                Authorization = "Basic " + authorization,
                                Key = key,
                                CustomerID = customerID,
                                TotAmt = Convert.ToString(totalAmt),
                                TxnCnt = Convert.ToString(ds.Tables[0].Rows.Count),
                                DatTxn = "20240205", //to automate Convert.ToString(ds.Tables[0].Rows[0]["DatTxn"]),
                                BatchRequestID = BatchRequestIDPrc,//batchRequestId,//BATCH in the sheet 
                                TxnDtls = new TxnDetailList
                                {
                                    Txn = objTransactionRSList
                                }
                            }
                        }
                    }
                };
            }

        }
        catch (Exception Ex)
        {
            // Log the exception details or handle it appropriately
            // objTrakckingRemarkRS.SetFailure("Getting error while fetching time details");
            return null;
        }

        return objTrakckingRemarkRS;
    }

    public Task<CanraRequest> GetBatchTrxnDetails1(string batchRequestId)
    {
        string senderAccountNo = ConfigurationManager.AppSettings["CanaraSenderAccountNo"];
        string senderAccountName = ConfigurationManager.AppSettings["CanaraSenderAccountName"];
        string customerID = ConfigurationManager.AppSettings["CanaraCustomerID"];
        string authorization = ConfigurationManager.AppSettings["CanaraAuthorization"];
        string key = "3iins2570n1pu4p52qie9c16dr";


        CanraRequest objTrakckingRemarkRS = new CanraRequest();
        List<TxnDetail> objTransactionRSList = new List<TxnDetail>();
        try
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@BatchRequestId", batchRequestId);
            //DataSet ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetTrackingRemarkActivation_WithTrackRemark", param);
            DataSet ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetBatchTrxnDetails", param);

            double totalAmt = 0;
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    //OL_BatchTransactionData tansaction = new OL_BatchTransactionData();
                    TxnDetail tansaction = new TxnDetail();
                    tansaction.TxnRefNo = Convert.ToString(dr["TxnRefNo"]);
                    tansaction.DrAcct = senderAccountNo; //hardcode
                    tansaction.SndrNm = senderAccountName; //hardcode
                    tansaction.TxnAmt = Convert.ToString(dr["TxnAmt"]);
                    tansaction.TxnType = Convert.ToString(dr["TxnType"]);
                    tansaction.BenefIFSC = Convert.ToString(dr["BenefIFSC"]);
                    tansaction.BenefAcNo = Convert.ToString(dr["BenefAcNo"]);
                    tansaction.BenefAcNm = Convert.ToString(dr["BenefAcNm"]);
                    tansaction.Nrtv = Convert.ToString(dr["Nrtv"]);

                    totalAmt = Convert.ToDouble(tansaction.TxnAmt) + totalAmt;
                    objTransactionRSList.Add(tansaction);
                }
                //txnDetailList.Txn = objTransactionRSList;


                objTrakckingRemarkRS = new CanraRequest
                {
                    Request = new CanraRequestBodyReq
                    {
                        Body = new CanraRequestBody
                        {
                            encryptData = new CanraReqEncryptData
                            {
                                Authorization = "Basic " + authorization,
                                Key = key,
                                CustomerID = customerID,
                                TotAmt = Convert.ToString(totalAmt),
                                TxnCnt = Convert.ToString(ds.Tables[0].Rows.Count),
                                DatTxn = "20231127", //to automate Convert.ToString(ds.Tables[0].Rows[0]["DatTxn"]),
                                BatchRequestID = "CARZ241120230448",//BATCH in the sheet 
                                TxnDtls = new TxnDetailList
                                {
                                    Txn = objTransactionRSList
                                }
                            }
                        }
                    }
                };
            }

        }
        catch (Exception Ex)
        {
            //objTrakckingRemarkRS.SetFailure("Getting error while fetching time details");
            return null;
        }
        return Task.FromResult(objTrakckingRemarkRS);
        //return objTrakckingRemarkRS;
    }
    public int SaveCanaraBatchInitiateResponseAsync(CanraSuccessResponseBody reqBatch)
    {

        SqlParameter[] param = new SqlParameter[0];
        //param[0] = new SqlParameter("@BookingId", order.bookingId);
        //param[1] = new SqlParameter("@ApprovalNo", order.approvalNo);
        //param[2] = new SqlParameter("@Amount", amount);
        var res = SqlHelper.ExecuteNonQuery("Prc_SaveCanaraBatchInitiateResponse", param);
        return res;
    }

    public CanraSignatureBodyRequest GetBatchInquiryRequest(string batchRequestId)
    {
        string senderAccountNo = ConfigurationManager.AppSettings["CanaraSenderAccountNo"];
        string senderAccountName = ConfigurationManager.AppSettings["CanaraSenderAccountName"];
        string customerID = ConfigurationManager.AppSettings["CanaraCustomerID"];
        string authorization = ConfigurationManager.AppSettings["CanaraAuthorization"];
        string key = "3iins2570n1pu4p52qie9c16dr";

        CanraSignatureBodyRequest objBatchInquiry = new CanraSignatureBodyRequest();

        try
        {

            objBatchInquiry = new CanraSignatureBodyRequest
            {
                Request = new CanraSignatureBodyModel
                {
                    Body = new CanraSignatureModel
                    {
                        encryptData = new SigData
                        {
                            Authorization = "Basic " + authorization,
                            Key = key,
                            CustomerID = customerID,
                            BatchRequestID = batchRequestId,
                            TxnRefNo = ""

                        }
                    }
                }
            };

        }
        catch (Exception Ex)
        {
            // Log the exception details or handle it appropriately
            // objTrakckingRemarkRS.SetFailure("Getting error while fetching time details");
            return null;
        }

        return objBatchInquiry;
    }

    public int SaveCanaraBatchInquiryResponse(CanaraBatchInquiryRoot reqBatch, String decryptedRes)
    {
        // Retrieve connection string from configuration
        string connectionString = ConfigurationManager.AppSettings["ConnectionString"];

        int res = 0;

        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            SqlTransaction transaction = null;

            try
            {
                connection.Open();

                // Begin a new transaction
                transaction = connection.BeginTransaction();

                DataTable dtDataTxn = new DataTable();
                dtDataTxn = ConvertToDataTable(reqBatch.Response.Body.TxnDtls.Txn, columnDataTypes, reqBatch.Response.Body.BatchRequestID);

                using (SqlCommand command = new SqlCommand("Prc_UpdateCanaraBatchTxnInquiryResponse", connection, transaction))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    // Add other parameters
                    command.Parameters.AddWithValue("@BatchRequestID", reqBatch.Response.Body.BatchRequestID.ToString());
                    command.Parameters.AddWithValue("@TotNeftAmt", Convert.ToDouble(reqBatch.Response.Body.TotNeftAmt));
                    command.Parameters.AddWithValue("@TxnNeftCnt", Convert.ToInt32(reqBatch.Response.Body.TxnNeftCnt));
                    command.Parameters.AddWithValue("@TotRtgsAmt", Convert.ToDouble(reqBatch.Response.Body.TotRtgsAmt));
                    command.Parameters.AddWithValue("@TxnRtgsCnt", Convert.ToInt32(reqBatch.Response.Body.TxnRtgsCnt));
                    command.Parameters.AddWithValue("@TotImpsAmt", Convert.ToDouble(reqBatch.Response.Body.TotImpsAmt));
                    command.Parameters.AddWithValue("@TxnImpsCnt", Convert.ToInt32(reqBatch.Response.Body.TxnImpsCnt));
                    command.Parameters.AddWithValue("@TotIntraAmt", Convert.ToDouble(reqBatch.Response.Body.TotIntraAmt));
                    command.Parameters.AddWithValue("@TxnIntraCnt", Convert.ToInt32(reqBatch.Response.Body.TxnIntraCnt));

                    // Add table-valued paramete
                    SqlParameter tvpParam = command.Parameters.AddWithValue("@tvp_TxnDetailsUpdate", dtDataTxn);
                    tvpParam.SqlDbType = SqlDbType.Structured;
                    tvpParam.TypeName = "dbo.tvp_TxnDetailsUpdateType"; // Replace with your TVP type name

                    res = command.ExecuteNonQuery();


                    // Commit the transaction if everything is successful
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LoginfoToLogFile("DB Exception=" + ex.Message + ":: Decrpted Response JSON : " + decryptedRes, ":: Method Name : SaveCanaraBatchInquiryResponse");

                // Rollback the transaction if an exception occurs
                if (transaction != null)
                {
                    transaction.Rollback();
                }

                // Handle the exception (log, throw, etc.)
                // Console.WriteLine($"An error occurred: {ex.Message}");
            }
            finally
            {
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }
            }
        }

        return res;
    }

    public int SaveCanaraBatchInquiryResponse1(CanaraBatchInquiryRoot reqBatch, string decryptedRes)
    {
        // Retrieve connection string from configuration
        string connectionString = ConfigurationManager.AppSettings["ConnectionString"];

        int res = 0;
        using (SqlConnection connection = new SqlConnection(connectionString))
        {

            SqlTransaction transaction = null;
            try
            {
                DataTable dtDataTxn = new DataTable();
                dtDataTxn = ConvertToDataTable(reqBatch.Response.Body.TxnDtls.Txn, columnDataTypes, reqBatch.Response.Body.BatchRequestID);

                connection.Open();

                // Begin a new transaction
                transaction = connection.BeginTransaction();

                using (SqlCommand command = new SqlCommand("Prc_UpdateCanaraBatchTxnInquiryResponse", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    // Add other parameters
                    command.Parameters.AddWithValue("@BatchRequestID", reqBatch.Response.Body.BatchRequestID.ToString());
                    command.Parameters.AddWithValue("@TotNeftAmt", Convert.ToDouble(reqBatch.Response.Body.TotNeftAmt));
                    command.Parameters.AddWithValue("@TxnNeftCnt", Convert.ToInt32(reqBatch.Response.Body.TxnNeftCnt));
                    command.Parameters.AddWithValue("@TotRtgsAmt", Convert.ToDouble(reqBatch.Response.Body.TotRtgsAmt));
                    command.Parameters.AddWithValue("@TxnRtgsCnt", Convert.ToInt32(reqBatch.Response.Body.TxnRtgsCnt));
                    command.Parameters.AddWithValue("@TotImpsAmt", Convert.ToDouble(reqBatch.Response.Body.TotImpsAmt));
                    command.Parameters.AddWithValue("@TxnImpsCnt", Convert.ToInt32(reqBatch.Response.Body.TxnImpsCnt));
                    command.Parameters.AddWithValue("@TotIntraAmt", Convert.ToDouble(reqBatch.Response.Body.TotIntraAmt));
                    command.Parameters.AddWithValue("@TxnIntraCnt", Convert.ToInt32(reqBatch.Response.Body.TxnIntraCnt));

                    // Add table-valued parameter
                    SqlParameter tvpParam = command.Parameters.AddWithValue("@tvp_TxnDetailsUpdate", dtDataTxn);
                    tvpParam.SqlDbType = SqlDbType.Structured;
                    tvpParam.TypeName = "dbo.tvp_TxnDetailsUpdateType"; // Replace with your TVP type name

                    res = command.ExecuteNonQuery();

                    // Commit the transaction if everything is successful
                    transaction.Commit();

                }
            }
            catch (Exception ex)
            {
                ErrorLog.LoginfoToLogFile("DB Exception=" + ex.Message + ":: Decrpted Response JSON : " + decryptedRes, ":: Method Name : SaveCanaraBatchInquiryResponse");

                // Rollback the transaction if an exception occurs
                if (transaction != null)
                {
                    transaction.Rollback();
                }
            }
            finally
            {
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }
            }
        }
        return res;
    }
    public static DataTable ConvertToDataTable<T>(IEnumerable<T> list, Dictionary<string, Type> columnDataTypes,string batchRequestId)
    {
        DataTable dataTable = new DataTable(typeof(T).Name);

        // Get properties of the object type that are included in the columnDataTypes dictionary
        PropertyInfo[] props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(p => columnDataTypes.ContainsKey(p.Name))
            .ToArray();

        // Create DataTable columns using selected object properties and specified data types
        foreach (PropertyInfo prop in props)
        {
            dataTable.Columns.Add(prop.Name, columnDataTypes[prop.Name]);
        }

        // Populate DataTable rows with object data
        foreach (T item in list)
        {
            DataRow row = dataTable.NewRow();
            foreach (PropertyInfo prop in props)
            {
                object value = prop.GetValue(item);
                if (prop.Name == "BatchRequestID" || prop.Name == "BatchRequestID")
                {
                    row[prop.Name] = batchRequestId;
                }
                // Handle null values
                if (value == null)
                {
                    row[prop.Name] = DBNull.Value;
                    //if (prop.Name == "UTRRrnNumber" || prop.Name == "UtrRrnNumber")
                    //{
                    //    row[prop.Name] = "P349230238409547";
                    //}
                    //else
                    //{
                    //    row[prop.Name] = DBNull.Value;
                    //}

                }
                else
                {
                    // Handle empty string values
                    if (value is string && string.IsNullOrEmpty((string)value))
                    {
                        row[prop.Name] = DBNull.Value; // or set to some default value if needed
                    }
                    else
                    {
                        row[prop.Name] = value.ToString();
                    }
                }
            }
            dataTable.Rows.Add(row);
        }

        return dataTable;
    }

}