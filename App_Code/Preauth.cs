﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CCValidationData
/// </summary>
public class Preauth
{
    public Preauth()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public string OrderID { get; set; }
    public string PageID { get; set; }

    public string SendSMSLater { get; set; }
    public string ClientCoID { get; set; }
    public string AgentEmailId { get; set; }
    public string InvoiceNo { get; set; }
    public string RentalDate { get; set; }
    public string RentalType { get; set; }
    public string PickupCity { get; set; }
    public string CustomerName { get; set; }
    public string amount { get; set; }
    public string SMSMobiles { get; set; }
    public string SMSMobiles1 { get; set; }
    public string bitIsVIP { get; set; }
    public string hdnAcceleration_No { get; set; }
    public string SendMailLater { get; set; }

    public string BookingID { get; set; }

    public string UnitName { get; set; }

    public string CityName { get; set; }
    public string PostURL { get; set; }
    public string PreAuthStatus { get; set; }
    public string FrmIdentifyCode { get; set; }

    public string LimoStatus { get; set; }
}