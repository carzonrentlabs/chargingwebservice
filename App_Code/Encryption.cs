﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for Encryption
/// </summary>
public class Encryption
{
    public Encryption()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #region SFA Security

    private static string _ParamSeperator = "::";
    private static string _KeyValueSeperator = "~~";

    /// <summary>
    /// Hashing of RequestArray using specified HashKey and then encrypting using enckey
    /// </summary>
    /// <param name="HashKey">Key used for hashing</param>
    /// <param name="EncKey">Key used for encryption</param>
    /// <param name="RequestArray">array of parameters</param>
    /// <returns>EncryptedData</returns>
    public static string WrapRequestData(string HashKey, string EncKey, ref string[,] RequestArray)
    {
        string QueryStringRawData = "";
        string rawHashData = HashKey;
        string strResult = "";
        int index = 0;
        string md5HashData = null;

        //Sort The Array First
        RequestArray = SortArray(RequestArray);
        // loop though the array and add each parameter value to the MD5 input
        index = 0;

        for (index = 0; index < RequestArray.GetLength(0); index++)
        {
            if (RequestArray[index, 1] != null)
            {
                if (Convert.ToString(RequestArray[index, 1]).Length > 0)
                {
                    //CHANGED
                    //QueryStringRawData += HttpUtility.UrlEncode(Convert.ToString(RequestArray[index, 0])) + _KeyValueSeperator + HttpUtility.UrlEncode(Convert.ToString(RequestArray[index, 1])) + _ParamSeperator;
                    QueryStringRawData += Convert.ToString(RequestArray[index, 0]) + _KeyValueSeperator + Convert.ToString(RequestArray[index, 1]) + _ParamSeperator;
                    rawHashData += Convert.ToString(RequestArray[index, 1]);
                }
            }
        }

        //Creating SecureHash Data
        md5HashData = CreateSHA256Signature(rawHashData);
        QueryStringRawData += "SecureHash" + _KeyValueSeperator + md5HashData;

        strResult = SFAEncrypt(EncKey, QueryStringRawData);
        return strResult;
    }

    /// <summary>
    ///  Check if Hash received in request is valid and return key/value pair of response
    /// </summary>
    /// <param name="HashKey">Key used for hashing</param>
    /// <param name="HashKey">Key used for encryption</param>
    /// <param name="EncryptedResponseData">Encrypted data</param>
    /// <param name="nvResponse">collection of parameters after decryption</param>
    /// <returns>true/false</returns>
    public static bool VerifySecureHash(string HashKey, string EncKey, string EncryptedResponseData, out NameValueCollection nvResponse)
    {
        string[,] TempArray;
        nvResponse = new NameValueCollection();
        if (VerifySecureHash(HashKey, EncKey, EncryptedResponseData, out TempArray))
        {
            //Add In namevaluecollection               
            for (int i = 0; i < TempArray.GetLength(0); i++)
            {
                nvResponse.Add(Convert.ToString(TempArray[i, 0]), Convert.ToString(TempArray[i, 1]));
            }
            return true;
        }
        else
            return false;
    }

    /// <summary>
    ///  Check if Hash received in request is valid and return array of params
    /// </summary>
    /// <param name="HashKey">Key used for hashing</param>
    /// <param name="HashKey">Key used for encryption</param>
    /// <param name="EncryptedResponseData">Encrypted data</param>
    /// <param name="ResponseArray">array of parameters after decryption</param>
    /// <returns>true/false</returns>
    private static bool VerifySecureHash(string HashKey, string EncKey, string EncryptedResponseData, out string[,] ResponseArray)
    {
        bool ReqSecureHashMatchCheck = true;
        string[] ColonSplit = Regex.Split(SFADecrypt(EncKey, EncryptedResponseData), _ParamSeperator);

        ResponseArray = new string[ColonSplit.Length - 1, 2]; //-1 to exclude SecureHash

        try
        {
            string MerReqSecureHash = string.Empty;

            for (int i = 0; i < ColonSplit.Length; i++)
            {
                if (Convert.ToString(ColonSplit[i]).Contains("SecureHash"))
                {
                    MerReqSecureHash = Convert.ToString(Regex.Split(ColonSplit[i], Regex.Escape(_KeyValueSeperator))[1]);
                }
                else
                {
                    ResponseArray[i, 0] = Convert.ToString(Regex.Split(ColonSplit[i], Regex.Escape(_KeyValueSeperator))[0]);
                    //CHANGED
                    //ResponseArray[i, 1] = HttpUtility.UrlDecode(Convert.ToString(Regex.Split(ColonSplit[i], Regex.Escape(_KeyValueSeperator))[1]));
                    ResponseArray[i, 1] = Convert.ToString(Regex.Split(ColonSplit[i], Regex.Escape(_KeyValueSeperator))[1]);

                }
            }

            ReqSecureHashMatchCheck = CompareHash(HashKey, ResponseArray, MerReqSecureHash);
        }
        catch (Exception ex)
        {
            ReqSecureHashMatchCheck = false;
        }

        return ReqSecureHashMatchCheck;
    }

    #region Create SHA 256 Signature
    private static string CreateSHA256Signature(string RawData)
    {
        System.Security.Cryptography.SHA256 hasher = System.Security.Cryptography.SHA256Managed.Create();
        byte[] HashValue = hasher.ComputeHash(Encoding.ASCII.GetBytes(RawData));

        string strHex = "";
        foreach (byte b in HashValue)
        {
            strHex += b.ToString("x2");
        }
        return strHex.ToUpper();
    }
    #endregion

    /// <summary>
    /// Comapare parameters integrity using HashKey
    /// </summary>
    /// <param name="HashKey">Key used for hashing</param>
    /// <param name="ParamArray">array of parameters</param>
    /// <param name="UserSecureHash">User generated hash</param>
    /// <returns>true/false</returns>
    public static bool CompareHash(string HashKey, string[,] ParamArray, string UserSecureHash)
    {
        bool IsHashMatched = false;
        string MerReqRawData = HashKey;
        try
        {
            //Sort The Array First
            ParamArray = SortArray(ParamArray);
            //Create Customer RawHashData
            for (int j = 0; j < ParamArray.GetLength(0); j++)
            {
                if (!Convert.ToString(ParamArray[j, 0]).Contains("SecureHash"))
                {
                    MerReqRawData += Convert.ToString(ParamArray[j, 1]);
                }
            }

            string CalculatedMD5HashData = string.Empty;
            //Create SecureHash With Customer RawHashData
            CalculatedMD5HashData = CreateSHA256Signature(MerReqRawData);

            //If SecureHash Matches As in the Response 
            if (CalculatedMD5HashData.ToUpper().Equals(UserSecureHash.ToUpper()))
                IsHashMatched = true;
            else
                IsHashMatched = false;
        }
        catch (Exception ex)
        {
            IsHashMatched = false;
        }

        return IsHashMatched;
    }

    #region SAFEncrypt
    private static string SFAEncrypt(string key, string TextToBeEncrypted)
    {
        string EncryptedData = string.Empty;
        try
        {
            RijndaelManaged RijndaelCipher = new RijndaelManaged();
            string EncryptionKey = key;
            byte[] PlainText = System.Text.Encoding.Unicode.GetBytes(TextToBeEncrypted);
            byte[] Salt = Encoding.ASCII.GetBytes(EncryptionKey.Length.ToString());
            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(EncryptionKey, Salt);
            //Creates a symmetric encryptor object. 
            ICryptoTransform Encryptor = RijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));
            MemoryStream memoryStream = new MemoryStream();
            //Defines a stream that links data streams to cryptographic transformations
            CryptoStream cryptoStream = new CryptoStream(memoryStream, Encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(PlainText, 0, PlainText.Length);
            //Writes the final state and clears the buffer
            cryptoStream.FlushFinalBlock();
            byte[] CipherBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            EncryptedData = Convert.ToBase64String(CipherBytes);
        }
        catch (Exception ex) { EncryptedData = string.Empty; }
        return EncryptedData;
    }
    #endregion

    #region SFADecrypt
    private static string SFADecrypt(string key, string TextToBeDecrypted)
    {
        RijndaelManaged RijndaelCipher = new RijndaelManaged();
        string DecryptionKey = key;
        string DecryptedData;
        try
        {
            byte[] EncryptedData = Convert.FromBase64String(TextToBeDecrypted);
            byte[] Salt = Encoding.ASCII.GetBytes(DecryptionKey.Length.ToString());
            //Making of the key for decryption
            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(DecryptionKey, Salt);
            //Creates a symmetric Rijndael decryptor object.
            ICryptoTransform Decryptor = RijndaelCipher.CreateDecryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));
            MemoryStream memoryStream = new MemoryStream(EncryptedData);
            //Defines the cryptographics stream for decryption.THe stream contains decrpted data
            CryptoStream cryptoStream = new CryptoStream(memoryStream, Decryptor, CryptoStreamMode.Read);

            byte[] PlainText = new byte[EncryptedData.Length];
            int DecryptedCount = cryptoStream.Read(PlainText, 0, PlainText.Length);
            memoryStream.Close();
            cryptoStream.Close();

            //Converting to string
            DecryptedData = Encoding.Unicode.GetString(PlainText, 0, DecryptedCount);
        }
        catch (Exception ex)
        {
            DecryptedData = string.Empty;
        }
        return DecryptedData;
    }
    #endregion

    #region Sorting Request Array
    /// <summary>
    /// Sort 2 - Dimwnsional string array
    /// </summary>
    /// <param name="MyArray">2-Dimensional string array</param>
    /// <returns>Sorted 2-Dimensional array</returns>
    public static string[,] SortArray(string[,] MyArray)
    {
        bool keepChecking = false;
        int loopCounter = 0;
        string firstKey = null;
        string secondKey = null;
        string firstValue = null;
        string secondValue = null;

        keepChecking = true;
        loopCounter = 0;

        while (!(keepChecking == false))
        {
            keepChecking = false;
            for (loopCounter = 0; loopCounter < (MyArray.GetLength(0) - 1); loopCounter++)
            {
                if (StringCompare(MyArray[loopCounter, 0], MyArray[loopCounter + 1, 0]))
                {
                    // transpose the key
                    firstKey = MyArray[loopCounter, 0];
                    secondKey = MyArray[(loopCounter + 1), 0];
                    MyArray[loopCounter, 0] = secondKey;
                    MyArray[(loopCounter + 1), 0] = firstKey;
                    // transpose the key's value
                    firstValue = MyArray[loopCounter, 1];
                    secondValue = MyArray[(loopCounter + 1), 1];
                    MyArray[loopCounter, 1] = secondValue;
                    MyArray[(loopCounter + 1), 1] = firstValue;
                    keepChecking = true;
                }
            }
        }
        return MyArray;
    }
    #endregion

    #region String Comparision
    private static Boolean StringCompare(string Value1, string Value2)
    {
        int counter;

        if (Value1 != null)
        {
            if (Value2 != null)
            {
                if (Value1.Length < Value2.Length)
                    counter = Value1.Length;
                else
                    counter = Value2.Length;

                for (int i = 0; i < counter - 1; i++)
                {
                    if (Value1[i] != Value2[i])
                    {
                        if (Value1[i] > Value2[i])
                            return true;
                        else
                            return false;
                        break;
                    }
                }
            }
            else
                return true;
        }
        return false;
    }
    #endregion

    #region Check Null Data Returned
    private string null2unknown(string data)
    {
        if (data == null || data.Length == 0)
            return "No Value Returned";
        else
            return data;
    }
    #endregion

    #endregion
}