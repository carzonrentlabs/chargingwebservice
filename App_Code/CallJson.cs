﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using System.Configuration;

/// <summary>
/// Summary description for CallJson
/// </summary>
public class CallJson
{
    public CallJson()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static string GetConfigurationValue(string key)
    {
        return ConfigurationManager.AppSettings[key];
    }

    public static string doGet(string fullUrl, Dictionary<string, string> headers = null)
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(fullUrl);
        if (headers != null)
            foreach (string key in headers.Keys)
                request.Headers.Add(key, headers[key]);
        request.Method = "GET";
        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        {
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                string responseFromServer = reader.ReadToEnd();
                return responseFromServer;
            }
        }
    }
    public static string doPost(string fullUrl, string dispositionRequestJSON, Dictionary<string, string> headers = null)
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(fullUrl);
        if (headers != null)
            foreach (string key in headers.Keys)
                request.Headers.Add(key, headers[key]);
        request.Method = "POST";
        using (var streamWriter = new StreamWriter(request.GetRequestStream()))
        {
            streamWriter.Write(dispositionRequestJSON);
            streamWriter.Flush();
        }
        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        {
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                string responseFromServer = reader.ReadToEnd();
                return responseFromServer;
            }
        }
    }
}