﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using ChargingWebService;
using System.Web.Script.Serialization;
using System.Web.Script.Services;

/// <summary>
/// Summary description for ServiceRest
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class ServiceRest : System.Web.Services.WebService
{

    public ServiceRest()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    //[WebMethod]
    //public string HelloWorld()
    //{
    //    return "Hello World";
    //}

    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public string RegisteredGuestPreauth(int BookingID, string AdditionalEmaiId, string AdditionalMobileNo, bool NotifyGuestYN)
    {
        ErrorLog.LoginfoToLogFile("start RegisteredGuestPreauth for BookingID : " + BookingID.ToString(), ""); //log file

        clsAdmin objAdmin = new clsAdmin();

        string jsonResponse = objAdmin.ClientIndivRegistrationModule(BookingID, AdditionalEmaiId, AdditionalMobileNo, NotifyGuestYN);

        ErrorLog.LoginfoToLogFile("End RegisteredGuestPreauth for BookingID : " + BookingID.ToString(), ""); //log file
        return sendJSONResponse(jsonResponse);
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public string ConfirmClosingREST(int BookingId
        , bool NotifyGuestYN, string Auth)
    {
        ErrorLog.LoginfoToLogFile("start ConfirmClosingREST for BookingID : " 
            + BookingId.ToString(), ""); //log file
        string ChargingStatus = "";
        int UserID = 1;
        string jsonResponse = "{ \"IsSuccess\": true}";
        try
        {
            ErrorLog.LoginfoToLogFile("start:BookingId:" + BookingId.ToString()
                + ",NotifyGuestYN:" + NotifyGuestYN.ToString(), "ConfirmClosing");

            clsAdmin objAdmin = new clsAdmin();
            ChargingStatus = objAdmin.CloseBooking(BookingId, "Confirm", NotifyGuestYN, UserID);

            if (ChargingStatus != "Success")
            {
                jsonResponse = "{ \"IsSuccess\": false, \"FailureMessage\":\"" + ChargingStatus + "\"}";
            }
            ErrorLog.LoginfoToLogFile("end:BookingId:" + BookingId.ToString()
                + ",NotifyGuestYN:" + NotifyGuestYN.ToString() + ",ChargingStatus:" + ChargingStatus, "ConfirmClosing");
        }
        catch (Exception Ex)
        {
            ChargingStatus = Ex.ToString();
            jsonResponse = "{ \"IsSuccess\": false, \"FailureMessage\":\"" + ChargingStatus + "\"}";
            ErrorLog.LoginfoToLogFile("BookingId:" + BookingId.ToString()
                + ",NotifyGuestYN:" + NotifyGuestYN.ToString() + ",UserID" + UserID.ToString() + ",ChargingStatus:" + ChargingStatus, "ConfirmClosing");
        }

        ErrorLog.LoginfoToLogFile("End ConfirmClosingREST for BookingID : " + BookingId.ToString(), ""); //log file
        return sendJSONResponse(jsonResponse);
    }

    private String sendJSONResponse(String json)
    {
        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.Output.Write(json);
        // Context.Response.End();
        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline**
        return string.Empty;
    }
}