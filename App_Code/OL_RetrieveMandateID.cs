﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for OL_RetrieveMandateID
/// </summary>
public class OL_RetrieveMandateID
{
    public string mercid { get; set; }
    public string customer_refid { get; set; }
    public string subscription_refid { get; set; }
    
}
