using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using context = System.Web.HttpContext;

/// <summary>
/// Summary description for ErrorLog
/// </summary>
public class ErrorLog
{
    public ErrorLog()
    {

    }
    public static void LogErrorToLogFile(Exception ee, string ModuleName)
    {
        try
        {
            string path = context.Current.Server.MapPath("~/ErrorLogging/");
            // check if directory exists
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            path = path + DateTime.Today.ToString("dd-MMM-yy") + ".log";
            // check if file exist
            if (!File.Exists(path))
            {
                File.Create(path).Dispose();
            }
            // log the error now
            using (StreamWriter writer = File.AppendText(path))
            {
                string error = ModuleName + "@" + DateTime.Now.ToString() + ";Error:" + ee.ToString();
                writer.WriteLine(error);
                writer.Flush();
                writer.Close();
            }
            //return userFriendlyError;
        }
        catch (Exception ex)
        {
            //throw;
            //return ex.ToString();
        }
    }

    public static void LoginfoToLogFile(string details, string ModuleName)
    {
        try
        {
            string path = context.Current.Server.MapPath("~/InfoLogging/");
            // check if directory exists
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            path = path + DateTime.Today.ToString("dd-MMM-yy") + ".log";
            // check if file exist
            if (!File.Exists(path))
            {
                File.Create(path).Dispose();
            }
            // log the error now
            using (StreamWriter writer = File.AppendText(path))
            {
                string error = ModuleName + "@" + DateTime.Now.ToString() + ";info:" + details;
                writer.WriteLine(error);
                writer.Flush();
                writer.Close();
            }
            //return ModuleName;
        }
        catch (Exception ex)
        {
            //throw;
            //return ex.ToString();
        }
    }
}