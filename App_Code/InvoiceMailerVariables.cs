﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CCValidationData
/// </summary>
public class InvoiceMailerVariables
{
    public InvoiceMailerVariables()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public string Service { get; set; }
    public bool CorDriveYN { get; set; }

    public string Status { get; set; }
    public int ClientCoID { get; set; }
    public int CCType { get; set; }
    public string PaymentMode { get; set; }
    public double TotalCost { get; set; }
    public string hdncontinueYN { get; set; }
    public bool AmexLinkYN { get; set; }
    public bool ChargingNRequired { get; set; }
    public string DSStatus { get; set; }
    public string BookingStatus { get; set; }
    public Int64 ClientCoIndivID { get; set; }
    public string ServiceType { get; set; }
    public string ErrorCode { get; set; }

    public bool PreAuthNotRequire { get; set; }
    public bool AmexSecondaryChargingYN { get; set; }
    public bool isPaymateCorporateModule { get; set; }
    public Boolean PreAuthYN { get; set; }
    public string PaymateSysRegCode { get; set; }
    public bool PDFYN { get; set; }
    public int ChargingLink { get; set; }
    public bool ProvisionalInvoiceForGuestYN { get; set; }
    public bool ProvisionalInvoiceForFacilitatorYN { get; set; }
    public bool ProvisionalInvoiceForTravelDeskYN { get; set; }
    public bool CCRegisteredYN { get; set; }
}