﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for RS_WebServiceMetaData
/// </summary>
public class RS_WebServiceMetaData
{
    public bool IsSuccess { get; set; }
    public string FailureMessage { get; set; }
    public int HttpStatus { get; set; }
    public bool IsSessionInvalid { get; set; }

    public void SetSuccess()
    {
        this.IsSuccess = true;
        this.HttpStatus = 200;
    }
    public void SetFailure(string FailureMessage)
    {
        this.IsSuccess = false;
        this.FailureMessage = FailureMessage;
        this.HttpStatus = 500;
    }
    public void SetFailure_BadRequest(string FailureMessage)
    {
        this.IsSuccess = false;
        this.FailureMessage = FailureMessage;
        this.HttpStatus = 400;
    }
    public void SetFailure(string FailureMessage, int HttpStatus)
    {
        this.IsSuccess = false;
        this.FailureMessage = FailureMessage;
        this.HttpStatus = HttpStatus;
    }
}