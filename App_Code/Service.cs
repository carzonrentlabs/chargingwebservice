using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using System.Data.Sql;
using ChargingWebService;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;
using ChecksumsTest;
using Jose;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.ServiceModel.Activation;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]

public class AuthHeader : System.Web.Services.Protocols.SoapHeader
{
    public string UserName;
    public string Password;
}
public class Service : System.Web.Services.WebService
{
    DataSet BK = new DataSet();
    DataSet DTClientBookDetails = new DataSet();
    DataSet SD = new DataSet();
    DataSet data = new DataSet();

    clsAdmin objAdmin = new clsAdmin();
    Adler32 adl = new Adler32();
    public AuthHeader Credentials;

    public Service()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
    string Password = System.Configuration.ConfigurationManager.AppSettings["CorPassWord"];
    string userName = System.Configuration.ConfigurationManager.AppSettings["Username"];

    //Master visa code below
    //[SoapHeader("Credentials")]
    [WebMethod(Description = "Used Internally")]
    public string GetCaptureStatusMaster(int BookingID, double Amount)
    {
        return objAdmin.GetCaptureStatusMaster(BookingID, Amount);
    }

    //Amex code below

    //[SoapHeader("Credentials")]
    [WebMethod(Description = "Used Internally")]
    public string GetCaptureStatusAmex(int BookingID, double Amount)
    {
        return objAdmin.GetCaptureStatusAmex(BookingID, Amount);
    }

    //[SoapHeader("Credentials")]
    [WebMethod(Description = "72 hour Amex Schedular (Pepsico, Microsoft etc)")]
    public void AutoCloseAmexDuties()
    {
        //if (userName == "Charge" && Password == "Charge")
        //{
        ErrorLog.LoginfoToLogFile("start", "AutoCloseAmexDuties");

        string ErrorLog1 = objAdmin.GetAmex72HoursAutoClosingDetails();

        ErrorLog.LoginfoToLogFile("End" + ";" + ErrorLog1, "AutoCloseAmexDuties");
        //}
    }

    //[SoapHeader("Credentials")]
    [WebMethod(Description = "72 hour for All Credit Card Bookings")]
    public void AutoCloseCreditCardDuties()
    {
        //ErrorLog.LoginfoToLogFile("Start method AutoCloseAmexDuties()", "");
        //if (userName == "Charge" && Password == "Charge")
        //{
        ErrorLog.LoginfoToLogFile("start", "AutoCloseCreditCardDuties");

        string ErrorLog1 = objAdmin.GetCreditCard72HoursAutoClosingDetails();

        ErrorLog.LoginfoToLogFile("End" + ";" + ErrorLog1, "AutoCloseCreditCardDuties");
        //}
    }

    //[SoapHeader("Credentials")]
    [WebMethod(Description = "Used Internally")]
    public string AmexCharging(int BookingId)
    {
        return objAdmin.GetAmexClosingDetails(BookingId);
    }

    //[SoapHeader("Credentials")]
    [WebMethod(Description = "Used Internally")]
    public string MasterVisaCharging(int BookingId)
    {
        return objAdmin.GetMasterVisaChargingDetails(BookingId);
    }

    //[SoapHeader("Credentials")]
    [WebMethod(Description = "Used Internally")]
    public string MasterVisaTrackOrderID(int BookingId)
    {
        return objAdmin.MasterVisaTrackOrderID(BookingId);
    }



    //[SoapHeader("Credentials")]
    [WebMethod(Description = "Used Internally")]
    public string EditClosing(int BookingId, DateTime DateIn, string TimeIn,
        Int32 GarageKmOut, Int32 GarageKmIn, Int32 GuestKmOut, Int32 GuestKmIn
        , DateTime GuestDateOut, DateTime GuestDateIn, string GuestTimeOut
        , string GuestTimeIn, double Parking, double Interstate, double others
        , string Remarks, bool NotifyGuestYN, int UserID)
    {
        //if (userName == "Charge" && Password == "Charge")
        //{
        DB db = new DB();
        EditClosingVariables EditVariable = new EditClosingVariables();
        EditVariable.BookingID = BookingId;
        EditVariable.DateIn = DateIn;
        EditVariable.TimeIn = db.CorrectTimeLength(TimeIn);
        EditVariable.GarageKmOut = GarageKmOut;
        EditVariable.GarageKmIn = GarageKmIn;
        EditVariable.GuestKmOut = GuestKmOut;
        EditVariable.GuestKmIn = GuestKmIn;
        EditVariable.GuestDateOut = GuestDateOut;
        EditVariable.GuestDateIn = GuestDateIn;
        EditVariable.GuestTimeOut = db.CorrectTimeLength(GuestTimeOut);
        EditVariable.GuestTimeIn = db.CorrectTimeLength(GuestTimeIn);
        EditVariable.Parking = Parking;
        EditVariable.Interstate = Interstate;
        EditVariable.Remarks = Remarks;
        EditVariable.NotifyGuestYN = NotifyGuestYN;
        EditVariable.UserID = UserID;

        if (EditVariable.ClientCoID == 1527 
            && EditVariable.GuestDateOut < Convert.ToDateTime("2016-09-20"))
        {
            EditVariable.BillingBasis = "gg";
        }
        return objAdmin.ClosingManualBooking(EditVariable);
    }

    //[WebMethod(Description = "Used Internally")]
    //public string EditClosingTesting(int BookingId, DateTime DateIn, string TimeIn,
    //   Int32 GarageKmOut, Int32 GarageKmIn, Int32 GuestKmOut, Int32 GuestKmIn
    //   , DateTime GuestDateOut, DateTime GuestDateIn, string GuestTimeOut
    //   , string GuestTimeIn, double Parking, double Interstate, double others
    //   , string Remarks, bool NotifyGuestYN, int UserID)
    //{
    //    //if (userName == "Charge" && Password == "Charge")
    //    //{
    //    ChargingWebService.DB db = new ChargingWebService.DB();
    //    EditClosingVariables EditVariable = new EditClosingVariables();
    //    EditVariable.BookingID = BookingId;
    //    EditVariable.DateIn = DateIn;
    //    EditVariable.TimeIn = db.CorrectTimeLength(TimeIn);
    //    EditVariable.GarageKmOut = GarageKmOut;
    //    EditVariable.GarageKmIn = GarageKmIn;
    //    EditVariable.GuestKmOut = GuestKmOut;
    //    EditVariable.GuestKmIn = GuestKmIn;
    //    EditVariable.GuestDateOut = GuestDateOut;
    //    EditVariable.GuestDateIn = GuestDateIn;
    //    EditVariable.GuestTimeOut = db.CorrectTimeLength(GuestTimeOut);
    //    EditVariable.GuestTimeIn = db.CorrectTimeLength(GuestTimeIn);
    //    EditVariable.Parking = Parking;
    //    EditVariable.Interstate = Interstate;
    //    EditVariable.Remarks = Remarks;
    //    EditVariable.NotifyGuestYN = NotifyGuestYN;
    //    EditVariable.UserID = UserID;

    //    if (EditVariable.ClientCoID == 1527
    //        && EditVariable.GuestDateOut < Convert.ToDateTime("2016-09-20"))
    //    {
    //        EditVariable.BillingBasis = "gg";
    //    }
    //    return objAdmin.ClosingManualBookingTesting(EditVariable);
    //}

    [WebMethod(Description = "Used Internally")]
    public string ConfirmClosing(int BookingId, bool NotifyGuestYN)
    {
        int UserID = 1;
        string ChargingStatus = "";
        //if (userName == "Charge" && Password == "Charge")
        //{
        //return objAdmin.CloseBooking(BookingId, "Confirm", NotifyGuestYN, 1);
        //}
        //return "Error in Method";
        try
        {
            ErrorLog.LoginfoToLogFile("start:BookingId:" + BookingId.ToString()
                + ",NotifyGuestYN:" + NotifyGuestYN.ToString() + ",UserID" + UserID.ToString(), "ConfirmClosing");

            ChargingStatus = objAdmin.CloseBooking(BookingId, "Confirm", NotifyGuestYN, UserID);

            ErrorLog.LoginfoToLogFile("end:BookingId:" + BookingId.ToString()
                + ",NotifyGuestYN:" + NotifyGuestYN.ToString() + ",UserID" + UserID.ToString() + ",ChargingStatus:" + ChargingStatus, "ConfirmClosing");
        }
        catch (Exception Ex)
        {
            ChargingStatus = Ex.ToString();

            ErrorLog.LoginfoToLogFile("BookingId:" + BookingId.ToString()
                + ",NotifyGuestYN:" + NotifyGuestYN.ToString() + ",UserID" + UserID.ToString() + ",ChargingStatus:" + ChargingStatus, "ConfirmClosing");
        }
        return ChargingStatus;
    }

    //[SoapHeader("Credentials")]
    [WebMethod(Description = "Used Internally")]
    public string ConfirmClosing_User(int BookingId
        , bool NotifyGuestYN, int UserID)
    {
        string ChargingStatus = "";
        try
        {
            ErrorLog.LoginfoToLogFile("start:BookingId:" + BookingId.ToString()
                + ",NotifyGuestYN:" + NotifyGuestYN.ToString() + ",UserID" + UserID.ToString(), "ConfirmClosing_User");

            ChargingStatus = objAdmin.CloseBooking(BookingId, "Confirm", NotifyGuestYN, UserID);

            ErrorLog.LoginfoToLogFile("end:BookingId:" + BookingId.ToString()
                + ",NotifyGuestYN:" + NotifyGuestYN.ToString() + ",UserID" + UserID.ToString() + ",ChargingStatus:" + ChargingStatus, "ConfirmClosing_User");
        }
        catch (Exception Ex)
        {
            ChargingStatus = Ex.ToString();

            ErrorLog.LoginfoToLogFile("BookingId:" + BookingId.ToString()
                + ",NotifyGuestYN:" + NotifyGuestYN.ToString() + ",UserID" + UserID.ToString() + ",ChargingStatus:" + ChargingStatus, "ConfirmClosing_User");
        }
        return ChargingStatus;
        //}
        //return "Error in Method";
    }

    //[SoapHeader("Credentials")]
    [WebMethod(Description = "Register Corporate to save card")]
    public string PaymateCorporateRegistration(int ClientCoId)
    {
        string returnstatus = "";
        try
        {
            returnstatus = objAdmin.PaymateCorporateRegistration(ClientCoId);
        }
        catch (Exception ex)
        {
            returnstatus = "Failure";
        }
        return returnstatus;
    }


    [WebMethod(Description = "Preauth Process for Google Saved Card for Master / Visa")]
    public string PaymateCorporateGooglePreauth(int BookingID, double Amt)
    {
        ErrorLog.LoginfoToLogFile("start:BookingId: " + BookingID.ToString() + ",PreauthAmt:" + Amt.ToString(), "PaymateCorporateGooglePreauth");
        string returnstatus = "";
        try
        {
            returnstatus = objAdmin.PaymateCorporateGooglePreauth(BookingID, Amt);
        }
        catch (Exception ex)
        {
            ErrorLog.LoginfoToLogFile("BookingId: " + BookingID.ToString() + ",PreauthAmt:" + Amt.ToString() + ", exception: " + ex, "PaymateCorporateGooglePreauth");
            returnstatus = ex.ToString();
        }
        ErrorLog.LoginfoToLogFile("end:BookingId: " + BookingID.ToString() + ",PreauthAmt:" + Amt.ToString(), "PaymateCorporateGooglePreauth");
        return returnstatus;
    }

    ////[SoapHeader("Credentials")]
    [WebMethod(Description = "New Registration Charging Process for Master / Visa / Amex")]
    public string NewRegistrationChargingProcess(int BookingId, double TotalCost)
    {
        ErrorLog.LoginfoToLogFile("start:BookingId: " + BookingId.ToString() + ",TotalCost:" + TotalCost.ToString(), "NewRegistrationChargingProcess");
        string returnstatus = "";
        try
        {
            PreAuthResult PVC = new PreAuthResult();
            PVC = objAdmin.NewRegistrationChargingProcess(BookingId, TotalCost);
            if (string.IsNullOrEmpty(PVC.transactionNo))
            {
                returnstatus = PVC.ChargingStatus;
            }
            else
            {
                returnstatus = PVC.ChargingStatus + ";" + PVC.transactionNo;
            }
        }
        catch (Exception ex)
        {
            ErrorLog.LoginfoToLogFile("BookingId: " + BookingId.ToString() + ",TotalCost:" + TotalCost.ToString() + ", exception: " + ex, "NewRegistrationChargingProcess");
            returnstatus = ex.ToString();
        }
        ErrorLog.LoginfoToLogFile("end:BookingId: " + BookingId.ToString() + ",TotalCost:" + TotalCost.ToString(), "NewRegistrationChargingProcess");
        return returnstatus;
    }

    //[SoapHeader("Credentials")]
    [WebMethod(Description = "New Registration Preauth Process for Master / Visa / Amex Only")]
    public string NewRegistrationPreauthProcess(int BookingId)
    {
        PreAuthResult CV = new PreAuthResult();
        string status = "";
        CV = objAdmin.NewRegistrationPreauthProcess(BookingId);
        if (string.IsNullOrEmpty(CV.errorMessage))
        {
            status = CV.ErrorCode;
        }
        else
        {
            status = CV.ErrorCode + ";" + CV.errorMessage;
        }
        return status;
    }

    //[SoapHeader("Credentials")]
    [WebMethod(Description = "New Refund Process")]
    public string RefundProcess(int BookingId, double RefundAmount, double TotalCost)
    {
        string status = "";
        status = objAdmin.RefundProcess(BookingId, RefundAmount, TotalCost);
        return status;
    }

    //[SoapHeader("Credentials")]
    [WebMethod(Description = "Charging Web Service (if return Success then continue else failure and show return message)")]
    public string ChargingWebServiceSavedCard(int BookingID, int UserID, bool NotifyGuestYN, bool BulkBookingChargeYN)
    {
        string status = "";
        status = objAdmin.ChargingWebService(BookingID, UserID, NotifyGuestYN, true, BulkBookingChargeYN);
        return status;
    }


    [WebMethod(Description = "Charging Web Service (if return Success then continue else failure and show return message)")]
    public string ChargingWebService(int BookingID, int UserID, bool NotifyGuestYN)
    {
        string status = "";
        //status = objAdmin.ChargingWebService(BookingID, UserID, NotifyGuestYN, false);
        status = ChargingWebService_New(BookingID, UserID, NotifyGuestYN, true);
        return status;
    }

    [WebMethod(Description = "Charging Web Service (if return Success then continue else failure and show return message)")]
    public string ChargingWebService_New(int BookingID, int UserID, bool NotifyGuestYN, bool ChargeYN)
    {
        string status = "";
        status = objAdmin.ChargingWebService(BookingID, UserID, NotifyGuestYN, ChargeYN);
        return status;
    }

    [WebMethod(Description = "Mail Consolidated Part")]
    public string InvoiceMailer(int BookingID)
    {
        string MailStatus = "";
        try
        {
            MailStatus = objAdmin.SendInvoiceMail(BookingID, false);
        }
        catch (Exception ex)
        {
            MailStatus = ex.ToString();
        }
        return MailStatus;
    }

    [WebMethod(Description = "One Way Closing")]
    public string OneWayClosing(Int32 bookingId, Int32 carCatID, DateTime dateIn, string timeIn, Int32 modelId, string waitingTimeHours, string waitingTimeMinute, double parking = 0, double interStateTax = 0)
    {
        ClosingVariables objClosingOL = new ClosingVariables();
        clsAdmin objAdmin = new clsAdmin();
        objClosingOL.BookingId = bookingId;
        objClosingOL.CarCatId = carCatID;
        //objClosing.PickupCityID = pickupcityId;
        // objClosing.Service = clientServicType;
        // objClosing.ClientCoID = clientCoId;
        //objClosing.DateOut = dateOut;
        objClosingOL.DateIn = dateIn;
        objClosingOL.TimeIn = timeIn;
        objClosingOL.CarModelId = modelId;
        //objClosing.DropOffCityId = dropOffCityId;
        objClosingOL.WaitingHr = waitingTimeHours;
        objClosingOL.WaitingMi = waitingTimeMinute;
        objClosingOL.ParkTollChages = Convert.ToInt32(parking);
        objClosingOL.InterstateTax = Convert.ToInt32(interStateTax);
        objClosingOL = objAdmin.GetOneWayClosing(objClosingOL);

        return objClosingOL.BasicRevenue.ToString() + "||" + objClosingOL.WaitingCharge.ToString();
    }

    [WebMethod(Description = "CheckPaymateRegistrationStatus")]
    public string PaymateCorporateRegistrationStatusCheck(int ClientCoIndivID)
    {
        string RegistrationStatus = "";
        clsAdmin objAdmin = new clsAdmin();
        RegistrationStatus = objAdmin.PaymateCorporateRegistrationStatusCheck(ClientCoIndivID);
        return RegistrationStatus;
    }

    [WebMethod(Description = "Check Bulk Corporate Credit Card Expiry")]
    public void BulkPaymateCorporateCheckCreditCardExpiry()
    {
        clsAdmin objAdmin = new clsAdmin();
        objAdmin.BulkPaymateCorporateCheckCreditCardExpiry();
    }

    [WebMethod(Description = "Check Corporate Credit Card Expiry")]
    public string PaymateCorporateCheckCreditCardExpiry(int ClientCoIndivID)
    {
        string RegistrationStatus = "";
        clsAdmin objAdmin = new clsAdmin();
        RegistrationStatus = objAdmin.PaymateCorporateCheckCreditCardExpiry(ClientCoIndivID);
        return RegistrationStatus;
    }

    [WebMethod(Description = "CheckPaymateRegistrationStatus")]
    public string PaymateCorporateChargingStatusCheck(int BookingID, bool UpdateBookingYN)
    {
        string RegistrationStatus = "";
        clsAdmin objAdmin = new clsAdmin();
        RegistrationStatus = objAdmin.PaymateCorporateChargingStatusCheck(BookingID, UpdateBookingYN);
        return RegistrationStatus;
    }

    [WebMethod(Description = "Master Visa Charging For Accenture")]
    public string AccentureChargingMethod(int BookingID, double TotalCost, string Misc1)
    {
        string ChargingStatus = "";
        clsAdmin objAdmin = new clsAdmin();
        ChargingStatus = objAdmin.checkChargingStatus(BookingID, TotalCost, Misc1);
        return ChargingStatus;
    }

    [WebMethod(Description = "Master Visa Charging For Accenture")]
    public string AccentureCheckChargingMethod(int BookingID, double TotalCost, string Misc1)
    {
        string ChargingStatus = "";
        clsAdmin objAdmin = new clsAdmin();
        ChargingStatus = objAdmin.checkChargingStatusnew(BookingID, TotalCost, Misc1);
        return ChargingStatus;

    }

    [WebMethod]
    public OL_CheckLoginResponse EmpDetails(string EmailID)
    {
        clsAdmin bll = new clsAdmin();
        OL_CheckLoginResponse dt = new OL_CheckLoginResponse();
        dt = bll.EmpDetailsNew(EmailID);
        //dt = bll.EmpDetails(EmailID);
        return dt;
    }

    [WebMethod]
    public OL_CheckBookingAllowed EmpDetailsUpdateBand(string EmailID
        , int ClientCoIndivID, string Band)
    {
        clsAdmin bll = new clsAdmin();
        OL_CheckBookingAllowed dt = new OL_CheckBookingAllowed();
        dt = bll.EmpDetailsUpdateBand(EmailID, ClientCoIndivID, Band);
        return dt;
    }

    [WebMethod(Description = "failed Charging scheduler")]
    public void BulkCharging()
    {
        try
        {
            clsAdmin objAdmin = new clsAdmin();
            objAdmin.BulkCharging();
        }
        catch (Exception ex)
        {
            ErrorLog.LogErrorToLogFile(ex, "BulkCharging");
        }
    }

    [WebMethod(Description = "Get CCAvenue SI Status")]
    public string GetSIStatus(int ClientCoIndivID)
    {
        string returnstring = "";
        try
        {
            CCAvenueAPI objAdmin = new CCAvenueAPI();
            returnstring = objAdmin.GetSIStatus(ClientCoIndivID);
        }
        catch (Exception ex)
        {
            ErrorLog.LogErrorToLogFile(ex, "GetSIStatus");
        }

        return returnstring;
    }

    [WebMethod(Description = "Get HDFC Order Status")]
    public string GetHDFCChargingStatus(string OrderID, int BookingID)
    {
        string returnstatus = "";
        try
        {
            CCAvenueAPI objAdmin = new CCAvenueAPI();
            returnstatus = objAdmin.GetHDFCChargingStatus(OrderID, BookingID);
        }
        catch (Exception ex)
        {
            ErrorLog.LogErrorToLogFile(ex, "GetHDFCChargingStatus");
        }
        return returnstatus;
    }

    [WebMethod(Description = "Get Amex Order Status")]
    public string GetCCAvenueAmexChargingStatus(string OrderID
        , int BookingID)
    {
        string returnstatus = "";
        try
        {
            CCAvenueAPI objAdmin = new CCAvenueAPI();
            returnstatus = objAdmin.GetCCAvenueAmexChargingStatus(OrderID, BookingID);
        }
        catch (Exception ex)
        {
            ErrorLog.LogErrorToLogFile(ex, "GetCCAvenueAmexChargingStatus");
        }
        return returnstatus;
    }

    [WebMethod(Description = "Charge CCAvenue SI")]
    public void ChargeSI(int ClientCoIndivID, int BookingID, string Totalcost)
    {
        try
        {
            CCAvenueAPI objAdmin = new CCAvenueAPI();
            objAdmin.ChargeSI(ClientCoIndivID, BookingID, Totalcost);
        }
        catch (Exception ex)
        {
            ErrorLog.LogErrorToLogFile(ex, "ChargeSI");
        }
    }

    [WebMethod(Description = "Bulk Charging Link to Send")]
    public void BulkChargingLink()
    {
        try
        {
            clsAdmin objAdmin = new clsAdmin();
            objAdmin.BulkChargingLink();
        }
        catch (Exception ex)
        {
            ErrorLog.LogErrorToLogFile(ex, "BulkChargingLink");
        }
    }

    [WebMethod(Description = "Bulk Charging Link to Send")]
    public void GetAmexOrderPaymentStatus(string resultIndicator, int bookingId)
    {
        try
        {
            AmexCardCls objAmex = new AmexCardCls();
            DataSet ds = new DataSet();
            ds = objAmex.GetAmexOrderDetails(resultIndicator, bookingId);
            OrderDetails bookingDetails = new OrderDetails();

            if (ds.Tables[0].Rows.Count > 0)
            {
                bookingDetails.bookingId = Convert.ToInt32(ds.Tables[0].Rows[0]["BookingId"]);
                bookingDetails.resultIndicator = ds.Tables[0].Rows[0]["AmexResultIndicator"].ToString();
                bookingDetails.chargedyn = Convert.ToBoolean(ds.Tables[0].Rows[0]["chargedyn"]);
                bookingDetails.approvalNo = ds.Tables[0].Rows[0]["ApprovalNo"].ToString();

                CallGetAmexOrderPaymentStatus(bookingDetails).Wait(); //new method call 
            }
        }
        catch (Exception ex)
        {
            ErrorLog.LogErrorToLogFile(ex, "BulkChargingLink");
        }
    }

    [WebMethod(Description = "Bulk Charging Link to Send")]
    public void UpdateAmexOrderAmount()
    {
        try
        {
            AmexCardCls objAmex = new AmexCardCls();
            DataSet ds = new DataSet();
            ds = objAmex.GetAmexOrderUpdateAmountDetails();

            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    OrderDetails bookingDetails = new OrderDetails();
                    bookingDetails.bookingId = Convert.ToInt32(ds.Tables[0].Rows[i]["BookingId"]);
                    bookingDetails.resultIndicator = ds.Tables[0].Rows[i]["AmexResultIndicator"].ToString();
                    bookingDetails.chargedyn = Convert.ToBoolean(ds.Tables[0].Rows[i]["chargedyn"]);
                    bookingDetails.approvalNo = ds.Tables[0].Rows[i]["ApprovalNo"].ToString();

                    CallGetAmexOrderPaymentStatus(bookingDetails).Wait();

                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.LogErrorToLogFile(ex, "BulkChargingLink");
        }
    }
    private async Task CallGetAmexOrderPaymentStatus(OrderDetails bookingDetails)
    {
        string responsestring = "";
        clsAdmin pbjCls = new clsAdmin();
        responsestring = await pbjCls.GetAmexOrderPaymentStatus(bookingDetails);
    }

    [WebMethod(Description = "checking Charing status")]
    public string CheckChargestatus(string orderid, string TransactionID, bool OldPreauthModule)
    {
        string Chargingstatus = "";
        clsAdmin objAdmin = new clsAdmin();
        Chargingstatus = objAdmin.checkCharge(orderid, TransactionID, OldPreauthModule);
        return Chargingstatus;
    }

    [WebMethod(Description = "test")]
    public bool testing()
    {
        /*
        string test = "";
        DateTime currentdate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
        DateTime gstapplydate = new DateTime(2017, 6, 24);
        if (currentdate >= gstapplydate)
        {
            test = "ok";
        }
        else
        {
            test = "not ok";
        }

        return test;
        */
        string certificate = "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQCqjUIvxkg2Fm8/" +
                                   "cavzyDeEOmRBJrjE500ItVd6d7Q27fKsBSq66doZbQjc+jHga0/gCFWBOwd5DMZ8" +
                                   "mKW+vtyZ806uZMo9NUb1dIIUF2t4Icgl8L/sQs3jEPf724aliM3hn3jnHDgO0rKh" +
                                   "DBGR1EmZB7gddwvgzQBdSfcYc7MQg6QeDqiI7D6A3VzbY2f7AJTtUJ4ScJech1HW" +
                                   "oK3sSnwNAScgQiEsdD/4kVS7OqF48FVf/YNJ/vOkbTVxt1/YgD+XFQ0lp77L6GL8" +
                                   "3yG/6ADA139zIqsIoZI/PBxmlP8bB4hgzgZj6SqBkAKvEjuC2Ub1uTw/dj6fz8P2" +
                                   "hEfb6/vJAgMBAAECggEAa0EWRHEPbGRDZcSYfGCG+TK3ZXlybPGAsl9IOg4ZWQne" +
                                   "uGsvaggyThcRodX9k8IooxzamMCEfI2aLTTZ1tC0bmRiMK3VuRROIYI5Y2UvAodM" +
                                   "FQAH0g5Y59j9+ncdf/A+eQsOv1gxsLC0AS9TxT+ZYRFHqc/5uk9xUTl4mPn5vAHl" +
                                   "7vL5PVG4STUVXp1f/id/cYewEY+AApJy3v8V5etlsFRStbBxI7kSONeCXsxzG/vw" +
                                   "kFgmaQxvQ/Dc8immIRP2y/2i+0VeFmsc7/BPqUHmx8IBsGdep8B+J8F+ETc5DhHG" +
                                   "FU3QWgqQdKCn37pTKfWzJWW1W+PLmJpsuD7D8PDe+QKBgQDdUcd8pv6mN8gcbZxK" +
                                   "m+K5W5W/Jdp2od+ZIZYMsQOsBacthC6jDUEKk3BkVZt6tyS+C63ZhuOZb+WPwLZv" +
                                   "Y9Lz8k1C5wscmgFuYRC92+qmoyPQd6dvH5vf3j9BkP22ZlKiPSis+9YhiEbaKGqb" +
                                   "SFISc0wQs9XIQjHYpCDMtkNOQwKBgQDFRu/MrvztYIRCewDta/1vro5/m4Ei/b7h" +
                                   "ocLoD6BvLlWrlLod4VHn82kYtSLPftQ4wGYhr5wm1XshvaJWK6RKJAnfx8WIsSF1" +
                                   "kJr5WptY1qzHoIBe3IrfHMaBZaO+GEixq52pXJPSIaQdumH9WXGLhoh7xoEls29A" +
                                   "F4tSAQsbAwKBgQC+RWuCoDM3had1wUpn3kL6ZmGEVy/NY/dOuFA/EwP8JVQHzqcl" +
                                   "qORtCjrLF8o2ozNka7gH1m7f4BJ1FxIBC8o1ze9cgDPOYByw36Q8BdBjeMf2lpgM" +
                                   "8hjZS0IIxXVKFgge0QOiTFRYucdoEO0uGOvuTs9rYKB/rWEbRmeod12/7wKBgQC2" +
                                   "fDLwZOJEdqaBlKS7+sv6Fj9snR0skAqVTvwri+D9V6AZ0r8DSt1oMYqDN3NW+1w2" +
                                   "jwsql/ZbGdkH0hIrob0cdA/KOFJqameoB7bDeGQBHru/AaIdnea8EjMxmSKJgo8k" +
                                   "ArsZ4jA/Da1xbmqcX7OfvDfePzZR+JStddoDZkGunwKBgQDV1ZGySUI2aa1PAZtx" +
                                   "CjnBut5XFkDT9iX9Rz/gTkC740hb9b7V2jgnt+IiUBIKAmXKl97GK7mtpLIH/fzU" +
                                   "xjsQ9q7TzmDBOdlSmxMElYmv8vqiIxSlvwLAUJlN2pnznAF9hfhXmMt2lUwCinur" +
                                   "tGALiLjVLDL3WRi/b8KC/wwUSQ==";

        string mandate_response = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ii1LSTNROW5OUjdiUm9meG1lWm9YcWJIWkdldyJ9.eyJhdWQiOiJmMjU1MDg1Zi03NDJjLTRmMTctYjE5ZC1hN2ZmNTFiNjg1ODQiLCJpc3MiOiJodHRwczovL2xvZ2luLm1pY3Jvc29mdG9ubGluZS5jb20vNmI4M2IwYWEtMzA4ZC00ZmZhLWIyNWMtYWUzMTEyYTViNjhkL3YyLjAiLCJpYXQiOjE2NzkyOTE3NTEsIm5iZiI6MTY3OTI5MTc1MSwiZXhwIjoxNjc5Mjk1NjUxLCJhaW8iOiJBVlFBcS84VEFBQUFpbVh6Sm9mWkJYMHdBTllacDMvRGhBUGpYaHNPZWVONHcxKzBISlZxYk45NUdlVDRsa3hFM2NjRnVhM2w0dzc5Zm12R3VMN0VORWtXY3ZNTXQxN1RFZEw3TndsVVhMNUZ0WTNDU2E2Zkt2QT0iLCJjX2hhc2giOiJrekRZOE1HLTdBMlhkNVpOM1VFdUJRIiwiY2MiOiJDbUR0RE5SWkFwL2pzcHc1Tk1KY3AyS2JmOXM4TGNlajdnVlAvVDVRUkhpUnVaRUxKbFBKamFLVFh2QmgvRUNTdnY0V2pIVU91bE96YkdWc0o4c0pqN080QThuVHNKd1lpMGVSdTFwTWI0dUNOSExXc21OcFdtNVdLN1R4MFZHWXBIZ1NEbU5oY25wdmJuSmxiblF1WTI5dEdoSUtFSVpqTEIyV2F4eExuTnFKZTRLdVhvMGlFZ29RNlBBdkZJb2hjRVc0T1ZsQ3VtcFZBRElDUVZNNEFVSUpDUURrMzBpVkZkdElTZ2tKQUdRTk1rNWMyMGc9IiwibmFtZSI6Iml0aW5mcmFzdHJ1Y3R1cmUgQ29yIiwibm9uY2UiOiI2MzgxNDg4ODY4OTUyNTAxMjEuTWpZd1kyWTJNakl0WVdRMk15MDBNRFV6TFRnMU4yWXROV05oWmpWa1kyRTRaRE5oTlRabU16Tm1OVEl0Wm1SalppMDBOR016TFdJNFptVXRNelV4WVRZeE56SXlNRFJpIiwib2lkIjoiOGZhOTQ2OWQtOTU2NC00MGNhLTk4YTYtNjE5MzQ4ZGEwZWYzIiwicHJlZmVycmVkX3VzZXJuYW1lIjoiaXRpbmZyYXN0cnVjdHVyZUBjYXJ6b25yZW50LmNvbSIsInJoIjoiMC5BWElBcXJDRGE0MHctay15WEs0eEVxVzJqVjhJVmZJc2RCZFBzWjJuXzFHMmhZUnlBSmsuIiwic3ViIjoiNndMQ1plTmg1SHdKb0ZKYTcybFhVb3V1d1FLQW9TRnhvT1JqdklsaFh2RSIsInRpZCI6IjZiODNiMGFhLTMwOGQtNGZmYS1iMjVjLWFlMzExMmE1YjY4ZCIsInV0aSI6IjZQQXZGSW9oY0VXNE9WbEN1bXBWQUEiLCJ2ZXIiOiIyLjAifQ.CwG8XbYlC-LFtbW-_IEJbLD4n6Q57CxtMyofkjgsO8rYXNXaQ3bqDRuGPpLyJrN6vc-3PHJedI020OBo3r7z-B-zFGG6T3UmuOBR_uJnFdcoRDK8hInqirHDVFS20aW8TghTzV83i770_jMiGJr_mXJ1_Kww1f5nUzO1tml3BW6ztCsy4pMshlvHODQD6MvXKZ2AgVwR-46Zxq-ZPD9Ah9FsJKG89tHk5ACzDOdLZsyhxEoSvqlBBVdU3wy0Gw6nyLbyl9w_YVsLndKnUeAjcxy3UMl3nyRi7if1xZEYoyR19TYyTp5NJnxwKbAQSs-LafOZRCVUywxx9vRev3I5og";
        //string secretKey = "tJL6Wr2JUsxLyNezPQh1J6zn6wSoDAhgRYSDkaMuEHy75VikiB8wg25WuR96gdMpookdlRvh7SnRvtjQN9b5m4zJCMpSRcJ5DuXl4mcd7Cg3Zp1C5-JmMq8J7m7OS9HpUQbA1yhtCHqP7XA4UnQI28J-TnGiAa3viPLlq0663Cq6hQw7jYo5yNjdJcV5-FS-xNV7UHR4zAMRruMUHxte1IZJzbJmxjKoEjJwDTtcd6DkI3yrkmYt8GdQmu0YBHTJSZiz-M10CY3LbvLzf-tbBNKQ_gfnGGKF7MvRCmPA_YF_APynrIG7p4vPDRXhpG3_CIt317NyvGoIwiv0At83kQ";
        //Jwk key = new Jwk(Encoding.UTF8.GetBytes(certificate));
        //var pgresponse = Jose.JWT.Decode(mandate_response, key, JwsAlgorithm.RS256);
        string abc = Decryption(mandate_response, certificate);

        //DB db = new DB();
        //bool abc = db.CheckIfParkingTollValidated(bookingid);
        return false;
    }

    public static string Decryption(string strText, string privateKey)
    {
        var testData = Encoding.UTF8.GetBytes(strText);

        using (var rsa = new RSACryptoServiceProvider(1024))
        {
            try
            {
                var base64Encrypted = strText;
                // server decrypting data with private key                    
                rsa.FromXmlString(privateKey);
                var resultBytes = Convert.FromBase64String(base64Encrypted);
                var decryptedBytes = rsa.Decrypt(resultBytes, true);
                var decryptedData = Encoding.UTF8.GetString(decryptedBytes);
                return decryptedData.ToString();
            }
            finally
            {
                rsa.PersistKeyInCsp = false;
            }
        }
    }

    [WebMethod]
    public bool CheckChargingLink(int bookingid)
    {
        ChargingWebService.DB db = new ChargingWebService.DB();
        return db.CheckChargingLink(bookingid);
    }

    [WebMethod]
    public string CheckclosingAllowed(int ClientCoID, string PaymentMode)
    {
        ChargingWebService.DB db = new ChargingWebService.DB();
        string closingAllowed = "";
        try
        {
            closingAllowed = db.CheckClosingAllowed(ClientCoID, PaymentMode);
        }
        catch (Exception ex)
        {
            closingAllowed = "";
        }
        return closingAllowed;
    }

    [WebMethod]
    public string RegisterStatusCheck(string TravellerID)
    {
        clsAdmin cls = new clsAdmin();
        string responsestring = "";
        try
        {
            TravellerResponseParam abc = new TravellerResponseParam();

            responsestring = cls.RegisterStatusCheck(TravellerID);
        }
        catch (Exception ex)
        {
            responsestring = "";
        }
        return responsestring;
    }

    [WebMethod]
    public void RechargingUnAuthorized()
    {
        ChargingWebService.DB db = new ChargingWebService.DB();
        db.RechargingUnAuthorized();
    }

    [WebMethod]
    public void BulkCorporateClosing()
    {
        ChargingWebService.DB db = new ChargingWebService.DB();
        db.BulkCorporateClosing();
    }

    [WebMethod]
    public string CloseWithOutParkingToll(int BookingID, int UserId, bool NotifyGuestYN)
    {
        ChargingWebService.ChargingDetails ch = new ChargingWebService.ChargingDetails();
        string status = "";
        status = ch.CloseWithOutParkingToll(BookingID, UserId, NotifyGuestYN);

        return status;
    }

    [WebMethod]
    public bool VendorCarApprovedYN(int BookingID)
    {
        bool ApproveYN = true;
        try
        {
            ChargingWebService.DB db = new ChargingWebService.DB();
            ApproveYN = db.VendorCarApprovedYN(BookingID);
        }
        catch (Exception ex)
        {
            ErrorLog.LogErrorToLogFile(ex, "VendorCarApprovedYN");
            ApproveYN = true;
        }
        return ApproveYN;
    }

    [WebMethod]
    public string checkChargingStatus_AccentureNew(int BookingID, double TotalCost, string Misc1)
    {
        clsAdmin admin = new clsAdmin();
        string accentureChargingStatus = "";

        accentureChargingStatus = admin.checkChargingStatus_AccentureNew(BookingID, TotalCost, Misc1);

        ErrorLog.LoginfoToLogFile("accentureChargingStatus:" + accentureChargingStatus, "checkChargingStatus_AccentureNew");

        return accentureChargingStatus;
    }

    [WebMethod]
    public string TransactionStatusCheck(int BookingID, string PreauthTransactionID, string TransactionMode)
    {
        NewChargingResponseParam abc = new NewChargingResponseParam();
        clsAdmin cls = new clsAdmin();
        abc = cls.TransactionStatusCheck(BookingID, PreauthTransactionID, TransactionMode);
        return abc.ResponseMessage;
    }

    [WebMethod(Description = "Send pre Debit Notification to the user.")]
    public void AmexPredebitNotification(int BookingId)
    {
        AmexCardCls objAmex = new AmexCardCls();
        DB db = new DB();
        string providerstring = db.CheckPreDebitNotificationProvider(BookingId);
        if (providerstring == "BillDesk")
        {
            objAmex.createInvoice(BookingId.ToString());
        }
        else if (providerstring == "CCAvenue")
        {
            CCAvenueAPI api = new CCAvenueAPI();
            api.CreatePreDebitNoteNotification(BookingId);
        }
        else
        {
            ErrorLog.LoginfoToLogFile("Card Not Registed for BookingID " + BookingId
                , "AmexPredebitNotification");
        }
    }

    [WebMethod(Description = "For check valid Mandate.")]
    public string CheckValidMandate(string MandateID)
    {
        AmexMandateOp objMandate = new AmexMandateOp();
        var result = objMandate.CheckValidMandate(MandateID);
        return result;
    }

    [WebMethod(Description = "For get the MandateID.")]
    public string GetMandateId(string TravellerID)
    {
        AmexMandateOp objMandate = new AmexMandateOp();
        var result = objMandate.GetMandateID(TravellerID);
        return result;
    }

    [WebMethod(Description = "Get Signature for Canara Batch")]
    public string GetSignatureCanara()
    {
        //string str = DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss.fffffff'Z'");
        //return str;

        CanraAPI api = new CanraAPI();
        string result = api.GetSignature();
        return result;
    }

    //[WebMethod(Description = "Post Canara Bank Transaction API")]
    //public async Task<string> PostCanaraTransaction(string BatchRequestID)
    //{
    //    CanraAPI api = new CanraAPI();
    //    RS_WebServiceMetaData cdfdfd = await api.PostBatchAsync(BatchRequestID);

    //    string serializedData = Newtonsoft.Json.JsonConvert.SerializeObject(cdfdfd);
    //    return serializedData;
    //}

    //[WebMethod(Description = "Post Canara Bank Transaction API")]
    //public async void PostCanaraTransaction(string BatchRequestID)
    //{
    //    CanraAPI api = new CanraAPI();
    //    await api.PostBatch(BatchRequestID);
    //}

    [WebMethod(Description = "Post Canara Bank Transaction API")]
    public string PostCanaraTransaction(string BatchRequestID)
    {
        CanraAPI api = new CanraAPI();
        RS_WebServiceMetaData dfsfs = api.PostBatchTest1(BatchRequestID);
        return new JavaScriptSerializer().Serialize(dfsfs);// api.PostBatchTest1(BatchRequestID);
    }

    [WebMethod(Description = "Post Canara Bank Transaction API")]
    public string PostCanaraBatchInquiry(string BatchRequestID)
    {
        CanraAPI api = new CanraAPI();
        RS_WebServiceMetaData dfsfs = api.GetBatchTxnInquiry(BatchRequestID);
        return new JavaScriptSerializer().Serialize(dfsfs);// api.PostBatchTest1(BatchRequestID);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public async Task<string> GetJsonDataAsync()
    {
        // Simulate an asynchronous operation
        await Task.Delay(1000);

        // Call another asynchronous method to get additional data
        string additionalData = await GetAdditionalDataAsync();

        // Simulate data retrieval
        var data = new { Message = "Hello, World!", AdditionalData = additionalData };

        // Convert the data to JSON
        string jsonData = new JavaScriptSerializer().Serialize(data);

        // Return the JSON result
        return jsonData;
    }

    private async Task<string> GetAdditionalDataAsync()
    {
        // Simulate another asynchronous operation
        await Task.Delay(500);

        // Return additional data
        return "This is additional data.";
    }

    //[WebMethod]
    //public string MVPreauth_ClassicAspIntegration(string ApprovalNo, string AgentEmailId, string InvoiceNo,
    //        double Amount, DateTime RentalDate, string RentalType, string PickupCity, string CustomerName)
    //{
    //    clsAdmin objAdmin = new clsAdmin();
    //    return objAdmin.MVPreauth_ClassicAspIntegration(ApprovalNo, AgentEmailId, InvoiceNo, Amount, RentalDate, RentalType, PickupCity, CustomerName);
    //}
}