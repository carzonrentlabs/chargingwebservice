﻿using System.Net;
using System.Web.Script.Serialization;
using ChargingWebService;
using RestSharp;
using System.Collections.Generic;
using System.Configuration;
//using System.IdentityModel.Tokens.Jwt;
using System;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security;
using Jose;
using Org.BouncyCastle.Asn1.Ocsp;
using System.Net.Http;
using System.Threading.Tasks;
using System.Threading;
using Newtonsoft.Json;
using System.Web.Http.Results;
using System.Security.Policy;
using System.Web;
using _API;
//using static EInvResponse;

public class CanraAPI
{
    private static string BaseUrL = ConfigurationManager.AppSettings["CanaraApiBaseURL"];
    private static string BatchInitiateURL = ConfigurationManager.AppSettings["CanaraBatchInitiateURL"];
    private static string BatchInquiryURL = ConfigurationManager.AppSettings["CanaraBatchInquiryURL"];
    private static string ClientIdOrKey = ConfigurationManager.AppSettings["CanaraClientIdOrKey"];
    private static string ClientSecret = ConfigurationManager.AppSettings["CanaraClientSecret"];
    private static string ClientCertificate = ConfigurationManager.AppSettings["CanaraClientCertificate"];
    private static string ApiInteractionId = ConfigurationManager.AppSettings["CanaraApiInteractionId"];
    private static string NEFT = ConfigurationManager.AppSettings["CanaraNEFT"];

    //private static string SHARED_SYMMETRIC_KEY = ConfigurationManager.AppSettings["CanaraSharedSymmetricKey"];
    private static string SHARED_SYMMETRIC_KEY = "2457bf1dc01659e2cd0af14dced615376aaab76f40c0e89d641421f807112b84";
    //    private static string privateKeyPEM = "-----BEGIN PRIVATE KEY-----\n" +
    //"MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCwZ+K+Ux2kRpHj\n" +
    //"DP2SgvCkSEaMuS8mot8H5qJzBSghyyXy2IosuyrbQpGrmSEKCgamJuGJqRBUgZXs\n" +
    //"dal0QpinTizaYdDRaPP+DCnylK9yAEyTEGU6dmTHRUMe1ae/Y3EnTWSsYHAAlPHP\n" +
    //"VJ5+pJLkuqNHIXZnZHqyPKAXzejcY3vlOD6w6qpPKCv3dGvv7sqpwnhB67TGYn0z\n" +
    //"Ehm7tnjcQ6oZGrjNVIZZ9IAmtfiNJPe5h+XysuKQ6sy5egAXYuF0JDIHW2Y5JaHq\n" +
    //"jtrWzA3xxunj9fAq10FhQ37kVVHC3N+Y9sXYxYHp4zHog58zx+21ZYPyKuW8JX8u\n" +
    //"yhBpj4LtAgMBAAECggEAL7lLLHZL9J9q5GQlTbfC5o7vFy8aRHeXowmQNVHV056j\n" +
    //"+5j9eLCCHaNayXO57n9b4SNvrNBiLJqKNth2KY/CwLBzjfkchyq/p6Ee0BPNiyft\n" +
    //"j3PGDxTsmsRwLi1bHnoGqL1VWRUV9/JToOWho11eqCad+aZh5ALY9tNT9FyufMCy\n" +
    //"Tinpg117Bdr723HsAl9WjUZgtT/3+p8OcqKKSJ7kvoEyW8VIdVnYq7L5hN2AP8ce\n" +
    //"iEnp3uDvaUd18gGIUVz4V+TbIcyeC++Wle0be1NY3cSfSXvBcRzaXAYTFPQP2/2u\n" +
    //"FEQjPltGEuS1PtOSToq6w7O0roWXiGEhjWkCGxW7QQKBgQDVFeOupYExcLUEXltt\n" +
    //"72lTZ0go40hSdLzMUb8WeJEo7hWFHrVW9QB9sNWdcNTlPeBKWmRjWsWB8Ueh5Ac+\n" +
    //"FmI+Y8MKk7+MSsEXhrL02deLhAs8sFDYM5OKFKhh8F8ssHYSVo01mYGvd9piuwUk\n" +
    //"ag54Tr/YKA18121Dh+jBF74VKQKBgQDT7uPx1agLWIPvzODcrEt3XchWa9NU/3JU\n" +
    //"w9rW+eUnxTQq2uyzCiqm4zo+dPn7EBfazf5QP5Q6YvPy/f/+WF4W0mQOUZDSgG6c\n" +
    //"gNtFibFO+0UIOPF9i3XMIt8amVseOiTQwR/JywZsCwRGBAr7UhHOIrhg0ehrJXQj\n" +
    //"/kjX0p1UJQKBgElTNv4qRcLVnfTa42t5Ly1cJSCs1X5KXY2Rs8fvxUPoac9dOdmq\n" +
    //"hXi7GUcMRLZ+DGiJonHuEnkcpiG3biaXXUdK3RsOrKOCNd/6oipPrDR+Q7+mjKtZ\n" +
    //"VDP319mb9aRTNM4qqnz/TfkrrSK8aJCXTlNoBexEHCARoa/TXOzVVrvBAoGBAMnz\n" +
    //"UD64K8Nz+3Vcs8FUZS0/rpG7ecv7BwWDBFvqENVO86EKsJcDTxVsXan6aeM1uKWF\n" +
    //"uZramvLwLCoWpAPITRBON431Z51PSRfVKh0fUlhC08s8B9JsPDnj2NlN3Sf2m/JY\n" +
    //"tWPjFSGLde0KGTTXRaQ6LZwFKgY/0GYj/2G5jrYJAoGAfVCuCPgyIjXEGWhTwBbW\n" +
    //"0szjQZrGAyBJnonKKLgH4ehEIlZJcfxiEORVTCSY7zkfK4wARJujSW/b6HXN8Mpa\n" +
    //"KIbuZTwaIYXGZCQxWyGq7J7m7VJZR6DKnWlLHpzlL1pBghC4qsDyR6Og7zZPlVl/\n" +
    //"ZaM1u0G/RtPwh6LJ0YJ47Ko=\n" +
    //"-----END PRIVATE KEY-----";

    private static string privateKeyPEM = "-----BEGIN PRIVATE KEY-----\n" +
"MIIG4zCCBcugAwIBAgIRAKmw4hwEKSpMrdTsHoQiIyIwDQYJKoZIhvcNAQELBQAw\n" +
"gZUxCzAJBgNVBAYTAkdCMRswGQYDVQQIExJHcmVhdGVyIE1hbmNoZXN0ZXIxEDAO\n" +
"BgNVBAcTB1NhbGZvcmQxGDAWBgNVBAoTD1NlY3RpZ28gTGltaXRlZDE9MDsGA1UE\n" +
"AxM0U2VjdGlnbyBSU0EgT3JnYW5pemF0aW9uIFZhbGlkYXRpb24gU2VjdXJlIFNl\n" +
"cnZlciBDQTAeFw0yMzA5MjcwMDAwMDBaFw0yNDEwMTIyMzU5NTlaMFwxCzAJBgNV\n" +
"BAYTAklOMQ4wDAYDVQQIEwVEZWxoaTEiMCAGA1UEChMZQ2Fyem9ucmVudCBJbmRp\n" +
"YSBQdnQuIEx0ZDEZMBcGA1UEAwwQKi5jYXJ6b25yZW50LmNvbTCCASIwDQYJKoZI\n" +
"hvcNAQEBBQADggEPADCCAQoCggEBAN77x9pejSriLUuapoYVZQijt0TarwYLTvWL\n" +
"FcKzWhYfEnSlNHIGhI/OFWMko++1cXUCXFT5A0422S0ULsnKMjFoXqlp5X+7v+qI\n" +
"ZhIzz6M+4AozxDnZZd7DJDKq+YUfB6ND52yU0pUR96ZxEqUTNwD+a7LXDZdN4TNx\n" +
"Cpq7qX31QXeeWIZxG8h7CVihLkas8Sivccf1fi/VQhe8Fis2ga7FCTo68VbNJbMa\n" +
"Y6lqTsYjmRSxOMD5I0n6OoRd2USAUd/8mP6ZcU+RgyBGHq4ZnV3cvA887GEbU7rr\n" +
"p2EIPSQDZGu3i22gKkPw/0WciylRpV5ZIt0djXwkVsvNB5snRYECAwEAAaOCA2Qw\n" +
"ggNgMB8GA1UdIwQYMBaAFBfZ1iUnZ/kxwklD2TA2RIxsqU/rMB0GA1UdDgQWBBSl\n" +
"6w5ZnMmamK2JUXFBPW8cO7Tm0DAOBgNVHQ8BAf8EBAMCBaAwDAYDVR0TAQH/BAIw\n" +
"ADAdBgNVHSUEFjAUBggrBgEFBQcDAQYIKwYBBQUHAwIwSgYDVR0gBEMwQTA1Bgwr\n" +
"BgEEAbIxAQIBAwQwJTAjBggrBgEFBQcCARYXaHR0cHM6Ly9zZWN0aWdvLmNvbS9D\n" +
"UFMwCAYGZ4EMAQICMFoGA1UdHwRTMFEwT6BNoEuGSWh0dHA6Ly9jcmwuc2VjdGln\n" +
"by5jb20vU2VjdGlnb1JTQU9yZ2FuaXphdGlvblZhbGlkYXRpb25TZWN1cmVTZXJ2\n" +
"ZXJDQS5jcmwwgYoGCCsGAQUFBwEBBH4wfDBVBggrBgEFBQcwAoZJaHR0cDovL2Ny\n" +
"dC5zZWN0aWdvLmNvbS9TZWN0aWdvUlNBT3JnYW5pemF0aW9uVmFsaWRhdGlvblNl\n" +
"Y3VyZVNlcnZlckNBLmNydDAjBggrBgEFBQcwAYYXaHR0cDovL29jc3Auc2VjdGln\n" +
"by5jb20wKwYDVR0RBCQwIoIQKi5jYXJ6b25yZW50LmNvbYIOY2Fyem9ucmVudC5j\n" +
"b20wggF9BgorBgEEAdZ5AgQCBIIBbQSCAWkBZwB1AHb/iD8KtvuVUcJhzPWHujS0\n" +
"pM27KdxoQgqf5mdMWjp0AAABitYYBVwAAAQDAEYwRAIgd19YtfXEo0FlAWtb4F/k\n" +
"peHqY96B+1GR3Yjkg65dtbcCIGoEwGTYN6ivz6DU+0x30p4Mm/4KVThbN3+ytz7t\n" +
"obn1AHYA2ra/az+1tiKfm8K7XGvocJFxbLtRhIU0vaQ9MEjX+6sAAAGK1hgFtAAA\n" +
"BAMARzBFAiBe0fVjefXe963duakpNGZzLCXO+9lID9zenL9SGYRwQAIhAPiOK4jD\n" +
"qfv1Ar4f//zjoL2x4kicSZtm9zkb9ii+ybjOAHYA7s3QZNXbGs7FXLedtM0TojKH\n" +
"Rny87N7DUUhZRnEftZsAAAGK1hgF2AAABAMARzBFAiEAsARzeQQSycQvbt4Qtikm\n" +
"MBX0aZ35dQ7thihbdFgoyfQCIEp/AgJPKPwnQ9GbGLn30Mk1LNKhRxbe6HF5MQji\n" +
"aJZqMA0GCSqGSIb3DQEBCwUAA4IBAQBLlO5n7Gq7SGVP9bKP2VZp6DFxIT6C5BWH\n" +
"MMhoxSJ8SOmXUaJVHeyoiGRA7+l46ff31FvnMDXpQm+FsflrufahGxMSLYJQgYAC\n" +
"Jt3Nqg0hk6RDHv39iosrR/2CWDBvbzkujztxlQY4M7ZUpCwV+rmrW3l1vIMB6lGU\n" +
"NPKMe2HozJZ3OU6hBbQ3i9FS4rknA42O1pb9Ce+E6ZcUFI02jgdGk4uRjfLH6gpR\n" +
"oryIs1TQt/q8bG5qenWIGSBIrrHpvgiCUC9xHSOXutc33yv2RF5U0LSttxT9VhnD\n" +
"RRNqEzE/r1VFLR0rKCW888Wsn2AAh5WmJKbpFqG/gPCk3/PtEGkg\n" +
"-----END PRIVATE KEY-----";

    public RS_WebServiceMetaData GetBatchTxnInquiry(string _batchRequestID)
    {
        RS_WebServiceMetaData _response = new RS_WebServiceMetaData();
        try
        {
            CanaraApiCls cls = new CanaraApiCls();

            CanraSignatureBodyRequest objInquiryRequest = new CanraSignatureBodyRequest();
            objInquiryRequest = cls.GetBatchInquiryRequest(_batchRequestID);

            CanraRequestFinal requestFinalInquiryObject = new CanraRequestFinal();
            requestFinalInquiryObject = GetEncryptedData(objInquiryRequest);

            JavaScriptSerializer js = new JavaScriptSerializer();
            JavaScriptSerializer js1 = new JavaScriptSerializer();

            var payLoadPlain = js.Serialize(objInquiryRequest);
            var finalEncRequestJSON = js1.Serialize(requestFinalInquiryObject);

            string signature = SignDataTest(payLoadPlain, privateKeyPEM);
            var httpResponse = BatchInqiryApiCall(signature, finalEncRequestJSON);

            string responseContent = "";
            
            try
            {
                // Optionally handle the response content here
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    responseContent = result.ToString();
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.FailureMessage = "StreamReader Exception" + ex.Message; //errorResponse.ErrorResponse.;
                return _response;
            }

            //Check if the request was successful
            if (httpResponse.StatusCode.ToString() == "OK")
            {
                JObject jsonResponse = JObject.Parse(responseContent);

                if (jsonResponse["Response"] != null)
                {
                    // Success response, deserialize to JsonResponse
                    CanraSuccessResponseBodyEnc successResponseEnc = new JavaScriptSerializer().Deserialize<CanraSuccessResponseBodyEnc>(responseContent);

                    // Process success response
                    string decryptedResponseJSON = DecodeSuccessEncReponse(successResponseEnc.Response.body.encryptData);
                    //decryptedResponseJSON = "{\r\n    \"Response\": {\r\n        \"Body\": {\r\n            \"UserName\": \"99484722M\",\r\n            \"CustomerID\": \"99484722\",\r\n            \"TotAmt\": \"3.00\",\r\n            \"TxnCnt\": \"3\",\r\n            \"DatTxn\": \"20231127\",\r\n            \"BatchRequestID\": \"CARZ241120230448\",\r\n            \"TxnRefNo\": \"\",\r\n            \"TotNeftAmt\": \"1.0\",\r\n            \"TxnNeftCnt\": \"1\",\r\n            \"TotRtgsAmt\": \"1.0\",\r\n            \"TxnRtgsCnt\": \"1\",\r\n            \"TotImpsAmt\": \"0.0\",\r\n            \"TxnImpsCnt\": \"0\",\r\n            \"TotIntraAmt\": \"1.0\",\r\n            \"TxnIntraCnt\": \"1\",\r\n            \"TxnDtls\": {\r\n                \"Txn\": [\r\n                    {\r\n                        \"utr_RRN_Number\": \"P349230238409547\",\r\n                        \"TxnRefNo\": \"1031658\",\r\n                        \"DrAcct\": \"2709214000005\",\r\n                        \"SndrNm\": \"SYED OWAIS UDDIN\",\r\n                        \"TxnAmt\": \"1.00\",\r\n                        \"TxnType\": \"NEFT\",\r\n                        \"BenefIFSC\": \"UTIB0000553\",\r\n                        \"BenefAcNo\": \"553010200000055\",\r\n                        \"BenefAcNm\": \"24X7 TRANSLINES\",\r\n                        \"Nrtv\": \"REMITTANCE1\",\r\n                        \"UTR_RRN_Number\": \"P349230238409547\",\r\n                        \"TxnStatus\": \"Successful\",\r\n                        \"Txn_init_date\": \"20231214\",\r\n                        \"Approved_By\": \"99484722C\",\r\n                        \"Approved_Date\": \"20231215\"\r\n                    },\r\n                    {\r\n                        \"TxnRefNo\": \"1045504\",\r\n                        \"DrAcct\": \"2709214000005\",\r\n                        \"SndrNm\": \"SYED OWAIS UDDIN\",\r\n                        \"TxnAmt\": \"1.00\",\r\n                        \"TxnType\": \"RTGS\",\r\n                        \"BenefIFSC\": \"ICIC0001354\",\r\n                        \"BenefAcNo\": \"135401533921\",\r\n                        \"BenefAcNm\": \"NIKHIL ANAND\",\r\n                        \"Nrtv\": \"REMITTANCE2\",\r\n                        \"TxnStatus\": \"Error\",\r\n                        \"Txn_init_date\": \"20231214\",\r\n                        \"Approved_By\": \"99484722C\",\r\n                        \"Approved_Date\": \"20231215\",\r\n                        \"Error_Code\": \"DIGX_CZ_PY_RTGSTRANSFER_0001\",\r\n                        \"Error_Msg\": \"Transaction Amount must be within minimum and maximum Txn amount\"\r\n                    },\r\n                    {\r\n                        \"utr_RRN_Number\": \"2023273011608313\",\r\n                        \"TxnRefNo\": \"1045505\",\r\n                        \"DrAcct\": \"2709214000005\",\r\n                        \"SndrNm\": \"SYED OWAIS UDDIN\",\r\n                        \"TxnAmt\": \"1.00\",\r\n                        \"TxnType\": \"INTRA\",\r\n                        \"BenefIFSC\": \"ICIC0000111\",\r\n                        \"BenefAcNo\": \"0922108041614\",\r\n                        \"BenefAcNm\": \"ASMA NAAZ\",\r\n                        \"Nrtv\": \"REMITTANCE2\",\r\n                        \"UTR_RRN_Number\": \"2023273011608313\",\r\n                        \"TxnStatus\": \"Successful\",\r\n                        \"Txn_init_date\": \"20231214\",\r\n                        \"Approved_By\": \"99484722C\",\r\n                        \"Approved_Date\": \"20231215\"\r\n                    }\r\n                ]\r\n            }\r\n        }\r\n    }\r\n}";

                    CanaraBatchInquiryRoot responseJSON = new JavaScriptSerializer().Deserialize<CanaraBatchInquiryRoot>(decryptedResponseJSON);

                    if (responseJSON != null)
                    {
                        CanaraApiCls dataCls = new CanaraApiCls();
                        if (dataCls.SaveCanaraBatchInquiryResponse(responseJSON, decryptedResponseJSON) > 0)
                        {
                            _response.IsSuccess = true;
                            _response.SetSuccess();
                        }
                        else
                        {
                            _response.IsSuccess = false;
                            _response.FailureMessage = "DB Error Response";
                        }
                    }
                    else
                    {
                        _response.IsSuccess = false;
                        _response.FailureMessage = "No Response from host";
                    }
                }
                else if (jsonResponse["ErrorResponse"] != null)
                {
                    // Error response, deserialize to CanraErrorResponse
                    CanraErrorResponse errorResponse = new JavaScriptSerializer().Deserialize<CanraErrorResponse>(responseContent);
                    _response.IsSuccess = false;
                    _response.FailureMessage = "Error Response"; //errorResponse.ErrorResponse.;
                }
                else
                {
                    // Handle unknown response format or other cases
                    _response.IsSuccess = false;
                    _response.FailureMessage = "No Response Type"; //errorResponse.ErrorResponse.;
                }
            }
            else
            {
                // Handle unsuccessful response here
                var errorMessage = httpResponse.ToString();
                _response.IsSuccess = false;
                _response.FailureMessage = "StatusCode not Ok" + errorMessage; //errorResponse.ErrorResponse.;
                return _response;
            }

        }
        catch (Exception ex)
        {
            string exMessage = ex.Message;
        }
        return _response;
    }
    
    public async Task<string> PostBatchTest(string _batchRequestID)
    {
        RS_WebServiceMetaData _response = new RS_WebServiceMetaData();

        try
        {
            CanaraApiConfiguration apiConfig = await LoadApiConfiguration();
            CanaraApiCls cls = new CanaraApiCls();

            CanraRequest requestObject = new CanraRequest();
            requestObject = cls.GetBatchTrxnDetails(_batchRequestID);

            JavaScriptSerializer js = new JavaScriptSerializer();
            JavaScriptSerializer js1 = new JavaScriptSerializer();
            var payLoadPlain = js.Serialize(requestObject);

            CanraRequestFinal requestFinalObject = new CanraRequestFinal();
            requestFinalObject = GetEncryptedData(requestObject);
            var finalEncRequestJSON = js1.Serialize(requestFinalObject);

            string signature = SignDataTest(payLoadPlain, privateKeyPEM);

            var request = BatchInitApiCall(signature, finalEncRequestJSON);

            // ... (rest of your existing code)

            // At the end of your method, return the appropriate response

        }
        catch (HttpRequestException ex)
        {
            _response.SetFailure("HTTP request failed: " + ex.Message);
        }
        catch (Exception ex)
        {
            _response.SetFailure("An unexpected error occurred: " + ex.Message);
        }

        // Return the failure response in case of exceptions
        return JsonConvert.SerializeObject(_response);
    }
    public RS_WebServiceMetaData PostBatchTest1(string _batchRequestID)
    {
        RS_WebServiceMetaData _response = new RS_WebServiceMetaData();

        try
        {
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            CanaraApiCls cls = new CanaraApiCls();

            CanraRequest requestObject = new CanraRequest();
            requestObject = cls.GetBatchTrxnDetails(_batchRequestID);

            JavaScriptSerializer js = new JavaScriptSerializer();
            JavaScriptSerializer js1 = new JavaScriptSerializer();
            var payLoadPlain = js.Serialize(requestObject);

            CanraRequestFinal requestFinalObject = new CanraRequestFinal();
            requestFinalObject = GetEncryptedData(requestObject);
            var finalEncRequestJSON = js1.Serialize(requestFinalObject);

            string signature = SignDataTest(payLoadPlain, privateKeyPEM);

            var httpResponse = BatchInitApiCall(signature, finalEncRequestJSON);
            var cType = httpResponse.ContentType;
            ErrorLog.LoginfoToLogFile("Canara httpResponse=" + httpResponse + "Content-Type : " + cType, "PostBatchTest1");

            string responseContent = "";

            try
            {
                // Optionally handle the response content here
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    responseContent = result.ToString();
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.FailureMessage = "StreamReader Exception" + ex.Message; //errorResponse.ErrorResponse.;
                return _response;
            }


            //Check if the request was successful
            if (httpResponse.StatusCode.ToString() == "OK" || httpResponse.StatusCode.ToString() == "Accepted")
            {
                JObject jsonResponse = JObject.Parse(responseContent);

                if (jsonResponse["Response"] != null)
                {
                    // Success response, deserialize to JsonResponse
                    CanraSuccessResponseBodyEnc successResponseEnc = new JavaScriptSerializer().Deserialize<CanraSuccessResponseBodyEnc>(responseContent);

                    // Process success response
                    string decryptedResponseJSON = DecodeSuccessEncReponse(successResponseEnc.Response.body.encryptData);
                    //decryptedResponseJSON = "{\"status\":{\"result\":\"ACCEPTED\",\"referenceNumber\":\"211227CA8AB0\",\"contextID\":\"91b576af-442a-49bc-8841-2af870c8d29a-000004a0,0\",\"message\":{\"code\":\"DIGX_APPROVAL_REQUIRED\",\"type\":\"INFO\"}},\"Response\":{\"Body\":{\"txnSummary\":{\"BatchNo\":\"CARZ241120230450\",\"TotalRecords\":\"3\",\"SumAmount\":\"3\",\"ErrorCode\":\"0\",\"ErrorDec\":\"\",\"BatchRequestID\":\"CARZ241120230450\"}}}}";
                    CanSuccessResponseBodyEnryptData responseJSON = new JavaScriptSerializer().Deserialize<CanSuccessResponseBodyEnryptData>(decryptedResponseJSON);

                    if (responseJSON.status != null && responseJSON.Response.Body.txnSummary.ErrorCode == "0")
                    {
                        //CanraSuccessResponseBody successResponseFinal = new JavaScriptSerializer().Deserialize<CanraSuccessResponseBody>(decryptedResponseJSON);

                        CanaraApiCls dataCls = new CanaraApiCls();
                        if (dataCls.SaveCanaraBatchInitiateResponse(responseJSON) > 0)
                        {
                            _response.IsSuccess = true;
                            _response.SetSuccess();
                        }
                        else
                        {
                            _response.IsSuccess = false;
                            _response.FailureMessage = "DB Error Response";
                        }
                    }
                    else
                    {
                        _response.IsSuccess = false;
                        _response.FailureMessage = responseJSON.Response.Body.txnSummary.ErrorDec;
                    }

                }
                else if (jsonResponse["ErrorResponse"] != null)
                {
                    // Error response, deserialize to CanraErrorResponse
                    CanraErrorResponse errorResponse = new JavaScriptSerializer().Deserialize<CanraErrorResponse>(responseContent);
                    _response.IsSuccess = false;
                    _response.FailureMessage = "Error Response"; //errorResponse.ErrorResponse.;
                }
                else
                {
                    // Handle unknown response format or other cases
                    _response.IsSuccess = false;
                    _response.FailureMessage = "No Response Type"; //errorResponse.ErrorResponse.;
                }
            }
            else
            {
                // Handle unsuccessful response here
                var errorMessage = httpResponse.ToString();
                _response.IsSuccess = false;
                _response.FailureMessage = "StatusCode not Ok" + errorMessage; //errorResponse.ErrorResponse.;
                return _response;
            }

        }
        catch (HttpRequestException ex)
        {
            _response.SetFailure("HTTP request failed:" + ex.Message);
            return _response;
        }
        catch (Exception ex)
        {
            _response.SetFailure("An unexpected error occurred: " + ex.Message);
            return _response;
        }
        // Return the failure response in case of exceptions
        return _response;

    }
    public async Task<string> PostBatchTestAsync(string _batchRequestID)
    {
        RS_WebServiceMetaData _response = new RS_WebServiceMetaData();

        try
        {
            CanaraApiCls cls = new CanaraApiCls();

            CanraRequest requestObject = new CanraRequest();
            requestObject = cls.GetBatchTrxnDetails(_batchRequestID);
            JavaScriptSerializer js = new JavaScriptSerializer();
            JavaScriptSerializer js1 = new JavaScriptSerializer();
            var payLoadPlain = js.Serialize(requestObject);

            CanraRequestFinal requestFinalObject = new CanraRequestFinal();
            requestFinalObject = await GetEncryptedDataAsync(requestObject);
            var finalEncRequestJSON = js1.Serialize(requestFinalObject);

            string signature = SignDataTest(payLoadPlain, privateKeyPEM);

            var request = await BatchInitApiCallAsync(signature, finalEncRequestJSON);

            // Set the TLS version to TLS 1.2
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            //var client = new HttpClient();
            //var response = await client.SendAsync(request).ConfigureAwait(false);

            // Check if the request was successful
            //if (response.IsSuccessStatusCode)
            //{
            //    // Optionally handle the response content here
            //    var responseContent = await response.Content.ReadAsStringAsync();
            //    JObject jsonResponse = JObject.Parse(responseContent);

            //    if (jsonResponse["Response"] != null)
            //    {
            //        // Success response, deserialize to JsonResponse
            //        CanraSuccessResponseBodyEnc successResponseEnc = new JavaScriptSerializer().Deserialize<CanraSuccessResponseBodyEnc>(responseContent);

            //        // Process success response
            //        string decryptedResponseJSON = DecodeSuccessEncReponse(successResponseEnc.Response.body.encryptData);
            //        CanSuccessResponseBodyEnryptData responseJSON = new JavaScriptSerializer().Deserialize<CanSuccessResponseBodyEnryptData>(decryptedResponseJSON);

            //        if (responseJSON.status != null && responseJSON.Response.Body.txnSummary.ErrorCode == "0")
            //        {
            //            CanraSuccessResponseBody successResponseFinal = new JavaScriptSerializer().Deserialize<CanraSuccessResponseBody>(decryptedResponseJSON);

            //            CanaraApiCls dataCls = new CanaraApiCls();
            //            if (await dataCls.SaveCanaraBatchInitiateResponseAsync(successResponseFinal) > 0)
            //            {
            //                _response.IsSuccess = true;
            //                _response.SetSuccess();
            //            }
            //            else
            //            {
            //                _response.IsSuccess = false;
            //                _response.FailureMessage = "DB Error Response";
            //            }
            //        }
            //        else
            //        {
            //            _response.IsSuccess = false;
            //            _response.FailureMessage = responseJSON.Response.Body.txnSummary.ErrorDec;
            //        }

            //    }
            //    else if (jsonResponse["ErrorResponse"] != null)
            //    {
            //        // Error response, deserialize to CanraErrorResponse
            //        CanraErrorResponse errorResponse = new JavaScriptSerializer().Deserialize<CanraErrorResponse>(responseContent);
            //        _response.IsSuccess = false;
            //        _response.FailureMessage = "Error Response"; //errorResponse.ErrorResponse.;
            //    }
            //    else
            //    {
            //        // Handle unknown response format or other cases
            //        _response.IsSuccess = false;
            //        _response.FailureMessage = "No Response Type"; //errorResponse.ErrorResponse.;
            //    }
            //}
            //else
            //{
            //    // Handle unsuccessful response here
            //    var errorMessage = await response.Content.ReadAsStringAsync();
            //    _response.IsSuccess = false;
            //    _response.FailureMessage = "Response Faied! Contact Anministrator"; //errorResponse.ErrorResponse.;
            //}

        }
        catch (HttpRequestException ex)
        {
            _response.SetFailure("HTTP request failed:" + ex.Message);
        }
        catch (Exception ex)
        {
            _response.SetFailure("An unexpected error occurred: " + ex.Message);
        }
        // Return the failure response in case of exceptions
        return "";

    }
    public HttpWebResponse BatchInqiryApiCall(string _sign, string _finalLoadJSON)
    {
        try
        {
            // Get the current UTC time
            DateTime utcNow = DateTime.UtcNow;
            string time_stamp = utcNow.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");

            // Set TLS version (use the appropriate version)
            var client = new HttpClient();

            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

            string URL = BaseUrL + BatchInquiryURL;
            var request = (HttpWebRequest)WebRequest.Create(URL);
            request.Method = "POST";
            request.Headers.Add("x-client-id", "E74yUTzCS47BQtlD9CJkvUA7A21rdO54");
            request.Headers.Add("x-client-secret", "u9AJhtiqRUy10hQzil2Dq8P2ZHaHNG8t");
            request.Headers.Add("x-api-interaction-id", ApiInteractionId);
            request.Headers.Add("x-timestamp", time_stamp);
            request.Headers.Add("x-client-certificate", ClientCertificate);
            request.Headers.Add("x-signature", _sign);
            request.Headers.Add("Cookie", "jj; TS0144dd86=01aee676795757bf75ce0c1b08b63a49fb87e907c4e47191da6092fac5be82ea7be8290afea1bb1366f9cc0f1af4a9ea29c9633604; TS0144dd86028=0192426cf82f696d6274cd189e0d0b9e00fa4fc05229a7754e7f8bc60c15d09e665df6a6a2d9b18636af5cabcb46b9181cb4ba346b");
            request.Headers.Add("X-Forwarded-For", "kkk");
            request.Accept = "application/json";
            request.ContentType = "application/json";


            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                string json = _finalLoadJSON;
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }
            var httpResponse = (HttpWebResponse)request.GetResponse();
            var cType = httpResponse.ContentType;
            ErrorLog.LoginfoToLogFile("Canara httpResponse=" + httpResponse + "Content-Type : " + cType, "BatchInqiryApiCall");

            if (httpResponse != null)
            {
                ErrorLog.LoginfoToLogFile("Canara httpResponse=" + httpResponse + "Content-Type : " + cType, "BatchInqiryApiCall");
            }
            else
            {
                ErrorLog.LoginfoToLogFile("Canara httpResponse is null" + httpResponse, "BatchInqiryApiCall");
            }

            return httpResponse;
        }
        catch (Exception ex)
        {
            ErrorLog.LoginfoToLogFile("Canara Catch " + ex.Message, "BatchInqiryApiCall");
            // Log or handle the exception
            return null;
        }
    }

    public HttpWebResponse BatchInitApiCall(string _sign, string _finalLoadJSON)
    {
        try
        {
            // Get the current UTC time
            DateTime utcNow = DateTime.UtcNow;
            string time_stamp = utcNow.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");

            // Set TLS version (use the appropriate version)
            var client = new HttpClient();

            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            //ServicePointManager.Expect100Continue = true;
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
            //       | SecurityProtocolType.Tls11
            //       | SecurityProtocolType.Tls12
            //       | SecurityProtocolType.Ssl3;

            string URL = BaseUrL + BatchInitiateURL;
            var request = (HttpWebRequest)WebRequest.Create(URL);
            request.Method = "POST";
            request.Headers.Add("x-client-id", ClientIdOrKey);
            request.Headers.Add("x-client-secret", ClientSecret);
            request.Headers.Add("x-api-interaction-id", ApiInteractionId);
            request.Headers.Add("x-timestamp", time_stamp);
            request.Headers.Add("x-client-certificate", ClientCertificate);
            request.Headers.Add("x-signature", _sign);
            request.Headers.Add("Cookie", "jj; TS0144dd86=01aee676795757bf75ce0c1b08b63a49fb87e907c4e47191da6092fac5be82ea7be8290afea1bb1366f9cc0f1af4a9ea29c9633604; TS0144dd86028=0192426cf82f696d6274cd189e0d0b9e00fa4fc05229a7754e7f8bc60c15d09e665df6a6a2d9b18636af5cabcb46b9181cb4ba346b");
            //request.Headers.Add("X-Forwarded-For", "10.254.4.80");
            //request.Headers.Add("X-Forwarded-For", "10.90.90.85");
            //request.Headers.Add("X-Forwarded-For", "10.90.90.85");
            request.Headers.Add("X-Forwarded-For", "119.226.49.122");
            request.Accept = "application/json";
            request.ContentType = "application/json";

            //var request = new HttpRequestMessage(HttpMethod.Post, BaseUrL);
            //request.Headers.Add("x-client-id", ClientIdOrKey);
            //request.Headers.Add("x-client-secret", ClientSecret);
            //request.Headers.Add("x-api-interaction-id", ApiInteractionId);
            //request.Headers.Add("x-timestamp", "20231127");
            //request.Headers.Add("x-client-certificate", ClientCertificate);
            //request.Headers.Add("x-signature", _sign);
            //request.Headers.Add("Cookie", "jj; TS0144dd86=01aee676795757bf75ce0c1b08b63a49fb87e907c4e47191da6092fac5be82ea7be8290afea1bb1366f9cc0f1af4a9ea29c9633604; TS0144dd86028=0192426cf82f696d6274cd189e0d0b9e00fa4fc05229a7754e7f8bc60c15d09e665df6a6a2d9b18636af5cabcb46b9181cb4ba346b");
            //request.Headers.Add("X-Forwarded-For", "kkk");
            //request.Headers.Add("Accept", "application/json");
            //var content = new StringContent(_finalLoadJSON, null, "application/json");
            //request.Content = content;

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                string json = _finalLoadJSON;
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }
            var httpResponse = (HttpWebResponse)request.GetResponse();
            var cType = httpResponse.ContentType;
            ErrorLog.LoginfoToLogFile("Canara httpResponse=" + httpResponse + "Content-Type : " + cType, "BatchInitApiCall");

            if (httpResponse != null)
            {
                ErrorLog.LoginfoToLogFile("Canara httpResponse=" + httpResponse + "Content-Type : " + cType, "BatchInitApiCall");
            }
            else
            {
                ErrorLog.LoginfoToLogFile("Canara httpResponse is null" + httpResponse, "BatchInitApiCall");
            }

            return httpResponse;
        }
        catch (Exception ex)
        {
            ErrorLog.LoginfoToLogFile("Canara Catch " + ex.Message, "BatchInitApiCall");
            // Log or handle the exception
            return null;
        }
    }
    public async Task<string> BatchInitApiCallAsync(string _sign, string _finalLoadJSON)
    {
        try
        {


            // Get the current UTC time
            DateTime utcNow = DateTime.UtcNow;
            string time_stamp = utcNow.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");

            // Set TLS version (use the appropriate version)
            var client = new HttpClient();

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                   | SecurityProtocolType.Tls11
                   | SecurityProtocolType.Tls12
                   | SecurityProtocolType.Ssl3;
            //ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

            var request = (HttpWebRequest)WebRequest.Create("https://uat-apibanking.canarabank.in/uat/apib/payment/batch-initiation");
            request.Method = "POST";
            request.Headers.Add("x-client-id", ClientIdOrKey);
            request.Headers.Add("x-client-secret", ClientSecret);
            request.Headers.Add("x-api-interaction-id", ApiInteractionId);
            request.Headers.Add("x-timestamp", "20231127");
            request.Headers.Add("x-client-certificate", ClientCertificate);
            request.Headers.Add("x-signature", _sign);
            request.Headers.Add("Cookie", "jj; TS0144dd86=01aee676795757bf75ce0c1b08b63a49fb87e907c4e47191da6092fac5be82ea7be8290afea1bb1366f9cc0f1af4a9ea29c9633604; TS0144dd86028=0192426cf82f696d6274cd189e0d0b9e00fa4fc05229a7754e7f8bc60c15d09e665df6a6a2d9b18636af5cabcb46b9181cb4ba346b");
            request.Headers.Add("X-Forwarded-For", "kkk");
            request.Accept = "application/json";
            request.ContentType = "application/json";

            //var request = new HttpRequestMessage(HttpMethod.Post, BaseUrL);
            //request.Headers.Add("x-client-id", ClientIdOrKey);
            //request.Headers.Add("x-client-secret", ClientSecret);
            //request.Headers.Add("x-api-interaction-id", ApiInteractionId);
            //request.Headers.Add("x-timestamp", "20231127");
            //request.Headers.Add("x-client-certificate", ClientCertificate);
            //request.Headers.Add("x-signature", _sign);
            //request.Headers.Add("Cookie", "jj; TS0144dd86=01aee676795757bf75ce0c1b08b63a49fb87e907c4e47191da6092fac5be82ea7be8290afea1bb1366f9cc0f1af4a9ea29c9633604; TS0144dd86028=0192426cf82f696d6274cd189e0d0b9e00fa4fc05229a7754e7f8bc60c15d09e665df6a6a2d9b18636af5cabcb46b9181cb4ba346b");
            //request.Headers.Add("X-Forwarded-For", "kkk");
            //request.Headers.Add("Accept", "application/json");
            //var content = new StringContent(_finalLoadJSON, null, "application/json");
            //request.Content = content;

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                string json = _finalLoadJSON;
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            string objResponse = "";
            var httpResponse = (HttpWebResponse)request.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                objResponse = result.ToString();
            }



            await Task.Delay(1000);
            //var response = await client.SendAsync(request).ConfigureAwait(false);
            // Handle the response
            //if (response.IsSuccessStatusCode)
            //{
            //    var responseContent = await response.Content.ReadAsStringAsync();
            //    // Process the response content as needed
            //}
            //else
            //{
            //    // Handle the error response
            //}
            return objResponse;
        }
        catch (Exception ex)
        {
            // Log or handle the exception
            return null;
        }
    }
    public async Task<CanaraApiConfiguration> LoadApiConfiguration()
    {
        var baseUri = ConfigurationManager.AppSettings["CanaraApiBaseURL"];
        if (baseUri == null)
        {
            // Handle the case where the configuration is missing
            return null;
        }

        return new CanaraApiConfiguration
        {
            BaseURL = baseUri,
            BatchInitiateURL = ConfigurationManager.AppSettings["CanaraBatchInitiateURL"],
            ClientIdOrKey = ConfigurationManager.AppSettings["CanaraClientIdOrKey"],
            ClientSecret = ConfigurationManager.AppSettings["CanaraClientSecret"],
            ClientCertificate = ConfigurationManager.AppSettings["CanaraClientCertificate"],
            ApiInteractionId = ConfigurationManager.AppSettings["CanaraApiInteractionId"],
            SharedSymmetricKey = ConfigurationManager.AppSettings["CanaraSharedSymmetricKey"],
            SSLPrivateKey = ConfigurationManager.AppSettings["CarzonrentSSLPrivateKey"],
            SSLPublicKey = ConfigurationManager.AppSettings["CarzonrentSSLPublicKey"]
        };
    }
    public string GetEncryptedData(string DATA_TO_ENCRYPT)
    {
        System.Security.Cryptography.RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
        rsa = new RSACryptoServiceProvider(2048);
        Aes aes1 = Aes.Create();
        aes1.Key = digestTest();
        string enc_token = Jose.JWT.Encode(DATA_TO_ENCRYPT, aes1.Key, JweAlgorithm.A256KW, JweEncryption.A128CBC_HS256);
        //string dec_token = Jose.JWT.Decode(enc_token, aes1.Key, JweAlgorithm.A256KW, JweEncryption.A128CBC_HS256);

        return enc_token;
    }
    public async Task<string> GetEncryptedDataAsync(string DATA_TO_ENCRYPT)
    {
        System.Security.Cryptography.RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
        rsa = new RSACryptoServiceProvider(2048);
        Aes aes1 = Aes.Create();
        aes1.Key = digestTest();
        string enc_token = Jose.JWT.Encode(DATA_TO_ENCRYPT, aes1.Key, JweAlgorithm.A256KW, JweEncryption.A128CBC_HS256);
        //string dec_token = Jose.JWT.Decode(enc_token, aes1.Key, JweAlgorithm.A256KW, JweEncryption.A128CBC_HS256);

        return enc_token;
    }
    public byte[] digestTest()
    {
        int numChars = SHARED_SYMMETRIC_KEY.Length;
        byte[] bytes = new byte[numChars / 2];

        for (int i = 0; i < numChars; i += 2)
        {
            bytes[i / 2] = Convert.ToByte(SHARED_SYMMETRIC_KEY.Substring(i, 2), 16);
        }

        return bytes;
    }
    public string SignDataTest(string data, string privateKeyPEM)
    {
        byte[] dataBytes = Encoding.UTF8.GetBytes(data);

        using (var reader = new StringReader(privateKeyPEM))
        {
            var pemReader = new PemReader(reader);
            //var keyPair = (AsymmetricCipherKeyPair)pemReader.ReadObject();


            var privateKeyParams = (RsaPrivateCrtKeyParameters)pemReader.ReadObject();
            var rsaParameters = DotNetUtilities.ToRSAParameters(privateKeyParams);


            //var rsaParameters = DotNetUtilities.ToRSAParameters((RsaPrivateCrtKeyParameters)keyPair.Private);
            using (var rsa = new RSACryptoServiceProvider())
            {
                rsa.ImportParameters(rsaParameters);

                // Use SHA-256 as the hash algorithm
                using (var sha256 = SHA256.Create())
                {
                    byte[] signatureByte = rsa.SignData(dataBytes, sha256);
                    return Convert.ToBase64String(signatureByte);
                }
            }
        }
    }
    static bool VerifyDataTest(string data, string sign, string publicKeyPEM)
    {
        byte[] dataBytes = Encoding.UTF8.GetBytes(data);
        byte[] signatureBytes = Convert.FromBase64String(sign);

        using (var reader = new StringReader(publicKeyPEM))
        {
            var pemReader = new PemReader(reader);
            var publicKey = pemReader.ReadObject() as RsaKeyParameters;

            if (publicKey == null)
            {
                // Handle the case where publicKey is null
                throw new InvalidOperationException("Public key is null or invalid.");
            }

            var rsaParameters = DotNetUtilities.ToRSAParameters(publicKey);
            using (var rsa = new RSACryptoServiceProvider())
            {
                rsa.ImportParameters(rsaParameters);

                // Use SHA-256 as the hash algorithm
                using (var sha256 = SHA256.Create())
                {
                    return rsa.VerifyData(dataBytes, sha256, signatureBytes);
                }
            }
        }

    }

    //complete success sample request by canara 
    public async Task<string> PostBatch1(string BatchRequestID)
    {
        string responseContent = "";
        try
        {
            // Set TLS version (use the appropriate version)
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            using (var client = new HttpClient())
            {
                var request = new HttpRequestMessage(HttpMethod.Post, "https://uat-apibanking.canarabank.in/uat/apib/payment/batch-initiation");
                request.Headers.Add("x-client-id", "HyAnrycZpMuiH20O5wcZNGiRFReX7KAP");
                request.Headers.Add("x-client-secret", "etjCQeqlGbvySmli7XmaPznnWtzpPoPh");
                request.Headers.Add("x-api-interaction-id", "f77cd3c6-69d5-4729-b530-c637c78a696e");
                request.Headers.Add("x-timestamp", "2023-12-14T09:53:41.916Z");
                request.Headers.Add("x-client-certificate", "MIIDkDCCAngCCQDyXxxC5oRTgjANBgkqhkiG9w0BAQsFADCBiTELMAkGA1UEBhMCSU4xCzAJBgNVBAgMAktMMRIwEAYDVQQHDAlCQU5HQUxPUkUxDzANBgNVBAoMBkNBTkFSQTEPMA0GA1UECwwGQ0FOQVJBMQ8wDQYDVQQDDAZDQU5BUkExJjAkBgkqhkiG9w0BCQEWF3JlbmppdGhnQGNhbmFyYWJhbmsuY29tMB4XDTIzMDcyNTA1Mjc1OVoXDTI0MDcyNDA1Mjc1OVowgYkxCzAJBgNVBAYTAklOMQswCQYDVQQIDAJLTDESMBAGA1UEBwwJQkFOR0FMT1JFMQ8wDQYDVQQKDAZDQU5BUkExDzANBgNVBAsMBkNBTkFSQTEPMA0GA1UEAwwGQ0FOQVJBMSYwJAYJKoZIhvcNAQkBFhdyZW5qaXRoZ0BjYW5hcmFiYW5rLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALBn4r5THaRGkeMM/ZKC8KRIRoy5Lyai3wfmonMFKCHLJfLYiiy7KttCkauZIQoKBqYm4YmpEFSBlex1qXRCmKdOLNph0NFo8/4MKfKUr3IATJMQZTp2ZMdFQx7Vp79jcSdNZKxgcACU8c9Unn6kkuS6o0chdmdkerI8oBfN6Nxje+U4PrDqqk8oK/d0a+/uyqnCeEHrtMZifTMSGbu2eNxDqhkauM1Uhln0gCa1+I0k97mH5fKy4pDqzLl6ABdi4XQkMgdbZjkloeqO2tbMDfHG6eP18CrXQWFDfuRVUcLc35j2xdjFgenjMeiDnzPH7bVlg/Iq5bwlfy7KEGmPgu0CAwEAATANBgkqhkiG9w0BAQsFAAOCAQEAlIB7CyTDUF5R+q2/OEs/WHFTwnOoTbTeg3LTeTnbPVcn8LT0n36xZTHNHcDnjj2B4lS5W91fp5q8DFnTK1d8mmTD1a9wSZaTIR1t0nvFIacmxR+yP7QljaQ4KiFTD+saRFWM/4yatvzqCDvNlJpkuOlIG9KtlAOxbxKQgF50Jmst2dGWFtrX9mNUhhVvfSuVOdxOqiunVXYTJwVHc+IHw33RkNiEEiPtH7kk8iYzb845+KbDv9GhQPiNgf682QQLZJmh2wtQ1ZR+DY4cR7V/eblb+2RmEPV6HpvMLV9TvcXgE8RpFmB7YSNj+rmah1JHjak/COQYcgrPzeFRDyr34Q==");
                request.Headers.Add("x-signature", "PIGHaBie01QGV62PHXg6J1QNrH5WcpmstzrGXUkp/ZOefM81kw5hTgSxsGR6v/uYvAm0xgIP/VtZMi/GrhlozlbW6hKZLLUZ4RmP/8BTFIhHNatsF2m49/mB5yB09DdgXLkhUM060AfAutW1I1vkq8ZewfOJASRkwS/0r4ekprAJFI73FDoC/X3j12uMhRdZEedmAhHUgtN4rC3miGkMiyFAIJci6obJu83/S3FBMJUhkETUqAX2Mi7iLIDX5jgrTgjK1nJ7QCzRjDE3LrORbZejGNfiSBg3B+6rK9epQrOY7VhG5G4dB/huhnO0L1JaidnTWzbJQY/usNd40xnM7A==");
                request.Headers.Add("Cookie", "jj; TS0144dd86=01aee676795757bf75ce0c1b08b63a49fb87e907c4e47191da6092fac5be82ea7be8290afea1bb1366f9cc0f1af4a9ea29c9633604; TS0144dd86028=0192426cf82f696d6274cd189e0d0b9e00fa4fc05229a7754e7f8bc60c15d09e665df6a6a2d9b18636af5cabcb46b9181cb4ba346b");
                request.Headers.Add("X-Forwarded-For", "kkk");
                request.Headers.Add("Accept", "application/json");
                var content = new StringContent("{\n    \"Request\": {\n        \"Body\": {\n            \"encryptData\": \"eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiQTI1NktXIn0.boigjwFXHwukiI7ADRCU-k0tzNyM3_pmOS3T967tgq3UTfRvdxGAvA.bO49cb_OJ-NTA1WhKnlO6w.-ZTYkDq29SEj6NwjAA2yNGNXLK-6XPH4cTrz8pXQfeOeXUH2a6F91Ak062fE1XcrTXa6yRT0q6smMm4NZhfe1WweIuvjpXX06K24g4fW0nbraZxrrkbI7IHCiIedCrh8UjZ4Pt4WYH2bNY4r2KdQy9lUk27iQ8yi14WFU5MwaZlKVbUZn9ECM3ZHb82gE-m_RPMKEJJS98jYE97ETQvUINeQESYzkGltM5W8Z6-RC-kNnp27VWFtmBLEM2EVAcKhaSiIM_qfAcES8SpzCXnos7t8ctTo_OmP6_SZXGcpn12QIui9tOAMCNqUNKM0gceiZ6sn_GAO6YBaBUcPeXiJFTHuJ9_3oBVA737Dy3VdY9_mi3RTo44tjF5bbtjHPHGLsjGePxQN_OuIapUW0M1z1Ox6PgUKd9-Kp2hqbTe-nU4tNQZgBvlQG6dg6-6IGjZxis3QBl-zDgul9HF-Qlz6trkFoO8R7SA9xk73mPvQ5b6lC96OkAl29c-9RdlQis5mwIdPG2NRbqWxEadE1_u7twvI1mWShwV-gb5GBZmbd0gfiqolz4M2pDwmlAa9mkKIuNOxczJiQ5iwolTXGrOJP5VVftdHodpd7MTykTQ6gefgf2sj8nh7im3HnYjN1XrEVKkogjmWVvcYVHrdKI6y2h85o0lNiJ-SO2iUxRbssci6P47tpdmpOqoj15ZY4zG3U3R-3alD6Rtw3SpQz61Nz7bCYwNorxPe9xrx2_YEw_qe3OGhdA4K4GnLQa8rdwK6wbDWhU5vZlykmc4sbJoBv0Bpt4gCcdgr_IEtfItg8-6lb7NXtAroMMBNQrhmNvqtH5kdZxBMA3nnrEhWV9aMgIz4xpJ3cqzIIYwpuq9EvXb3G1m_tvrBuexxhLuhXIUejSVASVchrf_YQnyEOG-fDH6gZtXHZV0uO_KysQn4k-G0oSp8vrqXzYCA-HZy7BCOCxBdwTij2l65BLUUkUemaUEi1puY3DCxhtZMHf7UVoaB6MTuJvbGUyGK-j7VEkgy6BWojRUneTzhPbgun3GPUiKi4fYJ-PemrgwBKnoahH24od-WsljNz_ZvYcrQS4dZ0wbAeUQJtU7myIfqvQAflpmhXpgdMvNwrSxkNnj2bcMw5jw8dIVDfjb0fBOCzBKMgEorWPn03VALJduc9yuk2vy9ZKoYwd5CE8f41-Ctrjz9c6zUK24kWQ4_mW7iKQts4xOBXtHA1A35Jl7UOWt6DdfTxbay0dP-4lE8ycRZMb1s7QMNDWDuapvTyHKCMjfuvVY1UKtwZRzBx5ZwClyxNm5Duhg-4AlymKhfkwpOjXSRECj5FQ3wPzjRrHAiJbwyYy7Os_tP5knnXsoTrGUt2syR-mnMsMg1m7s_dNvFzm41fOYSYj0s_RckecfiW7ZAGAURsdXdepRrapyv5tmk7NpOAfVXwdmzaDTjDVFeMlfKY9Mdu01S7SllPPPZm7UvOSeP5KmpJimqta0Mg-pUoMjjHW9pTzk59h1UZZWS3sWpsV3ZnJEUBl3cZ2wqj2daugAQtLmBbxkdYNr_hoLkBDGXEPT7hcNdQMeOBNMnZaB2zIN08JTy8vXgY1rFpbXd-jBAyXgZKszaflhc0dSbTP8BDslaTW6GZUePsoPDqmwO8h9tjF6dwRKL0RXESXmZARBsEVloVhQiG8b5LK5Lio1rHLEBT4mZRRt-5a0e6kyNDYB-lGj-Gxxh1q9WCfxrCjvIAT38mxTK_Z3rEQhx1MYlqoWRA8LBSfG7zi12N3W9r5xhAiiF50IyuG8jS0qLcoGKnBTiZcLQx0BOTugWxLiXWNX6j2qE2Y9klmVDhlIH0rLxFbTH1g3oxRiUTDjreme19Ys0WrLNzGryH9QQ2u6OIu_IHR4etMFIp6ObrJFnx4ZYEBbKB11TPDU-yOX6IPYZYbH9ILGcbiAdeX7G9Krx6YsJFAMdXAjmrwNPgRCvYV2bG1mBoygWwwekxJz8diwMihoH82RmR3wabzfMhk80riChOwsiVFeTE3_VibtuvPJUlFKbLE1HldcECQmbgVSkks1V3DelpP_lFZ35To58AsROlS8-aRBFdCUZU2jOwPaprqXjs1cJH7ZlwbEwz-lvUTJWvFO3-2Fx950Fx4wzSwwxpK4DzzO_VkElAUMwriQ1T1hDrufF5yn-CIOg0eBgRy4TJPgUQRbIITZG4RMi3rTV5cUrEkC004-JVZa-3XhhrLXJLk47n9GzsQwvaXTm7NDmzeeF3pFtcF2uQCMr1VRQtPMPhS4SzTqffUEsexSPpUFwdX-N7gG1d-lEbW7Mlz6Qv7OvlDhT51mz858e2uVb2BT8Co-2nWWQRmdClQ1oUFkAaN_E5kizNdLupe-OKb1luD4suk8yg_l0xrrpmhZ2X_ilGrb4MMpXNX2KGJYkSde64KpN6U5sO3471o4kRHdE76uLlEkpu3Bdc4-8GnNiMvUr3EJ_o5qyoZjRnTWchyVDrfSWOU4xEOou3bACWn-WVWXDYLq5T6HLsuaeFBzVg1kS3_jBz2jRdYq9jOF-z-Z-B6_nPh_XsxzZ0EF5lVsVtoIU9hxHm5NAHRen5Uqnlv5E-CM00S4riBTBxfrnrbpkCBGPDgXd4bn2EnXzpnAd45KopdR1ytHF2yeFLiZNWTWfjEGmN7qZxkHTsdJlZSnPYEqyqv027Ym7uZNe6sVm_tYcHkKHHX_3UD8RtGp1SktLAarteiyWx2cQ2aB4kbhliJzXsih1t2k6bq3P-FIJzGSuttX1jK3C9msDWAJzeHldjOu0ikm2AGpsBjGiO5Pv-QQ7dQKMeQzY5n2bNRmJ8ih-lJQfdcaas5cbIPNd9Vrl7xy-hgw4EWD1Yep4vix7lIdqGC8h5e5UkWTAXvIjPcu-lYnadC_yhiSjCD00RG1ZiHOLP1Ow6qEe0amsrSrAOyHEMfpzW0bUtJsx6Pi5pYTbyTCDw5jTjbzqigQE3diCHWFGO8q1foW96IH0-aRBNhzoc1V81U9iLpxXgB0WMwxcaTiYqJHSKNDxONNVB6XRVebvB5nAw_pu1OYb6GRNpPeBSJISj_mmww7Hv0EYduveZ5fyjUnURIer97TKcXUjwTDoXRIkL2N_0RK55oaYBQgLs3X5F7tSSowV5S9NPcgOgL7j3HMvZ9QphRlYrEA2COa5UXbA7ng02tHJALVgl4BztTVGtc8cgPeP3ojmdIDe-CPrmH2XMvhXJkPybXw_U7sCoh83QSa3jvqtAPgYOkvVlPn3maX_Mgzz8oHj9ne-MM3DmcBW8Irm4Y0J2ruRX6AJBlcw3v_XbWLdifBhzzgX6j6YFyL8-x8nF4l5GjsNFrNoAgR-UDtECPklLkEA_3V0CUm74FKoAA8HVl_Qsh73el3wEObJoBUruD5ACFDuRA93EoAvK4ogIvNI1N6Vkqg6xG_BHj7Ccyh32YnCyyABCBWTrLiv0swUw6doFVaaootSiwNSnOHiYn0_tKHeCS9mBZ_HjWs.NhG2bMlbqwKH4TDC2gCdrg\"\n        }\n    }\n}", null, "application/json");
                request.Content = content;

                var response = await client.SendAsync(request);

                // Check if the request was successful
                if (response.IsSuccessStatusCode)
                {
                    // Optionally handle the response content here
                    responseContent = await response.Content.ReadAsStringAsync();
                    // Do something with the response content
                }
                else
                {
                    // Handle unsuccessful response here
                    responseContent = await response.Content.ReadAsStringAsync();
                    // Log or throw an exception based on the use case
                }
            }
        }
        catch (Exception ex)
        {
            // Handle exceptions here
        }

        return responseContent;
    }
    public CanraRequestFinal GetEncryptedData(dynamic requestObject)
    {
        JavaScriptSerializer js = new JavaScriptSerializer();
        string dataToEncrypt = js.Serialize(requestObject.Request.Body.encryptData);

        CanraRequestFinal requestFinalObject = new CanraRequestFinal
        {
            Request = new CanraReqFinal
            {
                Body = new CanraRequestBodyFinal
                {
                    encryptData = GetEncryptedData(dataToEncrypt)
                }
            }
        };
        return requestFinalObject;
    }

    public async Task<CanraRequestFinal> GetEncryptedDataAsync(CanraRequest requestObject)
    {
        JavaScriptSerializer js = new JavaScriptSerializer();
        string dataToEncrypt = js.Serialize(requestObject.Request.Body.encryptData);

        CanraRequestFinal requestFinalObject = new CanraRequestFinal
        {
            Request = new CanraReqFinal
            {
                Body = new CanraRequestBodyFinal
                {
                    encryptData = await GetEncryptedDataAsync(dataToEncrypt)
                }
            }
        };
        return requestFinalObject;
    }

    private Task<CanraRequest> GetRequestPlainBodyAsync(string batchRequestID)
    {
        string senderAccountNo = ConfigurationManager.AppSettings["CanaraSenderAccountNo"];
        string senderAccountName = ConfigurationManager.AppSettings["CanaraSenderAccountName"];
        string customerID = ConfigurationManager.AppSettings["CanaraCustomerID"];
        string authorization = ConfigurationManager.AppSettings["CanaraAuthorization"];
        CanraRequest requestObject = null;

        try
        {
            // Assuming you have a CanraRequest class with appropriate properties
            #region running hardcode
            requestObject = new CanraRequest
            {
                Request = new CanraRequestBodyReq
                {
                    Body = new CanraRequestBody
                    {
                        encryptData = new CanraReqEncryptData
                        {
                            Authorization = "Basic " + authorization,
                            Key = "3iins2570n1pu4p52qie9c16dr",
                            CustomerID = customerID,
                            TotAmt = "3", //hardcode to remove
                            TxnCnt = "3", //hard code to remove
                            DatTxn = "20231127", //to automate
                            BatchRequestID = "CARZ241120230448",//BATCH in the sheet 
                            TxnDtls = new TxnDetailList
                            {
                                Txn = new List<TxnDetail>
                                        {
                                            new TxnDetail
                                            {
                                                TxnRefNo = "1031658", //CHECK_NUMBER in the sheet
                                                DrAcct = senderAccountNo, //hardcode
                                                SndrNm = senderAccountName, //hardcode
                                                TxnAmt = "1", //AMOUNT in the sheet
                                                TxnType = "NEFT", //column to get from Inderjeet
                                                BenefIFSC = "UTIB0000553",  //IFSC in the sheet
                                                BenefAcNo = "553010200000055", //VENDOR_ACCOUNT in the sheet
                                                BenefAcNm = "24X7 TRANSLINES", //BENEFICIARY_NAME in the sheet
                                                Nrtv = "REMITTANCE1", //in case NEFT then REMITTANCE1 else REMITTANCE2
                                            },
                                            new TxnDetail
                                            {
                                                TxnRefNo = "1045504", //CHECK_NUMBER in the sheet
                                                DrAcct = senderAccountNo, //hardcode
                                                SndrNm = senderAccountName, //hardcode
                                                TxnAmt = "1", //AMOUNT in the sheet
                                                TxnType = "RTGS", //column to get from Inderjeet
                                                BenefIFSC = "ICIC0001354",  //IFSC in the sheet
                                                BenefAcNo = "135401533921", //VENDOR_ACCOUNT in the sheet
                                                BenefAcNm = "NIKHIL ANAND", //BENEFICIARY_NAME in the sheet
                                                Nrtv = "REMITTANCE2", //in case NEFT then REMITTANCE1 else REMITTANCE2
                                            },
                                            new TxnDetail
                                            {
                                                TxnRefNo = "1045505", //CHECK_NUMBER in the sheet
                                                DrAcct = senderAccountNo, //hardcode
                                                SndrNm = senderAccountName, //hardcode
                                                TxnAmt = "1", //AMOUNT in the sheet
                                                TxnType = "INTRA",
                                                BenefIFSC= "ICIC0000111",
                                                BenefAcNo= "0922108041614",
                                                BenefAcNm= "ASMA NAAZ",
                                                //TxnType = "INTRA", //column to get from Inderjeet  
                                                //BenefIFSC = "IDIB000D006",  //IFSC in the sheet
                                                //BenefAcNo = "965556889", //VENDOR_ACCOUNT in the sheet
                                                //BenefAcNm = "HOLISTIC ENGINEERING PVT LTD", //BENEFICIARY_NAME in the sheet
                                                Nrtv = "REMITTANCE2", //in case NEFT then REMITTANCE1 else REMITTANCE2
                                            }
            }
                            }
                        }
                    }
                }
            };
            #endregion
        }
        catch
        {

        }
        return Task.FromResult(requestObject);
    }
    public string DecodeSuccessEncReponse(string data)
    {
        Aes aes1 = Aes.Create();
        var keyBytes = digestTest();// Convert.FromHexString(SHARED_SYMMETRIC_KEY);
        aes1.Key = keyBytes;
        string dec_token = JWT.Decode(data, aes1.Key, JweAlgorithm.A256KW, JweEncryption.A128CBC_HS256);
        return dec_token;
    }

    #region Code Not in use but for reference if required
    //public void PostBatch()
    //{
    //    JavaScriptSerializer js = new JavaScriptSerializer();
    //    CanraRequestModel obj = new CanraRequestModel();
    //    Requesttrandetails abc = new Requesttrandetails();
    //    List<Requesttrandetails> xyz = new List<Requesttrandetails>();

    //    CanraRequestBodyModel Bdy = new CanraRequestBodyModel();
    //    CanraRequestEncModel enc = new CanraRequestEncModel();

    //    string url = ConfigurationManager.AppSettings["CanaraURL"];
    //    string CanaraCustomerID = ConfigurationManager.AppSettings["CanaraCustomerID"];
    //    string CanaraAuthorization = ConfigurationManager.AppSettings["CanaraAuthorization"];
    //    string CanaraClientID = ConfigurationManager.AppSettings["CanaraClientID"];
    //    string CanaraClientSecret = ConfigurationManager.AppSettings["CanaraClientSecret"];
    //    string CanaraClientCertificate = ConfigurationManager.AppSettings["CanaraClientCertificate"];
    //    string CanaraInteractionId = ConfigurationManager.AppSettings["CanaraInteractionId"];

    //    obj.Authorization = "Basic " + CanaraAuthorization;
    //    obj.CustomerID = CanaraCustomerID;
    //    obj.TotAmt = "1"; //hardcode to remove
    //    obj.TxnCnt = "1"; //hard code to remove
    //    obj.DatTxn = "20231127"; //to automate
    //    obj.BatchRequestID = "CARZ241120230448"; //BATCH in the sheet 

    //    //Transaction Start
    //    abc.TxnRefNo = "1031658"; //CHECK_NUMBER in the sheet
    //    abc.DrAcct = "0307256054849"; //hardcode
    //    abc.SndrNm = "Carzonrent India Pvt Ltd."; //hardcode
    //    abc.TxnAmt = "1"; //AMOUNT in the sheet
    //    abc.TxnType = "NEFT"; //column to get from Inderjeet
    //    abc.BenefIFSC = "UTIB0000553";  //IFSC in the sheet
    //    abc.BenefAcNo = "553010200000055"; //VENDOR_ACCOUNT in the sheet
    //    abc.BenefAcNm = "24X7 TRANSLINES"; //BENEFICIARY_NAME in the sheet
    //    abc.Nrtv = "REMITTANCE1"; //in case NEFT then REMITTANCE1 else REMITTANCE2
    //    xyz.Add(abc);

    //    abc.TxnRefNo = "1045504"; //CHECK_NUMBER in the sheet
    //    abc.DrAcct = "0307256054849"; //hardcode
    //    abc.SndrNm = "Carzonrent India Pvt Ltd."; //hardcode
    //    abc.TxnAmt = "1"; //AMOUNT in the sheet
    //    abc.TxnType = "RTGS"; //column to get from Inderjeet
    //    abc.BenefIFSC = "ICIC0001354";  //IFSC in the sheet
    //    abc.BenefAcNo = "135401533921"; //VENDOR_ACCOUNT in the sheet
    //    abc.BenefAcNm = "NIKHIL ANAND"; //BENEFICIARY_NAME in the sheet
    //    abc.Nrtv = "REMITTANCE2"; //in case NEFT then REMITTANCE1 else REMITTANCE2
    //    xyz.Add(abc);

    //    abc.TxnRefNo = "1045505"; //CHECK_NUMBER in the sheet
    //    abc.DrAcct = "0307256054849"; //hardcode
    //    abc.SndrNm = "Carzonrent India Pvt Ltd."; //hardcode
    //    abc.TxnAmt = "1"; //AMOUNT in the sheet
    //    abc.TxnType = "INTRA"; //column to get from Inderjeet
    //    abc.BenefIFSC = "IDIB000D006";  //IFSC in the sheet
    //    abc.BenefAcNo = "965556889"; //VENDOR_ACCOUNT in the sheet
    //    abc.BenefAcNm = "HOLISTIC ENGINEERING PVT LTD"; //BENEFICIARY_NAME in the sheet
    //    abc.Nrtv = "REMITTANCE2"; //in case NEFT then REMITTANCE1 else REMITTANCE2
    //    xyz.Add(abc);
    //    //Transaction end

    //    obj.Txn = xyz;

    //    enc.encryptData = obj;
    //    Bdy.Body = enc;
    //    /*
    //    'use strict';
    //    const crypto = require('crypto');
    //    var jwt_secret = "secret";
    //    // enc_header and enc_payload are computed earlier
    //    var signature = crypto.createHmac('sha256', jwt_secret).update(enc_header +"."+ enc_payload).digest('base64');
    //    // ► 6C46KAaZGp6RjbSqGllfdQF7g8vXCp02NTSrz-PzeoI
    //     */
    //    //var tokenDescriptor = new SecurityTokenDescriptor{
    //    //    obj
    //    //};
    //    //var handler = new JwtSecurityTokenHandler();
    //    //string token = handler.CreateEncodedJwt(tokenDescriptor);

    //    var client = new RestClient(url);
    //    client.Timeout = -1;
    //    var request = new RestRequest(Method.POST);

    //    string sign = GetSignature();
    //    request.AddHeader("Content-Type", "application/json");
    //    request.AddHeader("x-client-id", CanaraClientID);
    //    request.AddHeader("x-client-secret", CanaraClientSecret);
    //    request.AddHeader("x-api-interaction-id", CanaraInteractionId);
    //    //request.AddHeader("x-timestamp", "2023-11-24T09:17:13.035Z");
    //    request.AddHeader("x-timestamp", DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss.fffffff'Z'"));
    //    request.AddHeader("x-client-certificate", CanaraClientCertificate);
    //    request.AddHeader("x-signature", sign);
    //    request.AddHeader("Content-Type", "application/json");
    //    request.AddHeader("Cookie", "jj");
    //    request.AddHeader("X-Forwarded-For", "kkk");
    //    request.AddHeader("Accept", "application/json");

    //    var body = js.Serialize(Bdy);

    //    request.AddParameter("application/json", body, ParameterType.RequestBody);
    //    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
    //    IRestResponse responseRest = client.Execute(request);
    //    var result = responseRest.Content;
    //    CanraResponseModel response = new JavaScriptSerializer().Deserialize<CanraResponseModel>(result);
    //}

    public string EncryptAndDecryptData(string DATA_TO_ENCRYPT, string SHARED_SYMMETRIC_KEY)
    {
        RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
        rsa = new RSACryptoServiceProvider(2048);
        Aes aes1 = Aes.Create();
        aes1.Key = Digest(SHARED_SYMMETRIC_KEY);
        string enc_token = Jose.JWT.Encode(DATA_TO_ENCRYPT, aes1.Key, JweAlgorithm.A256KW, JweEncryption.A128CBC_HS256);
        string Dec_token = Jose.JWT.Decode(enc_token, aes1.Key, JweAlgorithm.A256KW, JweEncryption.A128CBC_HS256);

        return "Encrypted Value: " + enc_token + "\nDecrypted Value: " + Dec_token;
    }

    private byte[] Digest(string SHARED_SYMMETRIC_KEY)
    {
        int numChars = SHARED_SYMMETRIC_KEY.Length;
        byte[] bytes = new byte[numChars / 2];

        for (int i = 0; i < numChars; i += 2)
        {
            bytes[i / 2] = Convert.ToByte(SHARED_SYMMETRIC_KEY.Substring(i, 2), 16);
        }

        return bytes;
    }
    private static bool VerifyData(string data, string signtr, string publicKeyPEM)
    {
        byte[] dataBytes = Encoding.UTF8.GetBytes(data);
        byte[] signatureBytes = Convert.FromBase64String(signtr);

        using (var reader = new StringReader(publicKeyPEM))
        {
            var pemReader = new PemReader(reader);
            var publicKey = pemReader.ReadObject() as RsaKeyParameters;

            if (publicKey == null)
            {
                throw new InvalidOperationException("Public key is null or invalid.");
            }

            var rsaParameters = DotNetUtilities.ToRSAParameters(publicKey);
            using (var rsa = new RSACryptoServiceProvider())
            {
                rsa.ImportParameters(rsaParameters);

                using (var sha256 = SHA256.Create())
                {
                    return rsa.VerifyData(dataBytes, sha256, signatureBytes);
                }
            }
        }
    }
    public string GetSignature()
    {
        JavaScriptSerializer js = new JavaScriptSerializer();
        CanraSignatureBodyModel obj = new CanraSignatureBodyModel();
        CanraSignatureModel sign = new CanraSignatureModel();
        SigData dd = new SigData();

        string baseUrl = ConfigurationManager.AppSettings["CanaraApiBaseURL"];
        string signatureURL = ConfigurationManager.AppSettings["CanaraSignatureURL"];

        string sharedSymmetricKey = ConfigurationManager.AppSettings["CanaraSharedSymmetricKey"];
        string carzonrentSSLPrivateKey = ConfigurationManager.AppSettings["CarzonrentSSLPrivateKey"];
        string carzonrentSSLPublicKey = ConfigurationManager.AppSettings["CarzonrentSSLPublicKey"];

        string CanaraKey = ConfigurationManager.AppSettings["CarzonrentSSLPrivateKey"];
        string CanaraCustomerID = ConfigurationManager.AppSettings["CanaraCustomerID"];
        string CanaraAuthorization = ConfigurationManager.AppSettings["CanaraAuthorization"];
        dd.Authorization = "Basic " + CanaraAuthorization;
        dd.Key = CanaraKey;
        dd.CustomerID = CanaraCustomerID;
        dd.BatchRequestID = "BATCHCOIR";
        dd.TxnRefNo = "";
        sign.encryptData = dd;
        obj.Body = sign;

        var client = new RestClient(baseUrl + signatureURL);
        client.Timeout = -1;
        var request = new RestRequest(Method.POST);
        request.AddHeader("Content-Type", "application/json");
        request.AddHeader("Authorization", "Basic R1dKMFM2Q2dlRTAyZ05Wbk1sUHJ5c2RFalZ0eVl6ZDM6ZG5sV1EwR21OSm4zM2lxcA==");
        request.AddHeader("Cookie", "TS0144dd86=01aee6767966058c7d53fa1805f15ba41e906030c6b5fb7049e27036b12ab1fb5af226a8c145fc8ae9ce6c288a10926e8535bbf471; TS0144dd86028=0192426cf8902a03896bb22c2b9d5d04e914b00dfe5860565035b0890f17d989243f96a1614cb6c802198c5e3f6224967d948996d1");
        var body = js.Serialize(obj);
        request.AddParameter("application/json", body, ParameterType.RequestBody);
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        IRestResponse responseRest = client.Execute(request);
        var result = responseRest.Content;
        return result;
    }
    #endregion

    public string GetSignatureNEFT()
    {
        JavaScriptSerializer js = new JavaScriptSerializer();
        CanraSignatureBodyModel obj = new CanraSignatureBodyModel();
        CanraSignatureNEFTModel sign = new CanraSignatureNEFTModel();
        SigDataNEFT dd = new SigDataNEFT();

        string baseUrl = ConfigurationManager.AppSettings["CanaraApiBaseURL"];
        string signatureURL = ConfigurationManager.AppSettings["CanaraSignatureURL"];

        string sharedSymmetricKey = ConfigurationManager.AppSettings["CanaraSharedSymmetricKey"];
        string carzonrentSSLPrivateKey = ConfigurationManager.AppSettings["CarzonrentSSLPrivateKey"];
        string carzonrentSSLPublicKey = ConfigurationManager.AppSettings["CarzonrentSSLPublicKey"];

        string CanaraKey = ConfigurationManager.AppSettings["CarzonrentSSLPrivateKey"];
        string CanaraCustomerID = ConfigurationManager.AppSettings["CanaraCustomerID"];
        string CanaraAuthorization = ConfigurationManager.AppSettings["CanaraAuthorization"];
        string CanaraTranPwd = ConfigurationManager.AppSettings["CanaraTranPwd"];
        string CanaraSenderAccountNo = ConfigurationManager.AppSettings["CanaraSenderAccountNo"];
        //dd.Authorization = "Basic " + CanaraAuthorization;
        //dd.Key = CanaraKey;
        //dd.CustomerID = CanaraCustomerID;
        //dd.BatchRequestID = "BATCHCOIR";
        //dd.TxnRefNo = "";
        dd.Authorization = CanaraAuthorization;
        dd.txnPassword = CanaraTranPwd;
        dd.srcAcctNumber = CanaraSenderAccountNo;
        dd.destAcctNumber = "";
        dd.customerID = CanaraCustomerID;
        dd.txnAmount = "1";
        dd.benefName = "Test";
        dd.userRefNo = "";

        sign.encryptData = dd;
        //obj.Body = sign;

        var client = new RestClient(baseUrl + signatureURL);
        client.Timeout = -1;
        var request = new RestRequest(Method.POST);
        request.AddHeader("Content-Type", "application/json");
        request.AddHeader("Authorization", "Basic R1dKMFM2Q2dlRTAyZ05Wbk1sUHJ5c2RFalZ0eVl6ZDM6ZG5sV1EwR21OSm4zM2lxcA==");
        request.AddHeader("Cookie", "TS0144dd86=01aee6767966058c7d53fa1805f15ba41e906030c6b5fb7049e27036b12ab1fb5af226a8c145fc8ae9ce6c288a10926e8535bbf471; TS0144dd86028=0192426cf8902a03896bb22c2b9d5d04e914b00dfe5860565035b0890f17d989243f96a1614cb6c802198c5e3f6224967d948996d1");
        //var body = js.Serialize(obj);
        var body = js.Serialize(sign);
        request.AddParameter("application/json", body, ParameterType.RequestBody);
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        IRestResponse responseRest = client.Execute(request);
        var result = responseRest.Content;
        return result;
    }

    public CanraNEFTRequest finalload()
    {
        CanraNEFTRequest neftrequest = new CanraNEFTRequest();
        CanraNEFTBodyModel neftbodymdl = new CanraNEFTBodyModel();

        CanraNEFT neftd = new CanraNEFT();
        neftd.txnCurrency = "INR";
        neftd.ifscCode = "HDFC0000792";
        neftd.narration = "SIT NEFT ON ENABLED APP";
        neftd.paymentMode = "N";
        neftd.valueDate = "22-03-2024";

        srcAccountDetail srcAccountDetails = new srcAccountDetail();
        srcAccountDetails.identity = "B001";
        srcAccountDetails.currency = "INR";
        srcAccountDetails.branchCode = "1228";

        destAccountDetail destAccountDetails = new destAccountDetail();
        destAccountDetails.currency = "INR";
        destAccountDetails.identity = "B001";

        neftd.encryptData = GetSignatureNEFT();
        neftd.srcAccountDetails = srcAccountDetails;
        neftd.destAccountDetails = destAccountDetails;
        neftbodymdl.Body = neftd;
        neftrequest.Request = neftbodymdl;

        return neftrequest;
    }

    public HttpWebResponse NEFTPost()
    {
        CanraNEFTRequest requeststr = new CanraNEFTRequest();

        requeststr = finalload();
        JavaScriptSerializer js1 = new JavaScriptSerializer();
        var finalEncRequestJSON = js1.Serialize(requeststr);

        try
        {
            // Get the current UTC time
            DateTime utcNow = DateTime.UtcNow;
            string time_stamp = utcNow.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");

            // Set TLS version (use the appropriate version)
            var client = new HttpClient();

            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

            string URL = BaseUrL + NEFT;
            var request = (HttpWebRequest)WebRequest.Create(URL);
            request.Method = "POST";
            request.Headers.Add("x-client-id", "E74yUTzCS47BQtlD9CJkvUA7A21rdO54");
            request.Headers.Add("x-client-secret", "u9AJhtiqRUy10hQzil2Dq8P2ZHaHNG8t");
            request.Headers.Add("x-api-interaction-id", ApiInteractionId);
            request.Headers.Add("x-timestamp", time_stamp);
            request.Headers.Add("x-client-certificate", ClientCertificate);
            //request.Headers.Add("x-signature", _sign);
            request.Headers.Add("x-signature", requeststr.Request.Body.encryptData);
            request.Headers.Add("Cookie", "jj; TS0144dd86=01aee676795757bf75ce0c1b08b63a49fb87e907c4e47191da6092fac5be82ea7be8290afea1bb1366f9cc0f1af4a9ea29c9633604; TS0144dd86028=0192426cf82f696d6274cd189e0d0b9e00fa4fc05229a7754e7f8bc60c15d09e665df6a6a2d9b18636af5cabcb46b9181cb4ba346b");
            request.Headers.Add("X-Forwarded-For", "65.1.77.54");
            request.Accept = "application/json";
            request.ContentType = "application/json";


            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                //string json = _finalLoadJSON;
                string json = finalEncRequestJSON;
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }
            var httpResponse = (HttpWebResponse)request.GetResponse();
            var cType = httpResponse.ContentType;
            ErrorLog.LoginfoToLogFile("Canara httpResponse=" + httpResponse + "Content-Type : " + cType, "BatchInqiryApiCall");

            if (httpResponse != null)
            {
                ErrorLog.LoginfoToLogFile("Canara httpResponse=" + httpResponse + "Content-Type : " + cType, "BatchInqiryApiCall");
            }
            else
            {
                ErrorLog.LoginfoToLogFile("Canara httpResponse is null" + httpResponse, "BatchInqiryApiCall");
            }

            return httpResponse;
        }
        catch (Exception ex)
        {
            ErrorLog.LoginfoToLogFile("Canara Catch " + ex.Message, "BatchInqiryApiCall");
            // Log or handle the exception
            return null;
        }
    }
}