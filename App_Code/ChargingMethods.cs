﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ChargingWebService
{
    public class ChargingMethods
    {
        ChargingWebService.clsAdmin objadmin = new ChargingWebService.clsAdmin();
        ChargingWebService.ChargingDetails CD = new ChargingWebService.ChargingDetails();
        ChargingWebService.DB db = new ChargingWebService.DB();
        public ChargingMethods()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public ClosingVariables ChargingMethod(int BookingID, ClosingVariables CV
            , int UserID, bool BulkBookingChargeYN = false)
        //BulkBookingChargeYN true in case saved card
        {
            if (CV.chargingstatus == "Charged")
            {
                CV.DSStatus = "C";
                CV.BookingStatus = "C";
                CV.ErrorCode = "Success";
            }
            else if (CV.ErrorCode == "")
            {
                DataSet creditcarddb = new DataSet();
                bool CloseYN = true;
                try
                {
                    creditcarddb = db.SaveCardLogs(BookingID);

                    if (creditcarddb.Tables[0].Rows.Count > 0)
                    {
                        for (int cc = 0; cc <= creditcarddb.Tables[0].Rows.Count - 1; cc++)
                        {
                            if (Convert.ToInt32(creditcarddb.Tables[0].Rows[cc]["Retry"]) > 1 && Convert.ToInt64(creditcarddb.Tables[0].Rows[cc]["diff"]) < 5)
                            {
                                CloseYN = false;
                                CV.ErrorCode = "Please try again after 5 min.";
                            }
                        }
                    }
                }
                catch (Exception card)
                {
                    ErrorLog.LogErrorToLogFile(card, "SaveCardLogs");
                    CloseYN = false;
                    CV.ErrorCode = "Please try again after 5 min.";
                }


                if (CloseYN)
                {
                    bool NewMVPreauthYN = false;

                    DataSet ds = objadmin.GetBookingDetails_Capture(BookingID);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int j = 0; j <= ds.Tables[0].Rows.Count - 1; j++)
                        {
                            CV.ChargingNRequired = Convert.ToBoolean(ds.Tables[0].Rows[j]["ChargingNRequired"]);
                            //DataSet DSCheckCreditCardChargingLink = new DataSet();
                            //DSCheckCreditCardChargingLink = objadmin.CheckCreditCardChargingLink(BookingID);

                            //if (DSCheckCreditCardChargingLink.Tables[0].Rows.Count > 0)
                            //{
                            //    for (int k = 0; k <= DSCheckCreditCardChargingLink.Tables[0].Rows.Count - 1; k++)
                            //    {
                            //        if (DSCheckCreditCardChargingLink.Tables[0].Rows[k]["Provider"].ToString() == "Amex"
                            //            && Convert.ToDouble(DSCheckCreditCardChargingLink.Tables[0].Rows[k]["totalhours"]) > 24) //changed to 24 hours from 72 hours        
                            //        {
                            //            CV.AmexSecondaryChargingYN = true;
                            //        }
                            //    }
                            //}

                            CCValidationData CVC = new CCValidationData();

                            CVC = objadmin.CheckChargedYN(BookingID); //checked if the booking is charged
                            CVC.CCType = Convert.ToInt32(ds.Tables[0].Rows[j]["CCType"]);
                            CVC.ClientCoID = Convert.ToInt32(ds.Tables[0].Rows[j]["ClientCoID"]);
                            CVC.CVVNumber = Convert.ToBoolean(ds.Tables[0].Rows[j]["CVVNumber"]);

                            if (ds.Tables[0].Rows[j]["Miscellaneous1"].ToString() != "") //TravelRequestNumber is the actual value it contains
                            {
                                CVC.AccentureClosing = true;
                            }
                            else
                            {
                                CVC.AccentureClosing = false;
                            }


                            //if (CVC.CCType == 0)
                            if (CVC.CCType == 0 && !Convert.ToBoolean(ds.Tables[0].Rows[0]["CCRegisteredYN"]))
                            {
                                CV.ErrorCode = "ChargingLink Send";
                                CV.DSStatus = "O";
                                CV.BookingStatus = "O";
                            }
                            else if (CV.CCType == 5)
                            {
                                CV.DSStatus = "C";
                                CV.BookingStatus = "C";
                                CV.ErrorCode = "Success";
                                CV.AmexSecondaryChargingYN = false;
                            }
                            //else if (CV.NewCCProcess == 1)
                            //{
                            //    if (CV.CCType == 1 || CV.CCType == 3 || CV.CCType == 8)
                            //    {
                            //        NewChargingResponseParam prr = new NewChargingResponseParam();

                            //        prr = objadmin.Charging_NewCCProcess(BookingID, CV.transactionid, CV.TotalCost);

                            //        if (prr.TransactionStatus == "SUCCESSFUL" && prr.ResponseCode == "E042")
                            //        {
                            //            CV.DSStatus = "C";
                            //            CV.BookingStatus = "C";
                            //            CV.ErrorCode = "Success";
                            //        }
                            //        else
                            //        {
                            //            CV.ErrorCode = "ChargingLink Send";
                            //            CV.DSStatus = "O";
                            //            CV.BookingStatus = "O";
                            //        }
                            //    }
                            //    //charging process
                            //}
                            else if (!CVC.ChargedYN)
                            {
                                if (BulkBookingChargeYN || CV.isPaymateCorporateModule)
                                {
                                    if (Convert.ToBoolean(ds.Tables[0].Rows[0]["CCRegisteredYN"]))
                                    {
                                        if (CV.CCAvenue)
                                        {
                                            if (Convert.ToInt32(ds.Tables[0].Rows[0]["totalHours"]) <= 24)
                                            {
                                                CV.DSStatus = "O";
                                                CV.BookingStatus = "O";
                                                CV.ErrorCode = "Charging can only be done after 24 hours of Debit Note.";
                                            }
                                            else
                                            {
                                                CCAvenueAPI api = new CCAvenueAPI();
                                                string returnccresult = api.ChargeSI
                                                    (Convert.ToInt32(CV.ClientCoIndivID)
                                                    , BookingID, Convert.ToString(CV.TotalCost));

                                                if (returnccresult == "Success")
                                                {
                                                    CV.DSStatus = "C";
                                                    CV.BookingStatus = "C";
                                                    CV.ErrorCode = "Success";
                                                }
                                                else
                                                {
                                                    CV.ErrorCode = "ChargingLink Send"; //to improvise later
                                                    CV.DSStatus = "O";
                                                    CV.BookingStatus = "O";
                                                }
                                            }
                                        }
                                        else //Bill Desk
                                        {
                                            if (Convert.ToInt32(ds.Tables[0].Rows[0]["totalHours"]) <= 24)
                                            {
                                                CV.DSStatus = "O";
                                                CV.BookingStatus = "O";
                                                CV.ErrorCode = "Charging can only be done after 24 hours of Debit Note.";
                                            }
                                            else
                                            {
                                                //if (Convert.ToBoolean(ds.Tables[0].Rows[0]["ApproveYN"])
                                                if (Convert.ToInt32(ds.Tables[0].Rows[0]["totalHours"]) >= 24) //48)
                                                {
                                                    string predebit = db.CheckPredebitNotificationStatus(BookingID);

                                                    if (predebit == "Success")
                                                    {
                                                        PreAuthResult PVC = new PreAuthResult();
                                                        AmexCardCls objAmexCls = new AmexCardCls();
                                                        PVC = objAmexCls.CreateTransaction(BookingID.ToString());

                                                        if (PVC.ChargingStatus == "Success")
                                                        {
                                                            CV.DSStatus = "C";
                                                            CV.BookingStatus = "C";
                                                            CV.ErrorCode = "Success";
                                                        }
                                                        else
                                                        {
                                                            CV.ErrorCode = "Registered Card Charging Failed."; //PVC.ChargingStatus;
                                                            CV.DSStatus = "O";
                                                            CV.BookingStatus = "O";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        CV.ErrorCode = "Registered Card Charging Failed.";
                                                        CV.DSStatus = "O";
                                                        CV.BookingStatus = "O";
                                                    }
                                                }
                                                else
                                                {
                                                    CV.DSStatus = "O";
                                                    CV.BookingStatus = "O";
                                                    CV.ErrorCode = "Charging can only be done after 24 hours of Debit Note.";
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        CV.DSStatus = "O";
                                        CV.BookingStatus = "O";
                                        CV.ErrorCode = "Card Not Registered.";
                                        //to check if registration mail to send
                                    }

                                }
                                else
                                {
                                    CV.ErrorCode = "ChargingLink Send";
                                    CV.DSStatus = "O";
                                    CV.BookingStatus = "O";
                                }
                            }
                            else if (1 == 2)
                            {
                                if (CV.isPaymateCorporateModule)
                                {
                                    PreAuthResult PVC = objadmin.NewRegistrationChargingProcess(BookingID, CV.TotalCost);
                                    string Provider;
                                    if (CV.CCType == 2)
                                    {
                                        Provider = "AmexBooking";
                                    }
                                    else
                                    {
                                        Provider = "MasterVisaBooking";
                                    }
                                    if (PVC.ChargingStatus != "Success")
                                    {


                                        if (PVC.ChargingStatus != "Batch Submitted and will take around 1 day to confirm Charging." && PVC.ChargingStatus != "076")
                                        {
                                            db.SaveMVLog(BookingID.ToString(), "", "", PVC.ChargingStatus, Provider);
                                            CV.ErrorCode = "ChargingLink Send";
                                        }
                                        else
                                        {
                                            CV.ErrorCode = "Batch Submitted and will take around 1 day to confirm Charging.";
                                            db.SaveMVLog(BookingID.ToString(), "", "", CV.ErrorCode, Provider);
                                        }
                                    }
                                    else
                                    {
                                        db.SaveMVLog(BookingID.ToString(), PVC.CaptureTransactionID, PVC.ErrorCode, PVC.ChargingStatus, Provider);
                                    }
                                    if (CV.CCType == 2)
                                    {
                                        CV.DSStatus = "O";
                                        CV.BookingStatus = "O";
                                    }
                                    else
                                    {
                                        if (PVC.ChargingStatus == "Success")
                                        {
                                            CV.DSStatus = "C";
                                            CV.BookingStatus = "C";
                                            CV.ErrorCode = "Success";
                                        }
                                    }
                                }
                                else
                                {

                                    if (CVC.CVVNumber == true && CVC.bookingMadeByIndiv == 1 && (CVC.CCType == 1 || CVC.CCType == 2 || CVC.CCType == 3) &&
                                        (CVC.ClientCoID == 860 || CVC.ClientCoID == 2530 || CVC.ClientCoID == 2840 || CVC.ClientCoID == 2933)
                                        && CVC.AccentureClosing == false)
                                    {
                                        CV.DSStatus = "C";
                                        CV.BookingStatus = "C";
                                        string Chargingstring = "";
                                        if (CVC.CCType == 2)
                                        {
                                            Chargingstring = objadmin.GetCaptureStatusAmex(BookingID, CV.TotalCost);
                                        }
                                        if (CVC.CCType == 1 || CVC.CCType == 3)
                                        {
                                            Chargingstring = objadmin.GetCaptureStatusMaster(BookingID, CV.TotalCost);
                                        }
                                        string[] strArray = Chargingstring.Split(';');
                                        if (CVC.CCType == 2)
                                        {
                                            if (strArray[0].ToString() == "Success")
                                            {
                                                objadmin.UpdateChargingStatus(BookingID, strArray[2].ToString(), strArray[3].ToString());
                                                CV.ErrorCode = "Success";
                                            }
                                            else
                                            {
                                                CV.ErrorCode = "ChargingLink Send";
                                                CV.DSStatus = "O";
                                                CV.BookingStatus = "O";
                                                db.SaveMVLog(BookingID.ToString(), "", "", strArray[0].ToString(), "AmexBooking");
                                            }
                                        }

                                        if (CVC.CCType == 1 || CVC.CCType == 3)
                                        {
                                            if (strArray[0].ToString() != "CAPTURED")
                                            {
                                                objadmin.SaveMVLog("", "", "Escape Paymate charging for HDFC Charging with Link.", BookingID, CV.TotalCost); //charging Link to send
                                                //CV.hdncontinueYN = "Error";
                                                //CV.ErrorCode = "Error";
                                                CV.ErrorCode = "ChargingLink Send";
                                            }
                                            else
                                            {
                                                CV.DSStatus = "C";
                                                CV.BookingStatus = "C";

                                                CV.ErrorCode = "Success";
                                                objadmin.UpdateChargingStatus(BookingID, strArray[5].ToString(), strArray[4].ToString());
                                            }
                                        }
                                    }
                                    //else if (CVC.CVVNumber == true && CVC.bookingMadeByIndiv == 0 && (CVC.CCType == 1 || CVC.CCType == 3) &&
                                    // (CVC.ClientCoID == 2610 || CVC.ClientCoID == 3248) && CVC.AccentureClosing == true) //Accenture closing Master Visa
                                    else if (CVC.AccentureClosing)
                                    {
                                        if (CVC.CCType == 1 || CVC.CCType == 3) //Accenture closing Master Visa
                                        {
                                            //check mv log status
                                            DataSet dsmvlog = new DataSet();
                                            dsmvlog = objadmin.CheckChargingdetails(BookingID);
                                            if (dsmvlog.Tables[0].Rows.Count <= 0)
                                            {
                                                string chargingstatus = "";

                                                if (Convert.ToBoolean(ds.Tables[0].Rows[j]["NewMVPreauthYN"]))
                                                {
                                                    chargingstatus = objadmin.checkChargingStatus_AccentureNew(BookingID, Convert.ToDouble(ds.Tables[0].Rows[j]["TotalCost"])
                                                        , ds.Tables[0].Rows[j]["Miscellaneous1"].ToString());
                                                }
                                                else
                                                {
                                                    chargingstatus = objadmin.checkChargingStatus(BookingID, Convert.ToDouble(ds.Tables[0].Rows[j]["TotalCost"])
                                                        , ds.Tables[0].Rows[j]["Miscellaneous1"].ToString());
                                                }

                                                if (chargingstatus != "ChargingLink Send" && chargingstatus != "Charged")
                                                {
                                                    //charging not successfull
                                                    CV.DSStatus = "O";
                                                    CV.BookingStatus = "O";
                                                    CV.ErrorCode = chargingstatus;
                                                    db.SaveMVLog(BookingID.ToString(), "", "", chargingstatus, "MasterVisaBooking");
                                                }
                                                else
                                                {
                                                    if (chargingstatus == "Charged")
                                                    {
                                                        CV.DSStatus = "C";
                                                        CV.BookingStatus = "C";
                                                        CV.ErrorCode = "Success";
                                                    }
                                                    else
                                                    {
                                                        CV.DSStatus = "O";
                                                        CV.BookingStatus = "O";
                                                        CV.ErrorCode = "ChargingLink Send";
                                                    }
                                                }
                                            }
                                            else    //to be check if working then make live
                                            {
                                                CV.DSStatus = "C";
                                                CV.BookingStatus = "C";
                                                CV.ErrorCode = "Success";
                                            }
                                        }
                                        //else if (CVC.CVVNumber == true && CVC.bookingMadeByIndiv == 0 && (CVC.CCType == 2) &&
                                        //(CVC.ClientCoID == 2610 || CVC.ClientCoID == 3248) && CVC.AccentureClosing == true) //Accenture closing Amex
                                        else if (CVC.CCType == 2) //Accenture closing Amex
                                        {
                                            CV.ErrorCode = objadmin.GetAmexClosingDetails(BookingID);
                                            if (CV.ErrorCode == "Success")
                                            {
                                                CV.DSStatus = "C";
                                                CV.BookingStatus = "C";
                                            }
                                            else
                                            {
                                                CV.DSStatus = "O";
                                                CV.BookingStatus = "O";
                                                CV.ErrorCode = "ChargingLink Send";
                                            }
                                            //CV.ChargingNRequired = false;
                                        }
                                    }
                                    else
                                    {
                                        //if (Convert.ToBoolean(ds.Tables[0].Rows[j]["ChargingNRequired"]) == true)  //if Nokia charging module
                                        if (CV.ChargingNRequired)  //if Nokia charging module scheduler for Nokia
                                        {
                                            //objadmin.InsertNokiaChargingDetails(BookingID, UserID, 0);
                                            CV.ErrorCode = "ChargingLink Send";
                                        }
                                        else
                                        {
                                            //if ((CV.CCType == 1 || CV.CCType == 3) && CV.PreAuthNotRequire == false)
                                            if (CV.CCType == 1 || CV.CCType == 3)
                                            {
                                                if (Convert.ToBoolean(ds.Tables[0].Rows[j]["NewMVPreauthYN"]))
                                                {
                                                    CV.ErrorCode = objadmin.GetMasterVisaChargingDetails_PreauthCharging(BookingID); //to Be checked the code //
                                                }
                                                else
                                                {
                                                    CV.ErrorCode = objadmin.GetMasterVisaChargingDetails(BookingID); //to Be checked the code //
                                                }
                                                if (CV.ErrorCode != "Success")
                                                {
                                                    CV.BookingStatus = "O";
                                                    CV.DSStatus = "O";
                                                    CV.ErrorCode = "ChargingLink Send";
                                                }
                                                else
                                                {
                                                    CV.BookingStatus = "C";
                                                    CV.DSStatus = "C";
                                                }
                                            }
                                            else if (CV.CCType == 2 && CV.AmexSecondaryChargingYN == false)
                                            {
                                                if ((CV.ClientCoID == 2631 || CV.ClientCoID == 2958 || CV.ClientCoID == 2940 || CV.ClientCoID == 835 || CV.ClientCoID == 3045 || CV.ClientCoID == 860 || CV.ClientCoID == 3007) && CV.AmexSecondaryChargingYN == false) //saves the details in a table for 72 hour amex logic
                                                {
                                                    //CV.hdncontinueYN = "Amex";
                                                    CV.DSStatus = "O";
                                                    CV.BookingStatus = "O";
                                                    CV.AmexLinkYN = true;
                                                    CV.ErrorCode = "ChargingLink Send";
                                                    //objadmin.SaveAmex_Voucher_Booking(BookingID, CV.TotalCost, 0);
                                                }
                                                else
                                                {
                                                    CV.DSStatus = "O";
                                                    CV.BookingStatus = "O";

                                                    if (Convert.ToBoolean(ds.Tables[0].Rows[j]["SecurityCardYN"]))  //if security car yn
                                                    {
                                                        if ((CV.CCNo.Trim() != CV.NewCCNo.Trim()) || (CV.transactionid.Trim() != "NoPreAuth"
                                                            && CV.authorizationid.Trim() != "NoPreAuth")) //to fetch the status in NewCredit card no and get the transactionid and authorizationid
                                                        {
                                                            if (CV.DSStatus != "P")
                                                            {
                                                                CV.ErrorCode = objadmin.GetAmexClosingDetails(BookingID); //changes to be done // preauth on the new credit card no to be done
                                                                if (CV.ErrorCode == "Success")
                                                                {
                                                                    CV.DSStatus = "C";
                                                                    CV.BookingStatus = "C";
                                                                }
                                                                else
                                                                {
                                                                    CV.DSStatus = "O";
                                                                    CV.BookingStatus = "O";
                                                                    CV.ErrorCode = "ChargingLink Send";
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            CV.DSStatus = "O";
                                                            CV.BookingStatus = "O";
                                                            CV.ErrorCode = "ChargingLink Send";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        //IF Trim(""&Request("ManualClose")) = "" Then //don't have the values
                                                        if (CV.transactionid.Trim() != "NoPreAuth" && CV.authorizationid.Trim() != "NoPreAuth")
                                                        //&& Convert.ToInt64(ds.Tables[0].Rows[j]["totaldays"]) <= 20)
                                                        {
                                                            CV.DSStatus = "C";
                                                            CV.BookingStatus = "C";
                                                            CV.ErrorCode = "Success";
                                                        }
                                                        else
                                                        {
                                                            CV.ErrorCode = objadmin.GetAmexClosingDetails(BookingID);
                                                            if (CV.ErrorCode == "Success")
                                                            {
                                                                CV.DSStatus = "C";
                                                                CV.BookingStatus = "C";
                                                            }
                                                            else
                                                            {
                                                                CV.DSStatus = "O";
                                                                CV.BookingStatus = "O";
                                                                CV.ErrorCode = "ChargingLink Send";
                                                            }
                                                        }
                                                        //else
                                                        //intApprovalNo = Trim(request.form("NewApprovalNumber"))
                                                        //end if
                                                    }
                                                }
                                                //else
                                                //{
                                                //    if ((CV.ClientCoID == 2631 || CV.ClientCoID == 2958 || CV.ClientCoID == 2940 || CV.ClientCoID == 835 || CV.ClientCoID == 3045 || CV.ClientCoID == 860 || CV.ClientCoID == 3007) && CV.AmexSecondaryChargingYN == false)
                                                //    {
                                                //        CV.hdncontinueYN = "Amex";
                                                //        CV.DSStatus = "O";
                                                //        CV.BookingStatus = "O";
                                                //        CV.AmexLinkYN = true;
                                                //        SaveAmex_Voucher_Booking(BookingID, Convert.ToDouble(ds.Tables[0].Rows[j]["TotalCost"]), 0);
                                                //    }
                                                //}
                                            }
                                            else //in case the bookingid is closed after void or the payment is made through (website / app / cor - retail charging link)
                                            {
                                                if (CV.AmexSecondaryChargingYN)
                                                {
                                                    CV.DSStatus = "O";
                                                    CV.BookingStatus = "O";
                                                    //CV.ErrorCode = "Charging Link has already been sent to Guest for the bookingid";
                                                }
                                                else
                                                {
                                                    if (CV.CCType == 1 || CV.CCType == 2 || CV.CCType == 3)
                                                    {
                                                        //to check the logic if the booking is not master / visa / amex 
                                                        CV.DSStatus = "C";
                                                        CV.BookingStatus = "C";
                                                        CV.ErrorCode = "Success";
                                                    }
                                                    else
                                                    {
                                                        CV.DSStatus = "O";
                                                        CV.BookingStatus = "O";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } //if not isPaymateCorporateModule 
                            }   //if (CVC.ChargedYN == false)

                            if (CV.ErrorCode != "Success")
                            {
                                if (!string.IsNullOrEmpty(CV.NewChargingLink) || CV.AmexSecondaryChargingYN)
                                {
                                    CV.ErrorCode = "Charging Link has already been sent to Guest for the bookingid";
                                }
                            }

                            //if (CV.ClientCoID == 3248 || CV.ClientCoID == 3249)
                            //{
                            if (CV.ErrorCode != "Success" && CV.ErrorCode != "Charging Link has already been sent to Guest for the bookingid"
                                && CV.ErrorCode != "Charging can only be done after 24 hours of Debit Note."
                                && CV.ErrorCode != "Card Not Registered." && CV.ErrorCode != "Registered Card Charging Failed.")
                            //&& CV.ErrorCode != "076" && CV.ErrorCode != "Batch Submitted and will take around 1 day to confirm Charging."
                            {
                                CV.ErrorCode = "ChargingLink Send";
                            }
                            //}

                            //if (CV.hdncontinueYN != "")
                            //{
                            //    CV.DSStatus = "O";
                            //    CV.BookingStatus = "O";
                            //    CV.ErrorCode = "ChargingLink Send";
                            //}
                            if (CV.AmexSecondaryChargingYN && CV.CCType != 5) //charging link send to Amex Card
                            {
                                CV.DSStatus = "O";
                                CV.BookingStatus = "O";
                                CV.ErrorCode = "ChargingLink Send";
                            }

                            if (CV.ErrorCode == "ChargingLink Send")
                            {
                                try
                                {
                                    SaveCCSchedulerDetails(BookingID, CV.TotalCost, UserID);
                                }
                                catch (Exception savedetail)
                                {

                                }
                            }

                            //objadmin.InsertNokiaChargingDetails(BookingID, UserID, 1);
                            if (CV.VoucherAmt >= 0)
                            {
                                objadmin.SaveAmex_Voucher_Booking(BookingID, CV.VoucherAmt, 1);
                            }

                            //CV.MerchantID = CD.UpdateCreditCardMerchant(BookingID, CV);

                        }
                    }
                    else
                    {
                        CV.ErrorCode = "BookingId does not exists.";
                    }
                } //end of if (CloseYN)

                if (CV.ErrorCode == "Success")
                {
                    db.deleteCardLogs(BookingID);

                }
            }

            return CV;
        }

        public void SaveCCSchedulerDetails(int BookingId, double TotalCost, int UserID)
        {
            SMSMailRef.SendSMSSoapClient savecharginglinkdetails = new SMSMailRef.SendSMSSoapClient();
            savecharginglinkdetails.SaveCCSchedulerLog(BookingId, TotalCost, UserID);
        }
    }
}