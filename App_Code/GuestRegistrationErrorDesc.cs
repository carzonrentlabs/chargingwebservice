﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

/// <summary>
/// Summary description for GuestRegistrationErrorDesc
/// </summary>
public class GuestRegistrationErrorDesc
{
    public GuestRegistrationErrorDesc()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static string GetTxnResponseCodeDescription(string TxnResponseCode)
    {
        string result = "";

        if (TxnResponseCode == null || String.Compare(TxnResponseCode, "null", true) == 0 || TxnResponseCode.Equals(""))
        {
            result = "null response";
        }
        else
        {
            switch (TxnResponseCode)
            {
                case "400": result = "Success"; break;
                case "401": result = "Invalid user name or reference number"; break;
                case "406": result = "Empty value/ Invalid Parameters"; break;
                case "410": result = "Invalid Transaction ID"; break;
                case "500": result = "Timeout/Error"; break;
                case "115": result = "Transaction Declined"; break;
                case "021": result = "Invalid login details"; break;
                case "022": result = "Invalid source"; break;
                case "023": result = "Denied by risk"; break;
                case "035": result = "Incorrect Expiry"; break;
                case "036": result = "Incorrect Credit card number"; break;
                case "059": result = "Transaction Declined"; break;
                case "062": result = "Invalid details"; break;
                case "API01": result = "Invalid Request."; break;
                case "API02": result = "invalid Merchant"; break;
                default: result = "Unable to be determined"; break;
            }
        }
        return result;
    }

    public static int GetCCTypeID(string CCTypeName)
    {
        int result = 0;

        if (CCTypeName == null || String.Compare(CCTypeName, "null", true) == 0 || CCTypeName.Equals(""))
        {
            result = 0;
        }
        else
        {
            switch (CCTypeName)
            {
                case "VISA": result = 1; break;
                case "MASTER": result = 3; break;
                case "AMEX": result = 2; break;
                default: result = 0; break;
            }
        }
        return result;
    }

    public static string GetConfigurationValue(string key)
    {
        return ConfigurationManager.AppSettings[key];
    }
}