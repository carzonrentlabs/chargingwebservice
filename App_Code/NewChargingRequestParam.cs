﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for PreauthValidation
/// </summary>
public class NewChargingRequestParam
{
    public authData AuthData { get; set; }
    public requestData RequestData { get; set; }
}

//public class CheckNewChargingRequestParam
//{
//    public authData AuthData { get; set; }
//    public requestDataNew RequestData { get; set; }
//}


public class CheckChargingRequestParam
{
    public authData AuthData { get; set; }
    public requestDataCheckCharging RequestData { get; set; }
}


public class CheckNewTravellerResponseParam
{
    public string ResponseCode { get; set; }
    public string ResponseMessage { get; set; }
    public string TravellerId { get; set; }
    public string CardNo { get; set; }
    public string CardBrand { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string EmailId { get; set; }
    public string MobileNo { get; set; }
    public string PartnerName { get; set; }
}


//public class requestDataNew
//{
//    public string AccessCode { get; set; }
//    public string PayMateAuthTransactionRefNo { get; set; }
//    public string InvoiceNo { get; set; }
//    public string PreAuthAmount { get; set; }
//    public string CaptureAmount { get; set; }
//}

public class requestDataCheckCharging
{
    public string AccessCode { get; set; }
    public string TransactionRefNo { get; set; }
    public string InvoiceNo { get; set; }
    public string TransactionMode { get; set; }
}

public class authData
{
    public string AuthUserName { get; set; }
    public string AuthPassword { get; set; }
    public string AuthCode { get; set; }
}

public class requestData
{
    public string AccessCode { get; set; }
    public string PayMateAuthTransactionRefNo { get; set; }
    public string InvoiceNo { get; set; }
    public string PreAuthAmount { get; set; }
    public string CaptureAmount { get; set; }
}

public class TravellerResponseParam
{
    public string ResponseCode { get; set; }
    public string ResponseMessage { get; set; }
    public string TravellerId { get; set; }
    public string CardNo { get; set; }
    public string CardBrand { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string EmailId { get; set; }
    public string MobileNo { get; set; }
    public string PartnerName { get; set; }
}

public class NewChargingResponseParam
{
    public string ResponseCode { get; set; }
    public string ResponseMessage { get; set; }
    public string TravellerId { get; set; }
    public string CardBrand { get; set; }
    public string CardNo { get; set; }
    public string InvoiceNo { get; set; }
    public string Amount { get; set; }
    public string TransactionRefNo { get; set; }
    public string TransactionStatus { get; set; }
    public string TransactionMode { get; set; }
    public string PartnerName { get; set; }
}


public class NewChargingThroughPreauthResponseParam
{
    public string ResponseCode { get; set; }
    public string ResponseMessage { get; set; }
    public string PayMateAuthTransactionRefNo { get; set; }
    public string InvoiceNo { get; set; }
    public string PreAuthAmount { get; set; }
    public string CaptureAmount { get; set; }
    public string TransactionRefNo { get; set; }
    public string TransactionStatus { get; set; }
    public string TransactionMode { get; set; }
    public string PartnerName { get; set; }
}