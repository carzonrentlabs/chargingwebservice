﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BillDeskModel
/// </summary>
/// 

public class CanraNEFTRequest
{
    public CanraNEFTBodyModel Request { get; set; }
}

public class CanraNEFTBodyModel
{
    public CanraNEFT Body { get; set; }
}

public class CanraNEFT
{
    public srcAccountDetail srcAccountDetails { get; set; }
    public destAccountDetail destAccountDetails { get; set; }
    public string txnCurrency { get; set; }
    public string ifscCode { get; set; }
    public string narration { get; set; }
    public string paymentMode { get; set; }
    public string valueDate { get; set; }
    public string encryptData { get; set; }
}

public class CanraSignatureNEFTModel
{
    public SigDataNEFT encryptData { get; set; }
}

public class SigDataNEFT
{
    public string Authorization { get; set; }
    public string txnPassword { get; set; }
    public string srcAcctNumber { get; set; }
    public string destAcctNumber { get; set; }
    public string customerID { get; set; }
    public string txnAmount { get; set; }
    public string benefName { get; set; }
    public string userRefNo { get; set; }
}

public class srcAccountDetail
{
    public string identity { get; set; }
    public string currency { get; set; }
    public string branchCode { get; set; }
}
public class destAccountDetail
{
    public string identity { get; set; }
    public string currency { get; set; }
}

public class CanraSignatureBodyRequest
{
    public CanraSignatureBodyModel Request { get; set; }
}
public class CanraSignatureBodyModel
{
    public CanraSignatureModel Body { get; set; }
}
public class CanraSignatureModel
{
    public SigData encryptData { get; set; }
}

public class SigData
{
    public string Authorization { get; set; }
    public string Key { get; set; }
    public string CustomerID { get; set; }
    public string BatchRequestID { get; set; }
    public string TxnRefNo { get; set; }
}

public class CanraRequestFinal
{
    public CanraReqFinal Request { get; set; }
}
public class CanraReqFinal
{
    public CanraRequestBodyFinal Body { get; set; }
}
public class CanraRequestBodyFinal
{
    public string encryptData { get; set; }
}
//Classes for making Request json 
#region Classes for making Request json

public class CanraRequest
{
    public CanraRequestBodyReq Request { get; set; }
}
public class CanraRequestBodyReq
{
    public CanraRequestBody Body { get; set; }
}
public class CanraRequestBody
{
    public CanraReqEncryptData encryptData { get; set; }
}

public class CanraReqEncryptData
{
    public string Authorization { get; set; }
    public string Key { get; set; }
    public string CustomerID { get; set; }
    public string TotAmt { get; set; }
    public string TxnCnt { get; set; }
    public string DatTxn { get; set; }
    public string BatchRequestID { get; set; }
    public TxnDetailList TxnDtls { get; set; }

}
public class TxnDetailList
{
    public List<TxnDetail> Txn { get; set; }

}
public class TxnDetail
{
    public string TxnRefNo { get; set; }
    public string DrAcct { get; set; }
    public string SndrNm { get; set; }
    public string TxnAmt { get; set; }
    public string TxnType { get; set; }
    public string BenefIFSC { get; set; }
    public string BenefAcNo { get; set; }
    public string BenefAcNm { get; set; }
    public string Nrtv { get; set; }
}
#endregion

//classes for success response 

#region classes for success response

public class CanraSuccessResponseBodyEnc
{
    public CanSuccessResponseEnc Response { get; set; }
}
public class CanSuccessResponseEnc
{
    public CanSuccessResponseBodyEnc body { get; set; }
}
public class CanSuccessResponseBodyEnc
{
    public string encryptData { get; set; }
}
public class CanraSuccessResponseBody
{
    public CanSuccessResponse Response { get; set; }
}
public class CanSuccessResponse
{
    public CanSuccessResponseBody body { get; set; }
}
public class CanSuccessResponseBody
{
    public CanSuccessResponseBodyEnryptData encryptData { get; set; }
}
public class CanSuccessResponseBodyEnryptData
{
    public CanraSuccessResponseStatus status { get; set; }
    public CanSuccessResponseBodyEnryptDataBody Response { get; set; }
}
public class CanraSuccessResponseStatus
{
    public string result { get; set; }
    public string referenceNumber { get; set; }
    public string contextID { get; set; }
    public CanraSuccessResponseMessage message { get; set; }
}
public class CanraSuccessResponseMessage
{
    public string code { get; set; }
    public string type { get; set; }
}
public class CanSuccessResponseBodyEnryptDataBody
{
    public CanSuccessResponseBodyEnryptDataBodyTxnSummary Body { get; set; }
}
public class CanSuccessResponseBodyEnryptDataBodyTxnSummary
{
    public CanraSuccessResponseTxnSummary txnSummary { get; set; }
}
public class CanraSuccessResponseTxnSummary
{
    public string BatchNo { get; set; }
    public string TotalRecords { get; set; }
    public string SumAmount { get; set; }
    public string ErrorCode { get; set; }
    public string ErrorDec { get; set; }
    public string BatchRequestID { get; set; }
}

public class CanaraApiConfiguration
{
    public string BaseURL { get; set; }
    public string BatchInitiateURL { get; set; }
    public string ClientIdOrKey { get; set; }
    public string ClientSecret { get; set; }
    public string ClientCertificate { get; set; }
    public string ApiInteractionId { get; set; }
    public string SharedSymmetricKey { get; set; }
    public string SSLPrivateKey { get; set; }
    public string SSLPublicKey { get; set; }
}
#endregion

//Error reponse classe 
#region Error response classes 
public class CanraErrorResponse
{
    public CanErrorResponseMetaData ErrorResponse { get; set; }
}
public class CanErrorResponseMetaData
{
    public CanErrorResponseMetadataStatus metadata { get; set; }
}
public class CanErrorResponseMetadataStatus
{
    public CanErrorResponseErrorDetails status { get; set; }
}
public class CanErrorResponseErrorDetails
{
    public string httpCode { get; set; }
    public string httpMessage { get; set; }
    public string moreInformation { get; set; }
}

#endregion

//batch inquiry response classes 
#region batch inquiry response classes 
public class CanaraTransactionDetail
{
    public string utr_RRN_Number { get; set; }
    public string TxnRefNo { get; set; }
    public string DrAcct { get; set; }
    public string SndrNm { get; set; }
    public string TxnAmt { get; set; }
    public string TxnType { get; set; }
    public string BenefIFSC { get; set; }
    public string BenefAcNo { get; set; }
    public string BenefAcNm { get; set; }
    public string Nrtv { get; set; }
    //[JsonProperty("UTR_RRN_Number")]
    //public string UTRRrnNumber { get; set; }
    public string TxnStatus { get; set; }
    public string Txn_init_date { get; set; }
    public string Approved_By { get; set; }
    public string Approved_Date { get; set; }
    public string Error_Code { get; set; }
    public string Error_Msg { get; set; }
}

public class CanaraTransactionDetails
{
    public List<CanaraTransactionDetail> Txn { get; set; }
}

public class CanaraBatchInquiryBody
{
    public string UserName { get; set; }
    public string CustomerID { get; set; }
    public string TotAmt { get; set; }
    public string TxnCnt { get; set; }
    public string DatTxn { get; set; }
    public string BatchRequestID { get; set; }
    public string TxnRefNo { get; set; }
    public string TotNeftAmt { get; set; }
    public string TxnNeftCnt { get; set; }
    public string TotRtgsAmt { get; set; }
    public string TxnRtgsCnt { get; set; }
    public string TotImpsAmt { get; set; }
    public string TxnImpsCnt { get; set; }
    public string TotIntraAmt { get; set; }
    public string TxnIntraCnt { get; set; }
    public CanaraTransactionDetails TxnDtls { get; set; }
}

public class CanaraBatchInquiryResponse
{
    public CanaraBatchInquiryBody Body { get; set; }
}

public class CanaraBatchInquiryRoot
{
    public CanaraBatchInquiryResponse Response { get; set; }
}

#endregion

//// unused code remove if not required
#region unused code remove if not required
//public class CanraResponseModel
//{
//    public Responsetxnstatus status { get; set; }
//    public ResponsetxnSummary txnSummary { get; set; }
//}

//public class ResponsetxnSummary
//{
//    public string BatchNo { get; set; }
//    public string TotalRecords { get; set; }
//    public string SumAmount { get; set; }
//    public string ErrorCode { get; set; }
//    public string ErrorDec { get; set; }
//    public string BatchRequestID { get; set; }
//}

//public class Responsetxnstatus
//{
//    public string result { get; set; }
//    public string referenceNumber { get; set; }
//    public string contextID { get; set; }
//    public strmessage message { get; set; }
//}

//public class strmessage
//{
//    public string code { get; set; }
//    public string type { get; set; }
//}
#endregion



























