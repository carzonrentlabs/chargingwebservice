﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BillDeskModel
/// </summary>
public class BillDeskModel
{   

    public string mercid { get; set; }
    public string orderid { get; set; }
    public string amount { get; set; }
    public DateTime order_date { get; set; }
    public string currency { get; set; }
    public string ru { get; set; }
    public AdditionalInfo additional_info { get; set; }
    public string itemcode { get; set; }
    public Invoice invoice { get; set; }
    public Device device { get; set; }

}

// Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
public class AdditionalInfo
{
    public string additional_info1 { get; set; }
    public string additional_info2 { get; set; }
    public string additional_info3 { get; set; }
    
}


public class AdditionalInfoInvoice
{
    public string additional_info1 { get; set; }
    public string additional_info2 { get; set; }
    public string additional_info3 { get; set; }
    public string additional_info4 { get; set; }
    public string additional_info5 { get; set; }
    public string additional_info6 { get; set; }
    public string additional_info7 { get; set; }
    public string additional_info8 { get; set; }
    public string additional_info9 { get; set; }
    public string additional_info10 { get; set; }
}
public class Device
{
    public string init_channel { get; set; }
    public string ip { get; set; }
    //public string mac { get; set; }
    //public string imei { get; set; }
    public string user_agent { get; set; }
    //public string accept_header { get; set; }
    //public string fingerprintid { get; set; }
}

public class GstDetails
{
    public string cgst { get; set; }
    public string sgst { get; set; }
    public string igst { get; set; }
    public string gst { get; set; }
    public string cess { get; set; }
    public string gstincentive { get; set; }
    public string gstpct { get; set; }
    public string gstin { get; set; }
}

public class Invoice
{
    public string invoice_number { get; set; }
    public string invoice_display_number { get; set; }
    public string customer_name { get; set; }
    public string invoice_date { get; set; }
    public GstDetails gst_details { get; set; }
}

public class CreateInvoice
{
    public string mercid { get; set; }
    public string subscription_refid { get; set; }
    public string customer_refid { get; set; }
    public AdditionalInfoInvoice additional_info { get; set; }
    public string invoice_number { get; set; }
    public string invoice_display_number { get; set; }
    public string invoice_date { get; set; }
    public string duedate { get; set; }
    public string debit_date { get; set; }
    public string amount { get; set; }
    public string early_payment_duedate { get; set; }
    public string early_payment_discount { get; set; }
    public string early_payment_amount { get; set; }
    public string late_payment_charges { get; set; }
    public string late_payment_amount { get; set; }
    public string net_amount { get; set; }
    public string currency { get; set; }
    public string mandateid { get; set; }
    public string description { get; set; }
}


public class clsRegistration
{
    public string mercid { get; set; }
    public string customer_refid { get; set; }
    public string subscription_refid { get; set; }
    public string subscription_desc { get; set; }
    public string currency { get; set; }
    public string frequency { get; set; }
    public string amount_type { get; set; }
    public string amount { get; set; }
    public string start_date { get; set; }
    public string end_date { get; set; }
    //public string recurrence_rule { get; set; }
    //public string debit_day { get; set; }
    public Customer customer { get; set; }
    public AdditionalInfo additional_info { get; set; }
    public Device device { get; set; }
    public string ru { get; set; }
}
public class Customer
{
    public string first_name { get; set; }
    public string last_name { get; set; }
    public string mobile { get; set; }
    public string mobile_alt { get; set; }
    public string email { get; set; }
    public string email_alt { get; set; }
}


public class CreateTransaction
{
    public string mercid { get; set; }
    public string orderid { get; set; }
    public string amount { get; set; }
    public string currency { get; set; }
    public string debit_request_no { get; set; }
    public string itemcode { get; set; }
    public AdditionalInfoInvoice additional_info { get; set; }
    public string txn_process_type { get; set; }
    public string mandateid { get; set; }
    public string invoice_id { get; set; }
    public string subscription_refid { get; set; }
}


public class DeleteMandate
{
    public string mercid { get; set; }
    public string action { get; set; }
    public string mandateid { get; set; }
    public Device device { get; set; }
    public string ru { get; set; }
}


public class CreateRefund
{
    public string transactionid { get; set; }
    public string orderid { get; set; }
    public string mercid { get; set; }
    public string transaction_date { get; set; }
    public string txn_amount { get; set; }
    public string refund_amount { get; set; }
    public string currency { get; set; }
    public Device device { get; set; }
    public string merc_refund_ref_no { get; set; }
    
}

public class GetMandate
{
    public string mercid { get; set; }
    public string mandateId { get; set; }
}
