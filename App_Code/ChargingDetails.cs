﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ChargingDetails
/// </summary>
namespace ChargingWebService
{
    public class ChargingDetails
    {
        ChargingWebService.clsAdmin objadmin = new ChargingWebService.clsAdmin();
        public ChargingDetails()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public ClosingVariables GetClosingDetails(int BookingID, int UserID)
        {
            DataSet ds = new DataSet();
            ClosingVariables CV = new ClosingVariables();
            ChargingWebService.DB db = new ChargingWebService.DB();
            ds = objadmin.GetBookingDetails_Confirm(BookingID);
            if (ds.Tables[0].Rows.Count > 0)
            {

                for (int k = 0; k <= ds.Tables[0].Rows.Count - 1; k++)
                {
                    //CV.BookingId = BookingID;
                    CV.CentralizedYN = Convert.ToString(ds.Tables[0].Rows[k]["CentralizedYN"]);

                    CV.CCRegisteredYN = Convert.ToBoolean(ds.Tables[0].Rows[k]["CCRegisteredYN"]);
                    CV.NewCCProcess = Convert.ToInt32(ds.Tables[0].Rows[k]["NewCCProcess"]);
                    CV.RentalType = Convert.ToInt32(ds.Tables[0].Rows[k]["RentalType"]);
                    CV.SanatizationCharges = Convert.ToDouble(ds.Tables[0].Rows[k]["SanatizationCharges"]);

                    CV.PendingYn = Convert.ToBoolean(ds.Tables[0].Rows[k]["PendingYn"]);

                    CV.LockYN = Convert.ToInt32(ds.Tables[0].Rows[k]["LockYN"]);
                    CV.LockReason = Convert.ToString(ds.Tables[0].Rows[k]["LockReason"]);
                    CV.NewChargingLink = Convert.ToString(ds.Tables[0].Rows[k]["NewChargingLink"]);

                    CV.CustomPkgYN = Convert.ToBoolean(ds.Tables[0].Rows[k]["CustomPkgYN"]);
                    CV.OutstationYN = Convert.ToBoolean(ds.Tables[0].Rows[k]["OutstationYN"]);
                    CV.Service = Convert.ToString(ds.Tables[0].Rows[k]["Service"]);
                    CV.ServiceUnitID = Convert.ToInt32(ds.Tables[0].Rows[k]["ServiceUnitID"]);
                    CV.CityID = Convert.ToInt32(ds.Tables[0].Rows[k]["PickUpCityID"]);
                    CV.Status = Convert.ToString(ds.Tables[0].Rows[k]["Status"]);
                    CV.DateClose = Convert.ToDateTime(ds.Tables[0].Rows[k]["DateClose"]);
                    CV.TimeClose = Convert.ToString(ds.Tables[0].Rows[k]["TimeClose"]);
                    CV.DateIn = Convert.ToDateTime(ds.Tables[0].Rows[k]["DateIn"]);
                    CV.DateOut = Convert.ToDateTime(ds.Tables[0].Rows[k]["DateOut"]);
                    CV.TimeIn = Convert.ToString(ds.Tables[0].Rows[k]["TimeIn"]);
                    CV.TimeOut = Convert.ToString(ds.Tables[0].Rows[k]["TimeOut"]);
                    CV.KMClose = Convert.ToInt64(ds.Tables[0].Rows[k]["KMClose"]);
                    CV.KMIn = Convert.ToInt64(ds.Tables[0].Rows[k]["KMIn"]);
                    CV.kmout = Convert.ToInt64(ds.Tables[0].Rows[k]["kmout"]);
                    CV.pointOpeniningKm = Convert.ToInt64(ds.Tables[0].Rows[k]["pointOpeniningKm"]);
                    CV.GuestOpDate = Convert.ToDateTime(ds.Tables[0].Rows[k]["GuestOpDate"]);
                    CV.GuestClDate = Convert.ToDateTime(ds.Tables[0].Rows[k]["GuestClDate"]);
                    CV.GuestOpTime = Convert.ToString(ds.Tables[0].Rows[k]["GuestOpTime"]);
                    CV.GuestClTime = Convert.ToString(ds.Tables[0].Rows[k]["GuestClTime"]);
                    CV.ClientCoID = Convert.ToInt32(ds.Tables[0].Rows[k]["ClientCoID"]);
                    CV.CCType = Convert.ToInt32(ds.Tables[0].Rows[k]["CCType"]);
                    CV.FXDGarageRate = Convert.ToDouble(ds.Tables[0].Rows[k]["FXDGarageRate"]);
                    CV.ApprovalAmount = Convert.ToDouble(ds.Tables[0].Rows[k]["ApprovalAmount"]);
                    CV.ApprovalNo = Convert.ToString(ds.Tables[0].Rows[k]["ApprovalNo"]);
                    CV.ParkTollChages = Convert.ToInt32(ds.Tables[0].Rows[k]["ParkTollChages"]);
                    CV.InterstateTax = Convert.ToInt32(ds.Tables[0].Rows[k]["InterstateTax"]);
                    CV.PkgID = Convert.ToInt64(ds.Tables[0].Rows[k]["PkgID"]);
                    CV.ExtraHr = Convert.ToDouble(ds.Tables[0].Rows[k]["ExtraHr"]);
                    CV.ExtraKM = Convert.ToInt32(ds.Tables[0].Rows[k]["ExtraKM"]);

                    CV.ExtraHrRate = Convert.ToDouble(ds.Tables[0].Rows[k]["ExtraHrRate"]);
                    CV.ExtraKMRate = Convert.ToDouble(ds.Tables[0].Rows[k]["ExtraKMRate"]);
                    CV.NightStayAmt = Convert.ToDouble(ds.Tables[0].Rows[k]["NightStayAmt"]);
                    CV.OutStnAmt = Convert.ToDouble(ds.Tables[0].Rows[k]["OutStnAmt"]);
                    CV.DiscountPC = Convert.ToDouble(ds.Tables[0].Rows[k]["DiscountPC"]);
                    CV.PkgRate = Convert.ToDouble(ds.Tables[0].Rows[k]["PkgRate"]);
                    CV.FuelSurcharge = Convert.ToDouble(ds.Tables[0].Rows[k]["FuelSurcharge"]);
                    CV.TotalHrsUsed = Convert.ToDouble(ds.Tables[0].Rows[k]["TotalHrsUsed"]);

                    if (CV.TotalHrsUsed > ((Convert.ToDouble(ds.Tables[0].Rows[k]["Totalkm"]) * 20) //logic by Yogender
                        + (Convert.ToDouble(ds.Tables[0].Rows[k]["Totalkm"]) * .1)))
                    {
                        CV.BookingForApprovalYN = true;
                    }
                    else
                    {
                        CV.BookingForApprovalYN = false;
                    }


                    CV.NoNight = Convert.ToInt32(ds.Tables[0].Rows[k]["NoNight"]);
                    CV.PaymentMode = Convert.ToString(ds.Tables[0].Rows[k]["PaymentMode"]);
                    CV.CarID = Convert.ToInt32(ds.Tables[0].Rows[k]["CarID"]);
                    CV.ChauffeurID = Convert.ToInt32(ds.Tables[0].Rows[k]["ChauffeurID"]);
                    CV.VendorCarYN = Convert.ToBoolean(ds.Tables[0].Rows[k]["VendorCarYN"]);
                    CV.VendorChauffYN = Convert.ToBoolean(ds.Tables[0].Rows[k]["VendorChauffYN"]);
                    CV.ServiceTaxPercent = Convert.ToDouble(ds.Tables[0].Rows[k]["ServiceTaxPercent"]);
                    CV.EduCessPercent = Convert.ToDouble(ds.Tables[0].Rows[k]["EduCessPercent"]);
                    CV.HduCessPercent = Convert.ToDouble(ds.Tables[0].Rows[k]["HduCessPercent"]);
                    CV.DSTPercent = Convert.ToDouble(ds.Tables[0].Rows[k]["DSTPercent"]);
                    CV.PreAuthNotRequire = Convert.ToBoolean(ds.Tables[0].Rows[k]["PreAuthNotRequire"]);
                    CV.TotalCost = Convert.ToDouble(ds.Tables[0].Rows[k]["TotalCost"]);
                    CV.CalculatedAmount = Convert.ToDouble(ds.Tables[0].Rows[k]["CalculatedAmount"]);
                    CV.PaidFullYN = Convert.ToInt32(ds.Tables[0].Rows[k]["PaidFullYN"]);
                    if (ds.Tables[0].Rows[k]["PaidFullDate"].ToString() != "")
                    {
                        CV.PaidFullDate = Convert.ToDateTime(ds.Tables[0].Rows[k]["PaidFullDate"]);
                    }
                    CV.Phone1 = Convert.ToString(ds.Tables[0].Rows[k]["Phone1"]);
                    CV.CCNo = Convert.ToString(ds.Tables[0].Rows[k]["CCNo"]);
                    CV.ExpYYMM = Convert.ToString(ds.Tables[0].Rows[k]["expyymm"]);
                    CV.BillingBasis = Convert.ToString(ds.Tables[0].Rows[k]["BillingBasis"]);
                    CV.ServiceTaxAmt = Convert.ToDouble(ds.Tables[0].Rows[k]["ServiceTaxAmt"]);
                    CV.HduTaxAmt = Convert.ToDouble(ds.Tables[0].Rows[k]["HduTaxAmt"]);

                    CV.EduTaxAmt = Convert.ToDouble(ds.Tables[0].Rows[k]["EduTaxAmt"]);
                    CV.VatAmt = Convert.ToDouble(ds.Tables[0].Rows[k]["VatAmt"]);
                    CV.Others = Convert.ToDouble(ds.Tables[0].Rows[k]["Others"]);
                    CV.SwachhBharatTaxPercent = Convert.ToDouble(ds.Tables[0].Rows[k]["SwachhBharatTaxPercent"]);
                    CV.SwachhBharatTaxAmt = Convert.ToDouble(ds.Tables[0].Rows[k]["SwachhBharatTaxAmt"]);
                    CV.applyservicetaxyn = Convert.ToBoolean(ds.Tables[0].Rows[k]["applyservicetaxyn"]);
                    CV.KrishiKalyanTaxPercent = Convert.ToDouble(ds.Tables[0].Rows[k]["KrishiKalyanTaxPercent"]);
                    CV.KrishiKalyanTaxAmt = Convert.ToDouble(ds.Tables[0].Rows[k]["KrishiKalyanTaxAmt"]);

                    CV.CGSTTaxPercent = Convert.ToDouble(ds.Tables[0].Rows[k]["CGSTTaxPercent"]);
                    CV.SGSTTaxPercent = Convert.ToDouble(ds.Tables[0].Rows[k]["SGSTTaxPercent"]);
                    CV.IGSTTaxPercent = Convert.ToDouble(ds.Tables[0].Rows[k]["IGSTTaxPercent"]);
                    CV.CGSTTaxAmt = Convert.ToDouble(ds.Tables[0].Rows[k]["CGSTTaxAmt"]);
                    CV.SGSTTaxAmt = Convert.ToDouble(ds.Tables[0].Rows[k]["SGSTTaxAmt"]);
                    CV.IGSTTaxAmt = Convert.ToDouble(ds.Tables[0].Rows[k]["IGSTTaxAmt"]);
                    CV.ClientGSTId = Convert.ToInt32(ds.Tables[0].Rows[k]["ClientGSTId"]);

                    //CV.KrishiYN = Convert.ToString(ds.Tables[0].Rows[k]["KrishiYN"]);
                    CV.hdncontinueYN = "";
                    CV.AmexLinkYN = false;
                    CV.DSStatus = "O";
                    CV.BookingStatus = "O";
                    CV.ClientCoIndivID = Convert.ToInt64(ds.Tables[0].Rows[k]["ClientCoIndivID"]);
                    CV.isPaymateCorporateModule = Convert.ToBoolean(ds.Tables[0].Rows[k]["isPaymateCorporateModule"]);
                    CV.EmailID = Convert.ToString(ds.Tables[0].Rows[k]["EmailID"]);
                    CV.ServiceType = Convert.ToString(ds.Tables[0].Rows[k]["ServiceType"]);
                    CV.trackid = Convert.ToString(ds.Tables[0].Rows[k]["trackid"]);
                    CV.CreatedBy = Convert.ToInt32(ds.Tables[0].Rows[k]["CreatedBy"]);

                    CV.transactionid = Convert.ToString(ds.Tables[0].Rows[k]["transactionid"]);
                    CV.authorizationid = Convert.ToString(ds.Tables[0].Rows[k]["authorizationid"]);
                    CV.ProvisionalInvoiceForGuestYN = Convert.ToBoolean(ds.Tables[0].Rows[k]["ProvisionalInvoiceForGuestYN"]);
                    CV.ProvisionalInvoiceForFacilitatorYN = Convert.ToBoolean(ds.Tables[0].Rows[k]["ProvisionalInvoiceForFacilitatorYN"]);
                    CV.EliteCarYN = Convert.ToBoolean(ds.Tables[0].Rows[k]["EliteCarYN"]);
                    CV.ProvisionalInvoiceForTravelDeskYN = Convert.ToBoolean(ds.Tables[0].Rows[k]["ProvisionalInvoiceForTravelDeskYN"]);

                    CV.SubsidiaryID = Convert.ToInt32(ds.Tables[0].Rows[k]["SubsidiaryID"]);

                    CV.CarCatId = Convert.ToInt32(ds.Tables[0].Rows[k]["Carcatid"]);
                    //CV.CityName = Convert.ToString(ds.Tables[0].Rows[k]["CityName"]);
                    //if (CV.BillingBasis != "pp" || CV.Service == "A" || CV.Service == "T")
                    //{
                    //    CV.GuestOpDate = CV.DateOut;
                    //    CV.GuestClDate = CV.DateIn;
                    //    CV.GuestOpTime = CV.TimeOut;
                    //    CV.GuestClTime = CV.TimeIn;
                    //    CV.pointOpeniningKm = CV.kmout;
                    //}
                    CV.AmexSecondaryChargingYN = false;
                    CV.ErrorCode = "";
                    CV.AllowClosingYN = true;
                    CV.ExtraAmount = Convert.ToDouble(ds.Tables[0].Rows[k]["ExtraAmount"]);
                    CV.CarModelId = Convert.ToInt32(ds.Tables[0].Rows[k]["ModelID"]);
                    CV.GSTSurchargeAmount = Convert.ToDouble(ds.Tables[0].Rows[k]["GSTSurchargeAmount"]);
                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[k]["Basic"].ToString()))
                    {
                        CV.BasicRevenue = Convert.ToDouble(ds.Tables[0].Rows[k]["Basic"]);
                        CV.Basic = Convert.ToDouble(ds.Tables[0].Rows[k]["Basic"]);
                        CV.AllowClosingYN = true;
                    }
                    else
                    {
                        CV.BasicRevenue = null;
                        CV.ErrorCode = "Booking Not Closed in Cor-Drive";
                        CV.AllowClosingYN = false;
                    }

                    CV.Pickupdate = Convert.ToDateTime(ds.Tables[0].Rows[k]["Pickupdate"]);

                    CV.chargingstatus = Convert.ToString(ds.Tables[0].Rows[k]["chargingstatus"]);

                    CV.SendApprovalLinkYN = Convert.ToBoolean(ds.Tables[0].Rows[0]["SendApprovalLinkYN"]);

                    CV.ServiceTypeID = Convert.ToInt32(ds.Tables[0].Rows[0]["ServiceTypeID"]);

                    CV.FuelSurcharge = Convert.ToDouble(ds.Tables[0].Rows[0]["FuelSurcharge"]);

                    CV.ApproveYN = Convert.ToBoolean(ds.Tables[0].Rows[0]["ApproveYN"]);

                    CV.IndicatedPrice = Convert.ToDouble(ds.Tables[0].Rows[0]["IndicatedPrice"]);

                    //CV.AmexBatchYN = Convert.ToInt32(ds.Tables[0].Rows[k]["AmexBatch"]);


                    CV.AppDistance = Convert.ToDouble(ds.Tables[0].Rows[0]["AppDistance"]);
                    CV.OdometerDistance = Convert.ToDouble(ds.Tables[0].Rows[0]["OdometerDistance"]);
                    CV.GPSDistance = Convert.ToDouble(ds.Tables[0].Rows[0]["GPSDistance"]);
                    CV.GPSDistanceFromGarage = Convert.ToDouble(ds.Tables[0].Rows[0]["GPSDistanceFromGarage"]);
                    CV.IndicatedPrice = Convert.ToDouble(ds.Tables[0].Rows[0]["IndicatedPrice"]);
                    CV.PlatFormFee = Convert.ToDouble(ds.Tables[0].Rows[0]["PlatFormFee"]);
                    CV.OriginCode = Convert.ToString(ds.Tables[0].Rows[0]["OriginCode"]);

                    string closingAllowed = "";
                    closingAllowed = db.CheckClosingAllowed(CV.ClientCoID, CV.PaymentMode);


                    string strCheckClosingforCurrentMonth = "";
                    strCheckClosingforCurrentMonth = db.CheckClosingforCurrentMonth(CV.DateIn, DateTime.Now);

                    if (!string.IsNullOrEmpty(closingAllowed))
                    {
                        CV.ErrorCode = closingAllowed;
                        CV.AllowClosingYN = false;
                    }
                    else if (!string.IsNullOrEmpty(strCheckClosingforCurrentMonth))
                    {
                        CV.ErrorCode = strCheckClosingforCurrentMonth;
                        CV.AllowClosingYN = false;
                    }
                    else
                    {

                        if (CV.DateIn <= Convert.ToDateTime("2017-07-31") && CV.ClientCoID != 2348 && CV.ClientCoID != 2365
                            && CV.ClientCoID != 2129 && CV.ClientCoID != 3196
                            && CV.ClientCoID != 3365 && CV.ClientCoID != 3364 && CV.ClientCoID != 2762 && CV.ClientCoID != 2866
                            && CV.ClientCoID != 3456 && CV.ClientCoID != 2896 && CV.ClientCoID != 2680 && CV.ClientCoID != 2061
                            && CV.ClientCoID != 3261 && CV.ClientCoID != 3008 && CV.ClientCoID != 2113 && CV.ClientCoID != 3060
                            && CV.ClientCoID != 3061 && CV.ClientCoID != 2561
                            && CV.ClientCoID != 46 && CV.ClientCoID != 48 && CV.ClientCoID != 3621 && CV.ClientCoID != 497)
                        {
                            CV.ErrorCode = "Cannot Close bookings before July due to GST implementation.";
                            CV.AllowClosingYN = false;
                        }
                        //else if (CV.Pickupdate >= Convert.ToDateTime("2017-12-01") && (CV.ClientCoID == 3468 || CV.ClientCoID == 3410))
                        //{
                        //    CV.ErrorCode = "Blocked due to Rate revision";
                        //    CV.AllowClosingYN = false;
                        //}
                        //else if (CV.ClientCoID == 3247)
                        //{
                        //    CV.ErrorCode = "Stopped Auto Closure on request of Tej Prakash.";
                        //    CV.AllowClosingYN = false;
                        //}
                        else
                        {
                            if (CV.ClientCoID == 2205 || CV.ClientCoID == 631) // || CV.CreatedBy == 3064)
                            {
                                CV.AllowClosingYN = true;
                                CV.ErrorCode = "";
                            }
                            else
                            {
                                DataSet gstdetails = new DataSet();

                                gstdetails = db.CheckClientGSTDetails(BookingID);
                                if (gstdetails.Tables[0].Rows.Count <= 0)
                                {
                                    CV.ErrorCode = "GST information not registered.";
                                    CV.AllowClosingYN = false;
                                }
                            }
                        }
                    }

                    if (CV.LockYN == 1)
                    {
                        CV.ErrorCode = CV.LockReason;
                        CV.AllowClosingYN = false;
                    }

                    //if (CV.Pickupdate >= Convert.ToDateTime("2018-01-22") && (CV.ClientCoID == 3249 || CV.ClientCoID == 3247 || CV.ClientCoID == 3248))
                    //{
                    //    CV.ErrorCode = "Closing stopped based on Request from Manish Agarwal.";
                    //    CV.AllowClosingYN = false;
                    //}

                    //if (CV.ClientCoID == 3398 || CV.ClientCoID == 3300 || CV.ClientCoID == 3298 || CV.ClientCoID == 3299)
                    //{
                    //    CV.ErrorCode = "Closing Blocked by Tej, due to GST information pending.";
                    //    CV.AllowClosingYN = false;
                    //}
                    //if (CV.ClientCoID == 3566)
                    //{
                    //    CV.ErrorCode = "Closing stopped based on Request from Tej Prakash.";
                    //    CV.AllowClosingYN = false;
                    //}
                    //if (CV.Pickupdate >= Convert.ToDateTime("2018-01-01") && (CV.ClientCoID == 3298 || CV.ClientCoID == 3299
                    //        || CV.ClientCoID == 3300 || CV.ClientCoID == 3398))
                    //{
                    //    CV.ErrorCode = "Closing stopped based on Request from Manish Agarwal.";
                    //    CV.AllowClosingYN = false;
                    //}

                    if (CV.EliteCarYN && CV.PaymentMode.ToLower() != "cc")
                    {
                        CV.ErrorCode = "";
                        CV.AllowClosingYN = true;
                    }

                    if (CV.ClientCoID == 2205)
                    {
                        if (CV.IndicatedPrice > CV.TotalCost)
                        {
                            CV.AllowClosingYN = false;
                            CV.ErrorCode = "The Total amount is less than the Booked Amount, please correct and proceed.";
                        }
                    }

                    bool CompanyRegisterdYN = db.CheckCompanyGSTDetails(CV.CityID);
                    if (!CompanyRegisterdYN)
                    {
                        CV.AllowClosingYN = false;
                        CV.ErrorCode = "COR GST not available for Closing";
                    }

                    bool ParkingValidated = false;

                    ParkingValidated = db.CheckIfParkingTollValidated(BookingID);
                    if (!ParkingValidated)
                    {
                        CV.AllowClosingYN = false;
                        CV.ErrorCode = "Parking/Toll not validated, please Validate and try again.";
                    }

                    bool vehiclevalidated = false;
                    vehiclevalidated = db.CheckIfVehicleisValidated(BookingID);
                    if (!vehiclevalidated)
                    {
                        CV.AllowClosingYN = false;
                        CV.ErrorCode = "Vehicle documents not uploaded.";
                    }

                    if (CV.PendingYn)
                    {
                        CV.AllowClosingYN = false;
                        CV.ErrorCode = "Please contact CMT team, as Subsidiary is in Pending status and cannot Close.";
                    }

                    if (!CV.ApproveYN)
                    {
                        CV.AllowClosingYN = false;
                        CV.ErrorCode = "Car/Vendor is not verified and cannot Close.";
                    }

                    OverLappingVariables ov = new OverLappingVariables();
                    ov = db.CheckOverlapping(BookingID, CV.ClientCoID, CV.kmout, CV.KMIn
                        , CV.GuestOpDate, CV.GuestClDate, CV.GuestOpTime, CV.GuestClTime
                        , CV.CarID, CV.VendorCarYN, CV.OutstationYN);

                    if (ov.OverLappingYN)
                    {
                        CV.ErrorCode = ov.OverLappingMessage;
                        CV.AllowClosingYN = false;
                    }


                    //if (CV.AmexBatchYN == 1)
                    //{
                    //    CV.ErrorCode = "Batch Already Summited, please wait for 24 hours.";
                    //    CV.AllowClosingYN = false;
                    //}

                    //if ((CV.KMClose - CV.pointOpeniningKm) > 100)
                    //{
                    //    CV.ErrorCode = "KM greater than 100, will be manually closed.";
                    //    CV.AllowClosingYN = false;
                    //}

                    //if (CV.GPSDistanceFromGarage > 30 || CV.GPSDistanceToGarage > 30)
                    //{
                    //    CV.ErrorCode = "Garage from or to distance greater than 30, will be manually closed.";
                    //    CV.AllowClosingYN = false;
                    //}

                    ////if(db.IsMYFBooking(BookingID.ToString()))
                    ////{

                    ////}

                    //if (CV.AppDistance == 0)
                    //{
                    //    if (CV.OdometerDistance > (CV.GPSDistance * 120 / 100))
                    //    {
                    //        CV.ErrorCode = "Odometer distance is greater than 20% from GPS distance, will be manually closed.";
                    //        CV.AllowClosingYN = false;
                    //    }
                    //}
                    //else
                    //{
                    //    if (CV.OdometerDistance > (CV.AppDistance * 120 / 100) && CV.AppDistance > (CV.GPSDistance * 120 / 100))
                    //    {
                    //        CV.ErrorCode = "Odometer distance is greater than 20% from App distance, will be manually closed.";
                    //        CV.AllowClosingYN = false;
                    //    }
                    //}

                    //if (BookingID == 11312230)
                    //{
                    //    CV.ErrorCode = "testing testing";
                    //    CV.AllowClosingYN = false;
                    //}

                    //if (string.IsNullOrEmpty(Convert.ToString(CV.BasicRevenue)))
                    if (CV.ErrorCode == "")
                    {
                        CV.VoucherAmt = VoucherAmt(BookingID); //gets voucher amount
                        CV.MerchantID = UpdateCreditCardMerchant(BookingID, CV);  //updates the credit card Merchant

                        DataSet close = new DataSet();


                        try
                        {
                            //Service Tax calculation proccess start on 06-July-2017

                            SMSMailRef.SendSMSSoapClient smsservice = new SMSMailRef.SendSMSSoapClient();

                            TaxClass.TaxDetails taxpercent = GetTaxPercentMethod(CV.ClientCoID
                                , CV.CityID, CV.DateIn.ToShortDateString()
                                , CV.CarID, CV.SubsidiaryID
                                , CV.Pickupdate.ToShortDateString()
                                , CV.CentralizedYN);

                            CV.ServiceTaxPercent = Convert.ToDouble(taxpercent.ServiceTaxPercent);
                            CV.SwachhBharatTaxPercent = Convert.ToDouble(taxpercent.SwachhBharatTaxPercent);
                            CV.KrishiKalyanTaxPercent = Convert.ToDouble(taxpercent.KrishiKalyanTaxPercent);

                            CV.CGSTTaxPercent = Convert.ToDouble(taxpercent.CGSTPercent);
                            CV.SGSTTaxPercent = Convert.ToDouble(taxpercent.SGSTPercent);
                            CV.IGSTTaxPercent = Convert.ToDouble(taxpercent.IGSTPercent);
                            CV.ClientGSTId = Convert.ToInt32(taxpercent.ClientGSTId);

                            double SubTotal = 0;

                            //if (CV.ClientCoID == 2205 || CV.ClientCoID == 2257 || CV.CarModelId == 135)
                            //{
                            //    CV.Basic = (CV.Basic * 100) / (100 + CV.ServiceTaxPercent
                            //        + CV.SwachhBharatTaxPercent + CV.KrishiKalyanTaxPercent
                            //        + CV.CGSTTaxPercent + CV.SGSTTaxPercent + CV.IGSTTaxPercent);
                            //    //calculation specific to easy cabs and cor-retail to be done below
                            //}

                            /*
                            if (@ClientCoID = 1923 or @ClientCoID  = 1684 or @ClientCoID  = 2205 or @ClientCoID  = 2257 or @ClientCoID = 3164)
		                    begin 
                                set @OthersExcludingTaxes = (@Others * 100)/ (@totaltaxper + 100)      
                            end 
                            if (@CarCatID = 25)      
		                    begin    
				                set @OthersExcludingTaxes = isnull((select sum(ExtraAmount) from CorIntCarBookingExtraAmount     
				                where extrasDesc not in ('Airport Parking') and bookingid = @BookingID and isnull(ExcludeForTaxCalculationYN,0) = 0),0)    
                                set @Others = isnull(@OthersExcludingTaxes,0)
                            end
                             */

                            //Consolidated savings discount start 
                            //SMSMailRef.SendSMSSoapClient savings = new SMSMailRef.SendSMSSoapClient(); //Live

                            //CV.DiscountPC = 0;
                            SMSMailRef.SendSMSSoapClient savings = new SMSMailRef.SendSMSSoapClient(); //Live

                            //SendSMSService_Test.SendSMSSoapClient savings = new SendSMSService_Test.SendSMSSoapClient(); //Test
                            try
                            {
                                CV.DiscountPC = savings.GetDiscount(BookingID, CV.DateOut.ToShortDateString(), CV.ClientCoID, CV.CarCatId, CV.ServiceTypeID);
                            }
                            catch (Exception discount)
                            {
                                CV.DiscountPC = 0;
                            }

                            CV.BasicBeforeDiscount = CV.Basic;

                            if (CV.DiscountPC > 0)
                            {
                                CV.DiscountAmt = ((CV.Basic - CV.FuelSurcharge) * CV.DiscountPC) / 100;
                                CV.Basic = CV.Basic - CV.DiscountAmt;
                            }
                            //Consolidated savings discount end

                            if (CV.CarModelId == 135)
                            {
                                SubTotal = Math.Round(CV.Basic + CV.ParkTollChages + CV.InterstateTax + CV.Others + CV.SanatizationCharges, 2);
                            }
                            else
                            {
                                SubTotal = Math.Round(CV.Basic + CV.ParkTollChages + CV.InterstateTax + CV.SanatizationCharges, 2);
                            }

                            if (CV.OriginCode == "MMT")
                            {
                                CV.CGSTTaxPercent = 0;
                                CV.SGSTTaxPercent = 0;
                                CV.IGSTTaxPercent = 0;
                            }

                            CV.ServiceTaxAmt = Math.Round((SubTotal * CV.ServiceTaxPercent) / 100, 2);
                            CV.SwachhBharatTaxAmt = Math.Round((SubTotal * CV.SwachhBharatTaxPercent) / 100, 2);
                            CV.KrishiKalyanTaxAmt = Math.Round((SubTotal * CV.KrishiKalyanTaxPercent) / 100, 2);
                            CV.CGSTTaxAmt = Math.Round((SubTotal * CV.CGSTTaxPercent) / 100, 2);
                            CV.SGSTTaxAmt = Math.Round((SubTotal * CV.SGSTTaxPercent) / 100, 2);
                            CV.IGSTTaxAmt = Math.Round((SubTotal * CV.IGSTTaxPercent) / 100, 2);
                            CV.TotalCost = Math.Round(SubTotal + CV.CGSTTaxAmt + CV.SGSTTaxAmt + CV.IGSTTaxAmt + CV.ExtraAmount + CV.PlatFormFee, 2);
                            //Service Tax calculation proccess end   on 06-July-2017

                            CV.Remarks = "";


                            //checking closing allowed or not start
                            DataSet dscheckclosingAllowed = new DataSet();
                            dscheckclosingAllowed = db.GetValidateForClosing(CV.DateIn, System.DateTime.Now);

                            if (dscheckclosingAllowed.Tables[0].Rows.Count > 0)
                            {
                                if (Convert.ToInt32(dscheckclosingAllowed.Tables[0].Rows[0]["AllowClosing"]) != 1)
                                {
                                    CV.ErrorCode = Convert.ToString(dscheckclosingAllowed.Tables[0].Rows[0]["BookingStatus"]);
                                    return CV;
                                }
                                else
                                {
                                    CV.Accountingdate = Convert.ToDateTime(dscheckclosingAllowed.Tables[0].Rows[0]["AccountingDate"]);
                                }
                            }
                            //checking closing allowed or not end


                            close = db.CloseBooking_Confirm(CV, BookingID, UserID);  //closing Proc
                            if (close.Tables[0].Rows.Count > 0)
                            {
                                CV.InterUnitYN = close.Tables[0].Rows[0]["InterUnitYN"].ToString();
                            }
                        }
                        catch (Exception ex)
                        {
                            CV.ErrorCode = ex.ToString();
                        }
                    }
                }
            }
            else
            {
                CV.ErrorCode = "Not a CorDrive Booking.";
            }
            return CV;
        }

        //private static TaxClass.TaxDetails GetTaxPercentMethod(int ClientCoID, int PickupCityID, string DateIn, int CarID, int SubsidiaryID)
        public TaxClass.TaxDetails GetTaxPercentMethod(int ClientCoID, int PickupCityID
            , string DateIn, int CarID, int SubsidiaryID
            , string PickupDate, string CentralizedYN)
        {
            TaxClass.TaxDetails taxvariable = new TaxClass.TaxDetails();
            string TaxPercentResponse = "";
            if (CentralizedYN == "Y")
            {
                TaxPercentResponse = CallJson.doGet(CallJson.GetConfigurationValue("TaxCalcBkRestURL") + "?ClientCoID="
                    + ClientCoID + "&PickupCityID=" + PickupCityID + "&DateIn=" + DateIn
                    + "&CarID=" + CarID + "&SubsidiaryID=" + SubsidiaryID
                    + "&PickupDate=" + PickupDate);
            }
            else
            {
                TaxPercentResponse = CallJson.doGet(CallJson.GetConfigurationValue("TaxCalcRestURL") + "?ClientCoID="
                    + ClientCoID + "&PickupCityID=" + PickupCityID + "&DateIn=" + DateIn 
                    + "&CarID=" + CarID + "&SubsidiaryID=" + SubsidiaryID
                    + "&PickupDate=" + PickupDate);
            }
            taxvariable = JsonConvert.DeserializeObject<TaxClass.TaxDetails>(TaxPercentResponse);
            return taxvariable;
        }

        public ClosingVariables GetChargingDetails(int BookingID, int UserID)
        {
            DataSet ds = new DataSet();
            ClosingVariables CV = new ClosingVariables();
            ds = objadmin.GetChargingDetails(BookingID);
            if (ds.Tables[0].Rows.Count > 0)
            {

                for (int k = 0; k <= ds.Tables[0].Rows.Count - 1; k++)
                {
                    CV.CCAvenue = Convert.ToBoolean(ds.Tables[0].Rows[k]["CCAvenue"]);
                    CV.CustomPkgYN = Convert.ToBoolean(ds.Tables[0].Rows[k]["CustomPkgYN"]);
                    CV.OutstationYN = Convert.ToBoolean(ds.Tables[0].Rows[k]["OutstationYN"]);
                    CV.Service = Convert.ToString(ds.Tables[0].Rows[k]["Service"]);
                    CV.ServiceUnitID = Convert.ToInt32(ds.Tables[0].Rows[k]["ServiceUnitID"]);
                    CV.CityID = Convert.ToInt32(ds.Tables[0].Rows[k]["PickUpCityID"]);
                    CV.Status = Convert.ToString(ds.Tables[0].Rows[k]["Status"]);
                    CV.DateClose = Convert.ToDateTime(ds.Tables[0].Rows[k]["DateClose"]);
                    CV.TimeClose = Convert.ToString(ds.Tables[0].Rows[k]["TimeClose"]);
                    CV.DateIn = Convert.ToDateTime(ds.Tables[0].Rows[k]["DateIn"]);
                    CV.DateOut = Convert.ToDateTime(ds.Tables[0].Rows[k]["DateOut"]);
                    CV.TimeIn = Convert.ToString(ds.Tables[0].Rows[k]["TimeIn"]);
                    CV.TimeOut = Convert.ToString(ds.Tables[0].Rows[k]["TimeOut"]);
                    CV.KMClose = Convert.ToInt64(ds.Tables[0].Rows[k]["KMClose"]);
                    CV.KMIn = Convert.ToInt64(ds.Tables[0].Rows[k]["KMIn"]);
                    CV.kmout = Convert.ToInt64(ds.Tables[0].Rows[k]["kmout"]);
                    CV.pointOpeniningKm = Convert.ToInt64(ds.Tables[0].Rows[k]["pointOpeniningKm"]);
                    CV.GuestOpDate = Convert.ToDateTime(ds.Tables[0].Rows[k]["GuestOpDate"]);
                    CV.GuestClDate = Convert.ToDateTime(ds.Tables[0].Rows[k]["GuestClDate"]);
                    CV.GuestOpTime = Convert.ToString(ds.Tables[0].Rows[k]["GuestOpTime"]);
                    CV.GuestClTime = Convert.ToString(ds.Tables[0].Rows[k]["GuestClTime"]);
                    CV.ClientCoID = Convert.ToInt32(ds.Tables[0].Rows[k]["ClientCoID"]);
                    CV.CCType = Convert.ToInt32(ds.Tables[0].Rows[k]["CCType"]);
                    CV.FXDGarageRate = Convert.ToDouble(ds.Tables[0].Rows[k]["FXDGarageRate"]);
                    CV.ApprovalAmount = Convert.ToDouble(ds.Tables[0].Rows[k]["ApprovalAmount"]);
                    CV.ApprovalNo = Convert.ToString(ds.Tables[0].Rows[k]["ApprovalNo"]);
                    CV.ParkTollChages = Convert.ToInt32(ds.Tables[0].Rows[k]["ParkTollChages"]);
                    CV.InterstateTax = Convert.ToInt32(ds.Tables[0].Rows[k]["InterstateTax"]);
                    CV.PkgID = Convert.ToInt64(ds.Tables[0].Rows[k]["PkgID"]);
                    CV.ExtraHr = Convert.ToDouble(ds.Tables[0].Rows[k]["ExtraHr"]);
                    CV.ExtraKM = Convert.ToInt32(ds.Tables[0].Rows[k]["ExtraKM"]);

                    CV.ExtraHrRate = Convert.ToDouble(ds.Tables[0].Rows[k]["ExtraHrRate"]);
                    CV.ExtraKMRate = Convert.ToDouble(ds.Tables[0].Rows[k]["ExtraKMRate"]);
                    CV.NightStayAmt = Convert.ToDouble(ds.Tables[0].Rows[k]["NightStayAmt"]);
                    CV.OutStnAmt = Convert.ToDouble(ds.Tables[0].Rows[k]["OutStnAmt"]);
                    CV.DiscountPC = Convert.ToDouble(ds.Tables[0].Rows[k]["DiscountPC"]);
                    CV.PkgRate = Convert.ToDouble(ds.Tables[0].Rows[k]["PkgRate"]);
                    CV.FuelSurcharge = Convert.ToDouble(ds.Tables[0].Rows[k]["FuelSurcharge"]);
                    CV.TotalHrsUsed = Convert.ToDouble(ds.Tables[0].Rows[k]["TotalHrsUsed"]);
                    CV.NoNight = Convert.ToInt32(ds.Tables[0].Rows[k]["NoNight"]);
                    CV.PaymentMode = Convert.ToString(ds.Tables[0].Rows[k]["PaymentMode"]);
                    CV.CarID = Convert.ToInt32(ds.Tables[0].Rows[k]["CarID"]);
                    CV.ChauffeurID = Convert.ToInt32(ds.Tables[0].Rows[k]["ChauffeurID"]);
                    CV.VendorCarYN = Convert.ToBoolean(ds.Tables[0].Rows[k]["VendorCarYN"]);
                    CV.VendorChauffYN = Convert.ToBoolean(ds.Tables[0].Rows[k]["VendorChauffYN"]);
                    CV.ServiceTaxPercent = Convert.ToDouble(ds.Tables[0].Rows[k]["ServiceTaxPercent"]);
                    CV.EduCessPercent = Convert.ToDouble(ds.Tables[0].Rows[k]["EduCessPercent"]);
                    CV.HduCessPercent = Convert.ToDouble(ds.Tables[0].Rows[k]["HduCessPercent"]);
                    CV.DSTPercent = Convert.ToDouble(ds.Tables[0].Rows[k]["DSTPercent"]);
                    CV.PreAuthNotRequire = Convert.ToBoolean(ds.Tables[0].Rows[k]["PreAuthNotRequire"]);
                    CV.TotalCost = Convert.ToDouble(ds.Tables[0].Rows[k]["TotalCost"]);
                    CV.CalculatedAmount = Convert.ToDouble(ds.Tables[0].Rows[k]["CalculatedAmount"]);
                    CV.PaidFullYN = Convert.ToInt32(ds.Tables[0].Rows[k]["PaidFullYN"]);
                    if (ds.Tables[0].Rows[k]["PaidFullDate"].ToString() != "")
                    {
                        CV.PaidFullDate = Convert.ToDateTime(ds.Tables[0].Rows[k]["PaidFullDate"]);
                    }
                    CV.Phone1 = Convert.ToString(ds.Tables[0].Rows[k]["Phone1"]);
                    CV.CCNo = Convert.ToString(ds.Tables[0].Rows[k]["CCNo"]);
                    CV.ExpYYMM = Convert.ToString(ds.Tables[0].Rows[k]["expyymm"]);
                    CV.BillingBasis = Convert.ToString(ds.Tables[0].Rows[k]["BillingBasis"]);
                    CV.ServiceTaxAmt = Convert.ToDouble(ds.Tables[0].Rows[k]["ServiceTaxAmt"]);
                    CV.HduTaxAmt = Convert.ToDouble(ds.Tables[0].Rows[k]["HduTaxAmt"]);

                    CV.EduTaxAmt = Convert.ToDouble(ds.Tables[0].Rows[k]["EduTaxAmt"]);
                    CV.VatAmt = Convert.ToDouble(ds.Tables[0].Rows[k]["VatAmt"]);
                    CV.Others = Convert.ToDouble(ds.Tables[0].Rows[k]["Others"]);
                    CV.SwachhBharatTaxPercent = Convert.ToDouble(ds.Tables[0].Rows[k]["SwachhBharatTaxPercent"]);
                    CV.SwachhBharatTaxAmt = Convert.ToDouble(ds.Tables[0].Rows[k]["SwachhBharatTaxAmt"]);
                    CV.applyservicetaxyn = Convert.ToBoolean(ds.Tables[0].Rows[k]["applyservicetaxyn"]);
                    CV.KrishiKalyanTaxPercent = Convert.ToDouble(ds.Tables[0].Rows[k]["KrishiKalyanTaxPercent"]);
                    CV.KrishiKalyanTaxAmt = Convert.ToDouble(ds.Tables[0].Rows[k]["KrishiKalyanTaxAmt"]);
                    //CV.KrishiYN = Convert.ToString(ds.Tables[0].Rows[k]["KrishiYN"]);
                    CV.hdncontinueYN = "";
                    CV.AmexLinkYN = false;
                    CV.DSStatus = "O";
                    CV.BookingStatus = "O";
                    CV.ClientCoIndivID = Convert.ToInt64(ds.Tables[0].Rows[k]["ClientCoIndivID"]);
                    CV.isPaymateCorporateModule = Convert.ToBoolean(ds.Tables[0].Rows[k]["isPaymateCorporateModule"]);
                    CV.EmailID = Convert.ToString(ds.Tables[0].Rows[k]["EmailID"]);
                    CV.ServiceType = Convert.ToString(ds.Tables[0].Rows[k]["ServiceType"]);
                    CV.trackid = Convert.ToString(ds.Tables[0].Rows[k]["trackid"]);
                    CV.CreatedBy = Convert.ToInt32(ds.Tables[0].Rows[k]["CreatedBy"]);

                    CV.transactionid = Convert.ToString(ds.Tables[0].Rows[k]["transactionid"]);
                    CV.authorizationid = Convert.ToString(ds.Tables[0].Rows[k]["authorizationid"]);
                    CV.ProvisionalInvoiceForGuestYN = Convert.ToBoolean(ds.Tables[0].Rows[k]["ProvisionalInvoiceForGuestYN"]);
                    CV.ProvisionalInvoiceForFacilitatorYN = Convert.ToBoolean(ds.Tables[0].Rows[k]["ProvisionalInvoiceForFacilitatorYN"]);
                    CV.ProvisionalInvoiceForTravelDeskYN = Convert.ToBoolean(ds.Tables[0].Rows[k]["ProvisionalInvoiceForTravelDeskYN"]);
                    CV.chargingstatus = Convert.ToString(ds.Tables[0].Rows[k]["chargingstatus"]);
                    //CV.CityName = Convert.ToString(ds.Tables[0].Rows[k]["CityName"]);
                    //if (CV.BillingBasis != "pp" || CV.Service == "A" || CV.Service == "T")
                    //{
                    //    CV.GuestOpDate = CV.DateOut;
                    //    CV.GuestClDate = CV.DateIn;
                    //    CV.GuestOpTime = CV.TimeOut;
                    //    CV.GuestClTime = CV.TimeIn;
                    //    CV.pointOpeniningKm = CV.kmout;
                    //}
                    CV.AmexSecondaryChargingYN = false;
                    CV.ErrorCode = "";
                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[k]["Basic"].ToString()))
                    {
                        CV.BasicRevenue = Convert.ToDouble(ds.Tables[0].Rows[k]["Basic"]);
                    }
                    else
                    {
                        CV.BasicRevenue = null;
                    }

                    if (CV.BasicRevenue == null)
                    {
                        CV.ErrorCode = "Booking Not Closed yet.";
                    }
                    else
                    {

                    }

                    CV.SendApprovalLinkYN = Convert.ToBoolean(ds.Tables[0].Rows[k]["SendApprovalLinkYN"]);
                    CV.chargingstatus = Convert.ToString(ds.Tables[0].Rows[k]["chargingstatus"]);
                    if (CV.chargingstatus == "Charged")
                    {
                        CV.ErrorCode = "Success";
                        CV.DSStatus = "C";
                        CV.BookingStatus = "C";
                    }

                    CV.AmexBatch = Convert.ToString(ds.Tables[0].Rows[k]["AmexBatch"]);
                }
            }
            else
            {
                CV.ErrorCode = "Technical issue in Charging service.";
            }
            return CV;
        }


        public string UpdateCreditCardMerchant(int BookingID, ClosingVariables CV)
        {
            string MerchantID = "";
            if (CV.CCType == 1 || CV.CCType == 2 || CV.CCType == 3)
            {
                if (CV.CCType == 1 || CV.CCType == 3)
                {
                    MerchantID = "8607";
                }
                else if (CV.CCType == 2)
                {
                    MerchantID = "9820390193";
                }
                objadmin.UpdateMerchantID(BookingID, MerchantID, CV.CCType); //test the code 
            }
            return MerchantID;
        }

        public double VoucherAmt(int BookingID)
        {
            double voucherAmt = 0;
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@BookingID", BookingID);

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset("prc_displayvoucherdetailDS", param);
            ClosingVariables CV = new ClosingVariables();
            CV.VoucherAmt = 0;
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int m = 0; m <= ds.Tables[0].Rows.Count - 1; m++)
                {
                    voucherAmt = CV.VoucherAmt + Convert.ToDouble(ds.Tables[0].Rows[m]["Denomination"]);
                }
            }
            else
            {
                voucherAmt = 0;
            }
            return voucherAmt;
        }

        public ClosingVariables GetClosingDetails_NonCorDrive(int BookingID)
        {

            DataSet ds = new DataSet();
            ClosingVariables CV = new ClosingVariables();
            ds = objadmin.GetBookingDetails_NonCorDrive(BookingID);
            if (ds.Tables[0].Rows.Count > 0)
            {

                for (int k = 0; k <= ds.Tables[0].Rows.Count - 1; k++)
                {
                    CV.CustomPkgYN = Convert.ToBoolean(ds.Tables[0].Rows[k]["CustomPkgYN"]);
                    CV.OutstationYN = Convert.ToBoolean(ds.Tables[0].Rows[k]["OutstationYN"]);
                    CV.Service = Convert.ToString(ds.Tables[0].Rows[k]["Service"]);
                    CV.ServiceUnitID = Convert.ToInt32(ds.Tables[0].Rows[k]["ServiceUnitID"]);
                    CV.CityID = Convert.ToInt32(ds.Tables[0].Rows[k]["PickUpCityID"]);
                    CV.Status = Convert.ToString(ds.Tables[0].Rows[k]["Status"]);
                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[k]["DateOut"].ToString()))
                    {
                        CV.DateOut = Convert.ToDateTime(ds.Tables[0].Rows[k]["DateOut"]);
                    }

                    if (CV.Status != "X")
                    {
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[k]["DateClose"].ToString()))
                        {
                            CV.DateClose = Convert.ToDateTime(ds.Tables[0].Rows[k]["DateClose"]);
                        }
                        CV.TimeClose = Convert.ToString(ds.Tables[0].Rows[k]["TimeClose"]);
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[k]["DateIn"].ToString()))
                        {
                            CV.DateIn = Convert.ToDateTime(ds.Tables[0].Rows[k]["DateIn"]);
                        }

                        CV.TimeIn = Convert.ToString(ds.Tables[0].Rows[k]["TimeIn"]);
                        CV.TimeOut = Convert.ToString(ds.Tables[0].Rows[k]["TimeOut"]);
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[k]["KMClose"].ToString()))
                        {
                            CV.KMClose = Convert.ToInt64(ds.Tables[0].Rows[k]["KMClose"]);
                        }
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[k]["KMIn"].ToString()))
                        {
                            CV.KMIn = Convert.ToInt64(ds.Tables[0].Rows[k]["KMIn"]);
                        }
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[k]["kmout"].ToString()))
                        {
                            CV.kmout = Convert.ToInt64(ds.Tables[0].Rows[k]["kmout"]);
                        }
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[k]["pointOpeniningKm"].ToString()))
                        {
                            CV.pointOpeniningKm = Convert.ToInt64(ds.Tables[0].Rows[k]["pointOpeniningKm"]);
                        }

                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[k]["GuestOpDate"].ToString()))
                        {
                            CV.GuestOpDate = Convert.ToDateTime(ds.Tables[0].Rows[k]["GuestOpDate"]);
                        }
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[k]["GuestClDate"].ToString()))
                        {
                            CV.GuestClDate = Convert.ToDateTime(ds.Tables[0].Rows[k]["GuestClDate"]);
                        }
                        CV.GuestOpTime = Convert.ToString(ds.Tables[0].Rows[k]["GuestOpTime"]);
                        CV.GuestClTime = Convert.ToString(ds.Tables[0].Rows[k]["GuestClTime"]);
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[k]["FXDGarageRate"].ToString()))
                        {
                            CV.FXDGarageRate = Convert.ToDouble(ds.Tables[0].Rows[k]["FXDGarageRate"]);
                        }

                        CV.ParkTollChages = Convert.ToInt32(ds.Tables[0].Rows[k]["ParkTollChages"]);
                        CV.InterstateTax = Convert.ToInt32(ds.Tables[0].Rows[k]["InterstateTax"]);
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[k]["PkgID"].ToString()))
                        {
                            CV.PkgID = Convert.ToInt64(ds.Tables[0].Rows[k]["PkgID"]);
                        }
                        CV.ExtraHr = Convert.ToDouble(ds.Tables[0].Rows[k]["ExtraHr"]);
                        CV.ExtraKM = Convert.ToInt32(ds.Tables[0].Rows[k]["ExtraKM"]);

                        CV.ExtraHrRate = Convert.ToDouble(ds.Tables[0].Rows[k]["ExtraHrRate"]);
                        CV.ExtraKMRate = Convert.ToDouble(ds.Tables[0].Rows[k]["ExtraKMRate"]);
                        CV.NightStayAmt = Convert.ToDouble(ds.Tables[0].Rows[k]["NightStayAmt"]);
                        CV.OutStnAmt = Convert.ToDouble(ds.Tables[0].Rows[k]["OutStnAmt"]);
                        CV.DiscountPC = Convert.ToDouble(ds.Tables[0].Rows[k]["DiscountPC"]);
                        CV.PkgRate = Convert.ToDouble(ds.Tables[0].Rows[k]["PkgRate"]);
                        CV.FuelSurcharge = Convert.ToDouble(ds.Tables[0].Rows[k]["FuelSurcharge"]);
                        CV.TotalHrsUsed = Convert.ToDouble(ds.Tables[0].Rows[k]["TotalHrsUsed"]);
                        CV.NoNight = Convert.ToInt32(ds.Tables[0].Rows[k]["NoNight"]);

                        CV.CarID = Convert.ToInt32(ds.Tables[0].Rows[k]["CarID"]);
                        CV.ChauffeurID = Convert.ToInt32(ds.Tables[0].Rows[k]["ChauffeurID"]);
                        CV.VendorCarYN = Convert.ToBoolean(ds.Tables[0].Rows[k]["VendorCarYN"]);
                        CV.VendorChauffYN = Convert.ToBoolean(ds.Tables[0].Rows[k]["VendorChauffYN"]);
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[k]["ServiceTaxPercent"].ToString()))
                        {
                            CV.ServiceTaxPercent = Convert.ToDouble(ds.Tables[0].Rows[k]["ServiceTaxPercent"]);
                        }
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[k]["EduCessPercent"].ToString()))
                        {
                            CV.EduCessPercent = Convert.ToDouble(ds.Tables[0].Rows[k]["EduCessPercent"]);
                        }
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[k]["HduCessPercent"].ToString()))
                        {
                            CV.HduCessPercent = Convert.ToDouble(ds.Tables[0].Rows[k]["HduCessPercent"]);
                        }
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[k]["DSTPercent"].ToString()))
                        {
                            CV.DSTPercent = Convert.ToDouble(ds.Tables[0].Rows[k]["DSTPercent"]);
                        }

                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[k]["ServiceTaxAmt"].ToString()))
                        {
                            CV.ServiceTaxAmt = Convert.ToDouble(ds.Tables[0].Rows[k]["ServiceTaxAmt"]);
                        }
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[k]["HduTaxAmt"].ToString()))
                        {
                            CV.HduTaxAmt = Convert.ToDouble(ds.Tables[0].Rows[k]["HduTaxAmt"]);
                        }
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[k]["EduTaxAmt"].ToString()))
                        {
                            CV.EduTaxAmt = Convert.ToDouble(ds.Tables[0].Rows[k]["EduTaxAmt"]);
                        }
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[k]["VatAmt"].ToString()))
                        {
                            CV.VatAmt = Convert.ToDouble(ds.Tables[0].Rows[k]["VatAmt"]);
                        }
                        CV.Others = Convert.ToDouble(ds.Tables[0].Rows[k]["Others"]);
                        CV.SwachhBharatTaxPercent = Convert.ToDouble(ds.Tables[0].Rows[k]["SwachhBharatTaxPercent"]);
                        CV.SwachhBharatTaxAmt = Convert.ToDouble(ds.Tables[0].Rows[k]["SwachhBharatTaxAmt"]);
                        CV.applyservicetaxyn = Convert.ToBoolean(ds.Tables[0].Rows[k]["applyservicetaxyn"]);
                        CV.KrishiKalyanTaxPercent = Convert.ToDouble(ds.Tables[0].Rows[k]["KrishiKalyanTaxPercent"]);
                        CV.KrishiKalyanTaxAmt = Convert.ToDouble(ds.Tables[0].Rows[k]["KrishiKalyanTaxAmt"]);

                        if (CV.BillingBasis != "pp" || CV.Service == "A" || CV.Service == "T")
                        {
                            CV.GuestOpDate = CV.DateOut;
                            CV.GuestClDate = CV.DateIn;
                            CV.GuestOpTime = CV.TimeOut;
                            CV.GuestClTime = CV.TimeIn;
                            CV.pointOpeniningKm = CV.kmout;
                        }

                        CV.CaptureTransactionID = Convert.ToString(ds.Tables[0].Rows[k]["transactionid"]);
                        CV.CaptureAmount = Convert.ToDouble(ds.Tables[0].Rows[k]["approvalAmt"]);
                    }


                    CV.ClientCoID = Convert.ToInt32(ds.Tables[0].Rows[k]["ClientCoID"]);
                    if (CV.Status != "X")
                    {
                        CV.CCType = Convert.ToInt32(ds.Tables[0].Rows[k]["CCType"]);
                    }
                    else
                    {
                        CV.CCType = Convert.ToInt32(ds.Tables[0].Rows[k]["GuestCCType"]);
                    }


                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[k]["ApprovalAmount"].ToString()))
                    {
                        CV.ApprovalAmount = Convert.ToDouble(ds.Tables[0].Rows[k]["ApprovalAmount"]);
                    }
                    CV.ApprovalNo = Convert.ToString(ds.Tables[0].Rows[k]["ApprovalNo"]);

                    CV.PaymentMode = Convert.ToString(ds.Tables[0].Rows[k]["PaymentMode"]);

                    CV.PreAuthNotRequire = Convert.ToBoolean(ds.Tables[0].Rows[k]["PreAuthNotRequire"]);
                    CV.TotalCost = Convert.ToDouble(ds.Tables[0].Rows[k]["TotalCost"]);
                    CV.CalculatedAmount = Convert.ToDouble(ds.Tables[0].Rows[k]["CalculatedAmount"]);
                    CV.PaidFullYN = Convert.ToInt32(ds.Tables[0].Rows[k]["PaidFullYN"]);
                    if (ds.Tables[0].Rows[k]["PaidFullDate"].ToString() != "")
                    {
                        CV.PaidFullDate = Convert.ToDateTime(ds.Tables[0].Rows[k]["PaidFullDate"]);
                    }
                    CV.Phone1 = Convert.ToString(ds.Tables[0].Rows[k]["Phone1"]);
                    CV.CCNo = Convert.ToString(ds.Tables[0].Rows[k]["CCNo"]);
                    CV.BillingBasis = Convert.ToString(ds.Tables[0].Rows[k]["BillingBasis"]);

                    //CV.KrishiYN = Convert.ToString(ds.Tables[0].Rows[k]["KrishiYN"]);
                    CV.hdncontinueYN = "";
                    CV.AmexLinkYN = false;
                    CV.DSStatus = "O";
                    CV.BookingStatus = "O";
                    CV.ClientCoIndivID = Convert.ToInt64(ds.Tables[0].Rows[k]["ClientCoIndivID"]);
                    CV.isPaymateCorporateModule = Convert.ToBoolean(ds.Tables[0].Rows[k]["isPaymateCorporateModule"]);
                    CV.EmailID = Convert.ToString(ds.Tables[0].Rows[k]["EmailID"]);
                    CV.ServiceType = Convert.ToString(ds.Tables[0].Rows[k]["ServiceType"]);
                    CV.CityName = Convert.ToString(ds.Tables[0].Rows[k]["CityName"]);

                    CV.AmexSecondaryChargingYN = false;
                    CV.transactionid = Convert.ToString(ds.Tables[0].Rows[k]["BookingTransactionID"]);
                    if (CV.transactionid != "" && CV.transactionid != "NoPreAuth" && (CV.CCType == 1 || CV.CCType == 3))
                    {
                        CV.PreAuthYN = true;
                    }
                    CV.ErrorCode = "";
                    CV.TrackingID = Convert.ToString(ds.Tables[0].Rows[k]["trackid"]);
                    CV.PaymateSysRegCode = Convert.ToString(ds.Tables[0].Rows[k]["PaymateSysRegCode"]);

                    CV.AutoPostBooking = Convert.ToBoolean(ds.Tables[0].Rows[k]["AutoPostBooking"]);

                    CV.BatchStatus = Convert.ToString(ds.Tables[0].Rows[k]["BatchStatus"]);

                    CV.NewMVPreauthYN = Convert.ToBoolean(ds.Tables[0].Rows[k]["NewMVPreauthYN"]);

                }
            }
            else
            {
                CV.ErrorCode = "Not a CorDrive Booking.";
            }
            return CV;
        }


        public string CloseWithOutParkingToll(int BookingID, int UserId, bool NotifyGuestYN)
        {
            string Status = "", mailstatus = "";
            DataSet ds = new DataSet();
            ClosingVariables CV = new ClosingVariables();
            ChargingWebService.DB db = new ChargingWebService.DB();
            bool ManualCloseYN = true;
            ManualCloseYN = db.CheckClosedDuty(BookingID);
            if (ManualCloseYN)
            {
                ds = objadmin.GetBookingDetails(BookingID);
            }
            else
            {
                ds = objadmin.GetBookingDetails_Confirm(BookingID);
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int k = 0; k <= ds.Tables[0].Rows.Count - 1; k++)
                {
                    CV.BookingId = BookingID;
                    CV.ClientCoID = Convert.ToInt32(ds.Tables[0].Rows[k]["ClientCoID"]);
                    CV.CityID = Convert.ToInt32(ds.Tables[0].Rows[k]["PickUpCityID"]);
                    CV.DateOut = Convert.ToDateTime(ds.Tables[0].Rows[k]["DateOut"]);
                    CV.DateIn = Convert.ToDateTime(ds.Tables[0].Rows[k]["DateIn"]);
                    CV.CarID = Convert.ToInt32(ds.Tables[0].Rows[k]["CarID"]);
                    CV.SubsidiaryID = Convert.ToInt32(ds.Tables[0].Rows[k]["SubsidiaryID"]);
                    CV.Basic = Convert.ToDouble(ds.Tables[0].Rows[k]["Basic"]);
                    CV.FuelSurcharge = Convert.ToDouble(ds.Tables[0].Rows[k]["FuelSurcharge"]);
                    CV.DiscountPC = Convert.ToDouble(ds.Tables[0].Rows[k]["DiscountPC"]);
                    CV.ServiceTypeID = Convert.ToInt32(ds.Tables[0].Rows[0]["ServiceTypeID"]);
                    CV.CarCatId = Convert.ToInt32(ds.Tables[0].Rows[k]["Carcatid"]);
                    CV.PaymentMode = Convert.ToString(ds.Tables[0].Rows[k]["PaymentMode"]);
                    CV.Others = Convert.ToDouble(ds.Tables[0].Rows[k]["Others"]);
                    CV.DSStatus = Convert.ToString(ds.Tables[0].Rows[k]["Status"]);
                    CV.SanatizationCharges = Convert.ToDouble(ds.Tables[0].Rows[k]["SanatizationCharges"]);
                    string closingAllowbasedonAccountingdate = db.CheckClosingforCurrentMonth(Convert.ToDateTime(ds.Tables[0].Rows[k]["DateIn"]), System.DateTime.Now);

                    if (string.IsNullOrEmpty(closingAllowbasedonAccountingdate))
                    {

                        if (CV.DSStatus == "C")
                        {
                            Status = "Booking Already Closed.";
                        }
                        else
                        {
                            SMSMailRef.SendSMSSoapClient smsservice = new SMSMailRef.SendSMSSoapClient();

                            TaxClass.TaxDetails taxpercent = GetTaxPercentMethod(CV.ClientCoID, CV.CityID
                                , CV.DateIn.ToShortDateString()
                                , CV.CarID, CV.SubsidiaryID, CV.Pickupdate.ToShortDateString()
                                , CV.CentralizedYN);

                            CV.ServiceTaxPercent = Convert.ToDouble(taxpercent.ServiceTaxPercent);
                            CV.SwachhBharatTaxPercent = Convert.ToDouble(taxpercent.SwachhBharatTaxPercent);
                            CV.KrishiKalyanTaxPercent = Convert.ToDouble(taxpercent.KrishiKalyanTaxPercent);

                            CV.CGSTTaxPercent = Convert.ToDouble(taxpercent.CGSTPercent);
                            CV.SGSTTaxPercent = Convert.ToDouble(taxpercent.SGSTPercent);
                            CV.IGSTTaxPercent = Convert.ToDouble(taxpercent.IGSTPercent);
                            CV.ClientGSTId = Convert.ToInt32(taxpercent.ClientGSTId);

                            //double SubTotal = 0;

                            SMSMailRef.SendSMSSoapClient savings = new SMSMailRef.SendSMSSoapClient(); //Live

                            //SendSMSService_Test.SendSMSSoapClient savings = new SendSMSService_Test.SendSMSSoapClient(); //Test

                            CV.Basic = CV.Basic - CV.Others;

                            CV.DiscountPC = savings.GetDiscount(CV.BookingId, CV.DateOut.ToShortDateString(), CV.ClientCoID, CV.CarCatId, CV.ServiceTypeID);

                            if (CV.DiscountPC > 0)
                            {
                                CV.DiscountAmt = (((CV.Basic + CV.SanatizationCharges) - CV.FuelSurcharge) * CV.DiscountPC) / 100;
                                CV.Basic = CV.Basic - CV.DiscountAmt;
                            }

                            CV.ParkTollChages = 0;
                            CV.InterstateTax = 0;
                            CV.Others = 0;

                            //SubTotal = Math.Round(CV.Basic, 2);

                            CV.ServiceTaxAmt = Math.Round(((CV.Basic + CV.SanatizationCharges) * CV.ServiceTaxPercent) / 100, 2);
                            CV.SwachhBharatTaxAmt = Math.Round(((CV.Basic + CV.SanatizationCharges) * CV.SwachhBharatTaxPercent) / 100, 2);
                            CV.KrishiKalyanTaxAmt = Math.Round(((CV.Basic + CV.SanatizationCharges) * CV.KrishiKalyanTaxPercent) / 100, 2);
                            CV.CGSTTaxAmt = Math.Round(((CV.Basic + CV.SanatizationCharges) * CV.CGSTTaxPercent) / 100, 2);
                            CV.SGSTTaxAmt = Math.Round(((CV.Basic + CV.SanatizationCharges) * CV.SGSTTaxPercent) / 100, 2);
                            CV.IGSTTaxAmt = Math.Round(((CV.Basic + CV.SanatizationCharges) * CV.IGSTTaxPercent) / 100, 2);
                            //CV.TotalCost = Math.Round(CV.Basic + CV.CGSTTaxAmt + CV.SGSTTaxAmt + CV.IGSTTaxAmt + CV.ExtraAmount, 0);
                            CV.TotalCost = Math.Round(CV.Basic + CV.SanatizationCharges + CV.CGSTTaxAmt + CV.SGSTTaxAmt + CV.IGSTTaxAmt, 0);

                            CV.Remarks = "Auto Closed After 72 hours with out Parking / Toll as it's Slip is not Provided after Validation.";
                            CV.BookingStatus = "C";
                            CV.DSStatus = "C";

                            DataSet close = new DataSet();
                            close = db.CloseBooking_WithOutParkingToll(CV, BookingID, UserId);  //closing Proc
                            if (close.Tables[0].Rows.Count > 0)
                            {
                                Status = "Success";

                                if (NotifyGuestYN)
                                {
                                    //send invoice mailer to guest
                                    mailstatus = objadmin.SendInvoiceMail(BookingID, CV.SendApprovalLinkYN);
                                }
                            }
                        }
                    }
                    else
                    {
                        Status = closingAllowbasedonAccountingdate;
                    }
                }
            }
            return Status;
        }
    }
}