﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

public class EInv
{
    public EInv()
    {
        //
        // TODO: Add constructor logic here
        //
    }

}
public class EBilling
{
    public string supplier_GSTIN { get; set; }
    public string supplier_Legal_Name { get; set; }
    public string supplier_trading_name { get; set; }
    public string supplier_City { get; set; }
    public string supplier_Address1 { get; set; }
    public string supplier_Address2 { get; set; }
    public string supplier_State { get; set; }
    public string supplier_Pincode { get; set; }
    //public string supplier_Phone { get; set; }
    //public string supplier_Email { get; set; }
    public List<InvoiceDetails> inv { get; set; }
}

public class InvoiceDetails
{
    public transaction_details transaction_details { get; set; }
    public document_details document_details { get; set; }
    //public export_details export_info { get; set; }
    //public extra_Information extra_Info { get; set; }
    public billing_Information billing_Information { get; set; }
    public shipping_Information shipping_Information { get; set; }
    public delivery_Information delivery_Information { get; set; }
    //public payee_Information payee_info { get; set; }
    //public ewaybill_information ewaybill_info { get; set; }
    public document_Total document_Total { get; set; }
    public List<items> items { get; set; }
}

public class transaction_details
{
    public string transactionMode { get; set; }
    public string invoice_type_code { get; set; }
    public string reversecharge { get; set; }
    public string ecom_GSTIN { get; set; }
    //public string IgstOnIntra { get; set; }
}

public class document_details
{
    public string invoice_subtype_code { get; set; }
    public string invoiceNum { get; set; }
    public string invoiceDate { get; set; }
    public string transaction_id { get; set; }
    public string plant { get; set; }
    public string custom { get; set; }
}

public class export_details
{
    public string shipping_bill_no { get; set; }
    public string shipping_bill_date { get; set; }
    public string port_code { get; set; }
    public string invoice_currency_code { get; set; }
    public string cnt_code { get; set; }
    public string refClm { get; set; }
    public string ExpDuty { get; set; }
}

public class extra_Information
{
    public string invoice_Period_Start_Date { get; set; }
    public string invoice_Period_End_Date { get; set; }
    public string preceeding_Invoice_Number { get; set; }
    public string preceeding_Invoice_Date { get; set; }
    public string invoice_Document_Reference { get; set; }
    public string receipt_Advice_ReferenceNo { get; set; }
    public string receipt_Advice_ReferenceDt { get; set; }
    public string tender_or_Lot_Reference { get; set; }
    public string contract_Reference { get; set; }
    public string external_Reference { get; set; }
    public string project_Reference { get; set; }
    public string refNum { get; set; }
    public string refDate { get; set; }
    public string remarks { get; set; }
    public string Url { get; set; }
    public string Docs { get; set; }
    public string Info { get; set; }
}

public class billing_Information
{
    public string billing_Name { get; set; }
    public string billing_trade_name { get; set; }
    public string billing_GSTIN { get; set; }
    public string billing_POS { get; set; }
    public string billing_City { get; set; }
    public string billing_State { get; set; }
    public string billing_Address1 { get; set; }
    public string billing_Address2 { get; set; }
    public string billing_Pincode { get; set; }
    //public string billing_Phone { get; set; }
    //public string billing_Email { get; set; }
}

public class shipping_Information
{
    public string shippingTo_Name { get; set; }
    public string shippingTo_trade_name { get; set; }
    public string shippingTo_GSTIN { get; set; }
    public string shippingTo_Place { get; set; }
    public string shippingTo_State { get; set; }
    public string shippingTo_Address1 { get; set; }
    public string shippingTo_Address2 { get; set; }
    public string shippingTo_Pincode { get; set; }
}

public class delivery_Information
{
    public string company_Name { get; set; }
    public string city { get; set; }
    public string state { get; set; }
    public string address1 { get; set; }
    public string address2 { get; set; }
    public string pincode { get; set; }
}

public class payee_Information
{
    public string payee_Name { get; set; }
    public string payer_Financial_Account { get; set; }
    public string modeofPayment { get; set; }
    public string financial_Institution_Branch { get; set; }
    public string payment_Terms { get; set; }
    public string payment_Instruction { get; set; }
    public string credit_Transfer { get; set; }
    public string direct_Debit { get; set; }
    public string creditDays { get; set; }
    public string paid_amount { get; set; }
    public string amount_due_for_payment { get; set; }
}

public class ewaybill_information
{
    public string ewb_transporter_id { get; set; }
    public string ewb_transMode { get; set; }
    public string ewb_transDistance { get; set; }
    public string ewb_transporterName { get; set; }
    public string ewb_transDocNo { get; set; }
    public string ewb_transDocDt { get; set; }
    public string ewb_vehicleNo { get; set; }
    public string ewb_subSupplyType { get; set; }
    public string ewb_vehicleType { get; set; }
}

public class document_Total
{
    public string roundoff { get; set; }
    public string total_assVal { get; set; }
    public string total_Invoice_Value { get; set; }
    public string igstvalue { get; set; }
    public string cgstvalue { get; set; }
    public string sgstvalue { get; set; }
    public string cessvalue { get; set; }
    public string stateCessValue { get; set; }
    public string val_for_cur { get; set; }
    public string Discount { get; set; }
    public string OthChrg { get; set; }
}

public class items
{
    public Int32 slno { get; set; }
    public string item_Description { get; set; }
    public string service { get; set; }
    public string hsn_code { get; set; }
    //public batch bth { get; set; }

    public string barcode { get; set; }
    public int quantity { get; set; }
    //public int freeQty { get; set; }
    public string uqc { get; set; }
    public double rate { get; set; }
    public double grossAmount { get; set; }
    public double discountAmount { get; set; }
    public double preTaxAmount { get; set; }
    public double assesseebleValue { get; set; }
    public double igst_rt { get; set; }
    public double cgst_rt { get; set; }
    public double sgst_rt { get; set; }
    public double cess_rt { get; set; }
    public double iamt { get; set; }
    public double camt { get; set; }
    public double samt { get; set; }
    public double csamt { get; set; }
    //public double cessnonadval { get; set; }
    //public double state_cess { get; set; }
    //public double stateCessAmt { get; set; }
    //public double stateCesNonAdvlAmt { get; set; }
    public double otherCharges { get; set; }
    public double itemTotal { get; set; }
    public double ordLineRef { get; set; }
    public string origin_Country { get; set; }
    public string prdSlNo { get; set; }
    public attribDtls attribDtls { get; set; }
}



public class batch
{
    public string batchName { get; set; }
    public string batchExpiry_Date { get; set; }
    public string warrantyDate { get; set; }
}
public class attribDtls
{
    public string attrib_name { get; set; }
    public string attrib_val { get; set; }
}

