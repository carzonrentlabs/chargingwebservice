﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Xml;
using System.Collections;
using System.Globalization;
using System.Net;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using RestSharp;
using Newtonsoft.Json.Linq;

/// <summary>
/// Summary description for clslAdmin
/// </summary>
/// 
namespace ChargingWebService
{
    public class clsAdmin
    {
        public int Retval;

        //string format;
        public clsAdmin()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet GetBookingDetails_Capture(int BookingID)
        {
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@BookingID", BookingID);

            return SqlHelper.ExecuteDataset("Prc_GetBkDetails_Chrging", param);
        }

        public DataSet GetBookingDetails_Google(int BookingID)
        {
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@BookingID", BookingID);

            return SqlHelper.ExecuteDataset("Prc_GetBkDetails_Google", param);
        }

        public DataSet CheckCreditCardChargingLink(int BookingID)
        {
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@BookingID", BookingID);

            return SqlHelper.ExecuteDataset("Prc_validatehdfcCharging", param);
        }

        public DataSet CheckChargingdetails(int BookingID)
        {
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@BookingID", BookingID);

            return SqlHelper.ExecuteDataset("PrcCheckChargingstatus", param);
        }


        public DataSet CheckAccentureChargingstatus(int BookingID)
        {
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@bookingid", BookingID);

            return SqlHelper.ExecuteDataset("Prc_GetDirectChargingDetail", param);
        }


        public void UpdateChargingStatus(int BookingID, string AuthCode, string TransactionID)
        {
            SqlParameter[] param = new SqlParameter[3];

            param[0] = new SqlParameter("@BookingID", BookingID);
            param[1] = new SqlParameter("@AuthCode", AuthCode);
            param[2] = new SqlParameter("@TransactionID", TransactionID);

            SqlHelper.ExecuteNonQuery("Prc_UpdateChargingStatus", param);
        }

        public void UpdateMerchantID(int BookingID, string MerchantID, int CardType)
        {
            if (!string.IsNullOrEmpty(MerchantID))
            {
                SqlParameter[] param = new SqlParameter[3];

                param[0] = new SqlParameter("@BookingID", BookingID);
                param[1] = new SqlParameter("@MerchantID", MerchantID);
                param[2] = new SqlParameter("@CardType", CardType);

                SqlHelper.ExecuteNonQuery("prc_CCMerchantID", param);
            }
        }

        public void InsertCorDriveClosedDetails(int BookingId, string ConfirmType, int SysUserID)
        {
            SqlParameter[] param = new SqlParameter[5];

            param[0] = new SqlParameter("@BookingId", BookingId);
            param[1] = new SqlParameter("@SysUserID", SysUserID);
            param[2] = new SqlParameter("@ConfirmType", ConfirmType);
            param[3] = new SqlParameter("@EditReason", "");
            param[4] = new SqlParameter("@EditComments", "");

            SqlHelper.ExecuteNonQuery("Prc_InsertCorDriveClosedDetails", param);
        }



        public void InsertNokiaChargingDetails(int BookingID, int UserID, int Status)
        {
            SqlParameter[] param = new SqlParameter[2];

            param[0] = new SqlParameter("@BookingID", BookingID);
            param[1] = new SqlParameter("@UserID", UserID);
            if (Status == 0)
            {
                SqlHelper.ExecuteNonQuery("insertNokiachargingdetail", param);
            }
            else
            {
                SqlHelper.ExecuteNonQuery("Prc_InsertBookingID_Email", param);
            }
        }

        public void SaveMVLog(string ErrorCode, string TransactionID, string Message, int BookingID, double Amount)
        {
            SqlParameter[] param = new SqlParameter[5];

            param[0] = new SqlParameter("@ErrorCode", ErrorCode);
            param[1] = new SqlParameter("@TransactionID", TransactionID);
            param[2] = new SqlParameter("@Message", Message);
            param[3] = new SqlParameter("@BookingID", BookingID);
            param[4] = new SqlParameter("@Amount", Amount);

            SqlHelper.ExecuteNonQuery("ProcMVChargingStatus_New", param);
        }

        public DataSet SaveMVLog_Booking(int BookingID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@BookingID", BookingID);

            return SqlHelper.ExecuteDataset("ProcUniqueTrackID_MV", param);
        }

        public void SaveAmex_Voucher_Booking(int BookingID, double TotalCost, int status)
        {
            SqlParameter[] param = new SqlParameter[2];

            if (status == 0)
            {
                param[0] = new SqlParameter("@BookingID", BookingID);
                param[1] = new SqlParameter("@Amount", TotalCost);

                SqlHelper.ExecuteNonQuery("ProcUniqueTrackID_Micro", param);
            }
            else
            {
                if (TotalCost > 0)
                {
                    param[0] = new SqlParameter("@BookingID", BookingID);
                    param[1] = new SqlParameter("@VoucherAmt", TotalCost);

                    SqlHelper.ExecuteNonQuery("prc_updateVoucherValue", param);
                }
            }
        }

        public void UPdateMVLog(string OrderNo, string ErrorCode, string TransactionID, string Message)
        {
            SqlParameter[] param = new SqlParameter[4];

            param[0] = new SqlParameter("@OrderNo", OrderNo);
            param[1] = new SqlParameter("@ErrorCode", ErrorCode);
            param[2] = new SqlParameter("@TransactionID", TransactionID);
            param[3] = new SqlParameter("@Message", Message);

            SqlHelper.ExecuteNonQuery("ProcMVChargingStatus", param);
        }

        public void UPdateMVLog_1(string OrderNo, string ErrorCode, string TransactionID, string Message)
        {
            SqlParameter[] param = new SqlParameter[4];

            param[0] = new SqlParameter("@OrderNo", OrderNo);
            param[1] = new SqlParameter("@ErrorCode", ErrorCode);
            param[2] = new SqlParameter("@TransactionID", TransactionID);
            param[3] = new SqlParameter("@Message", Message);

            SqlHelper.ExecuteNonQuery("ProcMVChargingStatus_1", param);
        }

        public DataSet GetBookingDetails_Confirm(int BookingID)
        {
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@BookingId", BookingID);

            return SqlHelper.ExecuteDataset("Prc_GetCordiveClosingDetails", param);
        }

        public DataSet GetBookingDetails(int BookingID)
        {
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@BookingId", BookingID);

            return SqlHelper.ExecuteDataset("Prc_GetBookingDetails", param);
        }

        public DataSet GetChargingDetails(int BookingID)
        {
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@BookingId", BookingID);

            return SqlHelper.ExecuteDataset("Prc_GetChargingDetails", param);
        }

        public DataSet GetInvoiceDetails(int BookingID)
        {
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@BookingId", BookingID);

            return SqlHelper.ExecuteDataset("Prc_GetInvoiceMailDetails", param);
        }

        public DataSet GetBookingDetails_NonCorDrive(int BookingID)
        {
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@BookingId", BookingID);

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset("Prc_CheckifDispatch", BookingID);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return SqlHelper.ExecuteDataset("Prc_GetCordiveClosingDetails_NonCorDrive", param);
            }
            else
            {
                return SqlHelper.ExecuteDataset("Prc_NewPreAuthMasterVisa", param);
            }
        }

        public double GetVoucherDetails(int BookingID)
        {
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@BookingID", BookingID);

            DataSet VoucherDetails = new DataSet();

            VoucherDetails = SqlHelper.ExecuteDataset("prc_displayvoucherdetailDS", param);
            double VoucherAmt = 0;
            if (VoucherDetails.Tables[0].Rows.Count > 0)
            {
                for (int l = 0; l <= VoucherDetails.Tables[0].Rows.Count - 1; l++)
                {
                    VoucherAmt = VoucherAmt + Convert.ToDouble(VoucherDetails.Tables[0].Rows[l]["Denomination"]);
                }
            }

            return VoucherAmt;
        }

        public DataSet GetMasterVisaDetail(string charging)
        {
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@Type", charging);

            return SqlHelper.ExecuteDataset("Prc_GetMasterVisaDetails", param);
        }

        public void UpdateMVChargingDetails(string OrderID, string CCNo, string AuthCode, string PGID, int status, string TransactionID)
        {
            SqlParameter[] param = new SqlParameter[6];

            param[0] = new SqlParameter("@OrderID", OrderID);
            param[1] = new SqlParameter("@CCNo", CCNo);
            param[2] = new SqlParameter("@AuthCode", AuthCode);
            param[3] = new SqlParameter("@PGID", PGID);
            param[4] = new SqlParameter("@status", status);
            param[5] = new SqlParameter("@transactionid", TransactionID);

            //SqlHelper.ExecuteNonQuery("Prc_UpdateCCNo", param);
            SqlHelper.ExecuteNonQuery("Prc_UpdateCCNoNew", param);
        }

        public string GetCreditCard72HoursAutoClosingDetails()
        {
            ErrorLog.LoginfoToLogFile("start", "GetCreditCard72HoursAutoClosingDetails");
            string ErrorLogDetails = "";
            DataSet ds = SqlHelper.ExecuteDataset("Prc_GetSchedulerData");

            if (ds.Tables[0].Rows.Count > 0)
            {
                ErrorLog.LoginfoToLogFile("Calling Amex Preauth Service", "GetCreditCard72HoursAutoClosingDetails");

                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    try
                    {
                        int UserID = 0;
                        bool NotifyGuestYN = true;

                        DB db = new DB();
                        string closingAllowbasedonAccountingdate = db.CheckClosingforCurrentMonth(Convert.ToDateTime(ds.Tables[0].Rows[i]["DateIn"]), System.DateTime.Now);

                        if (string.IsNullOrEmpty(closingAllowbasedonAccountingdate))
                        {
                            ErrorLogDetails = ChargingWebService(Convert.ToInt32(ds.Tables[0].Rows[i]["BookingID"]), UserID, NotifyGuestYN, true);
                        }
                        else
                        {
                            ErrorLogDetails = closingAllowbasedonAccountingdate;
                        }
                        ErrorLogDetails = ErrorLogDetails + " : BookingID : " + ds.Tables[0].Rows[i]["BookingID"].ToString() + ";";

                    }
                    catch (Exception ex)
                    {
                        ErrorLogDetails = ErrorLogDetails + " : BookingID : " + ds.Tables[0].Rows[i]["BookingID"].ToString() + " : " + ex.ToString() + ";";
                    }
                }
            }
            return ErrorLogDetails;
        }

        public string GetAmex72HoursAutoClosingDetails()
        {
            ErrorLog.LoginfoToLogFile("start", "GetAmex72HoursAutoClosingDetails");
            string ErrorLogDetails = "";
            DataSet ds = SqlHelper.ExecuteDataset("Prc_GetAmexAutoClosingDetails");

            if (ds.Tables[0].Rows.Count > 0)
            {
                //Preauth Code start here  

                ErrorLog.LoginfoToLogFile("Calling Amex Preauth Service", "GetAmex72HoursAutoClosingDetails");

                //string Preauthstatus = "";

                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {

                    try
                    {
                        //JsonResponse js = new JsonResponse();

                        //string amexPreAuthResponse = CallJson.doGet(CallJson.GetConfigurationValue("AmexPreAuthRestURL") + "?bookingId="
                        //    + ds.Tables[0].Rows[i]["BookingID"].ToString() + "&amexCardNumber=" + ds.Tables[0].Rows[i]["creditcardno"].ToString()
                        //    + "&expiryYY=" + ds.Tables[0].Rows[i]["expyymm"].ToString().Substring(0, 2)
                        //    + "&expiryMM=" + ds.Tables[0].Rows[i]["expyymm"].ToString().Substring(2, 2) + "&preauthAmount="
                        //    + Convert.ToDouble(ds.Tables[0].Rows[i]["totalcost"]));

                        //js = JsonConvert.DeserializeObject<JsonResponse>(amexPreAuthResponse);
                        //return js;
                        DB db = new DB();
                        string closingAllowbasedonAccountingdate = db.CheckClosingforCurrentMonth(Convert.ToDateTime(ds.Tables[0].Rows[i]["DateIn"]), System.DateTime.Now);

                        if (string.IsNullOrEmpty(closingAllowbasedonAccountingdate))
                        {
                            JsonResponse js = AmexChargingMethod(ds.Tables[0].Rows[i]["BookingID"].ToString(), ds.Tables[0].Rows[i]["creditcardno"].ToString()
                               , ds.Tables[0].Rows[i]["expyymm"].ToString().Substring(0, 2), ds.Tables[0].Rows[i]["expyymm"].ToString().Substring(2, 2)
                               , Convert.ToDouble(ds.Tables[0].Rows[i]["totalcost"]));


                            ErrorLogDetails = ErrorLogDetails + ds.Tables[0].Rows[i]["BookingID"].ToString() + ":status:" + js.status + ":error_message:"
                                + js.error_message + ":transaction_id:" + js.transaction_id + ":authorization_id:" + js.authorization_id + ":receipt_number:" + js.receipt_number
                                + ":preauth_amount:" + js.preauth_amount + ":order_id:" + js.order_id + ":order_info:" + js.order_info + ";";
                        }
                        else
                        {
                            ErrorLogDetails = ErrorLogDetails + ds.Tables[0].Rows[i]["BookingID"].ToString() + ":" + closingAllowbasedonAccountingdate;
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorLogDetails = ErrorLogDetails + ds.Tables[0].Rows[i]["BookingID"].ToString() + ":" + ex.ToString() + ";";
                    }
                }
            }
            return ErrorLogDetails;
        }

        private static JsonResponse AmexChargingMethod(string BookingID, string creditcardno, string expiryYY, string expiryMM, double totalcost)
        {
            //AmexPreAuth.AMEXPaymentRESTSoapClient Preauth = new AmexPreAuth.AMEXPaymentRESTSoapClient();

            JsonResponse js = new JsonResponse();

            string amexPreAuthResponse = CallJson.doGet(CallJson.GetConfigurationValue("AmexPreAuthRestURL") + "?bookingId="
                + BookingID + "&amexCardNumber=" + creditcardno + "&expiryYY=" + expiryYY + "&expiryMM=" + expiryMM + "&preauthAmount=" + totalcost);

            js = JsonConvert.DeserializeObject<JsonResponse>(amexPreAuthResponse);
            return js;
        }

        public string GetAmexClosingDetails(int BookingId)
        {
            string ErrorCode = "";
            ErrorLog.LoginfoToLogFile("BookingID:" + BookingId.ToString(), "GetAmexClosingDetails");
            DataSet ds = GetBookingDetails_Capture(BookingId);

            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    try
                    {
                        JsonResponse js = AmexChargingMethod(ds.Tables[0].Rows[i]["BookingID"].ToString(), ds.Tables[0].Rows[i]["creditcardno"].ToString()
                          , ds.Tables[0].Rows[i]["expyymm"].ToString().Substring(0, 2), ds.Tables[0].Rows[i]["expyymm"].ToString().Substring(2, 2)
                          , (Convert.ToDouble(ds.Tables[0].Rows[i]["TotalCost"]) * 100));

                        ErrorCode = js.status;

                        ErrorLog.LoginfoToLogFile("BookingID:" + BookingId.ToString() + ",response:" + js.status, "GetAmexClosingDetails");
                    }
                    catch (Exception ex)
                    {
                        ErrorCode = ex.ToString();
                        ErrorLog.LogErrorToLogFile(ex, "GetAmexClosingDetails");
                    }
                }
            }
            if (ErrorCode.ToUpper() == "SUCCESS")
            {
                ErrorCode = "Success";
            }
            return ErrorCode;
        }


        public string GetMasterVisaChargingDetails_PreauthCharging(int BookingId) //new Preauth / charging changes by Rahul
        {
            string ErrorCode = "";

            try
            {
                DataSet ds = GetBookingDetails_Capture(BookingId);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    //Preauth Code start here    
                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        double TotalCost = 0;

                        TotalCost = Convert.ToDouble(ds.Tables[0].Rows[i]["TotalCost"]);
                        if (TotalCost > 0)
                        {

                            //UATPMCorporate.PMServiceClient MV = new UATPMCorporate.PMServiceClient();
                            //UATPMCorporate.AuthenticateService credential = new UATPMCorporate.AuthenticateService();

                            PaymateRegistrationWCF.PMServiceClient MV = new PaymateRegistrationWCF.PMServiceClient();
                            PaymateRegistrationWCF.AuthenticateService credential = new PaymateRegistrationWCF.AuthenticateService();

                            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                            //credential.AuthUserName = "67592538";
                            //credential.AuthPassword = "5BF24F3AA4CA";
                            //credential.AuthCode = "2511C6A40B4A";

                            credential.AuthUserName = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthUserName");
                            credential.AuthPassword = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthPassword");
                            credential.AuthCode = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthCode");

                            string TrackOrderID = "";
                            string AccessCode = "A0771D0B-6A01-40FF-9347-DEA6CAFDD293";

                            ErrorLog.LoginfoToLogFile(ds.Tables[0].Rows[i]["transactionid"].ToString() + "," + ds.Tables[0].Rows[i]["ApprovalNo"].ToString(), "");

                            TrackOrderID = MV.GetTransactionStatus(credential, AccessCode, ds.Tables[0].Rows[i]["transactionid"].ToString(), ds.Tables[0].Rows[i]["ApprovalNo"].ToString());
                            //000|Successful|10428181|200706135353108|789.60|SUCCESSFUL|PreAuth
                            ErrorLog.LoginfoToLogFile(TrackOrderID, "GetTransactionStatus");

                            //TrackOrderID = MV.TrackStatus_OrderID(credential, ds.Tables[0].Rows[i]["trackid"].ToString(), "CCW");  //TrackID to be filled in the blank field

                            //string[] OrderIDstatus = TrackOrderID.Split('|');
                            string[] OrderIDstatus = SplitFunction(TrackOrderID);

                            string Errorcode, ErrorMessage, InvoiceNo, PayMateTransactionID, Amount, TransactionStatus, TransactionMode;


                            //string errorCode, transactionNr, strMerchantOrderID, numAmount, strTransactionMode;

                            Errorcode = OrderIDstatus[0].ToString();
                            ErrorMessage = OrderIDstatus[1].ToString();
                            InvoiceNo = OrderIDstatus[2].ToString();
                            PayMateTransactionID = OrderIDstatus[3].ToString();
                            Amount = OrderIDstatus[4].ToString();
                            TransactionStatus = OrderIDstatus[5].ToString();
                            TransactionMode = OrderIDstatus[6].ToString();

                            double VoucherAmt = GetVoucherDetails(BookingId);
                            double ApprovalAmt = Convert.ToDouble(ds.Tables[0].Rows[i]["ApprovalAmt"]);
                            if (VoucherAmt >= 0)
                            {
                                if (TotalCost < VoucherAmt)
                                {
                                    TotalCost = 0;
                                }
                                else
                                {
                                    TotalCost = TotalCost - VoucherAmt;
                                }

                                if (TotalCost > 0)
                                {
                                    string ChargingStatus = "";
                                    if (Errorcode == "000")
                                    {

                                        ErrorLog.LoginfoToLogFile(ds.Tables[0].Rows[i]["transactionid"].ToString() + "," + ds.Tables[0].Rows[i]["ApprovalNo"].ToString(), "");
                                        AccessCode = "2BD11CF0-79A2-4DA1-A3BE-C4DD632301C4";
                                        ChargingStatus = MV.MVAuthCapture(credential, AccessCode, ds.Tables[0].Rows[i]["transactionid"].ToString()
                                            , ds.Tables[0].Rows[i]["ApprovalNo"].ToString(), Amount, TotalCost.ToString());

                                        ErrorLog.LoginfoToLogFile(ChargingStatus, "MVAuthCapture");

                                        string[] ChargingDetails = SplitFunction(ChargingStatus);

                                        string ErrorcodeCharging, ErrorMessageCharging, InvoiceNoCharging, PayMateAuthTransactionID, AuthAmount, CaptureTransactionId, CaptureAmount;
                                        //000|Successful|10428181|200706135353108|789.60|200707124639109|1595

                                        ErrorcodeCharging = ChargingDetails[0].ToString();
                                        ErrorMessageCharging = ChargingDetails[1].ToString();
                                        InvoiceNoCharging = ChargingDetails[2].ToString();
                                        PayMateAuthTransactionID = ChargingDetails[3].ToString();
                                        AuthAmount = ChargingDetails[4].ToString();
                                        CaptureTransactionId = ChargingDetails[5].ToString();
                                        CaptureAmount = ChargingDetails[6].ToString();

                                        DB db = new DB();
                                        if (ErrorcodeCharging == "000")
                                        {
                                            ErrorCode = "Success";

                                            UpdateMVChargingDetails(ds.Tables[0].Rows[i]["ApprovalNo"].ToString(), "", CaptureTransactionId
                                                , CaptureTransactionId, 2, CaptureTransactionId);
                                        }
                                        else
                                        {
                                            ErrorCode = ErrorMessageCharging;
                                        }

                                        db.SaveMVLog(BookingId.ToString(), CaptureTransactionId, ErrorcodeCharging, ErrorMessageCharging, "MasterVisaBooking");
                                    }
                                }
                            }
                            else
                            {
                                ErrorCode = "Negative Voucher Amount";
                            }
                        } //total cost greater than 0
                        else
                        {
                            ErrorCode = "Error in Closing";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCode = ex.ToString();
                ErrorLog.LogErrorToLogFile(ex, "Error");
            }

            return ErrorCode;
        }

        public string GetMasterVisaChargingDetails(int BookingId)
        {
            string ErrorCode = "";

            try
            {
                DataSet ds = GetBookingDetails_Capture(BookingId);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    //Preauth Code start here    
                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        double TotalCost = 0;

                        TotalCost = Convert.ToDouble(ds.Tables[0].Rows[i]["TotalCost"]);
                        if (TotalCost > 0)
                        {

                            MasterVisaCharging.ServiceSoapClient MV = new MasterVisaCharging.ServiceSoapClient();
                            MasterVisaCharging.AuthenticateService credential = new MasterVisaCharging.AuthenticateService();

                            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                            credential.userName = "00011500";
                            credential.merchRefNo = "WNOUD";

                            string TrackOrderID = "";

                            TrackOrderID = MV.TrackStatus_OrderID(credential, ds.Tables[0].Rows[i]["trackid"].ToString(), "CCW");  //TrackID to be filled in the blank field

                            //string[] OrderIDstatus = TrackOrderID.Split('|');
                            string[] OrderIDstatus = SplitFunction(TrackOrderID);
                            string errorCode, transactionNr, strMerchantOrderID, numAmount, strTransactionMode;
                            errorCode = OrderIDstatus[0].ToString();
                            transactionNr = OrderIDstatus[1].ToString();
                            strMerchantOrderID = OrderIDstatus[2].ToString();
                            numAmount = OrderIDstatus[3].ToString();
                            strTransactionMode = OrderIDstatus[4].ToString();

                            if (errorCode != "400" && errorCode != "115")
                            {
                                ErrorCode = "****** Payment Gateway Error ******<br> Problem in dual verification<br>Error Code: " + errorCode + "<br>Transaction Mode: " + strTransactionMode;
                            }

                            double VoucherAmt = GetVoucherDetails(BookingId);
                            double ApprovalAmt = Convert.ToDouble(ds.Tables[0].Rows[i]["ApprovalAmt"]);
                            if (VoucherAmt >= 0)
                            {
                                if (TotalCost < VoucherAmt)
                                {
                                    TotalCost = 0;
                                }
                                else
                                {
                                    TotalCost = TotalCost - VoucherAmt;
                                }

                                if (TotalCost > 0)
                                {
                                    if (strTransactionMode.Trim() == "A" || (strTransactionMode.Trim() == "C" && errorCode.Trim() == "115"))
                                    {
                                        string ChargingStatus = "";

                                        if (TotalCost == ApprovalAmt)
                                        {
                                            ChargingStatus = MV.Settlement(credential, ds.Tables[0].Rows[i]["transactionid"].ToString()
                                                , ds.Tables[0].Rows[i]["trackid"].ToString(), TotalCost.ToString());
                                            //"Asp_Websersice_Settlement.asp" -->
                                        }
                                        else
                                        {
                                            ChargingStatus = MV.Adjust(credential, ds.Tables[0].Rows[i]["transactionid"].ToString()
                                                , ds.Tables[0].Rows[i]["trackid"].ToString(), ApprovalAmt.ToString(), TotalCost.ToString());
                                            //"Asp_Websersice_Adjustment.asp" -->
                                        }
                                        ErrorCode = ChargingStatus;

                                        string errorCode_New, transactionNr_New, strMessage, AuthCode, PGId;

                                        string[] Chargingstat = SplitFunction(ChargingStatus);
                                        errorCode_New = Chargingstat[0].ToString();
                                        transactionNr_New = Chargingstat[1].ToString();
                                        strMessage = Chargingstat[2].ToString();
                                        AuthCode = Chargingstat[3].ToString();
                                        PGId = Chargingstat[4].ToString();

                                        if (errorCode_New != "400")
                                        {
                                            ErrorCode = "****** Payment Gateway Error (Adjustment module) ******<br>Message: "
                                                + strMessage + "<br>ErrorCode: " + errorCode_New + "<br>Transaction Naration: " + transactionNr_New;
                                        }
                                        else
                                        {
                                            ErrorCode = "Success";
                                        }

                                        DB db = new DB();
                                        db.SaveMVLog(BookingId.ToString(), transactionNr_New, errorCode_New, ErrorCode, "MasterVisaBooking");

                                        UpdateMVChargingDetails(ds.Tables[0].Rows[i]["trackid"].ToString(), "", AuthCode, PGId, 2, transactionNr_New);
                                    }
                                    else
                                    {
                                        ErrorCode = "Cannot Charged as PreAuth is realised." + TrackOrderID;
                                    }
                                }
                            }
                            else
                            {
                                errorCode = "Negative Voucher Amount";
                            }
                        } //total cost greater than 0
                        else
                        {
                            ErrorCode = "Error in Closing";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCode = ex.ToString();
            }

            return ErrorCode;
        }

        public string[] SplitFunction(string ChargingStatus)
        {
            string[] Chargingstat = ChargingStatus.Split('|');

            return Chargingstat;
        }

        public string MasterVisaTrackOrderID(int BookingId)
        {
            string ErrorCode = "";

            try
            {
                DataSet ds = GetBookingDetails_Capture(BookingId);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    //Preauth Code start here    
                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        //double TotalCost = 0;

                        //TotalCost = Convert.ToDouble(ds.Tables[0].Rows[i]["TotalCost"]);
                        //if (TotalCost > 0)
                        // {

                        //MasterVisaCharging.ServiceSoapClient MV = new MasterVisaCharging.ServiceSoapClient();
                        //MasterVisaCharging.AuthenticateService credential = new MasterVisaCharging.AuthenticateService();

                        //UATPMCorporate.PMServiceClient MV = new UATPMCorporate.PMServiceClient();
                        //UATPMCorporate.AuthenticateService credential = new UATPMCorporate.AuthenticateService();

                        PaymateRegistrationWCF.PMServiceClient MV = new PaymateRegistrationWCF.PMServiceClient();
                        PaymateRegistrationWCF.AuthenticateService credential = new PaymateRegistrationWCF.AuthenticateService();

                        ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                        credential.AuthUserName = "67592538";
                        credential.AuthPassword = "5BF24F3AA4CA";
                        credential.AuthCode = "2511C6A40B4A";
                        //credential.userName = "00011500";
                        //credential.merchRefNo = "WNOUD";

                        //ErrorCode = MV.TrackStatus_OrderID(credential, ds.Tables[0].Rows[i]["trackid"].ToString(), "CCW");  //TrackID to be filled in the blank field

                        //string TrackOrderID = "";
                        string AccessCode = "A0771D0B-6A01-40FF-9347-DEA6CAFDD293";
                        ErrorCode = MV.GetTransactionStatus(credential, AccessCode, ds.Tables[0].Rows[i]["transactionid"].ToString(), ds.Tables[0].Rows[i]["ApprovalNo"].ToString());
                        // } //total cost greater than 0
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCode = ex.ToString();
            }

            return ErrorCode;
        }

        //closing module start 
        public CCValidationData CheckChargedYN(int BookingID)
        {
            CCValidationData CVC = new CCValidationData();

            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@bookingId", BookingID);

            CVC.bookingMadeByIndiv = 0;

            DataSet ds = SqlHelper.ExecuteDataset("Prc_CheckBookingidMadeBy", param);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    if (Convert.ToBoolean(ds.Tables[0].Rows[i]["ChargedYN"]) == true)
                    {
                        CVC.ChargedYN = Convert.ToBoolean(ds.Tables[0].Rows[0]["ChargedYN"]);
                    }
                    else
                    {
                        CVC.ChargedYN = false;
                    }
                }
                CVC.bookingMadeByIndiv = 1;
            }
            else
            {
                CVC.ChargedYN = false;
            }

            return CVC;
        }

        public InvoiceMailerVariables GetInvoiceMailerDetails(int BookingID)
        {
            DataSet ds = new DataSet();
            InvoiceMailerVariables CV = new InvoiceMailerVariables();
            ds = GetInvoiceDetails(BookingID);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int k = 0; k <= ds.Tables[0].Rows.Count - 1; k++)
                {
                    CV.CorDriveYN = Convert.ToBoolean(ds.Tables[0].Rows[k]["CorDriveYN"]);
                    CV.Service = Convert.ToString(ds.Tables[0].Rows[k]["Service"]);

                    CV.Status = Convert.ToString(ds.Tables[0].Rows[k]["BookingStatus"]);
                    CV.ClientCoID = Convert.ToInt32(ds.Tables[0].Rows[k]["ClientCoID"]);
                    CV.CCType = Convert.ToInt32(ds.Tables[0].Rows[k]["CCType"]);
                    CV.PaymentMode = Convert.ToString(ds.Tables[0].Rows[k]["PaymentMode"]);
                    CV.TotalCost = Convert.ToDouble(ds.Tables[0].Rows[k]["TotalCost"]);



                    CV.hdncontinueYN = Convert.ToString(ds.Tables[0].Rows[k]["ChargingLinkStatus"]);


                    //if (!string.IsNullOrEmpty(CV.hdncontinueYN))
                    if (string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[k]["chargingstatus"])) &&
                        Convert.ToString(ds.Tables[0].Rows[k]["ChargingLinkStatus"]) == "CC")
                    {
                        CV.ChargingLink = 1;
                    }
                    else
                    {
                        CV.ChargingLink = 0;
                    }

                    if (CV.hdncontinueYN == "Amex")
                    {
                        CV.AmexLinkYN = true;
                    }
                    else
                    {
                        CV.AmexLinkYN = false;
                    }
                    CV.ChargingNRequired = Convert.ToBoolean(ds.Tables[0].Rows[k]["ChargingNRequired"]);
                    CV.DSStatus = Convert.ToString(ds.Tables[0].Rows[k]["Status"]);
                    //CV.BookingStatus = Convert.ToString(ds.Tables[0].Rows[k]["BookingStatus"]);

                    CV.ClientCoIndivID = Convert.ToInt64(ds.Tables[0].Rows[k]["ClientCoIndivID"]);
                    CV.isPaymateCorporateModule = Convert.ToBoolean(ds.Tables[0].Rows[k]["isPaymateCorporateModule"]);
                    //CV.ServiceType = Convert.ToString(ds.Tables[0].Rows[k]["ServiceType"]);
                    CV.PDFYN = Convert.ToBoolean(ds.Tables[0].Rows[k]["PDFYN"]);
                    CV.ErrorCode = "";
                    CV.ProvisionalInvoiceForGuestYN = Convert.ToBoolean(ds.Tables[0].Rows[k]["ProvisionalInvoiceForGuestYN"]);
                    CV.ProvisionalInvoiceForFacilitatorYN = Convert.ToBoolean(ds.Tables[0].Rows[k]["ProvisionalInvoiceForFacilitatorYN"]);
                    CV.ProvisionalInvoiceForTravelDeskYN = Convert.ToBoolean(ds.Tables[0].Rows[k]["ProvisionalInvoiceForTravelDeskYN"]);
                    CV.CCRegisteredYN = Convert.ToBoolean(ds.Tables[0].Rows[k]["CCRegisteredYN"]);
                }
            }
            else
            {
                CV.ErrorCode = "Not a CorDrive Booking.";
            }
            return CV;
        }

        private void UpdateCarChauffeur(int BookingID, ClosingVariables CV, string InterUnitYN)
        {
            UpdateCarChauffeurStatus(CV, InterUnitYN, BookingID);
        }

        public void UpdateCarChauffeurStatus(ClosingVariables CV, string InterUnitYN, int BookingID)
        {
            SqlParameter[] param = new SqlParameter[4];
            int index = -1;
            param[index += 1] = new SqlParameter("@VendorCarYN", CV.VendorCarYN);
            param[index += 1] = new SqlParameter("@InterUnitYN", InterUnitYN);
            param[index += 1] = new SqlParameter("@LastKM", CV.KMIn);
            param[index += 1] = new SqlParameter("@CarID", CV.CarID);
            SqlHelper.ExecuteNonQuery("ProcDS_UpdateCarMaster", param);

            if (CV.ChauffeurID > 0)
            {
                if (CV.VendorChauffYN == false)
                {
                    if (InterUnitYN == "False")
                    {
                        CV.Flag = 1;
                        UpdateChauffeurStatus(CV.ChauffeurID, CV.Flag);
                    }
                    else
                    {
                        GetChauffeurStatus(BookingID, 1);
                        if (CV.DSChauff == CV.NRTDSChauff)
                        {
                            CV.Flag = 2;
                            UpdateChauffeurStatus(CV.DSChauff, CV.Flag);

                        }
                        else
                        {
                            CV.Flag = 3;
                            UpdateChauffeurStatus(CV.DSChauff, CV.Flag);
                        }
                    }
                }
                else
                {
                    if (InterUnitYN == "False")
                    {
                        CV.Flag = 4;
                        UpdateChauffeurStatus(CV.ChauffeurID, CV.Flag);
                    }
                    else
                    {
                        GetChauffeurStatus(BookingID, 2);
                        if (CV.DSChauff == CV.NRTDSChauff)
                        {
                            CV.Flag = 5;
                            UpdateChauffeurStatus(CV.DSChauff, CV.Flag);
                        }
                        else
                        {
                            CV.Flag = 6;
                            UpdateChauffeurStatus(CV.DSChauff, CV.Flag);
                        }
                    }
                }
            }
        }

        public void GetChauffeurStatus(int BookingID, int Status)
        {
            ClosingVariables CV = new ClosingVariables();
            SqlParameter[] param1 = new SqlParameter[2];
            int index = -1;
            param1[index += 1] = new SqlParameter("@BookingID", BookingID);
            param1[index += 1] = new SqlParameter("@Flag", Status);
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset("ProcDS_GetChauffeur", param1);
            if (ds.Tables[0].Rows.Count > 0)
            {
                CV.DSChauff = Convert.ToInt32(ds.Tables[0].Rows[0]["DSChauff"]);
                CV.NRTDSChauff = Convert.ToInt32(ds.Tables[0].Rows[0]["NRTDSChauff"]);
            }
        }

        public void UpdateChauffeurStatus(int ChauffeurID, int Flag)
        {
            SqlParameter[] param2 = new SqlParameter[2];
            int index = -1;
            param2[index += 1] = new SqlParameter("@ChauffeurID", ChauffeurID);
            param2[index += 1] = new SqlParameter("@Flag", Flag);

            SqlHelper.ExecuteNonQuery("ProcDS_UpdateCarMasterAvailableYN", param2);

        }

        public string CloseBooking(int BookingID, string Status, bool NotifyGuestYN, int UserID)
        {
            string ErrorCode = "";

            try
            {
                if (Status == "Confirm")
                {
                    ErrorCode = ConfirmBooking(BookingID, NotifyGuestYN, UserID);
                }
                else
                {
                    ErrorCode = "Edit cannot be performed";
                }
            }
            catch (Exception ex)
            {
                ErrorCode = ex.ToString();
            }
            return ErrorCode;
        }

        public string ClosingManualBooking(EditClosingVariables EditVariable)
        {
            try
            {
                DB db = new DB();

                EditVariable = db.GetEditClosingDetails(EditVariable); //get booking details

                DataSet gstdetails = new DataSet();

                ErrorLog.LoginfoToLogFile(EditVariable.BookingID.ToString(), "CheckClientGSTDetails");

                gstdetails = db.CheckClientGSTDetails(EditVariable.BookingID); //check if GST is registered
                if (gstdetails.Tables[0].Rows.Count <= 0)
                {
                    EditVariable.ClosingStatus = "GST information not registered.";
                }

                if (EditVariable.PendingYn)
                {
                    EditVariable.ClosingStatus = "Please contact CMT team, as Subsidiary is in Pending status and cannot Close.";
                }

                if (string.IsNullOrEmpty(EditVariable.ClosingStatus))
                {
                    ErrorLog.LoginfoToLogFile(EditVariable.BookingID.ToString(), "CheckClosingAllowed");

                    EditVariable.ClosingStatus = db.CheckClosingAllowed(EditVariable.ClientCoID, EditVariable.PayMentMode); //check if bulk batch running and closing stopped
                    if (string.IsNullOrEmpty(EditVariable.ClosingStatus))
                    {
                        ErrorLog.LoginfoToLogFile(EditVariable.BookingID.ToString(), "CheckClosingCategory");

                        EditVariable = db.CheckClosingCategory(EditVariable); //get category for closing

                        if (EditVariable.ClosingStatus == "Success")
                        {

                            EditVariable = db.GetTimeInterval(EditVariable);  //Get Trip Duration

                            ErrorLog.LoginfoToLogFile(EditVariable.BookingID.ToString(), "GetPackageDetails");

                            EditVariable = db.GetPackageDetails(EditVariable);  //package calculation

                            if (EditVariable.ClosingStatus != "No Pkg Exists.")
                            {
                                ErrorLog.LoginfoToLogFile(EditVariable.BookingID.ToString(), "GetFGRDetails");

                                EditVariable = db.GetFGRDetails(EditVariable);  //FGR calculation

                                EditVariable.FuelAmount = db.GetFuelSurchargeDetails(EditVariable);  //Fuel calculation

                                ChargingWebService.ChargingDetails cd = new ChargingWebService.ChargingDetails();

                                ErrorLog.LoginfoToLogFile(EditVariable.BookingID.ToString(), "GetTaxPercentMethod");

                                TaxClass.TaxDetails taxpercent = cd.GetTaxPercentMethod(EditVariable.ClientCoID, EditVariable.CityID
                                    , EditVariable.DateIn.ToShortDateString()
                                    , EditVariable.CarID, EditVariable.SubsidiaryID
                                    , EditVariable.PickUpDate.ToShortDateString()
                                    , EditVariable.CentralizedYN); //tax calculation

                                EditVariable.ServiceTaxPercent = Convert.ToDouble(taxpercent.ServiceTaxPercent);
                                EditVariable.EduCessPercent = Convert.ToDouble(taxpercent.EduCessPercent);
                                EditVariable.HduCessPercent = Convert.ToDouble(taxpercent.HduCessPercent);
                                EditVariable.SwachhBharatTaxPercent = Convert.ToDouble(taxpercent.SwachhBharatTaxPercent);
                                EditVariable.KrishiKalyanTaxPercent = Convert.ToDouble(taxpercent.KrishiKalyanTaxPercent);
                                EditVariable.DSTPercent = Convert.ToDouble(taxpercent.DSTPercent);

                                if (EditVariable.OriginCode == "MMT")
                                {
                                    EditVariable.CGSTTaxPercent = 0;
                                    EditVariable.SGSTTaxPercent = 0;
                                    EditVariable.IGSTTaxPercent = 0;
                                }
                                else
                                {
                                    EditVariable.CGSTTaxPercent = Convert.ToDouble(taxpercent.CGSTPercent);
                                    EditVariable.SGSTTaxPercent = Convert.ToDouble(taxpercent.SGSTPercent);
                                    EditVariable.IGSTTaxPercent = Convert.ToDouble(taxpercent.IGSTPercent);
                                }

                                EditVariable.ClientGSTId = Convert.ToInt32(taxpercent.ClientGSTId);

                                if (EditVariable.DiscType == "Fixed" || EditVariable.ClientCoID == 2205)
                                {
                                    EditVariable.DiscAmt = ((EditVariable.DiscAmt * 100) / (100 + EditVariable.CGSTTaxPercent
                                        + EditVariable.SGSTTaxPercent + EditVariable.IGSTTaxPercent));
                                }

                                Convenience conv = new Convenience();
                                conv = db.GetConvenienceYN(EditVariable);

                                if (EditVariable.PkgHrTrue == 0)
                                {
                                    EditVariable.PkgHrTrue = 1;
                                }

                                //Consolidated savings discount start 
                                SMSMailRef.SendSMSSoapClient savings = new SMSMailRef.SendSMSSoapClient(); //Live
                                //SendSMSService_Test.SendSMSSoapClient savings = new SendSMSService_Test.SendSMSSoapClient(); //Test

                                EditVariable.DiscPC = savings.GetDiscount(EditVariable.BookingID, EditVariable.DateOut.ToShortDateString(), EditVariable.ClientCoID
                                    , EditVariable.CarCatID, EditVariable.ServiceTypeID);

                                if (EditVariable.DiscPC > 0)
                                {
                                    EditVariable.DiscType = "Percent";
                                }
                                EditVariable.Basic = db.GetBasic(EditVariable, conv);

                                //if (EditVariable.DiscPC > 0)
                                //{
                                //    EditVariable.DiscAmt = ((EditVariable.Basic - EditVariable.FuelSurcharge) * EditVariable.DiscPC) / 100;
                                //    EditVariable.Basic = EditVariable.Basic - EditVariable.DiscAmt;
                                //}
                                //Consolidated savings discount end


                                //if (EditVariable.CarModelID == 135)
                                //{
                                //    EditVariable.SubTotal = Math.Round(EditVariable.Basic + EditVariable.Parking + EditVariable.Interstate + EditVariable.others, 2);
                                //}
                                //else
                                //{
                                EditVariable.SubTotal = Math.Round(EditVariable.Basic
                                    + EditVariable.SanatizationCharges + EditVariable.Parking
                                    + EditVariable.Interstate, 2);
                                //}

                                EditVariable.ServiceTaxAmt = Math.Round((EditVariable.SubTotal * EditVariable.ServiceTaxPercent) / 100, 2);
                                EditVariable.EduTaxAmt = Math.Round((EditVariable.ServiceTaxAmt * EditVariable.EduCessPercent) / 100, 2);
                                EditVariable.HduTaxAmt = Math.Round((EditVariable.ServiceTaxAmt * EditVariable.HduCessPercent) / 100, 2);
                                EditVariable.SwachhBharatTaxAmt = Math.Round((EditVariable.SubTotal * EditVariable.SwachhBharatTaxPercent) / 100, 2);
                                EditVariable.KrishiKalyanTaxAmt = Math.Round((EditVariable.SubTotal * EditVariable.KrishiKalyanTaxPercent) / 100, 2);

                                EditVariable.CGSTTaxAmt = Math.Round((EditVariable.SubTotal * EditVariable.CGSTTaxPercent) / 100, 2);
                                EditVariable.SGSTTaxAmt = Math.Round((EditVariable.SubTotal * EditVariable.SGSTTaxPercent) / 100, 2);
                                EditVariable.IGSTTaxAmt = Math.Round((EditVariable.SubTotal * EditVariable.IGSTTaxPercent) / 100, 2);
                                EditVariable.ExtraAmount = 0;


                                EditVariable.TotalCost = Math.Round(EditVariable.SubTotal
                                + EditVariable.CGSTTaxAmt + EditVariable.SGSTTaxAmt + EditVariable.IGSTTaxAmt
                                + EditVariable.PlatFormFee + EditVariable.ExtraAmount, 2);

                                DataSet close = new DataSet();
                                ClosingVariables CV = new ClosingVariables();

                                CV = CloseBooking(EditVariable);  //closing Proc

                                ErrorLog.LoginfoToLogFile(EditVariable.BookingID.ToString(), "CloseBooking_Confirm");

                                //checking closing allowed or not start
                                DataSet dscheckclosingAllowed = new DataSet();
                                dscheckclosingAllowed = db.GetValidateForClosing(EditVariable.DateIn, System.DateTime.Now);

                                if (dscheckclosingAllowed.Tables[0].Rows.Count > 0)
                                {
                                    if (Convert.ToInt32(dscheckclosingAllowed.Tables[0].Rows[0]["AllowClosing"]) != 1)
                                    {
                                        EditVariable.ClosingStatus = Convert.ToString(dscheckclosingAllowed.Tables[0].Rows[0]["BookingStatus"]);
                                        return EditVariable.ClosingStatus;
                                    }
                                    else
                                    {
                                        CV.Accountingdate = Convert.ToDateTime(dscheckclosingAllowed.Tables[0].Rows[0]["AccountingDate"]);
                                    }
                                }
                                //checking closing allowed or not end

                                ErrorLog.LoginfoToLogFile(EditVariable.BookingID.ToString() + "," + CV.Accountingdate, "CloseBooking_Confirm");

                                close = db.CloseBooking_Confirm(CV, EditVariable.BookingID, EditVariable.UserID);  //closing Proc
                                if (close.Tables[0].Rows.Count > 0)
                                {
                                    EditVariable.InterUnitYN = close.Tables[0].Rows[0]["InterUnitYN"].ToString();
                                }

                                //ds status check
                                if (EditVariable.DSStatus == "C")
                                {
                                    CV.BookingStatus = "C";
                                    CV.DSStatus = "C";
                                }

                                if (EditVariable.PayMentMode.ToLower() == "cc")
                                {
                                    if (EditVariable.chargingstatus == "charged")
                                    {
                                        CV.DSStatus = "C";
                                        CV.BookingStatus = "C";
                                        CV.ErrorCode = "Success";
                                    }
                                    else
                                    {
                                        if (!EditVariable.SendApprovalLinkYN)
                                        {
                                            ChargingMethods CM = new ChargingMethods();

                                            CV = CM.ChargingMethod(EditVariable.BookingID, CV, EditVariable.UserID); //to be tested before making it live
                                        }
                                        else
                                        {
                                            CV.ErrorCode = "ChargingLink Send";
                                            CV.DSStatus = "O";
                                            CV.BookingStatus = "O";
                                        }
                                    }
                                }
                                else
                                {
                                    if (EditVariable.PayMentMode.ToLower() == "ca")
                                    {
                                        CV.DSStatus = "C";
                                        CV.BookingStatus = "C";
                                        CV.ErrorCode = "Success";
                                    }
                                    else
                                    {
                                        if (EditVariable.ClientCoID == 727 || EditVariable.ClientCoID == 822 || EditVariable.ClientCoID == 862
                                            || EditVariable.ClientCoID == 863 || EditVariable.ClientCoID == 1417 || EditVariable.ClientCoID == 2626)
                                        {
                                            CV.DSStatus = "C";
                                            CV.BookingStatus = "C";
                                            CV.ErrorCode = "Success";
                                        }
                                        else
                                        {
                                            if (EditVariable.Parking == 0 && EditVariable.Interstate == 0 && EditVariable.others == 0 && EditVariable.TotalCost <= 5000)
                                            {
                                                CV.DSStatus = "C";
                                                CV.BookingStatus = "C";
                                                CV.ErrorCode = "Success";
                                            }
                                            else
                                            {
                                                //if (EditVariable.CreatedBy == 3064) //if bookings are created by COR Drive User Id then auto close instead of pending for approval
                                                //{
                                                CV.DSStatus = "C";
                                                CV.BookingStatus = "C";
                                                //}
                                                //else
                                                //{
                                                //    CV.DSStatus = "P";
                                                //    CV.BookingStatus = "O";
                                                //}
                                                CV.ErrorCode = "Success";
                                            }
                                        }
                                    }
                                }
                                //ds status check                           

                                if (CV.ErrorCode == "Success" || CV.ErrorCode == "ChargingLink Send")
                                {
                                    if (CV.ErrorCode == "Success")
                                    {
                                        UpdateCarChauffeur(EditVariable.BookingID, CV, CV.InterUnitYN);

                                        db.CloseBooking_UpdateStatus(EditVariable.BookingID, CV.BookingStatus, CV.DSStatus);

                                        InsertCorDriveClosedDetails(EditVariable.BookingID, "ExcelUpload", EditVariable.UserID);
                                    }

                                    string mailstatus = "";
                                    if (EditVariable.NotifyGuestYN)
                                    {
                                        mailstatus = SendInvoiceMail(EditVariable.BookingID, CV.SendApprovalLinkYN); //below code to check
                                    }
                                }
                            } //if no pkg exists
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogErrorToLogFile(ex, "ClosingManualBooking");
                EditVariable.ClosingStatus = ex.ToString();
            }

            return EditVariable.ClosingStatus;
        }

        //public string ClosingManualBookingTesting
        //    (EditClosingVariables EditVariable)
        //{
        //    try
        //    {
        //        DB db = new DB();

        //        EditVariable = db.GetEditClosingDetails(EditVariable); //get booking details

        //        DataSet gstdetails = new DataSet();

        //        EditVariable = db.CheckClosingCategory(EditVariable); //get category for closing

        //        if (EditVariable.ClosingStatus == "Success")
        //        {

        //            EditVariable = db.GetTimeInterval(EditVariable);  //Get Trip Duration

        //            //ErrorLog.LoginfoToLogFile(EditVariable.BookingID.ToString(), "GetPackageDetails");

        //            EditVariable = db.GetPackageDetails(EditVariable);  //package calculation
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorLog.LogErrorToLogFile(ex, "ClosingManualBooking");
        //        EditVariable.ClosingStatus = ex.ToString();
        //    }

        //    return EditVariable.ClosingStatus;
        //}

        public ClosingVariables CloseBooking(EditClosingVariables EditVariable)
        {
            ClosingVariables CV = new ClosingVariables();

            CV.SanatizationCharges = EditVariable.SanatizationCharges;

            CV.ParkTollChages = Convert.ToInt32(EditVariable.Parking);
            CV.InterstateTax = Convert.ToInt32(EditVariable.Interstate);
            CV.PkgID = EditVariable.PkgID;
            CV.PkgRate = EditVariable.PkgRate;
            CV.ExtraHr = EditVariable.ThresholdExtraHr;
            CV.ExtraHrRate = EditVariable.ExtraHrRate;
            CV.ExtraKM = Convert.ToInt32(EditVariable.ThresholdExtraKM);
            CV.ExtraKMRate = EditVariable.ExtraKMRate;
            CV.NightStayAmt = EditVariable.NightStayAllowance;
            CV.OutStnAmt = EditVariable.OutStationAllowance;

            CV.DiscountPC = EditVariable.DiscPC;
            CV.DiscountAmt = EditVariable.DiscAmt;
            CV.TotalCost = EditVariable.TotalCost;
            CV.CalculatedAmount = 0; //to check
            CV.PaidFullYN = 0;
            CV.PaymentMode = EditVariable.PayMentMode;
            CV.TotalHrsUsed = EditVariable.TotalHr;
            CV.NoNight = Convert.ToInt32(EditVariable.IntNoNights);

            CV.FuelSurcharge = EditVariable.FuelAmount;

            CV.ApprovalAmount = EditVariable.TotalCost;
            CV.ApprovalNo = ""; // EditVariable.ApprovalNo;
            CV.CCType = 0; // EditVariable.CCType; //to check

            CV.trackid = "";
            CV.kmout = EditVariable.GarageKmOut;
            CV.DateClose = EditVariable.DateIn;
            CV.TimeClose = EditVariable.TimeIn;
            CV.KMClose = EditVariable.GuestKmIn;
            CV.pointOpeniningKm = EditVariable.GuestKmOut;
            CV.DateIn = EditVariable.DateIn;
            CV.TimeIn = EditVariable.TimeIn;
            CV.KMIn = EditVariable.GarageKmIn;
            CV.Service = EditVariable.Service;
            CV.FXDGarageRate = EditVariable.FixedGarageRun;
            CV.CityID = EditVariable.CityID;

            CV.ServiceTaxPercent = EditVariable.ServiceTaxPercent;
            CV.EduCessPercent = EditVariable.EduCessPercent;
            CV.HduCessPercent = EditVariable.HduCessPercent;
            CV.SwachhBharatTaxPercent = EditVariable.SwachhBharatTaxPercent;
            CV.KrishiKalyanTaxPercent = EditVariable.KrishiKalyanTaxPercent;
            CV.DSTPercent = EditVariable.DSTPercent;
            CV.CGSTTaxPercent = EditVariable.CGSTTaxPercent;
            CV.SGSTTaxPercent = EditVariable.SGSTTaxPercent;
            CV.IGSTTaxPercent = EditVariable.IGSTTaxPercent;

            CV.ServiceTaxAmt = EditVariable.ServiceTaxAmt;
            CV.EduTaxAmt = EditVariable.EduTaxAmt;
            CV.HduTaxAmt = EditVariable.HduTaxAmt;
            CV.SwachhBharatTaxAmt = EditVariable.SwachhBharatTaxAmt;
            CV.KrishiKalyanTaxAmt = EditVariable.KrishiKalyanTaxAmt;
            CV.VatAmt = EditVariable.VatAmt;
            CV.CGSTTaxAmt = EditVariable.CGSTTaxAmt;
            CV.SGSTTaxAmt = EditVariable.SGSTTaxAmt;
            CV.IGSTTaxAmt = EditVariable.IGSTTaxAmt;
            CV.ClientGSTId = EditVariable.ClientGSTId;

            CV.PreAuthNotRequire = false; //EditVariable.PreauthNotRequired; //tocheck
            CV.CCNo = ""; //EditVariable.CCNO
            CV.ExpYYMM = ""; //EditVariable.ExpYYMM
            CV.GuestOpDate = EditVariable.GuestDateOut;
            CV.GuestClDate = EditVariable.GuestDateIn;
            CV.GuestOpTime = EditVariable.GuestTimeOut;
            CV.GuestClTime = EditVariable.GuestTimeIn;
            CV.ClientCoID = EditVariable.ClientCoID;

            CV.DSStatus = ""; //EditVariable.DSStatus
            CV.BookingStatus = ""; //EditVariable.BookingStatus
            CV.CustomPkgYN = EditVariable.CustomYN;
            CV.BillingBasis = EditVariable.BillingBasis;
            CV.Others = EditVariable.others;

            CV.VoucherAmt = 0;
            CV.MerchantID = ""; //EditVariable.MerchantID

            CV.Basic = EditVariable.Basic;
            CV.GSTSurchargeAmount = EditVariable.GSTSurchargeAmount;
            CV.Remarks = EditVariable.Remarks;

            return CV;
        }

        public string ConfirmBooking(int BookingID, bool NotifyGuestYN, int UserID)
        {
            ChargingWebService.ChargingDetails CD = new ChargingWebService.ChargingDetails();
            ChargingWebService.ChargingMethods CM = new ChargingWebService.ChargingMethods();
            ChargingWebService.DB db = new ChargingWebService.DB();
            ClosingVariables CV = new ClosingVariables();
            try
            {
                CV = CD.GetClosingDetails(BookingID, UserID);
                if (CV.BasicRevenue == null)
                {
                    CV.ErrorCode = "Booking Not Closed in Cor-Drive";
                }

                if ((CV.Status == "C" && CV.PaymentMode != "CC") || (CV.Status == "C" && CV.chargingstatus == "charged"))
                {
                    if (CV.Status == "C" && CV.chargingstatus == "charged")
                    {
                        CV.ErrorCode = "BookingID already Charged.";
                    }
                    if (CV.Status == "C" && CV.PaymentMode != "CC")
                    {
                        CV.ErrorCode = "BookingID already Closed.";
                    }
                }

                /*
                    1. fdd 300
                    2. hdd 200
                    3. trasfer 300
                    4. outstation -- per day 300 if for 4 days then 300 * 4 = 1200
                 */
                //*****

                if (db.IsMYFBooking(BookingID.ToString()))
                {
                    int kmcheck = 200;
                    if (CV.RentalType == 5 || CV.RentalType == 1) //transfer
                    {
                        kmcheck = 300;
                    }

                    if (CV.RentalType == 3) //FDD
                    {
                        kmcheck = 250; //200 km suggested by Vedha
                    }

                    if (CV.RentalType == 2) //HDD
                    {
                        kmcheck = 150; //100 km suggested by Vedha
                    }

                    if (CV.RentalType == 4) //outstation
                    {
                        kmcheck = 400 * Convert.ToInt32(CV.TotalHrsUsed);
                    }

                    if ((CV.KMClose - CV.pointOpeniningKm) > kmcheck)
                    {
                        CV.ErrorCode = "KM greater than " + kmcheck + ", will be manually closed.";
                        CV.AllowClosingYN = false;
                    }

                    if (CV.GPSDistanceFromGarage > 40 || CV.GPSDistanceToGarage > 40)
                    {
                        CV.ErrorCode = "Garage from or to distance greater than 40, will be manually closed.";
                        CV.AllowClosingYN = false;
                    }


                    if (CV.AppDistance == 0)
                    {
                        if (CV.OdometerDistance > (CV.GPSDistance * 130 / 100))
                        {
                            CV.ErrorCode = "Odometer distance is greater than 30% from GPS distance, will be manually closed.";
                            CV.AllowClosingYN = false;
                        }
                    }
                    else
                    {
                        if (CV.OdometerDistance > (CV.AppDistance * 130 / 100) && CV.AppDistance > (CV.GPSDistance * 130 / 100))
                        {
                            CV.ErrorCode = "Odometer distance is greater than 30% from App distance, will be manually closed.";
                            CV.AllowClosingYN = false;
                        }
                    }
                }
                //***
                if (CV.ErrorCode != "")
                {
                    db.UpdateCorDriveNotConfirmedReason(BookingID, CV.ErrorCode);
                }
                //update query to save the error code in the table
                //***

                if (CV.ErrorCode == "")
                {
                    if (CV.AllowClosingYN)
                    {
                        if (CV.Status == "C" && CV.PaymentMode.ToLower() != "cc")
                        {
                            CV.ErrorCode = "Booking is already Closed";
                        }
                        else if (CV.Status == "C" && CV.PaymentMode.ToLower() == "cc" && CV.chargingstatus == "charged")
                        {
                            CV.DSStatus = "C";
                            CV.BookingStatus = "C";
                            CV.ErrorCode = "Success";
                        }
                        else
                        {
                            if (CV.EliteCarYN && CV.PaymentMode.ToLower() != "cc")
                            {
                                CV.DSStatus = "C";
                                CV.BookingStatus = "C";
                                CV.ErrorCode = "Success";
                            }
                            else if (CV.PaymentMode.ToLower() == "cc")
                            {
                                //if (CV.chargingstatus == "charged" && CV.Pickupdate < "2023-03-01")
                                if (CV.chargingstatus == "charged")
                                {
                                    CV.DSStatus = "C";
                                    CV.BookingStatus = "C";
                                    CV.ErrorCode = "Success";
                                }
                                else
                                {
                                    if (!CV.SendApprovalLinkYN)
                                    {
                                        CV = CM.ChargingMethod(BookingID, CV, UserID); //to be tested before making it live
                                    }
                                    else
                                    {
                                        CV.ErrorCode = "ChargingLink Send";
                                        CV.DSStatus = "O";
                                        CV.BookingStatus = "O";
                                    }
                                }
                            }
                            else
                            {
                                if (CV.BookingForApprovalYN)
                                {
                                    CV.DSStatus = "P";
                                    CV.BookingStatus = "O";
                                    CV.ErrorCode = "Success";
                                }
                                if (CV.PaymentMode.ToLower() == "ca")
                                {
                                    CV.DSStatus = "C";
                                    CV.BookingStatus = "C";
                                    CV.ErrorCode = "Success";
                                }
                                else
                                {
                                    if (CV.ClientCoID == 727 || CV.ClientCoID == 822 || CV.ClientCoID == 862 || CV.ClientCoID == 863 ||
                                        CV.ClientCoID == 1417 || CV.ClientCoID == 2626)
                                    {
                                        CV.DSStatus = "C";
                                        CV.BookingStatus = "C";
                                        CV.ErrorCode = "Success";
                                    }
                                    else
                                    {
                                        if (CV.ParkTollChages == 0 && CV.InterstateTax == 0 && CV.Others == 0 && CV.TotalCost <= 5000)
                                        {
                                            CV.DSStatus = "C";
                                            CV.BookingStatus = "C";
                                            CV.ErrorCode = "Success";
                                        }
                                        else
                                        {
                                            if (CV.CreatedBy == 3064) //if bookings are created by COR Drive User Id then auto close instead of pending for approval
                                            {
                                                CV.DSStatus = "C";
                                                CV.BookingStatus = "C";
                                            }
                                            else
                                            {
                                                CV.DSStatus = "P";
                                                CV.BookingStatus = "O";
                                            }
                                            CV.ErrorCode = "Success";
                                        }
                                    }
                                }
                            }

                            //save and mail query start
                            bool SendEmailYN = true;
                            if (CV.CCRegisteredYN && string.IsNullOrEmpty(CV.chargingstatus))
                            {
                                SendEmailYN = false;
                            }

                            if (SendEmailYN)
                            {
                                if (CV.ErrorCode == "Success" || CV.ErrorCode == "ChargingLink Send")
                                {
                                    if (CV.ErrorCode == "Success")
                                    {
                                        UpdateCarChauffeur(BookingID, CV, CV.InterUnitYN);

                                        db.CloseBooking_UpdateStatus(BookingID, CV.BookingStatus, CV.DSStatus);

                                        CV.EditCorDrive = "Confirm";
                                        InsertCorDriveClosedDetails(BookingID, CV.EditCorDrive, UserID);
                                    }

                                    string mailstatus = "";
                                    if (CV.ErrorCode == "ChargingLink Send")
                                    {
                                        if (NotifyGuestYN && CV.DSStatus == "O" && CV.BookingStatus == "O")
                                        {
                                            mailstatus = SendInvoiceMail(BookingID, CV.SendApprovalLinkYN);
                                        }
                                    }
                                    else
                                    {


                                        //if (CV.ErrorCode != "076" && CV.ErrorCode != "Batch Submitted and will take around 1 day to confirm Charging.")
                                        //{
                                        if (NotifyGuestYN && CV.DSStatus == "C" && CV.BookingStatus == "C"
                                            || (CV.DSStatus == "P" && CV.BookingStatus == "O" && (CV.ProvisionalInvoiceForFacilitatorYN == true
                                            || CV.ProvisionalInvoiceForGuestYN == true || CV.ProvisionalInvoiceForTravelDeskYN == true)))
                                        {

                                            mailstatus = SendInvoiceMail(BookingID, CV.SendApprovalLinkYN);
                                        }
                                        //}
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CV.ErrorCode = ex.ToString();
            }
            return CV.ErrorCode;
        }

        public string EditBooking(int BookingID, bool NotifyGuestYN)
        {
            ChargingWebService.ChargingDetails CD = new ChargingWebService.ChargingDetails();
            ChargingWebService.ChargingMethods CM = new ChargingWebService.ChargingMethods();
            ClosingVariables CV = new ClosingVariables();
            CV = CD.GetClosingDetails(BookingID, 1);
            int UserID = 1;   //User ID to be checked and corrected
            try
            {
                if (!CV.SendApprovalLinkYN)
                {
                    CV = CM.ChargingMethod(BookingID, CV, UserID);
                }
            }
            catch (Exception ex)
            {
                CV.ErrorCode = ex.ToString();
            }
            return CV.ErrorCode;
        }

        public string ChargingWebService(int BookingID, int UserID
            , bool NotifyGuestYN, bool ChargeYN, bool BulkBookingChargeYN = false)//if return Success then continue else failure and show return message
        {
            ErrorLog.LoginfoToLogFile("BookingID:" + BookingID.ToString() + ",UserID:" + UserID.ToString()
                + ",NotifyGuestYN" + NotifyGuestYN.ToString() + ",ChargeYN" + ChargeYN.ToString()
                , "ChargingWebService");

            ChargingWebService.ChargingDetails CD = new ChargingWebService.ChargingDetails();
            ChargingWebService.ChargingMethods CM = new ChargingWebService.ChargingMethods();
            ChargingWebService.DB db = new ChargingWebService.DB();
            ClosingVariables CV = new ClosingVariables();
            CV = CD.GetChargingDetails(BookingID, UserID);
            //if (ChargingAmt > 0)
            //{
            //    CV.TotalCost = ChargingAmt;
            //}
            //int UserID = 1;   //User ID to be checked and corrected
            try
            {
                if (string.IsNullOrEmpty(CV.chargingstatus) && CV.AmexBatch == "")
                {
                    if ((!CV.SendApprovalLinkYN || ChargeYN) && CV.ErrorCode != "Success")
                    {
                        CV = CM.ChargingMethod(BookingID, CV
                            , UserID, BulkBookingChargeYN);
                    }

                }
                if (CV.AmexBatch == "Batch")
                {
                    CV.ErrorCode = "Batch Submitted and will take around 1 day to confirm Charging.";
                }

                ErrorLog.LoginfoToLogFile("BookingID:" + BookingID.ToString() + ",ErrorCode:" + CV.ErrorCode, "ChargingWebService");

                if (CV.ErrorCode == "Success")
                {
                    db.CloseBooking_UpdateStatus(BookingID, CV.BookingStatus, CV.DSStatus);

                    string mailstatus = "";
                    if (NotifyGuestYN)
                    {
                        mailstatus = SendInvoiceMail(BookingID, CV.SendApprovalLinkYN);
                    }
                }
            }
            catch (Exception ex)
            {
                CV.ErrorCode = ex.ToString();

                ErrorLog.LogErrorToLogFile(ex, "ChargingWebService" + "BookingID:" + BookingID.ToString());

            }
            return CV.ErrorCode;
        }

        public string SendInvoiceMail(int BookingID, bool SendApprovalLinkYN)
        {
            string ErrorCode = "";

            SMSMailRef.SendSMSSoapClient smsmail = new SMSMailRef.SendSMSSoapClient();
            //SMSMailRefTest.SendSMSSoapClient smsmail = new SMSMailRefTest.SendSMSSoapClient();
            ChargingWebService.DB db = new ChargingWebService.DB();

            InvoiceMailerVariables InvMailer = new InvoiceMailerVariables();
            InvMailer = GetInvoiceMailerDetails(BookingID);

            try
            {
                if (!InvMailer.CCRegisteredYN)
                {
                    if ((db.CheckChargingLink(BookingID) && InvMailer.DSStatus == "O" && InvMailer.Status == "O") || SendApprovalLinkYN) //new charging link
                    {
                        ErrorCode = smsmail.SendApprovalLink(BookingID, 1);
                    }
                    else
                    {
                        if (!InvMailer.PDFYN || InvMailer.Status == "O" || InvMailer.Status == "C")
                        {
                            if (InvMailer.ChargingNRequired) //Nokia Mailer
                            {
                                ErrorCode = smsmail.NokiaApprovalEmail(BookingID);
                            }
                            else
                            {
                                if (InvMailer.ClientCoID == 1923 || InvMailer.ClientCoID == 2257 || InvMailer.ClientCoID == 2205
                                    || InvMailer.ClientCoID == 1684)
                                {
                                    //if (CV.ClientCoID == 1684)
                                    //{
                                    //    ErrorCode = smsmail.BookingTrips_RetailEmail(BookingID, 1);  //DSCloseMail.asp
                                    //}
                                    ErrorCode = smsmail.BookingTrips_RetailEmail(BookingID, 0); //RetailMailFormat.asp
                                }
                                else
                                {
                                    if (InvMailer.DSStatus == "C" || (InvMailer.ClientCoID == 631 && InvMailer.DSStatus == "P")
                                        || InvMailer.ClientCoID == 1658 || InvMailer.ClientCoID == 2610 || InvMailer.ClientCoID == 3248
                                        || InvMailer.ClientCoID == 3249 || InvMailer.ClientCoID == 2969
                                        || (InvMailer.DSStatus == "P" && (InvMailer.ProvisionalInvoiceForGuestYN == true
                                        || InvMailer.ProvisionalInvoiceForFacilitatorYN == true || InvMailer.ProvisionalInvoiceForTravelDeskYN == true))
                                        )
                                    {
                                        if (string.IsNullOrEmpty(InvMailer.hdncontinueYN))
                                        {
                                            if ((InvMailer.ClientCoID == 3248 || InvMailer.ClientCoID == 3249) && InvMailer.DSStatus == "O")
                                            {
                                                ErrorCode = smsmail.SendInvoiceLink_New(BookingID, InvMailer.ChargingLink, InvMailer.AmexLinkYN); //Email_ChargingLink.asp    
                                            }
                                            else
                                            {
                                                ErrorCode = smsmail.SendInvoiceLink(BookingID);
                                            }
                                        }
                                        else
                                        {
                                            ErrorCode = smsmail.SendInvoiceLink_New(BookingID, InvMailer.ChargingLink, InvMailer.AmexLinkYN);    //Email_ChargingLink.asp    
                                        }
                                    }
                                    else if (InvMailer.DSStatus == "O")
                                    {
                                        if (!string.IsNullOrEmpty(InvMailer.hdncontinueYN))
                                        {
                                            ErrorCode = smsmail.SendInvoiceLink_New(BookingID, InvMailer.ChargingLink, InvMailer.AmexLinkYN);    //Email_ChargingLink.asp    
                                        }
                                    }
                                }
                            }
                        }
                    } //else of if condition if (db.CheckChargingLink(BookingID))
                }
            }
            catch (Exception ex)
            {
                ErrorCode = "Error Sending Mail : " + ex.ToString();
            }
            return ErrorCode;
        }

        //Closing module end

        public string GetCaptureStatusAmex(int BookingID, double Amount)
        {
            DataSet data = new DataSet();
            data = GetBookingDetails_Capture(BookingID);

            string returnstring = null;

            if (data.Tables[0].Rows.Count > 0)
            {
                //_TNS.VPCRequest conn = new _TNS.VPCRequest("https://vpos.amxvpos.com/vpcdps");
                _TNS.VPCRequest conn = new _TNS.VPCRequest("https://in.amxvpos.com/vpcdps");

                // Configure the Proxy details (if needed)
                conn.SetProxyHost("");
                conn.SetProxyUser("");
                conn.SetProxyPassword("");
                conn.SetProxyDomain("");
                conn.SetSecureSecret("");

                // Add the Digital Order Fields for the functionality you wish to use
                // Core Transaction Fields
                conn.AddDigitalOrderField("vpc_Version", "1");
                conn.AddDigitalOrderField("vpc_Command", "capture");
                conn.AddDigitalOrderField("vpc_AccessCode", "0C2E23EB");
                conn.AddDigitalOrderField("vpc_Merchant", "9820390193");
                conn.AddDigitalOrderField("vpc_User", "carzonama");
                conn.AddDigitalOrderField("vpc_Password", "password0");
                conn.AddDigitalOrderField("vpc_MerchTxnRef", BookingID.ToString()); //"4823519"); //Booking ID
                conn.AddDigitalOrderField("vpc_TransNo", data.Tables[0].Rows[0]["transactionid"].ToString()); //"404"); //Transaction No
                conn.AddDigitalOrderField("vpc_Amount", Convert.ToInt32(Amount).ToString()); //"1"); //Amount

                // Perform the transaction
                conn.SendRequest();

                // Check if the transaction was successful or if there was an error
                String vpc_TxnResponseCode = conn.GetResultField("vpc_TxnResponseCode", "Unknown"); //used

                // Set the display fields for the receipt with the result fields
                string Label_vpc_Message = conn.GetResultField("vpc_Message", "");
                string Label_vpc_MerchTxnRef = conn.GetResultField("vpc_MerchTxnRef", "Unknown");
                string Label_vpc_Merchant = conn.GetResultField("vpc_Merchant", "Unknown");
                string Label_vpc_OrderInfo = conn.GetResultField("vpc_OrderInfo", "Unknown");
                string Label_vpc_Amount = conn.GetResultField("vpc_Amount", "Unknown");
                string Label_vpc_TxnResponseCode = vpc_TxnResponseCode;
                string Label_vpc_AcqResponseCode = conn.GetResultField("vpc_AcqResponseCode", "Unknown");
                string Label_vpc_TransactionNo = conn.GetResultField("vpc_TransactionNo", "Unknown");
                string Label_vpc_ShopTransactionNo = conn.GetResultField("vpc_ShopTransactionNo", "Unknown");
                string Label_vpc_ReceiptNo = conn.GetResultField("vpc_ReceiptNo", "Unknown");
                string Label_vpc_AuthorizeId = conn.GetResultField("vpc_AuthorizeId", "Unknown");
                string Label_vpc_BatchNo = conn.GetResultField("vpc_BatchNo", "Unknown");
                string Label_vpc_TicketNo = conn.GetResultField("vpc_TicketNo", "Unknown");
                string Label_vpc_Card = conn.GetResultField("vpc_Card", "Unknown");
                string Label_vpc_AuthorisedAmount = conn.GetResultField("vpc_AuthorisedAmount", "Unknown");
                string Label_vpc_CapturedAmount = conn.GetResultField("vpc_CapturedAmount", "Unknown");
                string Label_vpc_RefundedAmount = conn.GetResultField("vpc_RefundedAmount", "Unknown");
                string Label_vpc_3DSstatus = conn.GetResultField("vpc_3DSstatus", "Unknown");

                string Label_vpc_TxnResponseCodeDesc = _TNS.PaymentCodesHelper.GetTxnResponseCodeDescription(vpc_TxnResponseCode); //code


                if (vpc_TxnResponseCode == "0") // && (Label_vpc_3DSstatus == "Y" || Label_vpc_3DSstatus == "A"))
                {
                    //SW.Rows.Add("Success", Label_vpc_Amount, Label_vpc_ReceiptNo, Label_vpc_TransactionNo, Label_vpc_AuthorizeId);
                    returnstring = "Success" + ";" + Label_vpc_Amount + ";" + Label_vpc_ReceiptNo + ";" + Label_vpc_TransactionNo + ";" + Label_vpc_AuthorizeId;
                }
                else
                {
                    returnstring = Label_vpc_Message;
                    //SW.Rows.Add(Label_vpc_Message, "", "", "", "");
                    try
                    {
                        ChargingWebService.DB db = new ChargingWebService.DB();
                        db.SaveCardFailureLogs(BookingID, vpc_TxnResponseCode, Label_vpc_TransactionNo, Label_vpc_Message, Amount, "AmexError");
                    }
                    catch (Exception ex)
                    {

                    }
                }
                //SW.Rows.Add(ex.Message.ToString(), "", "", "", "", "");
            }

            //S1.Tables.Add(SW);

            //return S1;
            return returnstring;
        }

        public string GetCaptureStatusMaster(int BookingID, double Amount)
        {
            //if (Credentials.UserName == userName && Credentials.Password == Password)
            //{
            DataSet data = new DataSet();

            data = GetBookingDetails_Capture(BookingID);

            string returnstring = null;

            if (data.Tables[0].Rows.Count > 0)
            {

                DataSet MV = new DataSet();

                MV = GetMasterVisaDetail("Charging");

                try
                {
                    //ID given by bank to Merchant (Tranportal ID)
                    //string id = "<id>90004302</id>";
                    string id = "<id>" + MV.Tables[0].Rows[0]["TranportalId"].ToString() + "</id>";

                    //Password given by bank to merchant (Tranportal Password)
                    //string password = "<password>password1</password>";
                    string password = "<password>" + MV.Tables[0].Rows[0]["TranportalPassword"].ToString() + "</password>";

                    // Pass Refund/Capture action code,For Refund pass 2 and for Catpure pass 5
                    //string action = "<action>5</action>";
                    string action = "<action>" + MV.Tables[0].Rows[0]["Action"].ToString() + "</action>";

                    //Pass PG Original purchase Transaction ID 
                    //string transid = "<transid>8007077251140510</transid>"; //405072055779 //4823564
                    //string transid = "<transid>" + data.Tables[0].Rows[0]["transactionid"].ToString() + "</transid>"; //405072055779 //4823564
                    string transid = "<transid>" + data.Tables[0].Rows[0]["authorizationid"].ToString() + "</transid>"; //405072055779 //4823564

                    //Pass PG Original purchase Amount 
                    //string amt = "<amt>1.00</amt>";
                    string amt = "<amt>" + Amount.ToString() + "</amt>";
                    //Pass PG Member name
                    string member = "<member>Testing</member>"; //Card holder name

                    //Pass original Trackid for merchant reference if he want,if he dont want then remove trackid variable(field) form the request string.
                    //string trackid = "<trackid>4823576</trackid>"; //bookingid
                    string trackid = "<trackid>" + data.Tables[0].Rows[0]["BookingID"].ToString() + "</trackid>"; //bookingid


                    /* Now merchant sets all the inputs in one string and passes to the Payment Gateway URL */
                    string data1 = id + password + action + transid + amt + member + trackid;

                    /* This is Payment Gateway Test URL where merchant sends request. This is test enviornment URL, 
                    production URL will be different and will be shared by Bank during production movement */
                    string url = MV.Tables[0].Rows[0]["Url"].ToString();  //"https://securepgtest.fssnet.co.in/pgway/servlet/TranPortalXMLServlet";

                    System.IO.StreamWriter myWriter = null;
                    // it will open a http connection with provided url
                    System.Net.HttpWebRequest objRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);//send data using objxmlhttp object
                    objRequest.Method = "POST";
                    objRequest.ContentLength = data1.Length;
                    objRequest.ContentType = "application/x-www-form-urlencoded";//to set content type
                    myWriter = new System.IO.StreamWriter(objRequest.GetRequestStream());
                    myWriter.Write(data1);
                    myWriter.Close();
                    string TranInqResponse;
                    System.Net.HttpWebResponse objResponse = (System.Net.HttpWebResponse)objRequest.GetResponse();
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(objResponse.GetResponseStream()))
                    {
                        /* The variable declartion where PG response wil be received*/
                        TranInqResponse = sr.ReadToEnd();
                        //receive the responce from objResponse object

                        string[] INQCheck = GetStringInBetween(TranInqResponse, "<result>", "</result>", false, false);
                        //This line will check if any error in TranInqResponse 		
                        if (INQCheck[0] == "CAPTURED" || INQCheck[0] == "APPROVED" || INQCheck[0] == "SUCCESS")
                        {

                            //Collect Transaction RESULT

                            string[] INQResResult = GetStringInBetween(TranInqResponse, "<result>", "</result>", false, false);//It will give  Result 
                            string[] INQResAmount = GetStringInBetween(TranInqResponse, "<amt>", "</amt>", false, false);//It will give Amount
                            string[] INQResTrackId = GetStringInBetween(TranInqResponse, "<trackid>", "</trackid>", false, false);//It will give TrackID 
                            string[] INQResPayid = GetStringInBetween(TranInqResponse, "<payid>", "</payid>", false, false);//It will give paymentid
                            string[] INQResRef = GetStringInBetween(TranInqResponse, "<ref>", "</ref>", false, false);//It will give Ref.NO.
                            string[] INQResTranid = GetStringInBetween(TranInqResponse, "<tranid>", "</tranid>", false, false);//It will give tranid
                            //MERCHANT CAN GET ALL RESULT PARAMETERS USING BELOW CODE 
                            /*
                            string[] INQResAutht=GetStringInBetween(TranInqResponse,"<auth>","</auth>",false,false);//It will give Auth 
                            string[] INQResAvr=GetStringInBetween(TranInqResponse,"<avr>","</avr>",false,false);//It will give AVR 
                            string[] INQResPostdate=GetStringInBetween(TranInqResponse,"<postdate>","</postdate>",false,false);//It will give  postdate
                            string[] INQResUdf1=GetStringInBetween(TranInqResponse,"<udf1>","</udf1>",false,false);//It will give udf1
                            string[] INQResUdf2=GetStringInBetween(TranInqResponse,"<udf2>","</udf2>",false,false);//It will give udf2
                            string[] INQResUdf3=GetStringInBetween(TranInqResponse,"<udf3>","</udf3>",false,false);//It will give udf3
                            string[] INQResUdf4=GetStringInBetween(TranInqResponse,"<udf4>","</udf4>",false,false);//It will give udf4
                            string[] INQResUdf5=GetStringInBetween(TranInqResponse,"<udf5>","</udf5>",false,false);//It will give udf5
                            */

                            //Displaying All Result
                            //Response.Write("<center>");
                            //Response.Write("Result: " + INQResResult[0]);
                            //Response.Write("<br>");
                            //Response.Write("Amount: " + INQResAmount[0]);
                            //Response.Write("<br>");
                            //Response.Write("Payment ID: " + INQResPayid[0]);
                            //Response.Write("<br>");
                            //Response.Write("Ref NO: " + INQResRef[0]);
                            //Response.Write("<br>");
                            //Response.Write("TransID: " + INQResTranid[0]);
                            //Response.Write("<br>");
                            //Response.Write("TrackID: " + INQResTrackId[0]);

                            //SW.Rows.Add(INQResResult[0], INQResAmount[0], INQResPayid[0], INQResRef[0]
                            //, INQResTranid[0], INQResTrackId[0]);

                            returnstring = INQResResult[0] + ";" + INQResAmount[0] + ";" + INQResPayid[0] + ";" + INQResRef[0]
                            + ";" + INQResTranid[0] + ";" + INQResTrackId[0];
                        }
                        else
                        {
                            //Response.Write("<center>");
                            //Response.Write("Error: " + TranInqResponse);
                            //SW.Rows.Add(TranInqResponse, "", "", "", "", "");
                            string[] INQResResult = GetStringInBetween(TranInqResponse, "<result>", "</result>", false, false);//It will give  Result 
                            string[] INQResTranid = GetStringInBetween(TranInqResponse, "<error_code_tag>", "</error_code_tag>", false, false);//It will give tranid

                            returnstring = INQResResult[0] + ";" + INQResTranid[0];

                            //try //commented as there is secondary card option to send
                            //{
                            //    SaveCardFailureLogs(BookingID, INQResTranid[0], "", INQResResult[0], Amount, "MVError");
                            //}
                            //catch (Exception ex)
                            //{

                            //}

                            //Response.Write("<center>");
                            //Response.Write("Error: " + TranInqResponse);
                            //SW.Rows.Add(INQResResult[0], "", "", "", "", INQResTranid[0]);

                        }

                    }

                }
                catch (Exception ex)
                {
                    returnstring = ex.Message.ToString();
                }

            }
            else
            {
                returnstring = "No Data Found.";
            }

            return returnstring;
        }

        public string[] GetStringInBetween(string strSource, string strBegin, string strEnd, bool includeBegin, bool includeEnd)
        {
            string[] result = { "", "" };
            int iIndexOfBegin = strSource.IndexOf(strBegin);
            if (iIndexOfBegin != -1)
            {
                // include the Begin string if desired

                if (includeBegin)
                {
                    iIndexOfBegin -= strBegin.Length;
                }
                strSource = strSource.Substring(iIndexOfBegin + strBegin.Length);
                int iEnd = strSource.IndexOf(strEnd);
                if (iEnd != -1)
                {  // include the End string if desired
                    if (includeEnd)
                    { iEnd += strEnd.Length; }
                    result[0] = strSource.Substring(0, iEnd);
                    // advance beyond this segment
                    if (iEnd + strEnd.Length < strSource.Length)
                    { result[1] = strSource.Substring(iEnd + strEnd.Length); }
                }
            }
            else
            // stay where we are
            { result[1] = strSource; }
            return result;
        }//String function end

        private DataSet GetBulkCharging()
        {
            return SqlHelper.ExecuteDataset("Prc_GetBulkCharging");
        }


        private DataSet GetPaymateCorporateCreditCardDetails()
        {
            return SqlHelper.ExecuteDataset("Prc_GetPaymateCorporateCreditCardDetails");
        }


        public void BulkCharging()
        {
            ErrorLog.LoginfoToLogFile("start", "BulkCharging");

            DataSet dsbulk = new DataSet();
            dsbulk = GetBulkCharging();

            if (dsbulk.Tables[0].Rows.Count > 0)
            {
                try
                {

                    ChargingWebService.ChargingDetails CD = new ChargingWebService.ChargingDetails();
                    ChargingWebService.ChargingMethods CM = new ChargingWebService.ChargingMethods();
                    ClosingVariables CV = new ClosingVariables();
                    DB db = new DB();
                    for (int i = 0; i <= dsbulk.Tables[0].Rows.Count - 1; i++)
                    {
                        ErrorLog.LoginfoToLogFile("start : " + Convert.ToString(dsbulk.Tables[0].Rows[i]["bookingid"]), "GetClosingDetails");

                        //string chargingstatus = checkChargingStatus(Convert.ToInt32(dsbulk.Tables[0].Rows[i]["bookingid"]),
                        // Convert.ToDouble(dsbulk.Tables[0].Rows[i]["totalcost"]), Convert.ToString(dsbulk.Tables[0].Rows[i]["etravelid"]));
                        //string chargingstatus = GetAmexClosingDetails(Convert.ToInt32(dsbulk.Tables[0].Rows[i]["bookingid"]));

                        try
                        {
                            CV = CD.GetChargingDetails(Convert.ToInt32(dsbulk.Tables[0].Rows[i]["bookingid"]), 1);

                            int UserID = 1;   //User ID to be checked and corrected

                            ErrorLog.LoginfoToLogFile("start : " + Convert.ToString(dsbulk.Tables[0].Rows[i]["bookingid"]) + "," + CV.ErrorCode, "ChargingMethod");

                            if (string.IsNullOrEmpty(CV.AmexBatch))
                            {
                                CV = CM.ChargingMethod(Convert.ToInt32(dsbulk.Tables[0].Rows[i]["bookingid"]), CV, UserID, true);

                                ErrorLog.LoginfoToLogFile("status : " + Convert.ToString(dsbulk.Tables[0].Rows[i]["bookingid"]) + "," + CV.ErrorCode, "ChargingMethod");


                                if (CV.ErrorCode == "Success")
                                {
                                    db.CloseBooking_UpdateStatus(Convert.ToInt32(dsbulk.Tables[0].Rows[i]["bookingid"]), CV.BookingStatus, CV.DSStatus);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorLog.LogErrorToLogFile(ex, "BulkCharging" + Convert.ToString(dsbulk.Tables[0].Rows[i]["bookingid"]));
                        }
                    }
                }
                catch (Exception e)
                {
                    ErrorLog.LogErrorToLogFile(e, "BulkCharging");
                }
            }
            else
            {
                ErrorLog.LoginfoToLogFile("No Records found.", "BulkCharging");
            }
        }

        public void BulkChargingLink()
        {
            ErrorLog.LoginfoToLogFile("start", "BulkChargingLink");

            DataSet dsbulk = new DataSet();
            dsbulk = GetBulkCharging();

            string mailstatus = "";
            if (dsbulk.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i <= dsbulk.Tables[0].Rows.Count - 1; i++)
                {
                    try
                    {
                        SMSMailRef.SendSMSSoapClient smsmail = new SMSMailRef.SendSMSSoapClient();

                        mailstatus = smsmail.SendApprovalLink(Convert.ToInt32(dsbulk.Tables[0].Rows[i]["bookingid"]), 1); //mail code
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.LogErrorToLogFile(ex, "BulkChargingLink");
                    }
                }
            }
            else
            {
                ErrorLog.LoginfoToLogFile("No Records found.", "BulkChargingLink");
            }
        }

        public string checkCharge(string orderid, string TransactionID, bool OldPreauthModule)
        {
            string TrackOrderID = "";

            if (OldPreauthModule)
            {
                MasterVisaCharging.ServiceSoapClient MV1 = new MasterVisaCharging.ServiceSoapClient();
                MasterVisaCharging.AuthenticateService credential1 = new MasterVisaCharging.AuthenticateService();

                credential1.userName = "00012955";
                credential1.merchRefNo = "SFHEK";

                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                TrackOrderID = MV1.TrackStatus_OrderID(credential1, orderid, "CCW");
            }
            else
            {
                //UATPMCorporate.PMServiceClient MV1 = new UATPMCorporate.PMServiceClient();
                //UATPMCorporate.AuthenticateService credential1 = new UATPMCorporate.AuthenticateService();

                PaymateRegistrationWCF.PMServiceClient MV1 = new PaymateRegistrationWCF.PMServiceClient();
                PaymateRegistrationWCF.AuthenticateService credential1 = new PaymateRegistrationWCF.AuthenticateService();

                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                credential1.AuthUserName = "67592538";
                credential1.AuthPassword = "5BF24F3AA4CA";
                credential1.AuthCode = "2511C6A40B4A";

                string AccessCode = "A0771D0B-6A01-40FF-9347-DEA6CAFDD293";
                TrackOrderID = MV1.GetTransactionStatus(credential1, AccessCode, TransactionID, orderid);
            }

            return TrackOrderID;
        }


        public string checkChargingStatus_AccentureNew(int BookingID, double TotalCost, string Misc1)
        {
            ErrorLog.LoginfoToLogFile("start of method;BookingID:" + BookingID
               + ",TotalCost:" + TotalCost + ",Misc1:" + Misc1, "checkChargingStatus_AccentureNew");

            string ErrorCode = "";
            try
            {
                DataSet dsSaveMVLog = new DataSet();
                dsSaveMVLog = SaveMVLog_Booking(BookingID);
                ErrorLog.LoginfoToLogFile("before getting db details", "checkChargingStatus_AccentureNew");
                DataSet ds = new DataSet();
                ds = GetMVLogDetails_Accenture(BookingID);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {

                        ErrorLog.LoginfoToLogFile("after getting db details", "checkChargingStatus_AccentureNew");

                        //UATPMCorporate.PMServiceClient MV = new UATPMCorporate.PMServiceClient();
                        //UATPMCorporate.AuthenticateService credential = new UATPMCorporate.AuthenticateService();

                        PaymateRegistrationWCF.PMServiceClient MV = new PaymateRegistrationWCF.PMServiceClient();
                        PaymateRegistrationWCF.AuthenticateService credential = new PaymateRegistrationWCF.AuthenticateService();

                        ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                        credential.AuthUserName = "67592538";
                        credential.AuthPassword = "5BF24F3AA4CA";
                        credential.AuthCode = "2511C6A40B4A";


                        string TrackOrderID = "";
                        string AccessCode = "";

                        //ErrorLog.LoginfoToLogFile(CV.CaptureTransactionID + "," + ds.Tables[0].Rows[i]["ApprovalNo"].ToString(), "");


                        AccessCode = "A0771D0B-6A01-40FF-9347-DEA6CAFDD293";

                        ErrorLog.LoginfoToLogFile("start of method GetTransactionStatus", "checkChargingStatus_AccentureNew");

                        TrackOrderID = MV.GetTransactionStatus(credential, AccessCode, "", BookingID.ToString());

                        ErrorLog.LoginfoToLogFile("GetTransactionStatus status : " + TrackOrderID, "checkChargingStatus_AccentureNew");

                        string[] OrderIDstatus = SplitFunction(TrackOrderID);

                        string Errorcode, ErrorMessage;

                        Errorcode = OrderIDstatus[0].ToString();
                        ErrorMessage = OrderIDstatus[1].ToString();
                        //InvoiceNo = OrderIDstatus[2].ToString();
                        //PayMateTransactionID = OrderIDstatus[3].ToString();
                        //Amount = OrderIDstatus[4].ToString();
                        //TransactionStatus = OrderIDstatus[5].ToString();
                        //TransactionMode = OrderIDstatus[6].ToString();

                        if (Errorcode == "000")
                        {
                            ErrorCode = TrackOrderID;
                        }
                        else
                        {
                            ErrorLog.LoginfoToLogFile("before charging: ", "checkChargingStatus_AccentureNew");

                            if (dsSaveMVLog.Tables[0].Rows.Count > 0)
                            {
                                string OrderInfo = "";
                                OrderInfo = dsSaveMVLog.Tables[0].Rows[0]["MTR"].ToString();

                                //code to charging
                                string SysRegCode, AgentEmailId, RANumber, RentalDate, RentalType, PickupCity, ReturnCity, PartnerName;
                                //SysRegCode = "dupZAk"; // ds.Tables[1].Rows[0]["PaymateSysRegCode"].ToString(); //to change in liver
                                SysRegCode = ds.Tables[1].Rows[0]["PaymateSysRegCode"].ToString(); //to change in liver
                                AgentEmailId = "rahul.jain@carzonrent.com";//ds.Tables[1].Rows[0]["EmailID"].ToString();
                                RANumber = BookingID.ToString();
                                RentalDate = ds.Tables[1].Rows[0]["PickUpDate"].ToString();
                                RentalType = "self"; //ds.Tables[1].Rows[0]["RentalTypeDescription"].ToString();
                                PickupCity = ds.Tables[1].Rows[0]["CityName"].ToString();
                                ReturnCity = ds.Tables[1].Rows[0]["CityName"].ToString();
                                PartnerName = "NA"; //ds.Tables[1].Rows[0]["ClientCoName"].ToString();

                                AccessCode = "747885DE-540A-488C-939F-96043C7FFDA6";

                                ErrorLog.LoginfoToLogFile("before ETrvCapture: ", "checkChargingStatus_AccentureNew");

                                string returnresult = MV.ETrvCapture(credential, AccessCode, Misc1, SysRegCode, AgentEmailId, BookingID.ToString()
                                    , TotalCost.ToString(), RANumber, RentalDate, RentalType, PickupCity, ReturnCity, PartnerName);
                                //(credential, TotalCost.ToString(), OrderInfo, Misc1, "D33F854B-2CAB-4551-B649-CADCC3A12E95");


                                ErrorLog.LoginfoToLogFile("after ETrvCapture: " + returnresult, "checkChargingStatus_AccentureNew");
                                //ErrorLog.LoginfoToLogFile("DirectCaptureProcess:" + returnresult, "checkChargingStatus");
                                //Errorcode = returnresult;

                                string[] SPreturnresult = SplitFunction(returnresult);

                                string ERRORCODECharging, ERRORMESSAGECharging, PayMateCaptureTransactionIDCharging, InvoiceNoCharging, AmountCharging, TravellerIdCharging, PartnerNameCharging;

                                ERRORCODECharging = SPreturnresult[0].ToString();
                                ERRORMESSAGECharging = SPreturnresult[1].ToString();
                                PayMateCaptureTransactionIDCharging = SPreturnresult[2].ToString();
                                InvoiceNoCharging = SPreturnresult[3].ToString();
                                AmountCharging = SPreturnresult[4].ToString();
                                TravellerIdCharging = SPreturnresult[5].ToString();
                                PartnerNameCharging = SPreturnresult[6].ToString();



                                try
                                {
                                    UPdateMVLog_1(OrderInfo, ERRORCODECharging, PayMateCaptureTransactionIDCharging, ERRORMESSAGECharging);
                                }
                                catch (Exception upd)
                                {
                                    ErrorLog.LogErrorToLogFile(upd, "UPdateMVLog_1");
                                }



                                ErrorLog.LoginfoToLogFile("after charging; Errorcode:" + returnresult, "checkChargingStatus_AccentureNew");

                                if (ERRORCODECharging != "000")
                                {
                                    ErrorCode = ERRORMESSAGECharging;

                                    //    //send a charging link to user
                                    //    //ChargingWebService.ChargingMethods charging = new ChargingWebService.ChargingMethods();
                                    //    //charging.SaveCCSchedulerDetails(BookingID, TotalCost, 1);
                                    //    //ErrorCode = "ChargingLink Send";
                                    //    ErrorCode = ""
                                }
                                else
                                {
                                    ErrorCode = "Charged";
                                }
                            }
                            else
                            {
                                ErrorCode = "0";
                            }

                        }  //else part end for if (CCerrorCode == "400")
                    }
                }
                else
                {
                    ErrorCode = "0";
                }
            }
            catch (Exception ex)
            {
                ErrorCode = "0";
                ErrorLog.LogErrorToLogFile(ex, "checkChargingStatus_AccentureNew:BookingID=" + BookingID + ",TotalCost=" + TotalCost + ",Misc1=" + Misc1);
            }

            return ErrorCode;
        }


        public string PaymateCorporateGooglePreauth(int BookingID, double Amt)
        {
            string PreauthStatus = "";

            PaymateRegistrationWCF.PMServiceClient paymateClient = new PaymateRegistrationWCF.PMServiceClient(); //currently working
            PaymateRegistrationWCF.AuthenticateService Auth = new PaymateRegistrationWCF.AuthenticateService(); //currently working

            //NewPaytmCorporateTLS12.PMServiceClient paymateClient = new NewPaytmCorporateTLS12.PMServiceClient(); //test TLS 1.2
            //NewPaytmCorporateTLS12.AuthenticateService Auth = new NewPaytmCorporateTLS12.AuthenticateService(); //test TLS 1.2

            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

            Auth.AuthUserName = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthUserName");
            Auth.AuthPassword = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthPassword");
            Auth.AuthCode = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthCode");

            string AccessCode = ""; //GuestRegistrationErrorDesc.GetConfigurationValue("CheckRegisterOrgAccessCode");

            DataSet ds = new DataSet();
            ds = GetMVLogDetails_Accenture(BookingID);

            string SysRegCode = "", AgentEmailId = "", RANumber = "", RentalDate = "", RentalType = "", PickupCity = "", ReturnCity = ""
                , PartnerName = "", TravellerId = "", EmpEmailId = "";

            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    SysRegCode = "dupZAk"; // ds.Tables[1].Rows[0]["PaymateSysRegCode"].ToString();
                    AgentEmailId = "rahul.jain@carzonrent.com";//ds.Tables[1].Rows[0]["EmailID"].ToString();
                    RANumber = BookingID.ToString();
                    RentalDate = ds.Tables[1].Rows[0]["PickUpDate"].ToString();
                    RentalType = "self"; //ds.Tables[1].Rows[0]["RentalTypeDescription"].ToString();
                    PickupCity = ds.Tables[1].Rows[0]["CityName"].ToString();
                    ReturnCity = ds.Tables[1].Rows[0]["CityName"].ToString();
                    PartnerName = "STL"; //ds.Tables[1].Rows[0]["ClientCoName"].ToString();
                    TravellerId = "";
                    EmpEmailId = "";
                }
            }
            AccessCode = "CB6FA1EE-6D9A-4C82-ABB8-763965204024";

            ErrorLog.LoginfoToLogFile("before ETrvCapture: ", "checkChargingStatus_AccentureNew");

            try
            {
                PreauthStatus = paymateClient.MVPartnerPreAuth(Auth, AccessCode, TravellerId, SysRegCode, EmpEmailId, AgentEmailId, RANumber
                    , Amt.ToString(), RANumber, RentalDate, RentalType, PickupCity, ReturnCity, PartnerName);

                //string[] SPreturnresult = SplitFunction(PreauthStatus);

                //string ERRORCODE, ERRORMESSAGE, PayMateAuthTransactionID, InvoiceNo, Amount;

                //ERRORCODE= SPreturnresult[0].ToString();
                //ERRORMESSAGE = SPreturnresult[1].ToString();
                //PayMateAuthTransactionID = SPreturnresult[2].ToString();
                //InvoiceNo = SPreturnresult[3].ToString();
                //Amount = SPreturnresult[4].ToString();
            }
            catch (Exception ex)
            {
                ErrorLog.LogErrorToLogFile(ex, "PaymateCorporateGooglePreauth");
            }

            ErrorLog.LoginfoToLogFile("status : " + PreauthStatus, "PaymateCorporateGooglePreauth"); //log file

            return PreauthStatus;
        }

        public string checkChargingStatus(int BookingID, double TotalCost, string Misc1)
        {
            string ErrorCode = "";
            try
            {
                DataSet dsSaveMVLog = new DataSet();
                dsSaveMVLog = SaveMVLog_Booking(BookingID);

                DataSet ds = new DataSet();
                ds = GetMVLogDetails(BookingID);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        //if (Convert.ToInt64(ds.Tables[0].Rows[i]["diff"]) > 5)//if time difference is greater than 5 min
                        //{

                        string URL = "https://secure.paymate.co.in/SMSGateway/Connect/CC_chkOrderID.aspx?Orderid="
                            + ds.Tables[0].Rows[i]["ApprovalNo"].ToString() + "&Mer_username=00012955&Mer_refno=SFHEK";

                        ErrorCode = PostMethod(URL);

                        ErrorLog.LoginfoToLogFile(ErrorCode + ", bookingid:" + BookingID + ", " + URL, "checkChargingStatus");

                        string[] OrderIDstatus = SplitFunction(ErrorCode);

                        string CCerrorCode, transactionNr, strMerchantOrderID, numAmount, strTransactionMode, strPGTranIDStatus;
                        CCerrorCode = OrderIDstatus[0].ToString();
                        transactionNr = OrderIDstatus[1].ToString();
                        strMerchantOrderID = OrderIDstatus[2].ToString();
                        numAmount = OrderIDstatus[3].ToString();
                        strTransactionMode = OrderIDstatus[4].ToString();
                        strPGTranIDStatus = OrderIDstatus[5].ToString();

                        if (ErrorCode != "")
                        {
                            UPdateMVLog(strMerchantOrderID, CCerrorCode, transactionNr, "SUCCESSFUL");
                        }

                        if (CCerrorCode == "400")
                        {
                            ErrorCode = "Charged";
                        }
                        else
                        {
                            if (CCerrorCode == "115" || CCerrorCode == "410" || CCerrorCode == "046")
                            {
                                if (dsSaveMVLog.Tables[0].Rows.Count > 0)
                                {
                                    string OrderInfo = "";
                                    OrderInfo = dsSaveMVLog.Tables[0].Rows[0]["MTR"].ToString();

                                    //code to charging

                                    //MVCharging.SecureCCEngineSoapClient MV = new MVCharging.SecureCCEngineSoapClient();
                                    MVCharging.WebAuthenticateService credential = new MVCharging.WebAuthenticateService();
                                    credential.userName = "00012955";
                                    credential.merchRefNo = "SFHEK";

                                    MVCharging.SecureCCEngineSoapClient MV = new MVCharging.SecureCCEngineSoapClient();

                                    //checking charging status
                                    //string ChargingStatus = checkCharge(OrderInfo);
                                    //checking charging status


                                    string returnresult = MV.DirectCaptureProcess(credential, TotalCost.ToString(), OrderInfo, Misc1, "D33F854B-2CAB-4551-B649-CADCC3A12E95");

                                    ErrorLog.LoginfoToLogFile("DirectCaptureProcess:" + returnresult, "checkChargingStatus");

                                    string[] SPreturnresult = SplitFunction(returnresult);

                                    string errorcode, TransactionID, MESSAGE;

                                    errorcode = SPreturnresult[0].ToString();
                                    TransactionID = SPreturnresult[1].ToString();
                                    MESSAGE = SPreturnresult[2].ToString();

                                    UPdateMVLog(OrderInfo, errorcode, TransactionID, MESSAGE);

                                    if (errorcode != "400")
                                    {
                                        //send a charging link to user
                                        //SaveMVLog(errorcode, TransactionID, MESSAGE, BookingID, TotalCost);
                                        ChargingWebService.ChargingMethods charging = new ChargingWebService.ChargingMethods();
                                        charging.SaveCCSchedulerDetails(BookingID, TotalCost, 1);
                                        ErrorCode = "ChargingLink Send";
                                    }
                                    else
                                    {
                                        ErrorCode = "Charged";
                                    }
                                }
                                else
                                {
                                    ErrorCode = "0";
                                }
                            }  //if (CCerrorCode == "115" || CCerrorCode == "410")
                            else
                            {
                                ErrorCode = "Waiting for Payment gateway response.";
                            }
                        }  //else part end for if (CCerrorCode == "400")
                        //} //if (Convert.ToInt64(ds.Tables[0].Rows[i]["diff"]) > 5) //if time difference is greater than 5 min
                        //else
                        //{
                        //    ErrorCode = "Please try again after 5 min.";
                        //}
                    }
                }
                else
                {
                    ErrorCode = "0";
                }
            }
            catch (Exception ex)
            {
                ErrorCode = "0";
            }

            return ErrorCode;
        }


        public string checkChargingStatusnew(int BookingID, double TotalCost, string Misc1)
        {
            string ErrorCode = "";
            try
            {
                DataSet ds = new DataSet();
                ds = GetMVLogDetails(BookingID);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        string URL = "https://secure.paymate.co.in/SMSGateway/Connect/CC_chkOrderID.aspx?Orderid="
                            + ds.Tables[0].Rows[i]["ApprovalNo"].ToString() + "&Mer_username=00012955&Mer_refno=SFHEK";

                        ErrorCode = PostMethod(URL);
                    }
                }
                else
                {
                    ErrorCode = "0";
                }
            }
            catch (Exception ex)
            {
                ErrorCode = "0";
            }

            return ErrorCode;
        }


        public string PostMethod(string URL)
        {
            string result1 = "";
            byte[] buffer1 = System.Text.Encoding.GetEncoding(1252).GetBytes(URL);
            try
            {
                WebRequest request1 = WebRequest.Create(URL);
                request1.Method = "POST";
                request1.ContentLength = buffer1.Length;

                request1.ContentType = "application/x-www-form-urlencoded";
                Stream dataStream1 = request1.GetRequestStream();
                dataStream1.Write(buffer1, 0, buffer1.Length);
                dataStream1.Close();

                HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
                Stream stm1 = response1.GetResponseStream();
                StreamReader sr1 = new StreamReader(stm1);
                result1 = sr1.ReadToEnd();

            }
            catch (Exception ex)
            {
                result1 = "";
            }
            return result1;
        }

        public DataSet GetMVLogDetails(int BookingID)
        {
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@Bookingid", BookingID);

            return SqlHelper.ExecuteDataset("prc_getapprovalno", param);
        }

        public DataSet GetMVLogDetails_Accenture(int BookingID)
        {
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@Bookingid", BookingID);

            return SqlHelper.ExecuteDataset("prc_getapprovalnoAccentureNew", param);
        }

        public string ClientIndivRegistrationModule(int BookingID, string AdditionalEmaiId, string AdditionalMobileNo, bool NotifyGuestYN)
        {
            string returnstring = null;
            PreauthValidation CVC = new PreauthValidation();
            PreAuthResult PResult = new PreAuthResult();
            DataSet ds = new DataSet();

            CVC.BookingID = BookingID;
            PResult.Response = "Failure";

            ds = GetBookingDetails_Google(CVC.BookingID);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    CVC.ClientCoIndivID = Convert.ToInt32(ds.Tables[0].Rows[i]["ClientCoIndivID"]);
                    CVC.GuestName = Convert.ToString(ds.Tables[0].Rows[i]["GuestName"]).Trim();
                    CVC.UnitName = Convert.ToString(ds.Tables[0].Rows[i]["UnitName"]).Trim();
                    CVC.dropoffdate = Convert.ToDateTime(ds.Tables[0].Rows[i]["dropoffdate"]);
                    CVC.CityName = Convert.ToString(ds.Tables[0].Rows[i]["CityName"]).Trim();
                    CVC.ApprovalAmt = Convert.ToDouble(ds.Tables[0].Rows[i]["ApprovalAmt"]);
                    CVC.FacilitatorId = Convert.ToInt32(ds.Tables[0].Rows[i]["FacilitatorID"]);
                    CVC.GuestPhoneNo = Convert.ToString(ds.Tables[0].Rows[i]["Phone1"]);
                    CVC.PickUpDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["PickUpDate"]);
                    CVC.PickUpTime = Convert.ToString(ds.Tables[0].Rows[i]["PickUpTime"]);
                    CVC.OldRegistrationStatus = Convert.ToString(ds.Tables[0].Rows[i]["GuestRegistrationStatus"]);

                    if (CVC.OldRegistrationStatus == "Old")
                    {
                        PResult = OldRegistrationProcess(CVC);  //old guest registration process
                    }
                    else if (CVC.OldRegistrationStatus == "New")
                    {
                        PResult = NewRegistrationPreauthProcess(BookingID); //New guest registration process
                    }
                    else
                    {

                    }

                    if ((PResult.ErrorCode == "400" && CVC.OldRegistrationStatus == "Old") || (CVC.OldRegistrationStatus == "New" && (PResult.ErrorCode == "000"
                        || PResult.ErrorCode == "059" || PResult.ErrorCode == "062")))
                    {
                        PResult.CCType = GuestRegistrationErrorDesc.GetCCTypeID(PResult.CCTypeName);

                        //*********************code commented

                        CVC = UpdatePreAuthStatus(PResult, BookingID);

                        if (CVC.DbOperationStatus == CommonConstant.SUCCEED)
                        {
                            if (NotifyGuestYN)
                            {
                                SMSMailRef.SendSMSSoapClient smsmail = new SMSMailRef.SendSMSSoapClient();

                                CVC.MailStatus = smsmail.BookingMailGoogle(CVC.BookingID, AdditionalEmaiId); //mail code

                                if (!string.IsNullOrEmpty(CVC.GuestPhoneNo))
                                {
                                    string guestMobile = "91" + CVC.GuestPhoneNo.ToString();
                                    if (guestMobile.Length == 12)
                                    {
                                        //string status = smsmail.SendSMSDetails(smsContent.ToString(), guestMobile, CVC.BookingID.ToString());
                                        string status = smsmail.SendSMSDetails_smsdetails_GetDetails("Guest", guestMobile, CVC.BookingID.ToString(), 0);

                                        CVC.SmsStatus = status.Equals("1") ? true : false;
                                        int smsStatus = UPdateSMSStatus(Convert.ToInt32(CVC.SmsStatus), CVC.BookingID);
                                    }
                                }

                                if (!string.IsNullOrEmpty(AdditionalMobileNo))
                                {
                                    string additionalMobile = "91" + AdditionalMobileNo.ToString();
                                    if (additionalMobile.Length == 12)
                                    {
                                        //string additionalstatus = smsmail.SendSMSDetails(smsContent.ToString(), additionalMobile, CVC.BookingID.ToString());
                                        string additionalstatus = smsmail.SendSMSDetails_smsdetails_GetDetails("Facilitator", additionalMobile, CVC.BookingID.ToString(), 0);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //return CVC;
            returnstring = new JavaScriptSerializer().Serialize(PResult);

            return returnstring;
        }

        private PreAuthResult OldRegistrationProcess(PreauthValidation CVC)
        {
            string Mer_UserName = "00011500";
            string Mer_Refno = "WNOUD";

            StringBuilder urlPost = new StringBuilder("https://secure.paymate.co.in/CCAPI/APIAuthTran.aspx?UserName=" + Mer_UserName);
            urlPost.Append("&RefNo=" + Mer_Refno.ToString());
            //urlPost.Append("&EmployeeName=" + objBookingOL.GuestName.ToString());
            urlPost.Append("&EmployeeName=" + CVC.GuestName);
            urlPost.Append("&AgentName=" + CVC.GuestName);
            urlPost.Append("&AgentCode=" + CVC.ClientCoIndivID.ToString());
            urlPost.Append("&AgentLocation=" + CVC.UnitName);
            urlPost.Append("&RANumber=" + CVC.BookingID);
            //urlPost.Append("&RentalDate=" + objBookingOL.DropOffDate.ToShortDateString());
            urlPost.Append("&RentalDate=" + CVC.dropoffdate.ToShortDateString());
            urlPost.Append("&PickupCity=" + CVC.CityName);
            urlPost.Append("&ReturnCity=" + CVC.CityName);
            urlPost.Append("&CustOrgRegId=" + CVC.ClientCoIndivID.ToString());
            urlPost.Append("&Amount=" + CVC.ApprovalAmt);
            urlPost.Append("&OrderId=" + CVC.BookingID);


            byte[] buffer1 = System.Text.Encoding.GetEncoding(1252).GetBytes(urlPost.ToString());
            WebRequest request1 = WebRequest.Create(urlPost.ToString());
            request1.Method = "POST";
            request1.ContentLength = buffer1.Length;

            request1.ContentType = "application/x-www-form-urlencoded";
            Stream dataStream1 = request1.GetRequestStream();
            dataStream1.Write(buffer1, 0, buffer1.Length);
            dataStream1.Close();

            HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
            Stream stm1 = response1.GetResponseStream();
            StreamReader sr1 = new StreamReader(stm1);
            string payMateRespose = sr1.ReadToEnd();

            string[] resultResponse = SplitFunction(payMateRespose);
            //return resultResponse;
            //string transactionNo, errorMessage, CCNo, AuthCode, PGId;
            PreAuthResult PResult = new PreAuthResult();
            PResult.ErrorCode = resultResponse[0]; //Error Code '400 if Sucess
            //objBookingOL.Transactionid = resultResponse[1]; ///Transaction ID
            PResult.transactionNo = resultResponse[1]; //Transaction ID
            PResult.errorMessage = resultResponse[2]; //Error Message
            PResult.ErrorDesc = GuestRegistrationErrorDesc.GetTxnResponseCodeDescription(PResult.ErrorCode);

            if (PResult.ErrorCode == "400")
            {
                PResult.Response = "Success";
                PResult.CCNo = resultResponse[3]; //Credit Card No
                PResult.AuthCode = resultResponse[4]; //Auth Code
                PResult.PGId = resultResponse[5];
                PResult.CCTypeName = resultResponse[6].ToString();
            }
            else
            {
                PResult.Response = "Failure";
            }
            return PResult;
        }

        public PreauthValidation UpdatePreAuthStatus(PreAuthResult objBookingOL, int BookingID)
        {
            PreauthValidation Preauth = new PreauthValidation();
            try
            {
                SqlParameter[] param = new SqlParameter[6];
                param[0] = new SqlParameter("@BookingID", BookingID);
                param[1] = new SqlParameter("@TransactionID", objBookingOL.transactionNo);
                param[2] = new SqlParameter("@CCNo", objBookingOL.CCNo);
                param[3] = new SqlParameter("@AuthCode", objBookingOL.AuthCode);
                param[4] = new SqlParameter("@PGId", objBookingOL.PGId);
                param[5] = new SqlParameter("@CCType", objBookingOL.CCType);
                Preauth.ObjDataTable = SqlHelper.ExecuteDatatable("SP_CORUpdatePreAuth_NewProcess", param);
                Preauth.DbOperationStatus = CommonConstant.SUCCEED;
            }
            catch (Exception Ex)
            {
                Preauth.DbOperationStatus = CommonConstant.FAIL;
            }
            return Preauth;
        }


        public CorporateEntity GetClientDetails(int ClientCoId)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@ClientCoId", ClientCoId);
                return CorporateEntity.Create(SqlHelper.ExecuteDatatable("SP_GetDetailsOfClient", param));
            }
            catch (Exception Ex)
            {
                return null;
            }

        }

        public CorporateEntity GetGuestDetails(int ClientCoIndivId)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@ClientCoId", ClientCoIndivId);
                return CorporateEntity.Create(SqlHelper.ExecuteDatatable("SP_GetDetailsOfGuest", param));
            }
            catch (Exception Ex)
            {
                return null;
            }

        }

        public int UpdateCCStatus_NewProcess(int ClientIndivId, int CCType, string CCNo, string Address, int Status)
        {
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@ClientIndivId", ClientIndivId);
            param[1] = new SqlParameter("@CCType", CCType);
            param[2] = new SqlParameter("@CCNo", CCNo);
            param[3] = new SqlParameter("@Address", Address);
            param[4] = new SqlParameter("@Status", Status);
            param[4].Direction = ParameterDirection.Output;
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_CCStatus_NewProcess", param);
            Status = (Int32)param[4].Value;
            return Status;

        }

        public int UPdateSMSStatus(int SmsStatus, int BookingId)
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@SMSStatus", SmsStatus);
            param[1] = new SqlParameter("@BookingID", BookingId);
            object status;
            status = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "ProcBook_UpdateSMSStatus", param);
            return Convert.ToInt32(status);
        }

        public int UpdatePaymateSysRegCodeForClient(int ClientCoId, string PaymateSysRegCode)
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@ClientCoId", ClientCoId);
            param[1] = new SqlParameter("@SysRegCode", PaymateSysRegCode);
            object status;
            status = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_UpdatePaymateSysRegCodeForClient", param);
            return Convert.ToInt32(status);
        }

        public static String SendMessageForGuest(PreauthValidation objBookingOL)
        {
            StringBuilder messageGuest = new StringBuilder();
            messageGuest.Append("Dear " + objBookingOL.GuestName + ", Your Carzonrent Booking ID for " + objBookingOL.PickUpDate.ToString("dd-MMM-yyyy") + " Pickup in ");
            messageGuest.Append(objBookingOL.CityName.ToString() + " at " + objBookingOL.PickUpTime + "Hrs. is " + objBookingOL.BookingID.ToString());
            messageGuest.Append(". Thank you for choosing COR. For assistance call  01141841212. To track your cab detail, billing information and share feedback download the COR RIDE mobile app at http://bit.ly/CORRide");
            return messageGuest.ToString();
        }

        public static String SendMessageForFacilitator(PreauthValidation objBookingOL)
        {
            StringBuilder messageFacilitator = new StringBuilder();
            messageFacilitator.Append("Dear Receiver, Your Carzonrent Booking ID for ");
            messageFacilitator.Append(objBookingOL.PickUpDate.ToString("dd-MMM-yyyy") + " Pickup in " + objBookingOL.CityName.ToString() + " at ");
            messageFacilitator.Append(objBookingOL.PickUpTime + "Hrs. is " + objBookingOL.BookingID.ToString());
            messageFacilitator.Append(". Thank you for choosing COR. For assistance call  01141841212. To track your cab detail, billing information and share feedback download the COR RIDE mobile app at http://bit.ly/CORRide");
            return messageFacilitator.ToString();
        }

        public PreauthValidation GetFacilitatorDetails(PreauthValidation objBookingOL)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@FacilitatorID", objBookingOL.FacilitatorId);
                objBookingOL.ObjDataTable = SqlHelper.ExecuteDatatable("Proc_GetSelectedFacilitatorDetails", param);
                if (objBookingOL.ObjDataTable.Rows.Count > 0)
                {
                    objBookingOL.FaclitatorPhoneNo = objBookingOL.ObjDataTable.Rows[0]["Phone1"].ToString();
                    objBookingOL.FacilitatorEmailId = objBookingOL.ObjDataTable.Rows[0]["EmailID"].ToString();

                }
                objBookingOL.DbOperationStatus = CommonConstant.SUCCEED;

            }
            catch (Exception Ex)
            {
                objBookingOL.DbOperationStatus = CommonConstant.FAIL;
            }
            return objBookingOL;
        }

        public string URLEncrypt(string strencrypt)
        {
            string cipher = "";
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            byte[] key = ASCIIEncoding.ASCII.GetBytes("Paymate1");
            byte[] iv = ASCIIEncoding.ASCII.GetBytes("Paymate1");
            try
            {
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, iv),
                CryptoStreamMode.Write);
                StreamWriter sw = new StreamWriter(cs);
                sw.Write(strencrypt);
                sw.Flush();
                cs.FlushFinalBlock();
                sw.Flush();
                byte[] hash = ms.GetBuffer();
                for (int i = 0; i < ms.Length; i++)
                    cipher += BitConverter.ToString(hash, i, 1);
                sw.Close();
                cs.Close();
                ms.Close();
            }
            catch (Exception ex)
            {
                cipher = ex.Message;
            }
            return cipher;
        }

        public PreAuthResult NewRegistrationPreauthProcess(int BookingID)
        {
            ChargingWebService.ChargingDetails CD = new ChargingWebService.ChargingDetails();
            ClosingVariables CV = new ClosingVariables();

            CV = CD.GetClosingDetails_NonCorDrive(BookingID);

            PreAuthResult PVC = new PreAuthResult();

            if (CV.Status == "X")
            {
                ErrorLog.LoginfoToLogFile("start PaymateCorporateRegistrationStatusCheck", "NewRegistrationPreauthProcess"); //log file

                string status = PaymateCorporateRegistrationStatusCheck(Convert.ToInt32(CV.ClientCoIndivID));

                string[] OrderIDstatus = SplitFunction(status);
                ErrorLog.LoginfoToLogFile("clientcoindivid :" + Convert.ToInt32(CV.ClientCoIndivID).ToString() + "status of order id : " + OrderIDstatus[0].ToString(), "NewRegistrationPreauthProcess"); //log file

                if (OrderIDstatus[0].ToString() == "000")
                {
                    //UpdateCorporateMVProcess(BookingID, "", "", "NoPreAuth", "", 0, "", "", 0);
                    PVC.Response = "Success";
                    PVC.ErrorCode = "000";
                    PVC.ErrorDesc = "Registered Successfully.";
                    PVC.CCNo = OrderIDstatus[5].ToString();
                    PVC.CCTypeName = OrderIDstatus[6].ToString().ToUpper();
                    PVC.transactionNo = "NoPreAuth";
                    //string CARDTYPE = OrderIDstatus[6].ToString().ToUpper();
                    if (PVC.CCTypeName == "AMEX")
                    {
                        PVC.CCType = 2;
                    }
                    if (PVC.CCTypeName == "MASTER")
                    {
                        PVC.CCType = 3;
                    }
                    if (PVC.CCTypeName == "VISA")
                    {
                        PVC.CCType = 1;
                    }
                }
                else
                {
                    PVC.Response = "Failure";
                }
            }
            else
            {
                if (CV.Status == "C")
                {
                    PVC.ChargingStatus = "Success";
                    CV.ErrorCode = "Success";
                }
                else
                {
                    if (CV.CCType == 1 || CV.CCType == 3)
                    {
                        //PVC = RegistrationMVPreauth(CV, BookingID, false, CV.TotalCost); //Preauth & Charging
                        PVC = RegistrationDirectMVCharging(CV, BookingID); //Direct Charging
                    }
                    else if (CV.CCType == 2)
                    {
                        PVC = RegistrationApprovalProcessAmex(CV, BookingID, CV.TotalCost);
                    }
                }
            }
            return PVC;
        }

        public void BulkPaymateCorporateCheckCreditCardExpiry()
        {
            DataSet dsbulk = new DataSet();
            dsbulk = GetPaymateCorporateCreditCardDetails();

            if (dsbulk.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i <= dsbulk.Tables[0].Rows.Count - 1; i++)
                {
                    try
                    {
                        PaymateCorporateCheckCreditCardExpiry(Convert.ToInt32(dsbulk.Tables[0].Rows[i]["ClientCoIndivID"]));
                    }
                    catch (Exception bulkexcep)
                    {
                        ErrorLog.LogErrorToLogFile(bulkexcep
                            , "BulkPaymateCorporateCheckCreditCardExpiry:Clientcoindivid" + dsbulk.Tables[0].Rows[i]["ClientCoIndivID"].ToString());
                    }
                }
            }
        }

        public string PaymateCorporateCheckCreditCardExpiry(int ClientCoIndivID)
        {
            string RegistrationStatus = "";
            //LiveCorporateRegistrationCharging.ServiceSoapClient paymateClient = new LiveCorporateRegistrationCharging.ServiceSoapClient();
            //LiveCorporateRegistrationCharging.AuthenticateService Auth = new LiveCorporateRegistrationCharging.AuthenticateService();

            PaymateRegistrationWCF.PMServiceClient paymateClient = new PaymateRegistrationWCF.PMServiceClient(); //currently working
            PaymateRegistrationWCF.AuthenticateService Auth = new PaymateRegistrationWCF.AuthenticateService(); //currently working

            //NewPaytmCorporateTLS12.PMServiceClient paymateClient = new NewPaytmCorporateTLS12.PMServiceClient(); //test TLS 1.2
            //NewPaytmCorporateTLS12.AuthenticateService Auth = new NewPaytmCorporateTLS12.AuthenticateService(); //test TLS 1.2

            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

            Auth.AuthUserName = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthUserName");
            Auth.AuthPassword = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthPassword");
            Auth.AuthCode = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthCode");

            string RegisterOrgAccessCode = GuestRegistrationErrorDesc.GetConfigurationValue("CheckCreditCardExpiry"); //0C5BAEB1-3FBD-4473-8AED-9B49AB3DAE9F
            CorporateEntity corporateEntity = GetGuestDetails(ClientCoIndivID);
            if (corporateEntity != null)
            {
                if (!string.IsNullOrEmpty(corporateEntity.SysRegCode))
                {
                    try
                    {
                        RegistrationStatus = paymateClient.CheckCardExpiry(Auth, RegisterOrgAccessCode, ClientCoIndivID.ToString(), "", corporateEntity.SysRegCode);
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.LogErrorToLogFile(ex, "PaymateCorporateCheckCreditCardExpiry");
                    }

                    ErrorLog.LoginfoToLogFile("status : " + RegistrationStatus, "PaymateCorporateRegistrationStatusCheck"); //log file

                    string[] OrderIDstatus = SplitFunction(RegistrationStatus);
                    //int CCType = 0;
                    string ERRORCODE, ERRORMESSAGE; //, TRAVELLERID, SYSREGCODE, CCNO, CARDTYPE;
                    ERRORCODE = OrderIDstatus[0].ToString();
                    ERRORMESSAGE = OrderIDstatus[1].ToString();

                    DB db = new DB();

                    bool status = db.UpdateCreditCardExpiryStatus(ClientCoIndivID, ERRORMESSAGE);

                    if (ERRORMESSAGE.Trim() == "Card has been expired.")
                    {
                        bool status11 = db.UpdateCreditCardRegisteryStatus(ClientCoIndivID);
                    }

                    //if (ERRORCODE == "100")
                    //{
                    //TRAVELLERID = OrderIDstatus[2].ToString();
                    //SYSREGCODE = OrderIDstatus[3].ToString();
                    //CCNO = OrderIDstatus[4].ToString();
                    //CARDTYPE = OrderIDstatus[5].ToString().ToUpper();
                    //if (CARDTYPE == "AMEX")
                    //{
                    //    CCType = 2;
                    //}
                    //if (CARDTYPE == "MASTER")
                    //{
                    //    CCType = 3;
                    //}
                    //if (CARDTYPE == "VISA")
                    //{
                    //    CCType = 1;
                    //}
                    //    RegistrationStatus = ERRORCODE.ToString() + "|" + ERRORMESSAGE.ToString();
                    //int Update = UpdateCCStatus_NewProcess(ClientCoIndivID, CCType, CCNO, "", 0);
                    //}
                    //else
                    //{
                    RegistrationStatus = ERRORCODE.ToString() + "|" + ERRORMESSAGE.ToString();
                    //}
                }
                else
                {
                    RegistrationStatus = "Failure|InvalidSysRegCode";
                }
            }
            else
            {
                RegistrationStatus = "Failure|InvalidClient";
            }

            return RegistrationStatus;
        }

        public string PaymateCorporateRegistrationStatusCheck(int ClientCoIndivID)
        {
            string RegistrationStatus = "";
            //LiveCorporateRegistrationCharging.ServiceSoapClient paymateClient = new LiveCorporateRegistrationCharging.ServiceSoapClient();
            //LiveCorporateRegistrationCharging.AuthenticateService Auth = new LiveCorporateRegistrationCharging.AuthenticateService();

            PaymateRegistrationWCF.PMServiceClient paymateClient = new PaymateRegistrationWCF.PMServiceClient(); //currently working
            PaymateRegistrationWCF.AuthenticateService Auth = new PaymateRegistrationWCF.AuthenticateService(); //currently working

            //NewPaytmCorporateTLS12.PMServiceClient paymateClient = new NewPaytmCorporateTLS12.PMServiceClient(); //test TLS 1.2
            //NewPaytmCorporateTLS12.AuthenticateService Auth = new NewPaytmCorporateTLS12.AuthenticateService(); //test TLS 1.2

            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

            Auth.AuthUserName = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthUserName");
            Auth.AuthPassword = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthPassword");
            Auth.AuthCode = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthCode");

            string RegisterOrgAccessCode = GuestRegistrationErrorDesc.GetConfigurationValue("CheckRegisterOrgAccessCode");
            CorporateEntity corporateEntity = GetGuestDetails(ClientCoIndivID);
            if (corporateEntity != null)
            {
                if (!string.IsNullOrEmpty(corporateEntity.SysRegCode))
                {
                    try
                    {
                        RegistrationStatus = paymateClient.VerifyTraveller(Auth, RegisterOrgAccessCode, ClientCoIndivID.ToString(), corporateEntity.SysRegCode);
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.LogErrorToLogFile(ex, "PaymateCorporateRegistrationStatusCheck");
                    }

                    ErrorLog.LoginfoToLogFile("status : " + RegistrationStatus, "PaymateCorporateRegistrationStatusCheck"); //log file

                    string[] OrderIDstatus = SplitFunction(RegistrationStatus);
                    int CCType = 0;
                    string ERRORCODE, ERRORMESSAGE, TRAVELLERID, SYSREGCODE, EMAILID, CCNO, CARDTYPE;
                    ERRORCODE = OrderIDstatus[0].ToString();

                    ERRORMESSAGE = OrderIDstatus[1].ToString();
                    if (ERRORCODE == "000")
                    {
                        TRAVELLERID = OrderIDstatus[2].ToString();
                        SYSREGCODE = OrderIDstatus[3].ToString();
                        EMAILID = OrderIDstatus[4].ToString();
                        CCNO = OrderIDstatus[5].ToString();
                        CARDTYPE = OrderIDstatus[6].ToString().ToUpper();
                        if (CARDTYPE == "AMEX")
                        {
                            CCType = 2;
                        }
                        if (CARDTYPE == "MASTER")
                        {
                            CCType = 3;
                        }
                        if (CARDTYPE == "VISA")
                        {
                            CCType = 1;
                        }

                        int Update = UpdateCCStatus_NewProcess(ClientCoIndivID, CCType, CCNO, "", 0);
                    }
                    //if (string.IsNullOrEmpty(orgRegistrationResponse))
                    //{
                    //    RegistrationStatus = "Failure|OrgRegistrationFailed";
                    //}
                    //string[] registrationResponse = orgRegistrationResponse.Split('|');
                    //if (registrationResponse.Length <= 2)
                    //{
                    //    return "Failure|Org|" + registrationResponse[1];
                    //}
                }
                else
                {
                    RegistrationStatus = "Failure|InvalidSysRegCode";
                }
            }
            else
            {
                RegistrationStatus = "Failure|InvalidClient";
            }

            return RegistrationStatus;
        }

        public string PaymateCorporateChargingStatusCheck(int BookingID, bool UpdateBookingYN)
        {
            ErrorLog.LoginfoToLogFile("start : " + BookingID.ToString(), "PaymateCorporateChargingStatusCheck"); //log file

            string Transtatus = "";
            //LiveCorporateRegistrationCharging.ServiceSoapClient paymateClient = new LiveCorporateRegistrationCharging.ServiceSoapClient();
            PaymateRegistrationWCF.PMServiceClient paymateClient = new PaymateRegistrationWCF.PMServiceClient();
            //LiveCorporateRegistrationCharging.AuthenticateService Auth = new LiveCorporateRegistrationCharging.AuthenticateService();
            PaymateRegistrationWCF.AuthenticateService Auth = new PaymateRegistrationWCF.AuthenticateService();
            //PayMateCorporateSvc.PMServiceClient paymateClient = new PayMateCorporateSvc.PMServiceClient();
            //PayMateCorporateSvc.AuthenticateService Auth = new PayMateCorporateSvc.AuthenticateService();

            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

            Auth.AuthUserName = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthUserName");
            Auth.AuthPassword = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthPassword");
            Auth.AuthCode = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthCode");


            ChargingWebService.ChargingDetails CD = new ChargingWebService.ChargingDetails();
            ClosingVariables CV = new ClosingVariables();
            CV = CD.GetClosingDetails_NonCorDrive(BookingID);

            string AccessCode = "";
            //if (CV.CCType == 2)
            //{
            AccessCode = GuestRegistrationErrorDesc.GetConfigurationValue("CheckChargingAccessCode");  //"E5F0914E-71D9-48F5-A4AF-9C68CF09F359";
            //}
            //else
            //{
            //    AccessCode = GuestRegistrationErrorDesc.GetConfigurationValue("DirectChargingAccessCode");  //"D33E5904-FFD5-41AB-9BB2-040288DC4CFD";
            //}

            if (CV.CaptureTransactionID == "NoPreAuth")
            {
                CV.CaptureTransactionID = BookingID.ToString();
            }

            ErrorLog.LoginfoToLogFile("start (GetTransactionStatus)  : " + BookingID.ToString()
                + "#" + Auth.AuthUserName + "#" + Auth.AuthPassword + "#" + Auth.AuthCode
                + "#" + AccessCode + "#" + CV.CaptureTransactionID, "PaymateCorporateChargingStatusCheck"); //log file

            Transtatus = paymateClient.GetTransactionStatus(Auth, AccessCode, CV.CaptureTransactionID, BookingID.ToString());

            ErrorLog.LoginfoToLogFile("charging status : " + BookingID.ToString() + "," + Transtatus, "PaymateCorporateChargingStatusCheck");
            //return Transtatus + ";" + AccessCode;
            string[] strTransStatus = SplitFunction(Transtatus);

            PreAuthResult PVC = new PreAuthResult();

            PVC.ErrorCode = strTransStatus[0].ToString();
            string Provider = "";
            if (CV.CCType == 2)
            {
                Provider = "AmexBooking";
            }
            else
            {
                Provider = "MasterVisaBooking";
            }
            if (PVC.ErrorCode == "000")
            {
                PVC.errorMessage = strTransStatus[1].ToString();
                PVC.InvoiceNo = strTransStatus[2].ToString();
                PVC.CaptureTransactionID = strTransStatus[3].ToString();
                PVC.Amount = Convert.ToDouble(strTransStatus[4]);
                PVC.ChargingStatus = strTransStatus[5].ToString();
                PVC.ErrorDesc = strTransStatus[6].ToString();

                if (PVC.ChargingStatus == "SUCCESSFUL" && PVC.ErrorDesc == "Capture")
                {
                    if (UpdateBookingYN)
                    {
                        UpdateAmexBatchBookingStatus(BookingID, PVC.ErrorCode, PVC.errorMessage, PVC.transactionNo, PVC.InvoiceNo, PVC.Amount, PVC.TravellerId, "", 0);
                    }
                    else
                    {
                        SaveMVLogNew(BookingID, PVC.ErrorCode, PVC.errorMessage, PVC.CaptureTransactionID);
                    }

                }
                else
                {
                    PVC.ErrorCode = "0001";
                    //if (CV.CCType == 2)
                    //{
                    //failure
                    //PVC = NewRegistrationChargingProcess(BookingID, CV.TotalCost); //TO CHECK AND THEN MADE LIVE
                    UpdateAmexBatchBookingFailureStatus(BookingID, PVC.ErrorCode, Provider);
                    //}
                }
            }
            else
            {
                //if (CV.CCType == 2)
                //{
                UpdateAmexBatchBookingFailureStatus(BookingID, PVC.ErrorCode, Provider);
                //}
            }

            ErrorLog.LoginfoToLogFile("end (GetTransactionStatus)  : " + Transtatus, "PaymateCorporateChargingStatusCheck"); //log file

            return PVC.ErrorCode;
        }

        public string PaymateCorporateRegistration(int clientCoId)
        {
            //LiveCorporateRegistrationCharging.ServiceSoapClient paymateClient = new LiveCorporateRegistrationCharging.ServiceSoapClient();
            //LiveCorporateRegistrationCharging.AuthenticateService Auth = new LiveCorporateRegistrationCharging.AuthenticateService();

            PaymateRegistrationWCF.PMServiceClient paymateClient = new PaymateRegistrationWCF.PMServiceClient();
            PaymateRegistrationWCF.AuthenticateService Auth = new PaymateRegistrationWCF.AuthenticateService();

            //BetaCorporateRegistrationCharging.ServiceSoapClient paymateClient = new BetaCorporateRegistrationCharging.ServiceSoapClient();
            //BetaCorporateRegistrationCharging.AuthenticateService Auth = new BetaCorporateRegistrationCharging.AuthenticateService();

            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

            Auth.AuthUserName = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthUserName");
            Auth.AuthPassword = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthPassword");
            Auth.AuthCode = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthCode");

            string RegisterOrgAccessCode = GuestRegistrationErrorDesc.GetConfigurationValue("RegisterOrgAccessCode");
            string RegisterOrgDomainAccessCode = GuestRegistrationErrorDesc.GetConfigurationValue("RegisterOrgDomainAccessCode");
            CorporateEntity corporateEntity = GetClientDetails(clientCoId);
            if (corporateEntity != null)
            {
                string SysRegCode = "";
                string orgRegistrationResponse = "";
                if (!string.IsNullOrEmpty(corporateEntity.SysRegCode))
                {
                    //return "Success|" + corporateEntity.SysRegCode;
                    SysRegCode = corporateEntity.SysRegCode;
                    UpdatePaymateSysRegCodeForClient(clientCoId, SysRegCode);
                }
                else
                {
                    /* string[] clientContactNumbers = corporateEntity.ClientContactNumber.Split(',');
                    string phoneNumber = clientContactNumbers[0].Replace(' ', '-').Replace('/', '-');
                    if(phoneNumber.Length == 10)
                    {
                        phoneNumber = "91-" + phoneNumber;
                    } */

                    string phoneNumber = "91-11-43083000";

                    orgRegistrationResponse = paymateClient.RegisterOrganization(Auth, RegisterOrgAccessCode, corporateEntity.ClientCoName,
                        corporateEntity.ClientCoId.ToString(), corporateEntity.ClientCoAddress, phoneNumber,
                        "Carzonrent", corporateEntity.ClientAdminEmailId);

                    if (string.IsNullOrEmpty(orgRegistrationResponse))
                    {
                        return "Failure|OrgRegistrationFailed";
                    }

                    string[] registrationResponse = orgRegistrationResponse.Split('|');
                    if (registrationResponse.Length <= 2)
                    {
                        if (registrationResponse[1].ToString() == "EmailId already exists.")
                        {
                            UpdatePaymateSysRegCodeForClient(clientCoId, SysRegCode);
                        }

                        return "Failure|Org|" + registrationResponse[1];
                    }

                    SysRegCode = registrationResponse[2];

                    UpdatePaymateSysRegCodeForClient(clientCoId, "DmNdhdpDJR");

                    // Register domain
                    string domain = corporateEntity.ClientAdminEmailId.Split('@')[1];

                    string registerOrgDomainResponse = paymateClient.RegisterOrgDomainName(Auth, RegisterOrgDomainAccessCode, SysRegCode, domain);
                    if (string.IsNullOrEmpty(registerOrgDomainResponse))
                    {
                        return "Failure|OrgDomainRegistrationFailed";
                    }
                    string[] domainRegistrationResponse = registerOrgDomainResponse.Split('|');
                    if (domainRegistrationResponse.Length <= 2)
                    {
                        return "Failure|OrgDomain|" + domainRegistrationResponse[1] + "|" + SysRegCode + "|" + domain + " | reg:" + orgRegistrationResponse;
                    }
                }

                return "Success|" + SysRegCode + "|reg:" + orgRegistrationResponse;

            }
            return "Failure|InvalidClient";
        }

        public PreAuthResult NewRegistrationChargingProcess(int BookingID, double TotalCost)
        {
            ChargingWebService.ChargingDetails CD = new ChargingWebService.ChargingDetails();
            ClosingVariables CV = new ClosingVariables();
            DB db = new DB();
            CV = CD.GetClosingDetails_NonCorDrive(BookingID);
            PreAuthResult PVC = new PreAuthResult();
            if (CV.Status == "C")
            {
                PVC.ChargingStatus = "Success";
            }
            else
            {
                bool ChargeYN = false;
                if (TotalCost == 0)
                {
                    if (!string.IsNullOrEmpty(CV.TotalCost.ToString()))
                    {
                        TotalCost = CV.TotalCost;
                        if (TotalCost > 0)
                        {
                            ChargeYN = true;
                        }
                    }
                    else
                    {
                        ChargeYN = true;
                    }
                }
                else
                {
                    ChargeYN = true;
                }

                string Provider;

                if (CV.CCType == 2)
                {
                    Provider = "AmexBooking";
                }
                else
                {
                    Provider = "MasterVisaBooking";
                }

                if (ChargeYN && CV.BatchStatus == "")
                {
                    if (CV.CCType == 2)
                    {
                        PVC = RegistrationApprovalProcessAmex(CV, BookingID, TotalCost);
                    }
                    else
                    {
                        PVC = NewRegistrationChargingProcessMasterVisa(CV, BookingID, TotalCost);
                        db.SaveMVLog(BookingID.ToString(), PVC.transactionNo, PVC.ErrorCode, PVC.errorMessage, "MasterVisaBooking");
                    }
                }
                else
                {
                    if (CV.BatchStatus != "")
                    {
                        PVC.ChargingStatus = "Batch Submitted and will take around 1 day to confirm Charging.";
                        db.SaveMVLog(BookingID.ToString(), "", "", PVC.ChargingStatus, Provider);
                    }
                    else
                    {
                        PVC.ChargingStatus = "Failure - " + PVC.errorMessage;
                        db.SaveMVLog(BookingID.ToString(), "", "", PVC.errorMessage, Provider);
                    }
                }
            }
            return PVC;
        }

        public PreAuthResult NewRegistrationChargingProcessMasterVisa(ClosingVariables CV, int BookingID, double TotalCost)
        {
            PreAuthResult PVC = new PreAuthResult();
            //PVC = RegistrationMVPreauth(CV, BookingID, true, TotalCost); //Preauth & Charging

            PVC = RegistrationDirectMVCharging(CV, BookingID); //Direct Charging

            return PVC;
        }

        public PreAuthResult RegistrationDirectMVCharging(ClosingVariables CV, int BookingID)
        {
            PreAuthResult PVC = new PreAuthResult();

            //LiveCorporateRegistrationCharging.ServiceSoapClient ObjCharging = new LiveCorporateRegistrationCharging.ServiceSoapClient();
            //LiveCorporateRegistrationCharging.AuthenticateService Auth = new LiveCorporateRegistrationCharging.AuthenticateService();
            string chargingCode = PaymateCorporateChargingStatusCheck(BookingID, true);
            if (chargingCode == "000")
            {
                PVC.ErrorCode = "000";
                PVC.errorMessage = "Success";
                PVC.transactionNo = "";
                PVC.InvoiceNo = "";
                PVC.Amount = 0;
                PVC.TravellerId = "";
                PVC.ChargingStatus = "Success";
            }
            else if (chargingCode == "001")
            {
                PVC.ErrorCode = "001";
                PVC.errorMessage = "Failure";
                PVC.transactionNo = "";
                PVC.InvoiceNo = "";
                PVC.Amount = 0;
                PVC.TravellerId = "";
                PVC.ChargingStatus = "Failure";
            }
            else
            {

                PaymateRegistrationWCF.PMServiceClient ObjCharging = new PaymateRegistrationWCF.PMServiceClient();
                PaymateRegistrationWCF.AuthenticateService Auth = new PaymateRegistrationWCF.AuthenticateService();

                Auth.AuthUserName = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthUserName"); //"67592538";
                Auth.AuthPassword = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthPassword"); //"5BF24F3AA4CA";
                Auth.AuthCode = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthCode");  //"2511C6A40B4A";

                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                //Auth.AuthUserName = GuestRegistrationErrorDesc.GetConfigurationValue("TestAuthUserName"); // "23630FF1";
                //Auth.AuthPassword = GuestRegistrationErrorDesc.GetConfigurationValue("TestAuthPassword");  //"06904A018FCC";
                //Auth.AuthCode = GuestRegistrationErrorDesc.GetConfigurationValue("TestAuthCode");  //"7122C67EB9A6";

                //booking code below
                string AccessCode = GuestRegistrationErrorDesc.GetConfigurationValue("DirectChargingAccessCode");  //"D33E5904-FFD5-41AB-9BB2-040288DC4CFD";

                ErrorLog.LoginfoToLogFile("start direct capture at Paymate MVDirectCapture", "RegistrationDirectMVCharging"); //log file
                //string SysRegCode = "";

                //SysRegCode = GuestRegistrationErrorDesc.GetConfigurationValue("LiveSysRegCode");  //"tQGcJM"; //Live
                //SysRegCode = GuestRegistrationErrorDesc.GetConfigurationValue("TestSysRegCode");  //"tVYfXG"; //Test

                string ChargingStatus = ObjCharging.MVDirectCapture(Auth, AccessCode, CV.ClientCoIndivID.ToString(), CV.PaymateSysRegCode, CV.EmailID
                    , "rahul.jain@carzonrent.com", BookingID.ToString(), CV.TotalCost.ToString(), BookingID.ToString()
                    , CV.DateOut.ToString("dd/MM/yyyy"), CV.ServiceType, CV.CityName.ToUpper(), CV.CityName.ToUpper(), "");


                ErrorLog.LoginfoToLogFile("After Capture at Paymate MVDirectCapture" + ":" + ChargingStatus, "RegistrationDirectMVCharging"); //log file 

                string[] strChargingStatus = SplitFunction(ChargingStatus);



                PVC.ErrorCode = strChargingStatus[0].ToString();
                PVC.errorMessage = strChargingStatus[1].ToString();
                if (PVC.ErrorCode == "000")
                {
                    PVC.transactionNo = strChargingStatus[2].ToString();
                    PVC.InvoiceNo = strChargingStatus[3].ToString();
                    PVC.Amount = Convert.ToDouble(strChargingStatus[4]);
                    PVC.TravellerId = strChargingStatus[5].ToString();
                    PVC.ChargingStatus = "Success";
                }
                else
                {
                    PVC.transactionNo = "";
                    PVC.InvoiceNo = "";
                    PVC.Amount = 0;
                    PVC.TravellerId = "";
                    PVC.ChargingStatus = "Failure, " + PVC.errorMessage;
                }

                ErrorLog.LoginfoToLogFile("UpdateCorporateMVProcess" + ":" + BookingID + "," + PVC.ErrorCode
                           + "," + PVC.errorMessage + "," + PVC.transactionNo + "," + PVC.InvoiceNo + "," + PVC.Amount
                           + "," + PVC.TravellerId, "RegistrationDirectMVCharging"); //log file 

                UpdateCorporateMVProcess(BookingID, PVC.ErrorCode, PVC.errorMessage, PVC.transactionNo
                    , PVC.InvoiceNo, PVC.Amount, PVC.TravellerId, "", 0);
            }
            return PVC;
        }


        public NewChargingResponseParam TransactionStatusCheck(int BookingID, string PreauthTransactionID, string TransactionMode)
        {

            string Responsestring = "", PartnerName = "";  //TravellerID to fill

            string AuthUserName = "", AuthPassword = "", AuthCode = "", AccessCode = "", url = "";
            AuthUserName = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessAuthUserName");
            AuthPassword = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessAuthPassword");
            AuthCode = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessAuthCode");
            AccessCode = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewTrasancationStateusAccessCode");
            url = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewPreauthVerifyTransactionURL");
            PartnerName = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessPartName");

            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;


            Dictionary<string, string> objFinalRequest = new Dictionary<string, string>();
            //STEP 1: Generate AuthData
            Dictionary<string, string> objAuthData = new Dictionary<string, string>();
            objAuthData.Add("AuthUserName", AuthUserName);
            objAuthData.Add("AuthPassword", AuthPassword);
            objAuthData.Add("AuthCode", AuthCode);
            objFinalRequest.Add("AuthData", JsonConvert.SerializeObject(objAuthData));

            //STEP 2: Generate RequestData
            Dictionary<string, string> objRequestData = new Dictionary<string, string>();
            objRequestData.Add("AccessCode", AccessCode); //"6CC8EABD-7A34-49C2-98C5-09D6A824C0F5");
            objRequestData.Add("TransactionRefNo", PreauthTransactionID);
            objRequestData.Add("InvoiceNo", BookingID.ToString());
            objRequestData.Add("TransactionMode", TransactionMode);
            objFinalRequest.Add("RequestData", JsonConvert.SerializeObject(objRequestData));


            ​//STEP 3: Generate FinalRequest

            string FinalRequest = JsonConvert.SerializeObject(objFinalRequest);
            //Sample FinalRequest: {"AuthData":"{\"AuthUserName\":\"4CADC2123E41\",\"AuthPassword\":\"BB8736C7C977\",\"AuthCode\":\"759B79DDF0E1\"}","RequestData":"{\"AccessCode\":\"6CC8EABD-7A34-49C2-98C5-09D6A824C0F5\",\"TravellerId\":\"2022920174652247\"}"}

            ErrorLog.LoginfoToLogFile("FinalRequest=" + FinalRequest, "RegisterStatusCheck");
            NewChargingResponseParam response = new NewChargingResponseParam();

            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = FinalRequest;
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                string objResponse = "";
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    objResponse = result.ToString();
                }

                string FinalResponse = JsonConvert.DeserializeObject<string>(objResponse);

                ErrorLog.LoginfoToLogFile("FinalResponse=" + FinalResponse, "RegisterStatusCheck"); //logs in text file

                response = JsonConvert.DeserializeObject<NewChargingResponseParam>(FinalResponse);

                Responsestring = FinalResponse.ToString();
            }
            catch (Exception ex)
            {
                //returnstring = ex.Message.ToString();
                response.ResponseMessage = ex.ToString();
            }

            return response;
        }

        public NewChargingThroughPreauthResponseParam Charging_MVThroughPreauth(int BookingID, string PreauthTransactionID, string PreauthAmount
            , string ChargingAmount)
        {

            string Responsestring = "", TravellerID = "", PartnerName = "";  //TravellerID to fill

            string AuthUserName = "", AuthPassword = "", AuthCode = "", AccessCode = "", url = "";
            AuthUserName = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessAuthUserName");
            AuthPassword = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessAuthPassword");
            AuthCode = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessAuthCode");
            AccessCode = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewPreauthChargingAccessCode");
            url = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewPreauthChargingURL");
            PartnerName = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessPartName");

            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;


            Dictionary<string, string> objFinalRequest = new Dictionary<string, string>();
            //STEP 1: Generate AuthData
            Dictionary<string, string> objAuthData = new Dictionary<string, string>();
            objAuthData.Add("AuthUserName", AuthUserName);
            objAuthData.Add("AuthPassword", AuthPassword);
            objAuthData.Add("AuthCode", AuthCode);
            objFinalRequest.Add("AuthData", JsonConvert.SerializeObject(objAuthData));
            //Sample AuthData:{"AuthUserName":"A6343F7F","AuthPassword":"CF5241FDB903","AuthCode":"6C41BA771BCF"} 

            //STEP 2: Generate RequestData
            Dictionary<string, string> objRequestData = new Dictionary<string, string>();
            objRequestData.Add("AccessCode", AccessCode); //"6CC8EABD-7A34-49C2-98C5-09D6A824C0F5");
            objRequestData.Add("PayMateAuthTransactionRefNo", PreauthTransactionID);
            objRequestData.Add("InvoiceNo", BookingID.ToString());
            objRequestData.Add("PreAuthAmount", PreauthAmount.ToString());
            objRequestData.Add("CaptureAmount", ChargingAmount.ToString());
            //objRequestData.Add("TravellerId", TravellerID);
            //objRequestData.Add("PartnerName", PartnerName);
            objFinalRequest.Add("RequestData", JsonConvert.SerializeObject(objRequestData));
            //= {"AccessCode":"64FC058D-2E52-4830-B7ABD01FF3939F48","TravellerId":"TRV100622090654","InvoiceNo":"INV140620222006","Amount":"5","PartnerName":"NA"}


            ​//STEP 3: Generate FinalRequest

            string FinalRequest = JsonConvert.SerializeObject(objFinalRequest);
            //Sample FinalRequest: {"AuthData":"{\"AuthUserName\":\"4CADC2123E41\",\"AuthPassword\":\"BB8736C7C977\",\"AuthCode\":\"759B79DDF0E1\"}","RequestData":"{\"AccessCode\":\"6CC8EABD-7A34-49C2-98C5-09D6A824C0F5\",\"TravellerId\":\"2022920174652247\"}"}

            ErrorLog.LoginfoToLogFile("FinalRequest=" + FinalRequest, "RegisterStatusCheck");

            NewChargingThroughPreauthResponseParam response = new NewChargingThroughPreauthResponseParam();

            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = FinalRequest;
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                string objResponse = "";
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    objResponse = result.ToString();
                }

                string FinalResponse = JsonConvert.DeserializeObject<string>(objResponse);

                ErrorLog.LoginfoToLogFile("FinalResponse=" + FinalResponse, "RegisterStatusCheck"); //logs in text file

                response = JsonConvert.DeserializeObject<NewChargingThroughPreauthResponseParam>(FinalResponse);

                Responsestring = FinalResponse.ToString();
            }
            catch (Exception ex)
            {
                //returnstring = ex.Message.ToString();
                response.ResponseMessage = ex.ToString();
            }

            return response;
        }

        public string RegisterStatusCheck(string TravellerID)
        {
            string Responsestring = "";

            string AuthUserName = "", AuthPassword = "", AuthCode = "", AccessCode = "", url = "";
            AuthUserName = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessAuthUserName");
            AuthPassword = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessAuthPassword");
            AuthCode = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessAuthCode");
            AccessCode = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessAccessCode");
            url = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewRegistrationVerifyRegistration");

            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;


            Dictionary<string, string> objFinalRequest = new Dictionary<string, string>();
            //STEP 1: Generate AuthData
            Dictionary<string, string> objAuthData = new Dictionary<string, string>();
            objAuthData.Add("AuthUserName", AuthUserName);
            objAuthData.Add("AuthPassword", AuthPassword);
            objAuthData.Add("AuthCode", AuthCode);
            objFinalRequest.Add("AuthData", JsonConvert.SerializeObject(objAuthData));


            //STEP 2: Generate RequestData
            Dictionary<string, string> objRequestData = new Dictionary<string, string>();
            objRequestData.Add("AccessCode", AccessCode); //"6CC8EABD-7A34-49C2-98C5-09D6A824C0F5");
            objRequestData.Add("TravellerId", TravellerID);
            objFinalRequest.Add("RequestData", JsonConvert.SerializeObject(objRequestData));

            ​//STEP 3: Generate FinalRequest

            string FinalRequest = JsonConvert.SerializeObject(objFinalRequest);

            ErrorLog.LoginfoToLogFile("FinalRequest=" + FinalRequest, "RegisterStatusCheck");


            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = FinalRequest;
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                string objResponse = "";
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    objResponse = result.ToString();
                }

                CheckNewTravellerResponseParam responseparam = new CheckNewTravellerResponseParam();
                string FinalResponse = JsonConvert.DeserializeObject<string>(objResponse);

                ErrorLog.LoginfoToLogFile("FinalResponse=" + FinalResponse, "RegisterStatusCheck"); //logs in text file

                responseparam = JsonConvert.DeserializeObject<CheckNewTravellerResponseParam>(FinalResponse);

                Responsestring = FinalResponse.ToString();
                //update code
                clsAdmin objadmin = new clsAdmin();
                objadmin.UpdateSavedCard(responseparam.ResponseCode, responseparam.ResponseMessage, TravellerID, responseparam.CardNo
                    , responseparam.CardBrand, "");
            }
            catch (Exception ex)
            {
                Responsestring = ex.ToString();
                ErrorLog.LoginfoToLogFile("response=" + ex.ToString(), "RegisterStatusCheck");
            }

            return Responsestring;
        }

        public NewChargingResponseParam Charging_NewCCProcess(int BookingID, string AuthTransactionID, double CaptureAmount)
        {
            NewChargingResponseParam response = new NewChargingResponseParam();
            DataSet data = new DataSet();
            data = GetBookingDetails_Capture(BookingID);

            if (data.Tables[0].Rows.Count > 0)
            {
                PreAuthResult PVC = new PreAuthResult();
                bool CheckChargingYN = true;
                if (string.IsNullOrEmpty(Convert.ToString(data.Tables[0].Rows[0]["ChargingTransactionID"])))
                {
                    CheckChargingYN = false;
                }
                //string chargingCode = "";
                NewChargingResponseParam response1 = new NewChargingResponseParam();


                if (CheckChargingYN)
                {
                    response1 = TransactionStatusCheck(BookingID, Convert.ToString(data.Tables[0].Rows[0]["ChargingTransactionID"]), "C");
                }

                if (response1.TransactionStatus == "SUCCESSFUL" && response1.ResponseCode == "E042") //check status from Paymate
                {
                    //sucessfull
                    UpdateChargingStatus_NewCCProcess(BookingID, response.TransactionRefNo, response.ResponseCode
                       , response.ResponseMessage, response1.TransactionStatus, response1.Amount);
                }
                else
                {
                    //*************************************
                    NewChargingThroughPreauthResponseParam response11 = new NewChargingThroughPreauthResponseParam();
                    response11 = Charging_MVThroughPreauth(BookingID, AuthTransactionID
                        , Convert.ToString(data.Tables[0].Rows[0]["IndicatedPrice"]), Convert.ToString(CaptureAmount));

                    //if (response11.TransactionStatus == "SUCCESSFUL" && response11.ResponseCode == "E042") //check status from Paymate
                    //{
                    UpdateChargingStatus_NewCCProcess(BookingID, response11.TransactionRefNo, response11.ResponseCode
                        , response11.ResponseMessage, response11.TransactionStatus, response11.CaptureAmount);
                    //}
                }
            }
            else
            {
                response.ResponseMessage = "No Data Found.";
            }
            return response;
        }

        public DataSet UpdateChargingStatus_NewCCProcess(int Bookingid, string TransactionID, string ResponseCode
            , string ResponseMessage, string TransactionStatus, string CaptureAmount)
        {
            SqlParameter[] param = new SqlParameter[6];

            param[0] = new SqlParameter("@BookingId", Bookingid);
            param[1] = new SqlParameter("@TransactionID", TransactionID);
            param[2] = new SqlParameter("@ResponseCode", ResponseCode);
            param[3] = new SqlParameter("@ResponseMessage", ResponseMessage);
            param[4] = new SqlParameter("@TransactionStatus", TransactionStatus);
            param[5] = new SqlParameter("@CaptureAmount", Convert.ToDouble(CaptureAmount));

            return SqlHelper.ExecuteDataset("Prc_UpdatePreauthCapture", param);
        }

        public string UpdateInvoiceStatus(string ResponseCode, string ResponseMessage, string TransactionId, string TransactionStatus, string TravellerId
            , string TransactionMode, string InvoiceNo, string CardNo, string CardBrand, string PartnerName, string Amount)
        {
            SqlParameter[] param = new SqlParameter[11];

            param[0] = new SqlParameter("@ResponseCode", ResponseCode);
            param[1] = new SqlParameter("@ResponseMessage", ResponseMessage);
            param[2] = new SqlParameter("@TransactionId", TransactionId);
            param[3] = new SqlParameter("@TransactionStatus", TransactionStatus);
            param[4] = new SqlParameter("@TravellerId", TravellerId);
            param[5] = new SqlParameter("@TransactionMode", TransactionMode);
            param[6] = new SqlParameter("@InvoiceNo", InvoiceNo);
            param[7] = new SqlParameter("@Card", CardNo);
            param[8] = new SqlParameter("@CardType", CardBrand);
            param[9] = new SqlParameter("@PartnerName", PartnerName);
            param[10] = new SqlParameter("@Amount", Convert.ToDouble(Amount));
            object obj = SqlHelper.ExecuteScalar("Prc_MVChargingProc", param);
            return Convert.ToString(obj);
        }


        //public string MVPreauth_ClassicAspIntegration(string ApprovalNo, string AgentEmailId, string InvoiceNo,
        //    double Amount, DateTime RentalDate, string RentalType, string PickupCity, string CustomerName)
        //{
        //    string url = "https://uat.paymate.co.in/PMCorporate/ProcessAuth.aspx";
        //    //DB db = new DB();

        //    string EncryptedParameter = "";
        //    string SysRegCode = "";

        //    SysRegCode = "CmYuEy"; //db.GetPaymateSysRegCodeForIndiv(ClientIndivID);
        //    string AuthUserName = "", AuthPassword = "", AuthCode = "";

        //    //Live
        //    // AuthUserName = "23630FF1";
        //    // AuthPassword = "06904A018FCC";
        //    // AuthCode = "7122C67EB9A6";

        //    //UAT
        //    AuthUserName = "67592538";
        //    AuthPassword = "5BF24F3AA4CA";
        //    AuthCode = "2511C6A40B4A";

        //    //AuthUserName=A1CBDF44&AuthPassword=50964C3DADA1&AuthCode=EF94F8D552B7&SysRegCode=TeDTVg&AgentEmailId=CZRAgt1@czr.com&InvoiceNo=INV00210&Amount=60&RentalDate=28/0
        //    //6/2020&RentalType=SELF&PickupCity=MUMBAI&ReturnCity=MUMBAI&CustomerName=Ajit Adhikari&CardTypes=Master,Visa&PartnerName=NA

        //    string CardTypes, PartnerName;
        //    CardTypes = "Master,Visa";
        //    PartnerName = "NA";

        //    EncryptedParameter = URLEncrypt("AuthUserName=" + AuthUserName + "&" + "AuthPassword=" + AuthPassword + "&" + "AuthCode=" + AuthCode + "&"
        //        + "SysRegCode=" + SysRegCode + "&" + "AgentEmailId=" + AgentEmailId + "&" + "InvoiceNo=" + ApprovalNo + "&" + "Amount=" + Amount
        //        + "&" + "RentalDate=" + RentalDate + "&" + "RentalType=" + RentalType + "&" + "PickupCity=" + PickupCity + "&" + "ReturnCity=" + PickupCity
        //        + "&" + "CustomerName=" + CustomerName + "&" + "CardTypes=" + CardTypes + "&" + "PartnerName=" + PartnerName);

        //    url = url + "?" + EncryptedParameter;
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
        //    sb.Append("<html><head></head><body>");
        //    sb.Append("<form name='form1' method='POST' action='" + url + "'>");
        //    sb.Append("</form>");
        //    sb.Append("<script language='javascript'>document.form1.submit();</script>");
        //    sb.Append("</body></html>");
        //    return sb.ToString();

        //    //Response.Write(sb.ToString());
        //}

        public string Updatesmsstatus(string Bookingid, string SMSStatus)
        {
            DB db = new DB();
            return db.Updatesmsstatus(Bookingid, SMSStatus);
        }

        public Preauth GetPreauthByBookingID(string OrderID)
        {
            DB db = new DB();
            Preauth pp = new Preauth();
            DataSet ds = new DataSet();
            ds = db.GetPreauthByBookingID(OrderID);
            if (ds.Tables[0].Rows.Count > 0)
            {
                pp.OrderID = ds.Tables[0].Rows[0]["OrderID"].ToString();
                pp.PageID = ds.Tables[0].Rows[0]["PageID"].ToString();
                pp.SendSMSLater = ds.Tables[0].Rows[0]["SendSMSLater"].ToString();
                pp.ClientCoID = ds.Tables[0].Rows[0]["ClientCoID"].ToString();
                pp.SMSMobiles = ds.Tables[0].Rows[0]["SMSMobiles"].ToString();
                pp.SMSMobiles1 = ds.Tables[0].Rows[0]["SMSMobiles1"].ToString();
                pp.bitIsVIP = ds.Tables[0].Rows[0]["VIP"].ToString();
                pp.hdnAcceleration_No = ds.Tables[0].Rows[0]["hdnAcceleration_No"].ToString();
                pp.SendMailLater = ds.Tables[0].Rows[0]["SendMailLater"].ToString();
                pp.LimoStatus = ds.Tables[0].Rows[0]["LimoStatus"].ToString();
            }
            return pp;
        }

        public void BeforePreAuth(string OrderID, string PageID, string OldBookingID, string SendSMSLater
             , string ClientCoID, string SMSMobiles, string SMSMobiles1
             , string bitIsVIP, string hdnAcceleration_No, string SendMailLater, string LimoStatus, string IDMain, string FYear)
        {
            DB db = new DB();

            db.BeforePreAuth(OrderID, PageID, OldBookingID, SendSMSLater, ClientCoID, SMSMobiles, SMSMobiles1, bitIsVIP
                , hdnAcceleration_No, SendMailLater, LimoStatus, IDMain, FYear);
        }

        public void UpdatePreauthStatus(string ResponseCode, string ResponseMessage, string TransactionId, string Card, string CardType
            , string InvoiceNo, string Amount)
        {
            DB db = new DB();

            db.UpdatePreauthStatus(ResponseCode, ResponseMessage, TransactionId, Card, CardType, InvoiceNo, Amount);

        }

        public void UpdatePreauthStatusNew(string ResponseCode, string ResponseMessage, string TransactionId, string CardNo, string CardBrand
            , string InvoiceNo, string Amount)
        {
            DB db = new DB();

            db.UpdatePreauthStatusNew(ResponseCode, ResponseMessage, TransactionId, CardNo, CardBrand, InvoiceNo, Amount);

        }

        public void UpdateSavedCard(string ResponseCode, string ResponseMessage, string TravellerId, string CardNo, string CardBrand, string Action)
        {
            DB db = new DB();

            db.UpdateSavedCard(ResponseCode, ResponseMessage, TravellerId, CardNo, CardBrand, Action);

        }

        public Preauth UpdateBookingPreauthStatus(string ResponseCode, string ResponseMessage, string TransactionId, string Card, string CardType
            , string InvoiceNo, string Amount, string LimoStatus) //, string PreAuthStatus)
        {
            DB db = new DB();

            Preauth pp = new Preauth();
            DataSet ds = new DataSet();

            pp = GetPreauthByBookingID(InvoiceNo);

            ds = db.UpdateBookingPreauthStatus(ResponseCode, ResponseMessage, TransactionId, Card, InvoiceNo, Amount, LimoStatus);

            if (ds.Tables[0].Rows.Count > 0)
            {
                pp.BookingID = ds.Tables[0].Rows[0]["BookingID"].ToString();
                pp.UnitName = ds.Tables[0].Rows[0]["UnitName"].ToString();
                pp.CityName = ds.Tables[0].Rows[0]["CityName"].ToString();
            }
            return pp;
        }

        public bool IsBookingDispatched(string BookingID)
        {
            DB db = new DB();
            return db.IsBookingDispatched(BookingID);
        }

        public PreAuthResult RegistrationMVPreauth(ClosingVariables CV, int BookingID, Boolean ChargingYN, double TotalCost)
        {
            //LiveCorporateRegistrationCharging.ServiceSoapClient ObjCharging = new LiveCorporateRegistrationCharging.ServiceSoapClient();
            //LiveCorporateRegistrationCharging.AuthenticateService Auth = new LiveCorporateRegistrationCharging.AuthenticateService();

            PaymateRegistrationWCF.PMServiceClient ObjCharging = new PaymateRegistrationWCF.PMServiceClient();
            PaymateRegistrationWCF.AuthenticateService Auth = new PaymateRegistrationWCF.AuthenticateService();

            Auth.AuthUserName = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthUserName"); //"67592538";
            Auth.AuthPassword = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthPassword"); //"5BF24F3AA4CA";
            Auth.AuthCode = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthCode");  //"2511C6A40B4A";

            //Auth.AuthUserName = GuestRegistrationErrorDesc.GetConfigurationValue("TestAuthUserName");  //"23630FF1";
            //Auth.AuthPassword = GuestRegistrationErrorDesc.GetConfigurationValue("TestAuthPassword");  //"06904A018FCC";
            //Auth.AuthCode = GuestRegistrationErrorDesc.GetConfigurationValue("TestAuthCode");  //"7122C67EB9A6";

            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

            //booking code below
            string AccessCode = GuestRegistrationErrorDesc.GetConfigurationValue("MVPreauthAccessCode");  //"64FC058D-2E52-4830-B7AB-D01FF3939F48";
            PreAuthResult PVC = new PreAuthResult();
            PVC.ChargingStatus = "Failure";
            if (CV.PreAuthYN == false) //if no Preauth done
            {
                ErrorLog.LoginfoToLogFile("start preauth at Paymate MVPreAuth", "RegistrationMVPreauth"); //log file
                //string SysRegCode = "";
                //SysRegCode = GuestRegistrationErrorDesc.GetConfigurationValue("LiveSysRegCode");  //"tQGcJM"; //Live
                //SysRegCode = GuestRegistrationErrorDesc.GetConfigurationValue("TestSysRegCode");  //"tVYfXG"; //Test

                string Preauthstatus = ObjCharging.MVPreAuth(Auth, AccessCode, CV.ClientCoIndivID.ToString(), CV.PaymateSysRegCode, CV.EmailID
                    , "rahul.jain@carzonrent.com", BookingID.ToString(), TotalCost.ToString(), BookingID.ToString()
                    , CV.DateOut.ToString("dd/MM/yyyy"), CV.ServiceType, CV.CityName.ToUpper(), CV.CityName.ToUpper(), "");


                ErrorLog.LoginfoToLogFile("After preauth at Paymate MVPreAuth" + ":" + Preauthstatus, "RegistrationMVPreauth"); //log file 

                string[] strPreauthStatus = SplitFunction(Preauthstatus);

                PVC.ErrorCode = strPreauthStatus[0].ToString();
                PVC.errorMessage = strPreauthStatus[1].ToString();

                PVC.transactionNo = "";
                PVC.InvoiceNo = "";
                PVC.Amount = 0;
                PVC.TravellerId = "";

                if (PVC.ErrorCode == "000")
                {
                    PVC.ChargingStatus = "Success";
                    ErrorLog.LoginfoToLogFile(BookingID + "," + PVC.ErrorCode
                       + "," + PVC.errorMessage + "," + PVC.transactionNo + "," + PVC.Amount.ToString(), "RegistrationMVPreauth"); //log file 

                    UpdateCorporateMVProcess(BookingID, PVC.ErrorCode, PVC.errorMessage, PVC.transactionNo
                       , PVC.InvoiceNo, PVC.Amount, PVC.TravellerId, "", 0);

                    PVC.transactionNo = strPreauthStatus[2].ToString();
                    PVC.InvoiceNo = strPreauthStatus[3].ToString();
                    PVC.Amount = Convert.ToDouble(strPreauthStatus[4]);
                    PVC.TravellerId = strPreauthStatus[5].ToString();
                    if (!ChargingYN)
                    {
                        PVC.Response = "Success";
                    }
                }
                else
                {
                    PVC.Response = "Failure";
                }
            } ////end if no Preauth done
            else
            {
                PVC.ErrorCode = "000";
            }

            if (PVC.ErrorCode == "000")
            {
                if (ChargingYN)
                {
                    PVC = RegistrationMVPreauthCharging(CV, BookingID, ObjCharging, Auth, PVC);
                }
            }

            return PVC;
        }

        public PreAuthResult RegistrationMVPreauthCharging(ClosingVariables CV, int BookingID
            //, LiveCorporateRegistrationCharging.ServiceSoapClient ObjCharging
            , PaymateRegistrationWCF.PMServiceClient ObjCharging
            //, LiveCorporateRegistrationCharging.AuthenticateService Auth, PreAuthResult PVC
            , PaymateRegistrationWCF.AuthenticateService Auth, PreAuthResult PVC
            )
        {
            //booking code below
            string AccessCode = GuestRegistrationErrorDesc.GetConfigurationValue("MVPreauthChargingAccessCode");  //"2BD11CF0-79A2-4DA1-A3BE-C4DD632301C4";

            ErrorLog.LoginfoToLogFile("start Capture at Paymate MVAuthCapture", "RegistrationMVPreauthCharging"); //log file

            //PreauthValidation PVC = new PreauthValidation();
            string Preauthstatus = ObjCharging.MVAuthCapture(Auth, AccessCode, PVC.transactionNo, BookingID.ToString(), CV.TotalCost.ToString()
                , CV.TotalCost.ToString());

            ErrorLog.LoginfoToLogFile("After Capture at Paymate MVAuthCapture" + ":" + Preauthstatus, "RegistrationMVPreauthCharging"); //log file 

            string[] strPreauthStatus = SplitFunction(Preauthstatus);
            PVC.ErrorCode = strPreauthStatus[0].ToString();
            PVC.errorMessage = strPreauthStatus[1].ToString();
            if (PVC.ErrorCode == "000")
            {
                PVC.ChargingStatus = "Success";
                PVC.InvoiceNo = strPreauthStatus[2].ToString();
                PVC.transactionNo = strPreauthStatus[3].ToString();
                PVC.Amount = Convert.ToDouble(strPreauthStatus[4]);
                PVC.CaptureTransactionID = strPreauthStatus[5].ToString();
                PVC.CaptureAmount = Convert.ToDouble(strPreauthStatus[6]);
                PVC.Response = "Success";
            }
            else
            {
                PVC.Response = "Failure";
                PVC.ChargingStatus = "Failure";
                PVC.transactionNo = "";
                PVC.InvoiceNo = "";
                PVC.Amount = 0;
                PVC.TravellerId = "";
            }

            ErrorLog.LoginfoToLogFile("UpdateCorporateMVProcess" + ":" + BookingID + "," + PVC.ErrorCode
                       + "," + PVC.errorMessage + "," + PVC.transactionNo + "," + PVC.Amount.ToString() + "," + PVC.CaptureTransactionID
                       + "," + PVC.CaptureAmount.ToString(), "RegistrationMVPreauthCharging"); //log file 

            UpdateCorporateMVProcess(BookingID, PVC.ErrorCode, PVC.errorMessage, PVC.transactionNo
                , PVC.InvoiceNo, PVC.Amount, PVC.TravellerId, PVC.CaptureTransactionID, PVC.CaptureAmount);

            return PVC;
        }

        public PreAuthResult RegistrationApprovalProcessAmex(ClosingVariables CV, int BookingID, double TotalCost)
        {
            DB db = new DB();
            //LiveCorporateRegistrationCharging.ServiceSoapClient ObjCharging = new LiveCorporateRegistrationCharging.ServiceSoapClient();
            //LiveCorporateRegistrationCharging.AuthenticateService Auth = new LiveCorporateRegistrationCharging.AuthenticateService();

            PaymateRegistrationWCF.PMServiceClient ObjCharging = new PaymateRegistrationWCF.PMServiceClient();
            PaymateRegistrationWCF.AuthenticateService Auth = new PaymateRegistrationWCF.AuthenticateService();

            Auth.AuthUserName = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthUserName"); //"67592538";
            Auth.AuthPassword = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthPassword"); //"5BF24F3AA4CA";
            Auth.AuthCode = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthCode");  //"2511C6A40B4A";

            //Auth.AuthUserName = GuestRegistrationErrorDesc.GetConfigurationValue("TestAuthUserName"); //"23630FF1";
            //Auth.AuthPassword = GuestRegistrationErrorDesc.GetConfigurationValue("TestAuthPassword"); //"06904A018FCC";
            //Auth.AuthCode = GuestRegistrationErrorDesc.GetConfigurationValue("TestAuthCode");  //"7122C67EB9A6";

            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

            //booking code below
            string AccessCode = GuestRegistrationErrorDesc.GetConfigurationValue("AmexBookingCreateAccessCode");  //"E5F0914E-71D9-48F5-A4AF-9C68CF09F359";

            ErrorLog.LoginfoToLogFile("start Create Booking at Paymate ABASInitCapture", "RegistrationApprovalProcessAmex"); //log file
            //string SysRegCode = "";
            //SysRegCode = GuestRegistrationErrorDesc.GetConfigurationValue("LiveSysRegCode");  //"tQGcJM"; //Live
            //SysRegCode = GuestRegistrationErrorDesc.GetConfigurationValue("TestSysRegCode");  //"tVYfXG"; //Test

            string chargingStatus = PaymateCorporateChargingStatusCheck(BookingID, true);

            PreAuthResult PVC = new PreAuthResult();

            if (chargingStatus == "059" || chargingStatus == "062")
            {
                PVC.Response = "The transaction is under process, and the status will be confirmed after 1 day";
                PVC.ChargingStatus = "The transaction is under process, and the status will be confirmed after 1 day";
                CV.ErrorCode = "The transaction is under process, and the status will be confirmed after 1 day";
            }
            else if (chargingStatus.Trim() != "000")
            {
                //booking creation on paymate side below
                string BookingStatus = ObjCharging.ABASInitCapture(Auth, AccessCode, CV.ClientCoIndivID.ToString(), CV.PaymateSysRegCode, CV.EmailID
                    , "rahul.jain@carzonrent.com", BookingID.ToString(), TotalCost.ToString(), BookingID.ToString()
                    , CV.DateOut.ToString("dd/MM/yyyy"), CV.ServiceType, CV.CityName.ToUpper(), CV.CityName.ToUpper());

                ErrorLog.LoginfoToLogFile("After Booking at Paymate ABASInitCapture" + ":" + BookingStatus, "RegistrationApprovalProcessAmex"); //log file 

                string[] OrderIDstatus = SplitFunction(BookingStatus);

                PVC.ChargingStatus = "Failure";
                //string errorCode, transactionNr, strMerchantOrderID, numAmount, strTransactionMode;
                PVC.ErrorCode = OrderIDstatus[0].ToString();
                PVC.errorMessage = OrderIDstatus[1].ToString();
                //In case of Sucess <ERRORCODE>|<ERRORMESSAGE>|<PayMateCaptureTransactionID>|<InvoiceNo>|<Amount>|<TravellerId>or<EmpEmailId>
                //059|Transaction booked Successfuly.|160802134556893|7449674|1882|2854848
                //In Case of failure <ERRORCODE>|<ERRORMESSAGE>

                if (PVC.ErrorCode == "059" || PVC.ErrorCode == "062")
                {

                    PVC.ChargingStatus = "Success";
                    PVC.Response = "Success";
                    if (PVC.ErrorCode == "059")
                    {
                        PVC.transactionNo = OrderIDstatus[2].ToString();
                        PVC.InvoiceNo = OrderIDstatus[3].ToString();
                        PVC.Amount = Convert.ToDouble(OrderIDstatus[4]);
                        PVC.TravellerId = OrderIDstatus[5].ToString();
                    }
                    else
                    {
                        //PVC.transactionNo = "160802134556893";  //OrderIDstatus[2].ToString();
                        //PVC.InvoiceNo = "7449674";
                        //PVC.Amount = 1882;
                        //PVC.TravellerId = "2854848";

                        PVC.transactionNo = "";
                        PVC.InvoiceNo = "";
                        PVC.Amount = 0;
                        PVC.TravellerId = "";
                        PVC.ChargingStatus = "";
                    }


                    AccessCode = GuestRegistrationErrorDesc.GetConfigurationValue("AmexApprovalAccessCode");  //"2193C7B2-C2DB-4DE0-BCC9-4797CD64E4F1";
                    ErrorLog.LoginfoToLogFile("start Capture at Paymate ABASConfirmBooking : " + PVC.transactionNo, "RegistrationApprovalProcessAmex"); //log file
                    //approval process code below
                    string ChargingStatus = ObjCharging.ABASConfirmBooking(Auth, AccessCode, PVC.transactionNo, BookingID.ToString(), "APPROVE");

                    ErrorLog.LoginfoToLogFile("After Capture at Paymate ABASConfirmBooking" + ":" + ChargingStatus, "RegistrationApprovalProcessAmex"); //log file 

                    string[] strChargingStatus = SplitFunction(ChargingStatus);
                    PVC.ErrorCode = strChargingStatus[0].ToString();
                    PVC.errorMessage = strChargingStatus[1].ToString();
                    if (PVC.ErrorCode == "076" || PVC.ErrorCode == "078")
                    {
                        ErrorLog.LoginfoToLogFile("UpdateCorporateMVProcess" + ":" + BookingID + "," + PVC.ErrorCode
                            + "," + PVC.errorMessage + "," + PVC.transactionNo + "," + PVC.InvoiceNo + "," + PVC.Amount
                            + "," + PVC.TravellerId, "RegistrationApprovalProcessAmex"); //log file 
                        UpdateCorporateMVProcess(BookingID, PVC.ErrorCode, PVC.errorMessage, PVC.transactionNo
                            , PVC.InvoiceNo, PVC.Amount, PVC.TravellerId, "", 0);
                        PVC.ChargingStatus = PVC.ErrorCode;
                        CV.ErrorCode = PVC.ErrorCode;

                        db.SaveMVLog(BookingID.ToString(), PVC.transactionNo, PVC.ErrorCode, PVC.errorMessage, "AmexBooking");
                    }
                    else
                    {
                        PVC.Response = "Failure";
                        PVC.ChargingStatus = "Failure";
                        CV.ErrorCode = "Failure";
                        db.SaveMVLog(BookingID.ToString(), "", PVC.ErrorCode, PVC.errorMessage, "AmexBooking");
                    }
                }
                else
                {
                    PVC.Response = "Failure";
                    PVC.ChargingStatus = "Failure";
                    CV.ErrorCode = "Failure";
                    db.SaveMVLog(BookingID.ToString(), "", PVC.ErrorCode, PVC.errorMessage, "AmexBooking");
                }
            }
            else
            {
                PVC.Response = "Success";
                PVC.ChargingStatus = "Success";
                PVC.ErrorCode = chargingStatus;
                PVC.errorMessage = "";
                db.SaveMVLog(BookingID.ToString(), "", PVC.ErrorCode, PVC.ChargingStatus, "AmexBooking");
            }
            return PVC;
        }

        public DataSet GetBookingSummary(string OrderID)
        {
            DB db = new DB();
            return db.GetBookingSummary(OrderID);
        }

        public DataSet GetGuestDetails(string ClientCoIndivID)
        {
            DB db = new DB();
            return db.GetGuestDetails(ClientCoIndivID);
        }

        public DataSet GetChargingData(int BookingID)
        {
            DB db = new DB();
            return db.GetChargingData(BookingID);
        }

        public string RefundProcess(int BookingID, double RefundAmount, double TotalCost)
        {
            ChargingWebService.ChargingDetails CD = new ChargingWebService.ChargingDetails();
            string status = "", RefundStatus = "";
            ClosingVariables CV = new ClosingVariables();
            PreAuthResult PVC = new PreAuthResult();
            CV = CD.GetClosingDetails_NonCorDrive(BookingID);
            bool InsertYN = false;

            if (CV.NewMVPreauthYN)
            {
                //UATPMCorporate.PMServiceClient MV = new UATPMCorporate.PMServiceClient();
                //UATPMCorporate.AuthenticateService credential = new UATPMCorporate.AuthenticateService();

                PaymateRegistrationWCF.PMServiceClient MV = new PaymateRegistrationWCF.PMServiceClient();
                PaymateRegistrationWCF.AuthenticateService credential = new PaymateRegistrationWCF.AuthenticateService();

                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                //credential.AuthUserName = "67592538";
                //credential.AuthPassword = "5BF24F3AA4CA";
                //credential.AuthCode = "2511C6A40B4A";

                credential.AuthUserName = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthUserName");
                credential.AuthPassword = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthPassword");
                credential.AuthCode = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthCode");

                string TrackOrderID = "";
                string AccessCode = "A0771D0B-6A01-40FF-9347-DEA6CAFDD293";

                ErrorLog.LoginfoToLogFile(CV.CaptureTransactionID + "," + CV.ApprovalNo, "");

                TrackOrderID = MV.GetTransactionStatus(credential, AccessCode, CV.CaptureTransactionID, CV.ApprovalNo);
                //000|Successful|10428181|200706135353108|789.60|SUCCESSFUL|PreAuth
                ErrorLog.LoginfoToLogFile(TrackOrderID, "GetTransactionStatus");

                string[] OrderIDstatus = SplitFunction(TrackOrderID);

                string Errorcode, ErrorMessage, InvoiceNo, PayMateTransactionID, Amount, TransactionStatus, TransactionMode;

                Errorcode = OrderIDstatus[0].ToString();
                ErrorMessage = OrderIDstatus[1].ToString();
                InvoiceNo = OrderIDstatus[2].ToString();
                PayMateTransactionID = OrderIDstatus[3].ToString();
                Amount = OrderIDstatus[4].ToString();
                TransactionStatus = OrderIDstatus[5].ToString();
                TransactionMode = OrderIDstatus[6].ToString();
                if (Errorcode == "000")
                {
                    string Refundstring;

                    AccessCode = "3CF36ACF-9B4E-43B2-A867-A1D7B57C5F5C";
                    Refundstring = MV.MVRefund(credential, AccessCode, CV.CaptureTransactionID, CV.ApprovalNo, TotalCost.ToString(), RefundAmount.ToString());
                    ErrorLog.LoginfoToLogFile(Refundstring, "MVRefund");
                    string[] REfundDetails = SplitFunction(Refundstring);

                    string ERRORCODERefund, ERRORMESSAGERefund, InvoiceNoRefund, ayMateCaptureTransactionID, CaptureAmount, PayMateRefundTransactionID, RefundAmountnew;
                    //000|Successful|10428181|200706135353108|789.60|200707124639109|1595

                    ERRORCODERefund = REfundDetails[0].ToString();
                    ERRORMESSAGERefund = REfundDetails[1].ToString();
                    InvoiceNoRefund = REfundDetails[2].ToString();
                    ayMateCaptureTransactionID = REfundDetails[3].ToString();
                    CaptureAmount = REfundDetails[4].ToString();
                    PayMateRefundTransactionID = REfundDetails[5].ToString();
                    RefundAmountnew = REfundDetails[6].ToString();

                    status = Refundstring;
                    CV.ErrorCode = ERRORCODERefund;
                }
            }
            else if (CV.isPaymateCorporateModule && !CV.NewMVPreauthYN)
            {

                //LiveCorporateRegistrationCharging.ServiceSoapClient ObjCharging = new LiveCorporateRegistrationCharging.ServiceSoapClient();
                //LiveCorporateRegistrationCharging.AuthenticateService Auth = new LiveCorporateRegistrationCharging.AuthenticateService();
                PaymateRegistrationWCF.PMServiceClient ObjCharging = new PaymateRegistrationWCF.PMServiceClient();
                PaymateRegistrationWCF.AuthenticateService Auth = new PaymateRegistrationWCF.AuthenticateService();

                Auth.AuthUserName = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthUserName"); //"67592538";
                Auth.AuthPassword = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthPassword"); //"5BF24F3AA4CA";
                Auth.AuthCode = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthCode");  //"2511C6A40B4A";

                //Auth.AuthUserName = GuestRegistrationErrorDesc.GetConfigurationValue("TestAuthUserName"); // "23630FF1";
                //Auth.AuthPassword = GuestRegistrationErrorDesc.GetConfigurationValue("TestAuthPassword");  //"06904A018FCC";
                //Auth.AuthCode = GuestRegistrationErrorDesc.GetConfigurationValue("TestAuthCode");  //"7122C67EB9A6";

                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                //booking code below
                string AccessCode = "";
                if (CV.CCType == 2)
                {
                    AccessCode = GuestRegistrationErrorDesc.GetConfigurationValue("AmexRefundAccessCode");  //"D33E5904-FFD5-41AB-9BB2-040288DC4CFD"; //Amex
                }
                else
                {
                    AccessCode = GuestRegistrationErrorDesc.GetConfigurationValue("MVRefundAccessCode");  //"D33E5904-FFD5-41AB-9BB2-040288DC4CFD"; //Master Visa
                }


                //string SysRegCode = "";

                //SysRegCode = GuestRegistrationErrorDesc.GetConfigurationValue("LiveSysRegCode");  //"tQGcJM"; //Live
                //SysRegCode = GuestRegistrationErrorDesc.GetConfigurationValue("TestSysRegCode");  //"tVYfXG"; //Test


                string Transtatus = ObjCharging.GetTransactionStatus(Auth, AccessCode, CV.CaptureTransactionID, BookingID.ToString());

                if (CV.CCType != 2)
                {
                    ErrorLog.LoginfoToLogFile("start refund at Paymate MVRefund", "RefundProcess"); //log file

                    RefundStatus = ObjCharging.MVRefund(Auth, AccessCode, CV.CaptureTransactionID, BookingID.ToString()
                        , TotalCost.ToString(), RefundAmount.ToString());

                    ErrorLog.LoginfoToLogFile("After refund at Paymate MVRefund" + ":" + RefundStatus, "RefundProcess"); //log file 
                }
                else
                {
                    ErrorLog.LoginfoToLogFile("start refund at Paymate ARefund", "RefundProcess"); //log file


                    RefundStatus = ObjCharging.ABASInitRefund(Auth, AccessCode, CV.CaptureTransactionID, BookingID.ToString(), TotalCost.ToString(), RefundAmount.ToString());

                    ErrorLog.LoginfoToLogFile("After refund at Paymate ARefund" + ":" + RefundStatus, "RefundProcess"); //log file                 
                }

                string[] strChargingStatus = SplitFunction(RefundStatus);
                PVC.ErrorCode = strChargingStatus[0].ToString();
                PVC.errorMessage = strChargingStatus[1].ToString();
                if (PVC.ErrorCode == "000")
                {
                    PVC.InvoiceNo = strChargingStatus[2].ToString();
                    PVC.CaptureTransactionID = strChargingStatus[3].ToString();
                    PVC.Amount = Convert.ToDouble(strChargingStatus[4]);
                    PVC.RefundTransactionID = strChargingStatus[5].ToString();
                    PVC.RefundAmount = Convert.ToDouble(strChargingStatus[6]);
                    status = "Success";
                }
                else
                {
                    PVC.InvoiceNo = "";
                    PVC.CaptureTransactionID = "";
                    PVC.Amount = 0;
                    PVC.RefundTransactionID = "";
                    PVC.RefundAmount = 0;
                    status = "Failure";
                }
            }

            else
            {
                MasterVisaCharging.ServiceSoapClient MV = new MasterVisaCharging.ServiceSoapClient();
                MasterVisaCharging.AuthenticateService credential = new MasterVisaCharging.AuthenticateService();

                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                PVC.ErrorCode = "";
                if ((CV.ClientCoID == 2610 || CV.ClientCoID == 3248) && CV.AutoPostBooking)
                {
                    credential.userName = "00012955";
                    credential.merchRefNo = "SFHEK";
                    RefundStatus = MV.Refund(credential, CV.CaptureTransactionID, CV.ApprovalNo, TotalCost.ToString(), RefundAmount.ToString());
                }
                else
                {
                    credential.userName = "00011500";
                    credential.merchRefNo = "WNOUD";

                    if ((CV.ClientCoID == 2610 || CV.ClientCoID == 3248) && !CV.AutoPostBooking)
                    {
                        RefundStatus = MV.Refund(credential, CV.CaptureTransactionID, CV.ApprovalNo, TotalCost.ToString(), RefundAmount.ToString());
                    }
                    else
                    {
                        RefundStatus = MV.Refund(credential, CV.CaptureTransactionID, BookingID.ToString(), TotalCost.ToString(), RefundAmount.ToString());
                    }

                }

                string RefundStatus1 = "";
                if (RefundStatus.Contains("Invalid transaction details."))
                {
                    RefundStatus1 = MV.Refund(credential, CV.CaptureTransactionID, CV.TrackingID.ToString(), TotalCost.ToString(), RefundAmount.ToString());

                    InsertRefundDetails(BookingID, RefundAmount, 1, RefundStatus1, PVC.ErrorCode);
                    InsertYN = true;
                }

                if (!string.IsNullOrEmpty(RefundStatus1))
                {
                    status = RefundStatus1;
                }
                else
                {
                    status = RefundStatus;
                }
            }

            try
            {
                if (!InsertYN)
                {
                    InsertRefundDetails(BookingID, RefundAmount, 1, status, PVC.ErrorCode);
                }
            }
            catch (Exception ex)
            {

            }

            return status;
        }

        public void InsertRefundDetails(int BookingId, double RefundAmount, int CreateBy, string RefundStatus, string ErrorCode)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[5];
            ObjparamUser[0] = new SqlParameter("@BookingId", BookingId);
            ObjparamUser[1] = new SqlParameter("@RefundAmount", RefundAmount);
            ObjparamUser[2] = new SqlParameter("@CreateBy", CreateBy);
            ObjparamUser[3] = new SqlParameter("@RefundStatus", RefundStatus);
            ObjparamUser[4] = new SqlParameter("@ErrorCode", ErrorCode);
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_InsertRefundStatus", ObjparamUser);
        }

        public void UpdateCorporateMVProcess(int BookingID, string ErrorCode, string Message, string TransactionID
            , string InvoiceID, double Amount, string TravellerID, string CaptureTransactionID, double CaptureAmount)
        {
            SqlParameter[] param = new SqlParameter[9];

            param[0] = new SqlParameter("@BookingID", BookingID);
            param[1] = new SqlParameter("@ErrorCode", ErrorCode);
            param[2] = new SqlParameter("@Message", Message);
            param[3] = new SqlParameter("@TransactionID", TransactionID);
            param[4] = new SqlParameter("@InvoiceID", InvoiceID);
            param[5] = new SqlParameter("@Amount", Amount);
            param[6] = new SqlParameter("@TravellerID", TravellerID);
            param[7] = new SqlParameter("@CaptureTransactionID", CaptureTransactionID);
            param[8] = new SqlParameter("@CaptureAmount", CaptureAmount);

            SqlHelper.ExecuteNonQuery("ProcCorporateMVProcess", param);
        }

        public void UpdateAmexBatchBookingStatus(int BookingID, string ErrorCode, string Message, string TransactionID
            , string InvoiceID, double Amount, string TravellerID, string CaptureTransactionID, double CaptureAmount)
        {
            SqlParameter[] param = new SqlParameter[9];

            param[0] = new SqlParameter("@BookingID", BookingID);
            param[1] = new SqlParameter("@ErrorCode", ErrorCode);
            param[2] = new SqlParameter("@Message", Message);
            param[3] = new SqlParameter("@TransactionID", TransactionID);
            param[4] = new SqlParameter("@InvoiceID", InvoiceID);
            param[5] = new SqlParameter("@Amount", Amount);
            param[6] = new SqlParameter("@TravellerID", TravellerID);
            param[7] = new SqlParameter("@CaptureTransactionID", CaptureTransactionID);
            param[8] = new SqlParameter("@CaptureAmount", CaptureAmount);

            SqlHelper.ExecuteNonQuery("ProcAmexBatchStatus", param);
        }

        public void SaveMVLogNew(int BookingID, string ErrorCode, string Message, string TransactionID)
        {
            SqlParameter[] param = new SqlParameter[4];

            param[0] = new SqlParameter("@BookingID", BookingID);
            param[1] = new SqlParameter("@ErrorCode", ErrorCode);
            param[2] = new SqlParameter("@Message", Message);
            param[3] = new SqlParameter("@TransactionID", TransactionID);
            SqlHelper.ExecuteNonQuery("ProcSaveMvLog", param);
        }

        public void UpdateAmexBatchBookingFailureStatus(int BookingID, string ErrorCode, string Provider)
        {
            SqlParameter[] param = new SqlParameter[3];

            param[0] = new SqlParameter("@BookingID", BookingID);
            param[1] = new SqlParameter("@ErrorCode", ErrorCode);
            param[2] = new SqlParameter("@Provider", Provider);

            SqlHelper.ExecuteNonQuery("ProcAmexBatchStatusFailure", param);
        }

        // One way closing 
        public ClosingVariables GetOneWayPackage(ClosingVariables objClosingObject)
        {
            DataSet dsPackage = new DataSet();
            SqlParameter[] param = new SqlParameter[8];
            param[0] = new SqlParameter("@CarCatID", objClosingObject.CarCatId);
            param[1] = new SqlParameter("@CityID", objClosingObject.PickupCityID);
            param[2] = new SqlParameter("@ClientType", objClosingObject.BookIndivID.Substring(0, 1));
            param[3] = new SqlParameter("@ClientID", objClosingObject.ClientCoID);
            param[4] = new SqlParameter("@DateOut", objClosingObject.DateOut);
            param[5] = new SqlParameter("@ModelID", objClosingObject.CarModelId);
            param[6] = new SqlParameter("@IndicatedPkgID", objClosingObject.PkgID);
            param[7] = new SqlParameter("@DropoffCityID", objClosingObject.DropOffCityId);
            dsPackage = SqlHelper.ExecuteDataset("GetOneWayCityTransferPackage", param);
            if (dsPackage.Tables[0].Rows.Count > 0)
            {
                objClosingObject.PkgID = Convert.ToInt64(dsPackage.Tables[0].Rows[0]["PkgID"]);
                objClosingObject.PkgRate = Convert.ToDouble(dsPackage.Tables[0].Rows[0]["PkgRate"]);
                objClosingObject.PkgHr = Convert.ToDouble(dsPackage.Tables[0].Rows[0]["PkgHrs"]);
                objClosingObject.PkgKM = Convert.ToDouble(dsPackage.Tables[0].Rows[0]["PkgKMs"]);
                objClosingObject.ExtraHrRate = Convert.ToDouble(dsPackage.Tables[0].Rows[0]["ExtraHrRate"]);
                objClosingObject.ExtraKMRate = Convert.ToDouble(dsPackage.Tables[0].Rows[0]["ExtraKMRate"]);
                objClosingObject.NightStayAmt = Convert.ToDouble(dsPackage.Tables[0].Rows[0]["NightStayAllowance"]);
                objClosingObject.ThresholdExtraHr = Convert.ToDouble(dsPackage.Tables[0].Rows[0]["ThresholdExtraHr"]);
            }
            else
            {
                objClosingObject.PkgID = 0;
                objClosingObject.PkgRate = 0;
            }
            return objClosingObject;
        }
        public ClosingVariables GetBookingDetails(ClosingVariables objClosingObject)
        {
            try
            {
                DataSet dsBooking = new DataSet();
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@BookingId", objClosingObject.BookingId);
                dsBooking = SqlHelper.ExecuteDataset("Prc_GetBookingDispatchDetails", param);
                if (dsBooking.Tables[0].Rows.Count > 0)
                {
                    objClosingObject.ServiceUnitID = Convert.ToInt32(dsBooking.Tables[0].Rows[0]["unitID"]);
                    objClosingObject.ClientCoIndivID = Convert.ToInt64(dsBooking.Tables[0].Rows[0]["clientcoindivid"]);
                    objClosingObject.Service = Convert.ToString(dsBooking.Tables[0].Rows[0]["Service"]);
                    objClosingObject.PickupCityID = Convert.ToInt32(dsBooking.Tables[0].Rows[0]["PickupCityId"]);
                    objClosingObject.DropOffCityId = Convert.ToInt32(dsBooking.Tables[0].Rows[0]["DropOffCityID"]);
                    objClosingObject.DateOut = Convert.ToDateTime(dsBooking.Tables[0].Rows[0]["dateout"]);
                    objClosingObject.TimeOut = Convert.ToString(dsBooking.Tables[0].Rows[0]["TimeOut"]);
                    objClosingObject.ClientCoID = Convert.ToInt32(dsBooking.Tables[0].Rows[0]["ClientCoId"]);
                    objClosingObject.CarID = Convert.ToInt32(dsBooking.Tables[0].Rows[0]["CarId"]);
                    objClosingObject.IndicatedDiscAmt = Convert.ToInt32(dsBooking.Tables[0].Rows[0]["indicatedDiscAmt"]);
                    objClosingObject.DiscountPC = Convert.ToInt32(dsBooking.Tables[0].Rows[0]["IndicatedDiscPC"]);
                    objClosingObject.BookIndivID = Convert.ToString(dsBooking.Tables[0].Rows[0]["BookIndivID"]);
                    objClosingObject.PkgID = Convert.ToInt64(dsBooking.Tables[0].Rows[0]["IndicatedPkgID"]);
                }
                else
                {
                    objClosingObject.ServiceUnitID = 0;
                    objClosingObject.ClientCoIndivID = 0;
                    objClosingObject.ClientCoID = 0;
                }

            }
            catch (Exception)
            {

                throw;
            }
            return objClosingObject;
        }


        public ClosingVariables GetServiceTax(ClosingVariables objClosingObject)
        {
            int flag = 0;
            DataSet dsService = new DataSet();
            SqlParameter[] param = new SqlParameter[4];
            if ((objClosingObject.DropOffCityId == 2 || objClosingObject.DropOffCityId == 29 || objClosingObject.DropOffCityId == 33 || objClosingObject.DropOffCityId == 34) && (objClosingObject.PickupCityID == 2 || objClosingObject.PickupCityID == 29 || objClosingObject.PickupCityID == 33 || objClosingObject.PickupCityID == 34))
            {
                if (objClosingObject.ServiceUnitID == 1 || objClosingObject.ServiceUnitID == 20 || objClosingObject.ServiceUnitID == 24 || objClosingObject.ServiceUnitID == 25)
                {
                    flag = 3;
                }
                else
                {
                    flag = 4;
                }
            }
            else
            {
                flag = 4;
            }
            //Special Car
            if (objClosingObject.PickupCityID != 27 || objClosingObject.PickupCityID != 29 || objClosingObject.PickupCityID != 30 || objClosingObject.PickupCityID != 31 || objClosingObject.PickupCityID != 37 || objClosingObject.PickupCityID != 40)
            {
                DataSet dsSpecialCar = new DataSet();
                SqlParameter[] paramSpl = new SqlParameter[1];
                param[0] = new SqlParameter("@carID", objClosingObject.CarID);
                dsSpecialCar = SqlHelper.ExecuteDataset("Prc_SpecialCarType", paramSpl);
                if (dsSpecialCar.Tables[0].Rows.Count > 0 && Convert.ToBoolean(dsSpecialCar.Tables[0].Rows[0]["SpecialCarType"]) == true)
                {
                    flag = 6;
                }
            }
            //End of Special Car
            param[0] = new SqlParameter("@clientcoindivid", objClosingObject.ClientCoIndivID);
            param[1] = new SqlParameter("@UnitID", objClosingObject.ServiceUnitID);
            param[2] = new SqlParameter("@Flag", flag);
            param[3] = new SqlParameter("@datein", objClosingObject.DateIn);
            dsService = SqlHelper.ExecuteDataset("ProcDS_UnitTaxPercent1", param);
            if (dsService.Tables[0].Rows.Count > 0)
            {
                objClosingObject.ServiceTaxPercent = Convert.ToDouble(dsService.Tables[0].Rows[0]["ServiceTaxPercent"]);
                objClosingObject.SwachhBharatTaxPercent = Convert.ToDouble(dsService.Tables[0].Rows[0]["SwachhBharatTaxPercent"]);
                objClosingObject.KrishiKalyanTaxPercent = Convert.ToDouble(dsService.Tables[0].Rows[0]["KrishiKalyanTaxPercent"]);
                objClosingObject.EduCessPercent = Convert.ToDouble(dsService.Tables[0].Rows[0]["EduCessPercent"]);
                objClosingObject.HduCessPercent = Convert.ToDouble(dsService.Tables[0].Rows[0]["HduCessPercent"]);
                objClosingObject.TotalServiceTaxPer = objClosingObject.ServiceTaxPercent + objClosingObject.SwachhBharatTaxPercent + objClosingObject.KrishiKalyanTaxPercent + ((objClosingObject.ServiceTaxPercent * objClosingObject.EduCessPercent) / 100) + ((objClosingObject.ServiceTaxPercent * objClosingObject.HduCessPercent) / 100);
                objClosingObject.IndicatedDiscAmt = (objClosingObject.IndicatedDiscAmt * 100) / (100 + objClosingObject.TotalServiceTaxPer);
            }
            return objClosingObject;
        }

        public ClosingVariables GetFixedGarageRunAMT(ClosingVariables objClosingObject)
        {

            SqlParameter[] param = new SqlParameter[6];
            DataSet dsFGR = new DataSet();
            param[0] = new SqlParameter("@CityID", objClosingObject.PickupCityID);
            param[1] = new SqlParameter("@Service", objClosingObject.Service);
            param[2] = new SqlParameter("@CompanyID", objClosingObject.ClientCoID);
            param[3] = new SqlParameter("@CarCatID", objClosingObject.CarCatId);
            param[4] = new SqlParameter("@OutStationYN", objClosingObject.OutstationYN);
            param[5] = new SqlParameter("@DateOut", objClosingObject.DateOut);
            dsFGR = SqlHelper.ExecuteDataset("ProcDS_FindFixedGarageRunAMT_1", param);
            if (dsFGR.Tables[0].Rows.Count > 0)
            {
                objClosingObject.FXDGarageRate = Convert.ToDouble(dsFGR.Tables[0].Rows[0]["Amount"]);
            }
            else
            {
                objClosingObject.FXDGarageRate = 0;
            }
            return objClosingObject;
        }
        public ClosingVariables GetNoOfNightAndDay(ClosingVariables objClosingObject)
        {
            System.TimeSpan span = objClosingObject.DateIn.Subtract(objClosingObject.DateOut);
            objClosingObject.NoNight = Convert.ToInt32(Math.Floor(span.TotalDays));
            objClosingObject.NoDays = objClosingObject.NoNight + 1;
            objClosingObject.TotalHrsUsed = objClosingObject.DateIn.AddHours(Convert.ToInt32(objClosingObject.TimeIn.Substring(0, 2))).AddMinutes(Convert.ToInt32(objClosingObject.TimeIn.Substring(2, 2))).Subtract(
                                objClosingObject.DateOut.AddHours(Convert.ToInt32(objClosingObject.TimeOut.Substring(0, 2))).AddMinutes(Convert.ToInt32(objClosingObject.TimeOut.Substring(2, 2)))).TotalHours;
            if (objClosingObject.NoNight == 0 && (Convert.ToInt32(objClosingObject.TimeOut) > 2200 || Convert.ToInt32(objClosingObject.TimeOut) < 0600 || Convert.ToInt32(objClosingObject.TimeIn) > 2200 || Convert.ToInt32(objClosingObject.TimeIn) < 0600))
            {
                objClosingObject.NoNight = objClosingObject.NoNight + 1;
            }
            else if (objClosingObject.NoNight >= 1 && objClosingObject.TotalHrsUsed > 24)
            {
                objClosingObject.NoNight = objClosingObject.NoNight + 1;
            }
            return objClosingObject;
        }

        public ClosingVariables GetWaitingCharges(ClosingVariables objClosingObject)
        {
            objClosingObject.WaitingTime = objClosingObject.WaitingHr + objClosingObject.WaitingMi;
            int quotient = 0;
            int remainder = 0;
            if (Convert.ToInt32(objClosingObject.WaitingTime) > 0000)
            {
                objClosingObject.TotalWaitingTimeMinute = Convert.ToDouble((Convert.ToInt32(objClosingObject.WaitingHr) * 60) + Convert.ToInt32(objClosingObject.WaitingMi));
                objClosingObject.ExtraThresholdMinute = objClosingObject.ThresholdExtraHr * 60;
                objClosingObject.WaitingMinute = objClosingObject.TotalWaitingTimeMinute - objClosingObject.ExtraThresholdMinute;
                if (objClosingObject.WaitingMinute > 0)
                {
                    quotient = Convert.ToInt32(Math.Floor(Convert.ToDouble(objClosingObject.WaitingMinute) / 60));
                    remainder = Convert.ToInt32(objClosingObject.WaitingMinute % 60);
                    if (quotient >= 1)
                    {
                        objClosingObject.WaitingCharge = quotient * objClosingObject.ExtraHrRate;
                    }
                    else
                    {
                        objClosingObject.WaitingCharge = 0;
                    }
                    if (remainder > 0)
                    {
                        objClosingObject.WaitingCharge = objClosingObject.WaitingCharge + objClosingObject.ExtraHrRate;
                    }
                }
                else
                {
                    objClosingObject.WaitingCharge = 0;
                }
            }
            else
            {
                objClosingObject.WaitingCharge = 0;
            }

            return objClosingObject;
        }

        public ClosingVariables GetFuelSurCharges(ClosingVariables objClosingObject)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet dsFSC = new DataSet();
            param[0] = new SqlParameter("@BookingId", objClosingObject.BookingId);
            dsFSC = SqlHelper.ExecuteDataset("Prc_CalculateFuelSurCharge_New", param);
            if (dsFSC.Tables[0].Rows.Count > 0)
            {
                objClosingObject.FuelSurcharge = Convert.ToDouble(dsFSC.Tables[0].Rows[0]["FuelSurcharge"]);
            }
            else
            {
                objClosingObject.FuelSurcharge = 0;
            }
            if (objClosingObject.DropOffCityId > 0 && objClosingObject.Service == "T")
            {
                objClosingObject.FuelSurcharge = 0;
            }
            return objClosingObject;
        }

        public ClosingVariables GetOneWayClosing(ClosingVariables objClosingObject)
        {
            try
            {
                objClosingObject = GetBookingDetails(objClosingObject);  // BookingAndDispatch Details
                objClosingObject = GetOneWayPackage(objClosingObject); // Get Pacakge
                if (objClosingObject.PkgID > 0 && objClosingObject.PkgRate > 0)
                {
                    objClosingObject = GetServiceTax(objClosingObject);   // Get Service Tax
                    objClosingObject = GetFixedGarageRunAMT(objClosingObject); // Get Garage Run Amount
                    objClosingObject = GetNoOfNightAndDay(objClosingObject); // Get nubmer of Night and day
                    objClosingObject = GetWaitingCharges(objClosingObject); // Get Waiting Charges
                    objClosingObject = GetFuelSurCharges(objClosingObject); // Get Fuelsur Charges

                    objClosingObject.BasisWithServiceTax = Convert.ToDouble((objClosingObject.PkgRate + (objClosingObject.NoNight * objClosingObject.NightStayAmt) + objClosingObject.WaitingCharge + objClosingObject.FXDGarageRate + objClosingObject.FuelSurcharge) * (100 - Convert.ToDouble(objClosingObject.DiscountPC))) / 100;
                    objClosingObject.BasicRevenue = Math.Round(Convert.ToDouble((objClosingObject.BasisWithServiceTax * 100) / (100 + objClosingObject.TotalServiceTaxPer)), 2);
                    //objClosingObject.TotalCost=Convert.ToDouble((objClosingObject.PkgRate +(objClosingObject.NoNight * objClosingObject.NightStayAmt)+ objClosingObject.WaitingCharge + objClosingObject.FXDGarageRate+objClosingObject.FuelSurcharge)*(100-Convert.ToDouble(objClosingObject.DiscountPC)))/100;
                    objClosingObject.TotalCost = Convert.ToDouble(objClosingObject.BasisWithServiceTax) + objClosingObject.ParkTollChages + objClosingObject.InterstateTax + Convert.ToDouble(((objClosingObject.ParkTollChages + objClosingObject.InterstateTax) * objClosingObject.TotalServiceTaxPer) / 100);

                }
                else
                {

                }

            }
            catch (Exception Ex)
            {
                throw;
            }
            return objClosingObject;
        }
        public void UpdateQRPaymentCallBackRespose(PayUCallbackResponse callbackData)
        {
            DB db = new DB();

            db.UpdateQRPaymentCallBackRespose(callbackData);

        }

        public async Task<string> GetAmexOrderPaymentStatus(OrderDetails bookingDetails)
        {
            string responsestring = "";
            string acsEci = "";
            string orderStatus = "";
            string orderAuthStatus = "";
            string responseGateCode = "";
            string responseGateRecomm = "";
            string chargingStatus = "";
            string chargingResult = "";
            string chargingErrorDescription = "";
            string chargingErrorCause = "";
            bool updateAmount = false;  //keep  value = true when amount update 
            try
            {
                bool result = false;
                AmexCardCls objAmex = new AmexCardCls();
                string transactionId = "";

                var amex_get_order_status_api_url = ConfigurationManager.AppSettings["GetOrderStatusApiUrl"];
                var amex_gateway_host = ConfigurationManager.AppSettings["GatewayHost"];
                var amex_auth_value = ConfigurationManager.AppSettings["AmexAuthValue"];
                //var return_url = ConfigurationManager.AppSettings["ReturnUrl"];
                var merchant_name = ConfigurationManager.AppSettings["MerchantName"];
                var intraction_operation = ConfigurationManager.AppSettings["IntractionOperationType"];
                var api_operation = ConfigurationManager.AppSettings["ApiOperation"];
                var username = ConfigurationManager.AppSettings["Username"];
                var password = ConfigurationManager.AppSettings["Password"];

                var apiUrl = amex_gateway_host + amex_get_order_status_api_url + bookingDetails.approvalNo;

                // Set the TLS version to TLS 1.2
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                var client = new HttpClient();
                var request = new HttpRequestMessage(HttpMethod.Get, apiUrl);

                // Set the Authorization header
                var authValue = Convert.ToBase64String(Encoding.ASCII.GetBytes(username + ":" + password));
                request.Headers.Add("Authorization", "Basic " + authValue);

                var response = await client.SendAsync(request);
                var responseContent = await response.Content.ReadAsStringAsync();


                AmexGetOrderStatusResponse responseObj = JsonConvert.DeserializeObject<AmexGetOrderStatusResponse>(responseContent);


                if (responseObj != null)
                {
                    chargingStatus = responseObj.status;
                    chargingResult = responseObj.result;

                    if (responseObj.error == null)
                    {
                        if (chargingStatus == "CAPTURED" && responseObj.transaction.Count() > 2)
                        {
                            Transaction transaction3 = responseObj.transaction[2];
                            acsEci = transaction3.authentication.ThreeDS.acsEci;
                            orderStatus = transaction3.order.status;
                            orderAuthStatus = transaction3.order.authenticationStatus;
                            responseGateCode = transaction3.response.gatewayCode;
                            responseGateRecomm = transaction3.response.gatewayRecommendation;

                            if (acsEci == "05"
                                && orderStatus == "CAPTURED"
                                && orderAuthStatus == "AUTHENTICATION_SUCCESSFUL"
                                && responseGateCode == "APPROVED")
                            //&& responseGateRecomm == "NO_ACTION")
                            {
                                result = true;
                            }
                            else
                            {
                                result = false;
                            }
                        }
                        else if (chargingStatus != "CAPTURED" && responseObj.transaction.Count() > 2)
                        {
                            Transaction transaction31 = responseObj.transaction[2];
                            acsEci = transaction31.authentication.ThreeDS.acsEci;
                            orderStatus = transaction31.order.status;
                            orderAuthStatus = transaction31.order.authenticationStatus;
                            responseGateCode = transaction31.response.gatewayCode;
                            responseGateRecomm = transaction31.response.gatewayRecommendation;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                    else
                    {
                        result = false;
                        chargingStatus = responseObj.status == null ? responseObj.result : responseObj.status;
                        chargingErrorDescription = responseObj.error.explanation;
                        chargingErrorCause = responseObj.error.cause;

                    }

                    if (responseObj.authentication != null)
                    {
                        transactionId = responseObj.authentication._3ds.transactionId;
                    }

                    if (!updateAmount)
                    {
                        objAmex.UpdateOrderPaymentStatus(bookingDetails, chargingStatus
                       , transactionId, result, acsEci, orderStatus,
                       orderAuthStatus, responseGateCode, responseGateRecomm, chargingStatus,
                       chargingResult, chargingErrorDescription, chargingErrorCause, Convert.ToString(responseObj.amount));

                        //Session["ResponseText"] = "BookingId : " + bookingDetails.bookingId
                        responsestring = "BookingId : " + bookingDetails.bookingId
                            + "<br/> Status : " + chargingStatus;
                    }
                    else
                    {
                        objAmex.UpdateOrderAmount(bookingDetails, Convert.ToString(responseObj.amount));
                        responsestring = "";
                    }
                }
                else
                {
                    responsestring = "BookingId : " + bookingDetails.bookingId
                        + "Some error occcured";
                }
            }
            catch (Exception ex)
            {
                //Response.Write("Error: " + ex.Message);
                //Session["ResponseText"] = "Error in Charging BookingID : "
                responsestring = "Error in Charging BookingID : "
                    + bookingDetails.bookingId + "<br/>Error : " + ex.Message;
                //Response.Redirect("Message.aspx");
            }
            return responsestring;
        }



        public string getBajajToken()
        {
            string BajajGetAuthTokenURL = ConfigurationManager.AppSettings["BajajGetAuthTokenURL"];
            string bajajClientID = ConfigurationManager.AppSettings["BajajFinClientID"];
            string bajajClientSecret = ConfigurationManager.AppSettings["BajajFinClientSecret"];
            string bajajResourceURL = ConfigurationManager.AppSettings["BajajFinResourceURL"];
            string bajajCookie = ConfigurationManager.AppSettings["BajajTokenCookie"];

            string URL = BajajGetAuthTokenURL;

            string token = "";
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var oAuthclient = new RestClient(URL);
            //oAuthclient.Timeout = -1;
            var request = new RestRequest(Method.POST);
            //request.AddHeader("cache-control", "no-cache");
            //request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("Cookie", bajajCookie);
            request.AddParameter("grant_type", "client_credentials");
            request.AddParameter("client_id", bajajClientID);
            request.AddParameter("client_secret", bajajClientSecret);
            request.AddParameter("resource", bajajResourceURL);
            //request.AddStringBody(body, DataFormat.Json);
            IRestResponse response = oAuthclient.Execute(request);
            if (response.Content != "")
            {
                JObject jObj = JObject.Parse(response.Content);
                token = Convert.ToString(jObj["access_token"]);
            }

            return token;
        }

        public string DecryptStringNew(string content, string key
            , string iv)
        {
            try
            {
                byte[] keyBytes = Encoding.UTF8.GetBytes(key);
                byte[] ivBytes = Encoding.UTF8.GetBytes(iv);
                byte[] ciphertext = Convert.FromBase64String(content);

                using (Aes aesAlg = Aes.Create())
                {
                    aesAlg.Key = keyBytes;
                    aesAlg.IV = ivBytes;

                    ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                    using (MemoryStream msDecrypt = new MemoryStream(ciphertext))
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                    {
                        return srDecrypt.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public OL_CheckLoginResponse EmpDetailsNew(string EmailID)
        {
            string bajajGetAuthTokenURL = ConfigurationManager.AppSettings["BajajGetEployeeDetailsBaseURL"];
            string ocpApimSubscriptionKey = ConfigurationManager.AppSettings["BajajOcpApimSubscriptionKey"];

            OL_CheckLoginResponse responseclass = new OL_CheckLoginResponse();
            string tokenMS = getBajajToken();  //New Token Generation

            ErrorLog.LoginfoToLogFile("EmpDetailsNew:tokey="
            + tokenMS, "EmpDetailsNew");

            if (string.IsNullOrEmpty(EmailID))
            {
                responseclass.IsSucess = false;
                responseclass.Message = "Email not found.";
                return responseclass;
            }
            string url = bajajGetAuthTokenURL + "employeeprofile-api/employeedetails?identifier=" + EmailID + "&app=carzonrent";

            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.AddHeader("ocp-apim-subscription-key", ocpApimSubscriptionKey);
            request.AddHeader("Authorization", "Bearer " + tokenMS);  //newly added by bajaj
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            IRestResponse response = client.Execute(request);

            //var options = new RestClientOptions(bajajGetAuthTokenURL)
            //{
            //    MaxTimeout = -1,
            //};
            //var client = new RestClient(options);
            //var request = new RestRequest("/employeeprofile-api/employeedetails?identifier=vaishanavi.dudhe@bajajfinserv.in&app=carzonrent", Method.Get);
            //request.AddHeader("ocp-apim-subscription-key", ocpApimSubscriptionKey);
            //request.AddHeader("Authorization", "Bearer " + tokenMS);
            ////request.AddHeader("Content-Type", "application/json");


            // Check if the response has a JSON content type
            string contentType = response.ContentType.ToLower();

            //response.Content = "25a42iv9DN7dL56zXMH8IpPwRISdR6GgVtVYB2LEhkbxZ5PcxS+ohbNIUwqNBefEMlLSC5UtyyQKFkUvdpJbSwYLMq8ME8T8WrDAYxT3Lmhk1095zlAUQ9AUPBuI/KyLCurX/626UHwg/p/RraETVy9ET4P/Dj5v6ymZYI/s/MEbeHjeKm9XtZaPJQuvWSigXPBR/06AmL3R1P6jzhGfb4qBnD0RVmhPJLFXxVt0z+KGP3YwKoAAbPP6OSiUgJpfjGNT4XCYJDTdQzUtXkbH9V8PmOw68jhR4EbzoJRX8QJFEzTLFXNaYbrPmaYaj2N5WoiChTzbte9kN2lJRYB9MiKhWx4ri6pKb6B6rBJwNkp0AJml1KTPz3KAkyYlwBuIb+PArUqD3QA1v7H0qS3lvEaWIU82EYQPNh3kV8dNLlEDiL0m39ukIPhyWC56nluDeV1xdp5DT2lmpi3cixXRH0wwBBLvZhtH2p83LXxyZqw=";

            ErrorLog.LoginfoToLogFile("EmpDetailsNew: contentType="
                + contentType, "EmpDetailsNew");


            ErrorLog.LoginfoToLogFile("EmpDetailsNew: Content="
                + response.Content, "EmpDetailsNew");

            string token = "", error = "", message = "";
            if (contentType.Contains("application/json"))
            //|| contentType.Contains("text/html"))
            {
                //token = response.Content;
                // It's a JSON response
                // You can then deserialize the JSON content if needed
                JObject jObj = JObject.Parse(response.Content);
                try
                {
                    //JObject jObj = JObject.Parse(response.Content);
                    error = Convert.ToString(jObj["statusCode"]);
                    message = Convert.ToString(jObj["message"]);
                }
                catch (Exception ex)
                {
                    responseclass.IsSucess = false;
                    responseclass.Message = ex.ToString();
                }
            }
            else if (contentType.Contains("text/html; charset=utf-8"))
            //else if (contentType.Contains("text/html"))
            {
                // It's a string response token
                token = response.Content;
            }
            else
            {
                responseclass.IsSucess = false;
                responseclass.Message = "No json or string token found in response ";
            }

            bool TestEmailID = false;

            if (EmailID == "Ulivetest3@bajajfinance.onmicrosoft.com" || EmailID == "Ulivetest6@bajajfinance.onmicrosoft.com")
            {
                TestEmailID = true;
            }

            //if (string.IsNullOrEmpty(token) && error.Trim() == "Data match not found" && !TestEmailID)
            if (string.IsNullOrEmpty(token) && (error.Trim() == "Data match not found" || !string.IsNullOrEmpty(message)) && !TestEmailID) //modified on 07-12-2023
            {
                responseclass.IsSucess = false;
                responseclass.Message = error.Trim() == "" ? message.Trim() : "";
            }

            if (string.IsNullOrEmpty(responseclass.Message))
            {
                string FName = "", EmailIdr = "", Phone = "", Gender = "", Band = ""
                    , Department = "";
                if (!TestEmailID)
                {
                    string keyBase64 = "";
                    keyBase64 = "C&F)J@NcRfUjXn2r4u7x!A%D*G-KaPdS";  //"DWIzFkO22qfVMgx2fIsxOXnwz10pRuZfFJBvf4RS3eY=";
                    string ivBase64 = "fUjXn2r5u8x/A%D*";           // "AcynMwikMkW4c7+mHtwtfw==";
                    string decrypt1 = DecryptStringNew(token, keyBase64, ivBase64);  // New decryption method
                    ErrorLog.LoginfoToLogFile("EmpDetailsNew after decrupt response : "
                        + decrypt1, "EmpDetailsNew");

                    JObject AObj = JObject.Parse(decrypt1);

                    ////Additional data if required to be used
                    //BusinessGroup = Convert.ToString(AObj["Business Group"]);
                    //CCC = Convert.ToString(AObj["Cost Centre Code"]);
                    //CCN = Convert.ToString(AObj["Cost Centre Name"]);
                    //Department = Convert.ToString(AObj["Department"]);
                    //EmpCode = Convert.ToString(AObj["Emp code"]);
                    //EmpStatus = Convert.ToString(AObj["Employee Status"]);
                    //SubDep = Convert.ToString(AObj["Sub Department"]);
                    //SubSubDep = Convert.ToString(AObj["Sub Sub Department"]);


                    FName = Convert.ToString(AObj["Name"]);
                    EmailIdr = Convert.ToString(AObj["Email"]);
                    Phone = Convert.ToString(AObj["Mobile number"]);
                    Gender = Convert.ToString(AObj["Gender"]);
                    Band = Convert.ToString(AObj["Band"]).Trim();
                    Department = Convert.ToString(AObj["Department"]).Trim();
                }
                else
                {
                    FName = "Test Test";
                    EmailIdr = EmailID;
                    Phone = "9800000000";
                    Gender = "Male";
                    Department = "Test";
                }
                DB insta = new DB();
                DataSet result = insta.SaveEmpDetails(FName, Phone
                    , EmailIdr, Gender, Band, Department);

                if (result.Tables[0].Rows.Count > 0)
                {
                    responseclass.LoginId = Convert.ToString(result.Tables[0].Rows[0]["EmailID"]);
                    responseclass.Password = Convert.ToString(result.Tables[0].Rows[0]["pass"]);
                    if (string.IsNullOrEmpty(responseclass.LoginId) && string.IsNullOrEmpty(responseclass.Password))
                    {
                        responseclass.IsSucess = false;
                        responseclass.Message = "Invalid EmailID.";
                    }
                    else
                    {
                        if (Convert.ToBoolean(result.Tables[0].Rows[0]["IsSucess"]))
                        {
                            responseclass.IsSucess = true;
                        }
                        else
                        {
                            responseclass.IsSucess = false;
                            responseclass.Message = "Only GB08+ and Personal Secretaries are authorized to book cabs.";
                        }
                    }
                }
                else
                {
                    responseclass.IsSucess = false;
                    responseclass.Message = "No Data Found.";
                }
            }
            return responseclass;
        }

        public OL_CheckBookingAllowed EmpDetailsUpdateBand(string EmailID
            , int ClientCoIndivID, string BandName)
        {

            OL_CheckBookingAllowed responseclass = new OL_CheckBookingAllowed();

            //if (string.IsNullOrEmpty(BandName))
            //{
            string bajajGetAuthTokenURL = ConfigurationManager.AppSettings["BajajGetEployeeDetailsBaseURL"];
            string ocpApimSubscriptionKey = ConfigurationManager.AppSettings["BajajOcpApimSubscriptionKey"];


            string tokenMS = getBajajToken();  //New Token Generation

            ErrorLog.LoginfoToLogFile("EmpDetailsUpdateBand:tokey="
            + tokenMS, "EmpDetailsUpdateBand");

            if (string.IsNullOrEmpty(EmailID))
            {
                responseclass.IsSucess = false;
                responseclass.Message = "Email not found.";
                return responseclass;
            }
            string url = bajajGetAuthTokenURL + "employeeprofile-api/employeedetails?identifier=" + EmailID + "&app=carzonrent";

            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.AddHeader("ocp-apim-subscription-key", ocpApimSubscriptionKey);
            request.AddHeader("Authorization", "Bearer " + tokenMS);  //newly added by bajaj
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            IRestResponse response = client.Execute(request);

            // Check if the response has a JSON content type
            string contentType = response.ContentType.ToLower();

            ErrorLog.LoginfoToLogFile("EmpDetailsUpdateBand: contentType="
                + contentType, "EmpDetailsUpdateBand");


            ErrorLog.LoginfoToLogFile("EmpDetailsUpdateBand: Content="
                + response.Content, "EmpDetailsUpdateBand");

            string token = "", error = "", message = "";
            if (contentType.Contains("application/json"))
            //|| contentType.Contains("text/html"))
            {
                //token = response.Content;
                // It's a JSON response
                // You can then deserialize the JSON content if needed
                JObject jObj = JObject.Parse(response.Content);
                try
                {
                    //JObject jObj = JObject.Parse(response.Content);
                    error = Convert.ToString(jObj["statusCode"]);
                    message = Convert.ToString(jObj["message"]);
                }
                catch (Exception ex)
                {
                    responseclass.IsSucess = false;
                    responseclass.Message = ex.ToString();
                }
            }
            else if (contentType.Contains("text/html; charset=utf-8"))
            {
                // It's a string response token
                token = response.Content;
            }
            else
            {
                responseclass.IsSucess = false;
                responseclass.Message = "No json or string token found in response ";
            }

            if (string.IsNullOrEmpty(token)
                && (error.Trim() == "Data match not found"
                || !string.IsNullOrEmpty(message)))
            {
                responseclass.IsSucess = false;
                responseclass.Message = error.Trim() == "" ? message.Trim() : "";
            }

            if (string.IsNullOrEmpty(responseclass.Message))
            {
                string FName = "", EmailIdr = "", Phone = "", Gender = ""
                    , Band = ""
                    , Department = "";

                string keyBase64 = "";
                keyBase64 = "C&F)J@NcRfUjXn2r4u7x!A%D*G-KaPdS";  //"DWIzFkO22qfVMgx2fIsxOXnwz10pRuZfFJBvf4RS3eY=";
                string ivBase64 = "fUjXn2r5u8x/A%D*";           // "AcynMwikMkW4c7+mHtwtfw==";
                string decrypt1 = DecryptStringNew(token, keyBase64, ivBase64);  // New decryption method
                ErrorLog.LoginfoToLogFile("EmpDetailsUpdateBand after decrupt response : "
                    + decrypt1, "EmpDetailsNew");

                JObject AObj = JObject.Parse(decrypt1);

                try
                {
                    FName = Convert.ToString(AObj["Name"]);
                    EmailIdr = Convert.ToString(AObj["Email"]);
                    Phone = Convert.ToString(AObj["Mobile number"]);
                    Gender = Convert.ToString(AObj["Gender"]);
                    Band = Convert.ToString(AObj["Band"]).Trim();
                    Department = Convert.ToString(AObj["Department"]).Trim();

                    DB insta = new DB();
                    DataSet result = insta.SaveEmpDetailsUpdate(ClientCoIndivID
                        , Gender, Band, Department);

                    if (result.Tables[0].Rows.Count > 0)
                    {
                        responseclass.IsSucess = Convert.ToBoolean(result.Tables[0].Rows[0]["IsSucess"]);
                        responseclass.Message = Convert.ToString(result.Tables[0].Rows[0]["MsgStr"]);
                    }
                    else
                    {
                        responseclass.IsSucess = false;
                        responseclass.Message = "No Band Found for the Employee.";
                    }
                }
                catch  (Exception aaaa)
                {
                    responseclass.IsSucess = false;
                    responseclass.Message = "No match found for identifier";
                }
            }
            //}
            //else
            //{
            //    if (BandName == "GB08" || BandName == "GB09" || BandName == "GB10")
            //    {
            //        responseclass.IsSucess = true;
            //        if (BandName == "GB08" || BandName == "GB09")
            //        {
            //            responseclass.Message = "CC";
            //        }
            //        else
            //        {
            //            responseclass.Message = "Cr";
            //        }
            //    }
            //    else
            //    {
            //        responseclass.IsSucess = false;
            //        responseclass.Message = "Only GB08+ and Personal Secretaries are authorized to book cabs.";
            //    }
            //}
            return responseclass;
        }
    }
}