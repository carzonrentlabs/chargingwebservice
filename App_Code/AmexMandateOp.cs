﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using ChargingWebService;
using Jose;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
/// <summary>
/// Summary description for AmexMandateOp
/// </summary>
public class AmexMandateOp
{
    public AmexMandateOp()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public string CheckValidMandate(string mandateID)
    {
        var pgresponse = string.Empty;
        try
        {
            //string mandateID;
            var RetriveMnadteUrl = ConfigurationManager.AppSettings["RetriveMnadteUrl"];
            var merchantKey = ConfigurationManager.AppSettings["BilldeskMerchantID"];
            var clientID = ConfigurationManager.AppSettings["BilldeskClientID"];
            // mandateID = "MA1BC500125507";
            GetMandate objMandate = new GetMandate();
            objMandate.mercid = merchantKey;
            objMandate.mandateId = mandateID;


            string postData = JsonConvert.SerializeObject(objMandate);

            var payload = new
            {

                postData = JsonConvert.SerializeObject(objMandate)

            };
            Int32 unixTimestamp = (Int32)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            var headers = new Dictionary<string, object>()
            {
                { "alg", "HS256"},
                {"clientid", clientID},


            };
            var secretKey = ConfigurationManager.AppSettings["BilldeskEncryptionkey"];
            Jwk key = new Jwk(Encoding.UTF8.GetBytes(secretKey));
            var token = Jose.JWT.Encode(postData, key, JwsAlgorithm.HS256, headers);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var bdTimestamp = unixTimestamp.ToString();
            var traceid = Guid.NewGuid().ToString("N").Substring(0, 15);
            var client = new RestClient(RetriveMnadteUrl);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Accept", "application/jose");
            request.AddHeader("Content-Type", "application/jose");
            request.AddHeader("BD-Traceid", traceid);
            request.AddHeader("BD-Timestamp", bdTimestamp);
            var body = token;
            request.AddParameter("application/jose", body, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            string result = response.Content;
            // Console.WriteLine(response.Content);
            pgresponse = Jose.JWT.Decode(result, key, JwsAlgorithm.HS256);
            //ErrorLog.LoginfoToLogFile(pgresponse ," Invoice decryprted response ");

            ErrorLog.LoginfoToLogFile("***********************@Check valid Mandate*********************", "&&\n");

            ErrorLog.LoginfoToLogFile("request check valid mandate traceId " + traceid 
                + " Time Stamp " + bdTimestamp + "", postData);
            ErrorLog.LoginfoToLogFile("@Response", "&&\n");

            ErrorLog.LoginfoToLogFile("response check valid mandate traceId " 
                + traceid + " Time Stamp " + bdTimestamp + "", pgresponse);
            ErrorLog.LoginfoToLogFile("**********************************************************************", "&&\n");
            JObject jObj = JObject.Parse(pgresponse);

        }
        catch (Exception ex)
        {

            ErrorLog.LoginfoToLogFile(ex.Message, "Error Message");
        }
        return pgresponse;

    }

    public string GetMandateID(string TravellerID)
    {
        var pgresponse = string.Empty;
        try
        {
            //string mandateID;
            var RetriveMnadteUrl = ConfigurationManager.AppSettings["RetriveMandateListUrl"];
            var merchantKey = ConfigurationManager.AppSettings["BilldeskMerchantID"];
            var clientID = ConfigurationManager.AppSettings["BilldeskClientID"];
            // mandateID = "MA1BC500125507";
            //GetMandate objMandate = new GetMandate();
            //objMandate.mercid = merchantKey;
            //objMandate.mandateId = mandateID;
            OL_RetrieveMandateID objMandate = new OL_RetrieveMandateID();
            objMandate.mercid = merchantKey;
            //objMandate.customer_refid = "Cus" + Convert.ToString(ClientCoindivID);
            //objMandate.subscription_refid  = "Sub" + Convert.ToString(ClientCoindivID);

            objMandate.customer_refid = "Cus" + TravellerID;
            objMandate.subscription_refid = "Sub" + TravellerID;

            string postData = JsonConvert.SerializeObject(objMandate);

            var payload = new
            {

                postData = JsonConvert.SerializeObject(objMandate)

            };
            Int32 unixTimestamp = (Int32)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            var headers = new Dictionary<string, object>()
            {
                { "alg", "HS256"},
                {"clientid", clientID},


            };
            var secretKey = ConfigurationManager.AppSettings["BilldeskEncryptionkey"];
            Jwk key = new Jwk(Encoding.UTF8.GetBytes(secretKey));
            var token = Jose.JWT.Encode(postData, key, JwsAlgorithm.HS256, headers);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var bdTimestamp = unixTimestamp.ToString();
            var traceid = Guid.NewGuid().ToString("N").Substring(0, 15);
            var client = new RestClient(RetriveMnadteUrl);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Accept", "application/jose");
            request.AddHeader("Content-Type", "application/jose");
            request.AddHeader("BD-Traceid", traceid);
            request.AddHeader("BD-Timestamp", bdTimestamp);
            var body = token;
            request.AddParameter("application/jose", body, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            string result = response.Content;
            // Console.WriteLine(response.Content);
            pgresponse = Jose.JWT.Decode(result, key, JwsAlgorithm.HS256);
            //ErrorLog.LoginfoToLogFile(pgresponse ," Invoice decryprted response ");

            ErrorLog.LoginfoToLogFile("***********************@Get MandateID*********************", "&&\n");

            ErrorLog.LoginfoToLogFile("Request Get MandateID traceid " 
                + traceid + " Time Stamp " + bdTimestamp + "", postData);
            ErrorLog.LoginfoToLogFile("@Response", "&&\n");

            ErrorLog.LoginfoToLogFile("Response Get MandateID traceId " 
                + traceid + " Time Stamp " + bdTimestamp + "", pgresponse);
            ErrorLog.LoginfoToLogFile("**********************************************************************", "&&\n");
            JObject jObj = JObject.Parse(pgresponse);

        }
        catch (Exception ex)
        {

            ErrorLog.LoginfoToLogFile(ex.Message, "Error Message");
        }
        return pgresponse;

    }
}