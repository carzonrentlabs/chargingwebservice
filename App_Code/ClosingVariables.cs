﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CCValidationData
/// </summary>
public class ClosingVariables
{
    public ClosingVariables()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public bool CCRegisteredYN { get; set; }
    public bool CustomPkgYN { get; set; }
    public bool CCAvenue { get; set; }
    public bool OutstationYN { get; set; }
    public string Service { get; set; }
    public int ServiceUnitID { get; set; }
    public int CityID { get; set; }
    public string Status { get; set; }
    public DateTime DateClose { get; set; }
    public string TimeClose { get; set; }
    public DateTime DateIn { get; set; }
    public DateTime DateOut { get; set; }
    public string TimeIn { get; set; }
    public string TimeOut { get; set; }
    public Int64 KMClose { get; set; }
    public Int64 KMIn { get; set; }
    public Int64 kmout { get; set; }
    public Int64 pointOpeniningKm { get; set; }
    public DateTime GuestOpDate { get; set; }
    public DateTime GuestClDate { get; set; }
    public string GuestOpTime { get; set; }
    public string GuestClTime { get; set; }
    public int ClientCoID { get; set; }
    public int CCType { get; set; }
    public double FXDGarageRate { get; set; }
    public double ApprovalAmount { get; set; }
    public string ApprovalNo { get; set; }
    public int ParkTollChages { get; set; }
    public int InterstateTax { get; set; }
    public Int64 PkgID { get; set; }
    public double ExtraHr { get; set; }
    public int ExtraKM { get; set; }
    public double ExtraHrRate { get; set; }
    public double ExtraKMRate { get; set; }
    public double NightStayAmt { get; set; }
    public double OutStnAmt { get; set; }

    public double PkgRate { get; set; }
    public double FuelSurcharge { get; set; }
    public double TotalHrsUsed { get; set; }
    public bool BookingForApprovalYN { get; set; }
    public int NoNight { get; set; }
    public string PaymentMode { get; set; }
    public int CarID { get; set; }
    public int ChauffeurID { get; set; }
    public bool VendorCarYN { get; set; }
    public bool VendorChauffYN { get; set; }
    public double ServiceTaxPercent { get; set; }
    public double EduCessPercent { get; set; }
    public double HduCessPercent { get; set; }
    public double DSTPercent { get; set; }
    public bool PreAuthNotRequire { get; set; }
    public double TotalCost { get; set; }
    public double CalculatedAmount { get; set; }
    public int PaidFullYN { get; set; }
    public DateTime PaidFullDate { get; set; }
    public string Phone1 { get; set; }
    public string CCNo { get; set; }
    public string BillingBasis { get; set; }
    public double ServiceTaxAmt { get; set; }
    public double HduTaxAmt { get; set; }
    public double EduTaxAmt { get; set; }
    public double VatAmt { get; set; }
    public double Others { get; set; }
    public double SwachhBharatTaxPercent { get; set; }
    public double SwachhBharatTaxAmt { get; set; }
    public bool applyservicetaxyn { get; set; }
    public double KrishiKalyanTaxPercent { get; set; }
    public double KrishiKalyanTaxAmt { get; set; }

    public double CGSTTaxPercent { get; set; }
    public double SGSTTaxPercent { get; set; }
    public double IGSTTaxPercent { get; set; }
    public double CGSTTaxAmt { get; set; }
    public double SGSTTaxAmt { get; set; }
    public double IGSTTaxAmt { get; set; }
    public int ClientGSTId { get; set; }

    //public string KrishiYN { get; set; }
    public double VoucherAmt { get; set; }
    public string MerchantID { get; set; }
    public string BookingStatus { get; set; }
    public string DSStatus { get; set; }
    public int Flag { get; set; }
    public int DSChauff { get; set; }
    public int NRTDSChauff { get; set; }

    public bool ChargingNRequired { get; set; }

    public bool AmexLinkYN { get; set; }
    public string hdncontinueYN { get; set; }
    public bool AmexSecondaryChargingYN { get; set; }
    public string EditCorDrive { get; set; }

    public string NewCCNo { get; set; }

    public string authorizationid { get; set; }
    public Int64 ClientCoIndivID { get; set; }
    public bool isPaymateCorporateModule { get; set; }
    public string EmailID { get; set; }
    public string ServiceType { get; set; }
    public string CityName { get; set; }

    public string ErrorCode { get; set; }
    //public string ErrorMessage { get; set; }
    public string transactionid { get; set; }
    //public string InvoiceNo { get; set; }
    //public double Amount { get; set; }
    //public string TravellerId { get; set; }

    public string CaptureTransactionID { get; set; }
    public double CaptureAmount { get; set; }
    //public string RefundTransactionID { get; set; }
    //public double RefundAmount { get; set; }
    public Boolean PreAuthYN { get; set; }
    public string TrackingID { get; set; }
    public string PaymateSysRegCode { get; set; }

    //Add by Bk Sharma date 11-11-2016
    public int BookingId { get; set; }
    public int CarCatId { get; set; }
    public int PickupCityID { get; set; }
    public int DropOffCityId { get; set; }
    public int CarModelId { get; set; }
    public string WaitingHr { get; set; }
    public string WaitingMi { get; set; }
    public double PkgHr { get; set; }
    public double PkgKM { get; set; }
    public double ThresholdExtraHr { get; set; }
    public Nullable<double> TotalServiceTaxPer { get; set; }
    public Nullable<double> IndicatedDiscAmt { get; set; }
    public string BookIndivID { get; set; }
    public Nullable<double> WaitingCharge { get; set; }
    public Nullable<double> TotalWaitingTimeMinute { get; set; }
    public Nullable<double> ExtraThresholdMinute { get; set; }
    public Nullable<int> NoDays { get; set; }
    public string WaitingTime { get; set; }
    public Nullable<double> WaitingMinute { get; set; }
    public Nullable<double> BasicRevenue { get; set; }
    public Nullable<double> BasisWithServiceTax { get; set; }
    public bool AutoPostBooking { get; set; }
    public string InterUnitYN { get; set; }
    public string ExpYYMM { get; set; }

    public int CreatedBy { get; set; }
    public string trackid { get; set; }

    public bool ProvisionalInvoiceForGuestYN { get; set; }
    public bool ProvisionalInvoiceForFacilitatorYN { get; set; }
    public bool EliteCarYN { get; set; }
    public bool ProvisionalInvoiceForTravelDeskYN { get; set; }

    public int SubsidiaryID { get; set; }
    public double Basic { get; set; }
    public double BasicBeforeDiscount { get; set; }
    public double ExtraAmount { get; set; }

    public bool AllowClosingYN { get; set; }
    public double GSTSurchargeAmount { get; set; }

    public string NewChargingLink { get; set; }
    public string chargingstatus { get; set; }
    public DateTime Pickupdate { get; set; }
    public string BatchStatus { get; set; }

    public int LockYN { get; set; }
    public string LockReason { get; set; }

    public bool SendApprovalLinkYN { get; set; }

    public string Remarks { get; set; }

    public string AmexBatch { get; set; }

    public double DiscountPC { get; set; }
    public double DiscountAmt { get; set; }
    //public double DiscountID { get; set; }

    public int ServiceTypeID { get; set; }

    public bool PendingYn { get; set; }

    public bool ApproveYN { get; set; }

    public double IndicatedPrice { get; set; }

    public double PlatFormFee { get; set; }
    //public int AmexBatchYN { get; set; }
    public DateTime Accountingdate { get; set; }

    public double SanatizationCharges { get; set; }

    public bool NewMVPreauthYN { get; set; }

    public double AppDistance { get; set; }
    public double OdometerDistance { get; set; }
    public double GPSDistance { get; set; }
    public double GPSDistanceFromGarage { get; set; }
    public double GPSDistanceToGarage { get; set; }
    public int RentalType { get; set; }
    public int NewCCProcess { get; set; }
    public string CentralizedYN { get; set; }
    public string OriginCode { get; set; }
}