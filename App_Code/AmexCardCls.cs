﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using ChargingWebService;
using Jose;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace ChargingWebService
{
    public class AmexCardCls
    {

        public DataSet GetGuestDetails(string ClientCoIndivID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@ClientCoIndivID", Convert.ToInt64(ClientCoIndivID));
            return SqlHelper.ExecuteDataset("prc_GetGuestDetailsForAmex", param);
        }

        public DataSet GetGuestDetailsForCCAvenue(string ClientCoIndivID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@ClientCoIndivID", Convert.ToInt64(ClientCoIndivID));
            return SqlHelper.ExecuteDataset("prc_GetGuestDetailsForCCAvenue", param);
        }

        public DataSet GetGuestDetailsForCCAvenueFetchOnly(int ClientCoIndivID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@ClientCoIndivID", Convert.ToInt64(ClientCoIndivID));
            return SqlHelper.ExecuteDataset("prc_GetGuestDetailsForCCAvenueFethOnly", param);
        }

        public DataSet GetGuestDetailsForCCAvenueCharging(int ClientCoIndivID, int BookingID)
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@ClientCoIndivID", Convert.ToInt64(ClientCoIndivID));
            param[1] = new SqlParameter("@BookingID", Convert.ToInt64(BookingID));
            return SqlHelper.ExecuteDataset("prc_GetGuestDetailsForCCAvenueCharging", param);
        }
        public DataSet GetGuestDetailsForHDFCFetchOnly(string OrderNo)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@OrderNo", OrderNo);
            return SqlHelper.ExecuteDataset("prc_GetGuestDetailsForCCAvenueFethOnly", param);
        }
        public int UpdateMandateToken(string TravellerId, string MandateCode, string AuthorisationCode)
        {
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@TravellerId", TravellerId);
            param[1] = new SqlParameter("@mandateCode", Convert.ToString(MandateCode));
            param[2] = new SqlParameter("@authorisationCode", Convert.ToString(AuthorisationCode));
            return SqlHelper.ExecuteNonQuery("prc_updateclientMandatecode", param);
        }

        public DataSet BookingDetails(int BookingID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@BookingID", BookingID);
            return SqlHelper.ExecuteDataset("prc_getBookingdetailsAmexUser", param);
        }

        public DataSet BookingDetailsAmexDirectCharge(int BookingID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@BookingID", BookingID);
            return SqlHelper.ExecuteDataset("prc_getBookingdetailsAmexDirectPay", param);
        }

        public DataSet BookingDetailsHDFCDirectCharge(int BookingID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@BookingID", BookingID);
            return SqlHelper.ExecuteDataset("prc_getBookingdetailsHDFCDirectPay", param);
        }

        public DataSet BookingDetailsForTransaction(int BookingID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@BookingID", BookingID);
            return SqlHelper.ExecuteDataset("prc_getBookingdetailsAmexUserForCharged", param);
        }


        public int UpdateSaveCardDetails(string TravellerId, string CardStatus, string ErrorCode, string ErrorType, string ErrorDesc
            , string MandateID, int flag, string CardNum, string PaymentAccountID, string CardHolderName)
        {
            SqlParameter[] param = new SqlParameter[10];
            param[0] = new SqlParameter("@MandateID", MandateID);
            param[1] = new SqlParameter("@TravellerId", TravellerId);
            param[2] = new SqlParameter("@CardStatus", CardStatus);
            param[3] = new SqlParameter("@ErrorCode", ErrorCode);
            param[4] = new SqlParameter("@ErrorType", ErrorType);
            param[5] = new SqlParameter("@ErrorDesc", ErrorDesc);
            param[6] = new SqlParameter("@Flag", flag);
            param[7] = new SqlParameter("@CardMaskNum", CardNum);
            param[8] = new SqlParameter("@PaymnetAcID", PaymentAccountID);
            param[9] = new SqlParameter("@CardHolderName", CardHolderName);
            //return SqlHelper.ExecuteNonQuery("prc_UpdateSaveCardStatus", param);
            return SqlHelper.ExecuteNonQuery("prc_UpdateSaveCardStatus_new", param);

        }

        public int UpdateCCAvenueSaveCardDetails(string order_id, string tracking_id
            , string bank_ref_no, string order_status, string payment_mode, string card_name
            , string status_message, string si_ref_no, string si_created, string si_status
            , string si_mer_ref_no)
        {
            ErrorLog.LoginfoToLogFile(order_id
                + "," + tracking_id + "," + bank_ref_no + "," + order_status
                + "," + payment_mode + "," + card_name + "," + status_message
                + "," + si_ref_no + "," + si_created + "," + si_status
                + "," + si_mer_ref_no
                , "UpdateCCAvenueSaveCardDetails");

            SqlParameter[] param = new SqlParameter[11];
            param[0] = new SqlParameter("@order_id", order_id);
            param[1] = new SqlParameter("@tracking_id", tracking_id);
            param[2] = new SqlParameter("@bank_ref_no", bank_ref_no);
            param[3] = new SqlParameter("@order_status", order_status);
            param[4] = new SqlParameter("@payment_mode", payment_mode);
            param[5] = new SqlParameter("@card_name", card_name);
            param[6] = new SqlParameter("@status_message", status_message);
            param[7] = new SqlParameter("@si_ref_no", si_ref_no);
            param[8] = new SqlParameter("@si_created", si_created);
            param[9] = new SqlParameter("@si_status", si_status);
            param[10] = new SqlParameter("@si_mer_ref_no", si_mer_ref_no);
            return SqlHelper.ExecuteNonQuery("prc_UpdateSaveCardStatus_CCAvenue", param);
        }

        public int UpdateBillDeskInvoice(int BookingID, string BilldeskInvID, string BDInvoiceReqNum, string status, string ErrorCode, string ErrorType, string ErrorDesc)
        {
            SqlParameter[] param = new SqlParameter[7];
            param[0] = new SqlParameter("@bookingID", BookingID);
            param[1] = new SqlParameter("@billDeskInvoiceID", BilldeskInvID);
            param[2] = new SqlParameter("@billDeskReqNum", BDInvoiceReqNum);
            param[3] = new SqlParameter("@status", status);
            param[4] = new SqlParameter("@errorCode", ErrorCode);
            param[5] = new SqlParameter("@errorType", ErrorType);
            param[6] = new SqlParameter("@errorDesc", ErrorDesc);
            return SqlHelper.ExecuteNonQuery("prc_UpdatePredebitInvStatus", param);
        }

        public int UpdateHDFCChargingDetails(int BookingID, string order_id
    , string tracking_id, string bank_ref_no, string order_status
    , string payment_mode, string card_name, string status_message
    , bool ChargedYN)
        {
            ErrorLog.LoginfoToLogFile(BookingID + "," + order_id
                + "," + tracking_id + "," + bank_ref_no + "," + order_status
                + "," + payment_mode + "," + card_name + "," + status_message
                , "UpdateHDFCChargingDetails");

            SqlParameter[] param = new SqlParameter[9];
            param[0] = new SqlParameter("@order_id", order_id);
            param[1] = new SqlParameter("@tracking_id", tracking_id);
            param[2] = new SqlParameter("@bank_ref_no", bank_ref_no);
            param[3] = new SqlParameter("@order_status", order_status);
            param[4] = new SqlParameter("@payment_mode", payment_mode);
            param[5] = new SqlParameter("@card_name", card_name);
            param[6] = new SqlParameter("@status_message", status_message);
            param[7] = new SqlParameter("@BookingId", BookingID);
            param[8] = new SqlParameter("@ChargedYN", ChargedYN);
            return SqlHelper.ExecuteNonQuery("prc_UpdateHDFCChargingStatus", param);
        }


        public int UpdateCCAvenueAmexChargingDetails(int BookingID, string order_id
        , string tracking_id, string bank_ref_no, string order_status
        , string payment_mode, string card_name, string status_message
        , bool ChargedYN)
        {
            ErrorLog.LoginfoToLogFile(BookingID + "," + order_id
                + "," + tracking_id + "," + bank_ref_no + "," + order_status
                + "," + payment_mode + "," + card_name + "," + status_message
                , "UpdateCCAvenueAmexChargingDetails");

            SqlParameter[] param = new SqlParameter[9];
            param[0] = new SqlParameter("@order_id", order_id);
            param[1] = new SqlParameter("@tracking_id", tracking_id);
            param[2] = new SqlParameter("@bank_ref_no", bank_ref_no);
            param[3] = new SqlParameter("@order_status", order_status);
            param[4] = new SqlParameter("@payment_mode", payment_mode);
            param[5] = new SqlParameter("@card_name", card_name);
            param[6] = new SqlParameter("@status_message", status_message);
            param[7] = new SqlParameter("@BookingId", BookingID);
            param[8] = new SqlParameter("@ChargedYN", ChargedYN);
            return SqlHelper.ExecuteNonQuery("prc_UpdateCCAvenueAmexChargingStatus", param);
        }

        public int UpdateBillDeskChargedDetails(int BookingID, string transactionID, string orderID, string authStatus, string chargeAmount, string bankid,
            string bankRefNo, string cardMaskedValue, string ErrorCode, string ErrorType, string ErrorDesc, string authCode, string transactionDate)
        {
            SqlParameter[] param = new SqlParameter[12];
            param[0] = new SqlParameter("@OrderID", orderID);
            param[1] = new SqlParameter("@AutStatus", authStatus);
            param[2] = new SqlParameter("@bookingID", BookingID);
            param[3] = new SqlParameter("@ErrorCode", ErrorCode);
            param[4] = new SqlParameter("@TransactionID", transactionID);
            param[5] = new SqlParameter("@ErrorMesage", ErrorDesc);
            param[6] = new SqlParameter("@Amount", Convert.ToDecimal(chargeAmount));
            param[7] = new SqlParameter("@BankId", bankid);
            param[8] = new SqlParameter("@BankRefNum", bankRefNo);
            param[9] = new SqlParameter("@AuthCode", authCode);
            param[10] = new SqlParameter("@CardMaskValue", cardMaskedValue);
            param[11] = new SqlParameter("@TransactionDate", transactionDate);
            return SqlHelper.ExecuteNonQuery("prc_UpdateAmexChargedDetails", param);

        }

        public DataSet GetGuestDetailforDeleteMandate(string ClientCoIndivID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@ClientCoIndivID", Convert.ToInt64(ClientCoIndivID));
            return SqlHelper.ExecuteDataset("prc_GetGuestDetailsForDeleteMandate", param);
        }


        public int CancelMandateToken(int ClinetCoIndivID, string MandateCode, string AuthorisationCode)
        {
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@clientCoindivID", Convert.ToInt64(ClinetCoIndivID));
            param[1] = new SqlParameter("@mandateCode", Convert.ToString(MandateCode));
            param[2] = new SqlParameter("@authorisationCode", Convert.ToString(AuthorisationCode));
            return SqlHelper.ExecuteNonQuery("prc_CancelclientMandate", param);
        }

        public void createInvoice(string BookingID)
        {
            try
            {
                // string BookingID;
                var PreDebitUrl = ConfigurationManager.AppSettings["BilldeskPreDebitNoteUrl"];
                var merchantKey = ConfigurationManager.AppSettings["BilldeskMerchantID"];
                var clientID = ConfigurationManager.AppSettings["BilldeskClientID"];
                //BookingID = "10437123";//"10437536"; //"10437529";// Request.QueryString["ClientCoIndivID"].ToString();
                String BDInvoiceReqNum = DateTime.Now.ToString("HH:mm:ss").Replace(":", "") + BookingID;
                AmexCardCls objAmex = new AmexCardCls();
                //objRegistrationAmex_OL = new clsRegistration();
                CreateInvoice objCreateInvoice = new CreateInvoice();
                DataSet ds = new DataSet();
                // ds = objAmex.BookingDetails(Convert.ToInt32(BookingID));
                ds = BookingDetails(Convert.ToInt32(BookingID));
                if (ds.Tables[0].Rows.Count > 0)
                {

                    objCreateInvoice.mercid = merchantKey;
                    //objCreateInvoice.subscription_refid = "Sub" + ds.Tables[0].Rows[0]["ClientCoIndivID"].ToString();
                    objCreateInvoice.subscription_refid = "Sub" + ds.Tables[0].Rows[0]["TravellerId"].ToString();
                    //objCreateInvoice.customer_refid = "Cus" + ds.Tables[0].Rows[0]["ClientCoIndivID"].ToString();
                    objCreateInvoice.customer_refid = "Cus" + ds.Tables[0].Rows[0]["TravellerId"].ToString();
                    objCreateInvoice.additional_info = new AdditionalInfoInvoice();
                    objCreateInvoice.additional_info.additional_info1 = "NA";
                    objCreateInvoice.additional_info.additional_info2 = "NA";
                    objCreateInvoice.additional_info.additional_info3 = "NA";
                    objCreateInvoice.additional_info.additional_info4 = "NA";
                    objCreateInvoice.additional_info.additional_info5 = "NA";
                    objCreateInvoice.additional_info.additional_info6 = "NA";
                    objCreateInvoice.additional_info.additional_info7 = "NA";
                    objCreateInvoice.additional_info.additional_info8 = "NA";
                    objCreateInvoice.additional_info.additional_info9 = "NA";
                    objCreateInvoice.additional_info.additional_info10 = "NA";
                    objCreateInvoice.invoice_number = BDInvoiceReqNum;//DateTime.Now.ToString("HH:mm:ss").Replace(":", "") + BookingID;
                    objCreateInvoice.invoice_display_number = BookingID;
                    objCreateInvoice.invoice_date = Convert.ToDateTime(ds.Tables[0].Rows[0]["PickUpDate"]).ToString("yyyy'-'MM'-'dd''");
                    //objCreateInvoice.duedate = Convert.ToDateTime(ds.Tables[0].Rows[0]["debitDate"]).ToString("yyyy'-'MM'-'dd''");
                    //objCreateInvoice.debit_date = Convert.ToDateTime(ds.Tables[0].Rows[0]["debitDate"]).ToString("yyyy'-'MM'-'dd''");
                    objCreateInvoice.duedate = DateTime.Now.AddDays(1).ToString("yyyy'-'MM'-'dd''");
                    objCreateInvoice.debit_date = DateTime.Now.AddDays(1).ToString("yyyy'-'MM'-'dd''");



                    objCreateInvoice.amount = ds.Tables[0].Rows[0]["TotalCost"].ToString();
                    //objCreateInvoice.early_payment_duedate = "";
                    //objCreateInvoice.early_payment_discount = "";
                    //objCreateInvoice.early_payment_amount = "";
                    //objCreateInvoice.late_payment_charges = "";
                    //objCreateInvoice.late_payment_amount = "";
                    objCreateInvoice.net_amount = ds.Tables[0].Rows[0]["TotalCost"].ToString();
                    objCreateInvoice.currency = "356";
                    objCreateInvoice.mandateid = Convert.ToString(ds.Tables[0].Rows[0]["MandateID"]);
                    objCreateInvoice.description = "Create Invoice before 24 hours.";


                }
                else
                {
                    ErrorLog.LoginfoToLogFile("Please provide valid Invoice Number", " Message " + BookingID + "&");
                    //Session["ResponseText"] = "Plesae Provide valid Invoice Number.";
                    //Response.Redirect("Message.aspx");
                }

                string postData = JsonConvert.SerializeObject(objCreateInvoice);

                var payload = new
                {

                    postData = JsonConvert.SerializeObject(objCreateInvoice)

                };
                Int32 unixTimestamp = (Int32)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                var headers = new Dictionary<string, object>()
            {
                { "alg", "HS256"},
                {"clientid", clientID},


            };
                var secretKey = ConfigurationManager.AppSettings["BilldeskEncryptionkey"];
                Jwk key = new Jwk(Encoding.UTF8.GetBytes(secretKey));
                var token = Jose.JWT.Encode(postData, key, JwsAlgorithm.HS256, headers);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                var bdTimestamp = unixTimestamp.ToString();
                var traceid = Guid.NewGuid().ToString("N").Substring(0, 15);
                var client = new RestClient(PreDebitUrl);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Accept", "application/jose");
                request.AddHeader("Content-Type", "application/jose");
                request.AddHeader("BD-Traceid", traceid);
                request.AddHeader("BD-Timestamp", bdTimestamp);
                var body = token;
                request.AddParameter("application/jose", body, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                string result = response.Content;
                // Console.WriteLine(response.Content);
                var pgresponse = Jose.JWT.Decode(result, key, JwsAlgorithm.HS256);
                //ErrorLog.LoginfoToLogFile(pgresponse ," Invoice decryprted response ");

                ErrorLog.LoginfoToLogFile("***********************@Request AmexPreDebitNote*********************", "&&\n");
                ErrorLog.LoginfoToLogFile("For predebit Notification booking " + BookingID + "  TraceID " + traceid + " and timeStamp "
                    + bdTimestamp + " ", token);
                ErrorLog.LoginfoToLogFile("&&", "&&\n");
                ErrorLog.LoginfoToLogFile("For predebit Notification booking " + BookingID + "  TraceID " + traceid + " and timeStamp "
                    + bdTimestamp + " ", postData);
                ErrorLog.LoginfoToLogFile("@Response", "&&\n");
                ErrorLog.LoginfoToLogFile("For predebit Notification booking " + BookingID + "  TraceID " + traceid + " and timeStamp "
                    + bdTimestamp + " ", result);
                ErrorLog.LoginfoToLogFile("&&", "&&\n");
                ErrorLog.LoginfoToLogFile("For predebit Notifacation booking " + BookingID + "  TraceID " + traceid + " and timeStamp "
                    + bdTimestamp + " ", pgresponse);
                ErrorLog.LoginfoToLogFile("**********************************************************************", "&&\n");
                JObject jObj = JObject.Parse(pgresponse);
                string billDeskInvoiceID = (string)jObj["invoice_id"];
                string verificationErrorCode = string.Empty;
                string verificationErroDesc = string.Empty;
                string verificationErroType = string.Empty;
                string status = (string)jObj["status"];
                if (billDeskInvoiceID != null)
                {
                    verificationErrorCode = (string)jObj["verification_error_code"];
                    verificationErroDesc = (string)jObj["verification_error_desc"];
                    verificationErroType = (string)jObj["verification_error_type"];
                }
                else
                {
                    verificationErrorCode = (string)jObj["error_code"];
                    verificationErroDesc = (string)jObj["message"];
                    verificationErroType = (string)jObj["error_type"];
                }
                //int resultCode = objAmex.UpdateBillDeskInvoice(Convert.ToInt32(BookingID), billDeskInvoiceID, BDInvoiceReqNum
                //, status, verificationErrorCode, verificationErroType, verificationErroDesc);
                int resultCode = UpdateBillDeskInvoice(Convert.ToInt32(BookingID), billDeskInvoiceID, BDInvoiceReqNum, status
                    , verificationErrorCode, verificationErroType, verificationErroDesc);
            }
            catch (Exception ex)
            {

                ErrorLog.LoginfoToLogFile(ex.Message, "Error Message");
            }

        }

        public PreAuthResult CreateTransaction(string BookingID)
        {
            PreAuthResult objPvc = new PreAuthResult();
            string chargingStatus = string.Empty;
            try
            {

                //string BookingID;
                var ChargedUrl = ConfigurationManager.AppSettings["BilldeskChargedUrl"];
                var merchantKey = ConfigurationManager.AppSettings["BilldeskMerchantID"];
                var clientID = ConfigurationManager.AppSettings["BilldeskClientID"];
                //  BookingID = "10437123";//"10437536";//"10437529";// Request.QueryString["ClientCoIndivID"].ToString();
                // AmexCardCls objAmex = new AmexCardCls();
                //objRegistrationAmex_OL = new clsRegistration();
                //AmexTransaction objMakeTransaction = new AmexTransaction();
                CreateTransaction objMakeTransaction = new CreateTransaction();
                DataSet ds = new DataSet();

                ds = BookingDetailsForTransaction(Convert.ToInt32(BookingID));

                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (Convert.ToInt32(ds.Tables[0].Rows[0]["HourDiff"].ToString()) >= 24)
                    {
                        objMakeTransaction.mercid = merchantKey;
                        objMakeTransaction.orderid = "ORD" + DateTime.Now.ToString("HH:mm:ss").Replace(":", "") + BookingID;
                        objMakeTransaction.amount = ds.Tables[0].Rows[0]["TotalCost"].ToString();
                        objMakeTransaction.currency = "356";
                        objMakeTransaction.txn_process_type = "si";
                        objMakeTransaction.mandateid = Convert.ToString(ds.Tables[0].Rows[0]["MandateID"]);
                        //objMakeTransaction.subscription_refid = "Sub" + Convert.ToString(ds.Tables[0].Rows[0]["ClientCoIndivID"]);
                        objMakeTransaction.subscription_refid = "Sub" + Convert.ToString(ds.Tables[0].Rows[0]["TravellerId"]);
                        objMakeTransaction.invoice_id = Convert.ToString(ds.Tables[0].Rows[0]["BilldeskInvoiceID"]); ;
                        objMakeTransaction.debit_request_no = "3";
                        objMakeTransaction.additional_info = new AdditionalInfoInvoice();
                        objMakeTransaction.additional_info.additional_info1 = "NA";
                        objMakeTransaction.additional_info.additional_info2 = "NA";
                        objMakeTransaction.additional_info.additional_info3 = "NA";
                        objMakeTransaction.additional_info.additional_info4 = "NA";
                        objMakeTransaction.additional_info.additional_info5 = "NA";
                        objMakeTransaction.additional_info.additional_info6 = "NA";
                        objMakeTransaction.additional_info.additional_info7 = "NA";
                        objMakeTransaction.additional_info.additional_info8 = "NA";
                        objMakeTransaction.additional_info.additional_info9 = "NA";
                        objMakeTransaction.additional_info.additional_info10 = "NA";
                        objMakeTransaction.itemcode = "DIRECT";
                        string postData = JsonConvert.SerializeObject(objMakeTransaction);

                        var payload = new
                        {

                            postData = JsonConvert.SerializeObject(objMakeTransaction)

                        };
                        Int32 unixTimestamp = (Int32)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                        var headers = new Dictionary<string, object>()
                        {
                            { "alg", "HS256"},
                            {"clientid", clientID},
                        };
                        var secretKey = ConfigurationManager.AppSettings["BilldeskEncryptionkey"];
                        Jwk key = new Jwk(Encoding.UTF8.GetBytes(secretKey));
                        var token = Jose.JWT.Encode(postData, key, JwsAlgorithm.HS256, headers);
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        var bdTimestamp = unixTimestamp.ToString();
                        var traceid = Guid.NewGuid().ToString("N").Substring(0, 15);
                        var client = new RestClient(ChargedUrl);
                        client.Timeout = -1;
                        var request = new RestRequest(Method.POST);
                        request.AddHeader("Accept", "application/jose");
                        request.AddHeader("Content-Type", "application/jose");
                        request.AddHeader("BD-Traceid", traceid);
                        request.AddHeader("BD-Timestamp", bdTimestamp);
                        //var body = @"eyJhbGciOiJIUzI1NiIsImNsaWVudGlkIjoiY2Fyem9uMnVhdCIsIkNvbnRlbnQtVHlwZSI6ImFwcGxpY2F0aW9uL2pvc2UiLCJBY2NlcHQiOiJhcHBsaWNhdGlvbi9qb3NlIiwiQkQtVHJhY2VpZCI6IjA3ZTlkZThhMzBiNDQ0ZGNiZmQxNzhiNWVkNWY1OGFlIiwiQkQtVGltZXN0YW1wIjoiMTY2NDY2NTE0NyJ9.eyJtZXJjaWQiOiJDQVJaT04yVUFUIiwiY3VzdG9tZXJfcmVmaWQiOiJDdXM0MTg5ODM5Iiwic3Vic2NyaXB0aW9uX3JlZmlkIjoiU3ViNDE4OTgzOSIsInN1YnNjcmlwdGlvbl9kZXNjIjoiQ2Fyem9ucmVudCBoYXBweSBjdXN0b21lciIsImN1cnJlbmN5IjoiMzU2IiwiZnJlcXVlbmN5IjoiYWRob2MiLCJhbW91bnRfdHlwZSI6Im1heCIsImFtb3VudCI6IjE1MDAwLjAwIiwic3RhcnRfZGF0ZSI6IjIwMjItMTAtMDEiLCJlbmRfZGF0ZSI6IjIwMzItMTAtMDEiLCJjdXN0b21lciI6eyJmaXJzdF9uYW1lIjoiRGV2ZWxvcGVycyIsImxhc3RfbmFtZSI6Ikt1bWFyIiwibW9iaWxlIjoiOTE5NTYwMjA3NzUwIiwibW9iaWxlX2FsdCI6IiIsImVtYWlsIjoiYmhhcmR3YWpuN0BnbWFpbC5jb20iLCJlbWFpbF9hbHQiOiIifSwiYWRkaXRpb25hbF9pbmZvIjp7ImFkZGl0aW9uYWxfaW5mbzEiOiIiLCJhZGRpdGlvbmFsX2luZm8yIjoiIiwiYWRkaXRpb25hbF9pbmZvMyI6IiJ9LCJkZXZpY2UiOnsiaW5pdF9jaGFubmVsIjoiaW50ZXJuZXQiLCJpcCI6IjE3LjIzMy4xMDcuOTIiLCJtYWMiOiIxMS1BQy01OC0yMS0xQi1BQSIsImltZWkiOiI5OTAwMDAxMTIyMzM0NDUiLCJ1c2VyX2FnZW50IjoiTW96aWxsYS81LjAiLCJhY2NlcHRfaGVhZGVyIjoidGV4dC9odG1sIiwiZmluZ2VycHJpbnRpZCI6IjYxYjEyYzE4YjVkMGNmOTAxYmUzNGEyM2NhNjRiYjE5In0sInJ1IjoiaHR0cHM6Ly9pbnN0YXRlc3QuY2Fyem9ucmVudC5jb20vY2hhcmdpbmd3ZWJzZXJ2aWNlL0JpbGxEZXNrUmVzcG9uc2UuYXNweCJ9.tIAoXCQyOaGrL5QnbxgydTK-duchQ76YCt1zbpMOD8I";
                        var body = token;
                        request.AddParameter("application/jose", body, ParameterType.RequestBody);
                        IRestResponse response = client.Execute(request);
                        string result = response.Content;
                        // Console.WriteLine(response.Content);
                        var pgresponse = Jose.JWT.Decode(result, key, JwsAlgorithm.HS256);
                        ErrorLog.LoginfoToLogFile("******************@Request AmexTranscation*********************", "&&\n");
                        ErrorLog.LoginfoToLogFile("For booking " + BookingID + " headers are TracID " + traceid + " and timeStamp "
                            + bdTimestamp + " ", token);
                        ErrorLog.LoginfoToLogFile("&&", "&&\n");
                        ErrorLog.LoginfoToLogFile("For booking " + BookingID + " headers are TracID " + traceid + " and timeStamp "
                            + bdTimestamp + " ", postData);
                        ErrorLog.LoginfoToLogFile("@Response", "&&\n");
                        ErrorLog.LoginfoToLogFile("Response of transaction booking " + BookingID + " headers are TracID " + traceid
                            + " and timeStamp " + bdTimestamp + " ", result);
                        ErrorLog.LoginfoToLogFile("&&", "&&\n");
                        ErrorLog.LoginfoToLogFile("Response of transaction booking " + BookingID + " headers are TracID " + traceid
                            + " and timeStamp " + bdTimestamp + " ", pgresponse);
                        ErrorLog.LoginfoToLogFile("**************************************************************", "&&\n");
                        JObject jObj = JObject.Parse(pgresponse);
                        string authStatus = (string)jObj["auth_status"];
                        string transactionErrorCode = (string)jObj["transaction_error_code"];
                        string transactionErroDesc = (string)jObj["transaction_error_desc"];
                        string transactionErroType = (string)jObj["transaction_error_type"];
                        string transactiondate = (string)jObj["transaction_date"];
                        string transactionID = (string)jObj["transactionid"];
                        string orderID = (string)jObj["orderid"];
                        string chargeAmount = (string)jObj["charge_amount"];
                        if (chargeAmount == null)
                        {
                            chargeAmount = "0";
                        }
                        string bankid = (string)jObj["bankid"];
                        string bankRefNo = (string)jObj["bank_ref_no"];
                        string cardMaskedValue = "";
                        if (authStatus == "0300" && transactionErrorCode == "TRS0000")
                        {
                            objPvc.ChargingStatus = "Success";
                            objPvc.ErrorCode = "TRS0000";
                        }
                        else
                        {
                            objPvc.ChargingStatus = transactionErroDesc;
                            objPvc.ErrorCode = transactionErrorCode;
                        }
                        //else if (authStatus == "0002")
                        //{
                        //    chargingStatus = "0002";
                        //}
                        //else
                        //{
                        //    chargingStatus = "0399";
                        //}

                        string authCode = (string)jObj["authcode"];
                        if (transactionErrorCode == "" || transactionErrorCode == null)
                        {
                            transactionErrorCode = (string)jObj["error_code"];
                            transactionErroDesc = (string)jObj["message"];
                            transactionErroType = (string)jObj["error_type"];
                        }
                        int resultCode = UpdateBillDeskChargedDetails(Convert.ToInt32(BookingID), transactionID, orderID, authStatus
                            , chargeAmount, bankid, bankRefNo, cardMaskedValue, transactionErrorCode, transactionErroType
                            , transactionErroDesc, authCode, transactiondate);
                    }
                    else
                    {
                        ErrorLog.LoginfoToLogFile("Charging can only be done after 24 hours of Debit Note. BookingID=" + BookingID, "CreateTransaction");
                        objPvc.ChargingStatus = "Charging can only be done after 24 hours of Debit Note.";
                        objPvc.ErrorCode = "";
                    }
                }
                else
                {
                    ErrorLog.LoginfoToLogFile("Record not found for " + BookingID, "CreateTransaction");
                    objPvc.ChargingStatus = "No Record found for the BookingID.";
                    objPvc.ErrorCode = "";
                }
            }
            catch (Exception ex)
            {
                objPvc.ChargingStatus = ex.Message;
                objPvc.ErrorCode = "";
                ErrorLog.LoginfoToLogFile(ex.Message, "Error Message");
            }
            return objPvc;
        }
        public int SaveSessionIndicator(int BookingId, string SessionIndicator, string approvalNo)
        {
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@BookingId", BookingId);
            param[1] = new SqlParameter("@SessionIndicator", Convert.ToString(SessionIndicator));
            param[2] = new SqlParameter("@ApprovalNo", Convert.ToString(approvalNo));
            //return SqlHelper.ExecuteNonQuery("prc_saveAmexSessionIndicator", param);
            return SqlHelper.ExecuteNonQuery("prc_saveAmexSessionIndicatorNew", param);
        }

        public int SaveHDFCSession(int BookingId, string approvalNo)
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@BookingId", BookingId);
            param[1] = new SqlParameter("@ApprovalNo", Convert.ToString(approvalNo));
            return SqlHelper.ExecuteNonQuery("prc_saveHDFCSession", param);
        }

        public int SaveAmexSession(int BookingId, string approvalNo)
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@BookingId", BookingId);
            param[1] = new SqlParameter("@ApprovalNo", Convert.ToString(approvalNo));
            return SqlHelper.ExecuteNonQuery("prc_saveAmexSession", param);
        }

        public DataSet GetHDFCOrderDetails(string OrderNo)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@OrderNo", OrderNo);
            return SqlHelper.ExecuteDataset("prc_getGetHDFCOrderDetail", param);
        }

        public DataSet GetAmexOrderDetails(string resultIndicator, int bookingId)
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@ResultIndicator", resultIndicator);
            param[1] = new SqlParameter("@BookingId", bookingId);
            return SqlHelper.ExecuteDataset("prc_getGetAmexOrderDetail1", param);
        }
        public DataSet GetAmexOrderUpdateAmountDetails()
        {
            SqlParameter[] param = new SqlParameter[0];
            return SqlHelper.ExecuteDataset("prc_getGetAmexOrderDetailForAmountUpdate", param);
            // return SqlHelper.ExecuteDataset("prc_getGetAmexOrderDetailForStatUpdate", param);
        }
        public DataSet GetAmexOrderDetailsRefund(string approvalNo)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@ApprovalNo", approvalNo);
            return SqlHelper.ExecuteDataset("prc_getGetAmexOrderDetail1Refund", param);
        }
        public int UpdateOrderPaymentStatus(OrderDetails order, string ChargingMessage
            , string TransactionId, bool ChargedYN, string acsEci, string orderStatus
            , string orderAuthStatus, string responseGateCode
            , string responseGateRecomm, string chargingStatus
            , string chargingResult, string chargingErrorDescription, string chargingErrorCause, string amount)
        {
            SqlParameter[] param = new SqlParameter[16];
            param[0] = new SqlParameter("@BookingId", order.bookingId);
            param[1] = new SqlParameter("@ApprovalNo", order.approvalNo);
            param[2] = new SqlParameter("@ChargingMessage", ChargingMessage);
            param[3] = new SqlParameter("@TransactionId", TransactionId);
            param[4] = new SqlParameter("@ResultIndicator", order.resultIndicator);
            param[5] = new SqlParameter("@ChargedYN", ChargedYN);
            param[6] = new SqlParameter("@AcsEci", acsEci);
            param[7] = new SqlParameter("@OrderStatus", orderStatus);
            param[8] = new SqlParameter("@OrderAuthStatus", orderAuthStatus);
            param[9] = new SqlParameter("@ResponseGateCode", responseGateCode);
            param[10] = new SqlParameter("@ResponseGateRecomm", responseGateRecomm);
            param[11] = new SqlParameter("@chargingStatus", chargingStatus);
            param[12] = new SqlParameter("@ChargingResult", chargingResult);
            param[13] = new SqlParameter("@ChargingErrorDescription", chargingErrorDescription);
            param[14] = new SqlParameter("@ChargingStatus", chargingErrorCause);
            param[15] = new SqlParameter("@Amount", amount);
            //return SqlHelper.ExecuteNonQuery("prc_UpdateInvoiceStatusNewAmex", param);
            //return SqlHelper.ExecuteNonQuery("prc_UpdateInvoiceStatusNewAmex1", param);
            //return SqlHelper.ExecuteNonQuery("prc_UpdateInvoiceStatusNewAmex11", param);
            return SqlHelper.ExecuteNonQuery("prc_UpdateInvoiceStatusNewAmex111", param);
        }
        public int UpdateOrderAmount(OrderDetails order, string amount)
        {
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@BookingId", order.bookingId);
            param[1] = new SqlParameter("@ApprovalNo", order.approvalNo);
            param[2] = new SqlParameter("@Amount", amount);
            var res = SqlHelper.ExecuteNonQuery("PrcUpdateAmexTransactionAmount", param);
            return res;
        }
        public DataSet EncryptDecryptNew(string BKID, string Amount, int Enc_DecYN)
        {
            SqlParameter[] param = new SqlParameter[3];

            param[0] = new SqlParameter("@BookingID", BKID);
            param[1] = new SqlParameter("@Amount", Amount);
            param[2] = new SqlParameter("@Enc_DecYN", Enc_DecYN);

            return SqlHelper.ExecuteDataset("Prc_Encrypt_N_Decrypt", param);
        }
        public int UpdatRefundStatus(OrderDetails order, RefundResponse refundResponse, bool chargedYN, string refundTransactionId)
        {
            SqlParameter[] param = new SqlParameter[6];
            param[0] = new SqlParameter("@BookingId", order.bookingId);
            param[1] = new SqlParameter("@ApprovalNo", order.approvalNo);
            param[2] = new SqlParameter("@RefundAmount", refundResponse.order.totalRefundedAmount);
            param[3] = new SqlParameter("@RefundOrderStatus", refundResponse.order.status);
            param[4] = new SqlParameter("@RefundYN", chargedYN);
            param[5] = new SqlParameter("@RefundTransactionId", refundTransactionId);
            var res = SqlHelper.ExecuteNonQuery("prc_AmexUpdatRefundStatus", param);
            return res;
        }
    }
}