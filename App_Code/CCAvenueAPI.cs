﻿using System;
using System.Data;
using System.Net;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using CCA.Util;
using ChargingWebService;
using RestSharp;

public class CCAvenueAPI
{
    public string CreatePreDebitNoteNotification(int BookingID)
    {
        int ClientCoIndivID;
        string Totalcost;
        DB db = new DB();
        CCAvenueVariable.PredebitNotDetails predebit = new CCAvenueVariable.PredebitNotDetails();
        predebit = db.GetPredebitNotDetails(BookingID);
        ClientCoIndivID = predebit.ClientCoIndivID;
        Totalcost = Convert.ToString(predebit.TotalCost);

        string strparam = "ClientCoIndivID:" + ClientCoIndivID
            + ",BookingID:" + BookingID + ",Totalcost:" + Totalcost;

        string returnstring = "", url = "", workingKey = ""
        , strAccessCode = "", strmerchant_id = "";

        CCACrypto ccaCrypto = new CCACrypto();

        workingKey = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueWorkingKey");
        strAccessCode = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueAccessCode");
        strmerchant_id = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueMerchant_id");
        url = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueServiceURL");

        JavaScriptSerializer js = new JavaScriptSerializer();

        CCAvenueVariable.PredebitNotificationSIRequest request = new CCAvenueVariable.PredebitNotificationSIRequest();
        CCAvenueVariable.PredebitNotificationEncRequest encrequest = new CCAvenueVariable.PredebitNotificationEncRequest();

        AmexCardCls objAmex = new AmexCardCls();
        DataSet ds = new DataSet();
        ds = objAmex.GetGuestDetailsForCCAvenueFetchOnly(ClientCoIndivID);
        string si_sub_ref_no = "", reference_no = "";

        if (ds.Tables[0].Rows.Count > 0)
        {
            reference_no = ds.Tables[0].Rows[0]["reference_no"].ToString();
            si_sub_ref_no = ds.Tables[0].Rows[0]["si_sub_ref_no"].ToString();
        }

        request.access_code = strAccessCode;
        request.request_type = "JSON";
        request.response_type = "JSON";
        request.command = "createPreDebitNotification";
        request.si_sub_ref_no = si_sub_ref_no; //Unique HDFC sub reference number shared at setup SI.
        request.si_mer_charge_ref_no = BookingID.ToString(); //Unique merchant reference number for the SI charge.
        request.si_currency = "INR";
        request.si_amount = Totalcost;
        request.si_mer_ref_no1 = BookingID.ToString();
        request.si_mer_ref_no2 = "REF2";
        request.version = "1.2";


        encrequest.si_sub_ref_no = request.si_sub_ref_no; //Unique HDFC sub reference number shared at setup SI.
        encrequest.si_mer_charge_ref_no = BookingID.ToString(); //Unique merchant reference number for the SI charge.
        encrequest.si_amount = Totalcost;
        encrequest.si_currency = request.si_currency;

        string FinalRequest = JsonConvert.SerializeObject(encrequest);

        request.enc_request = ccaCrypto.Encrypt(FinalRequest, workingKey);

        string FinalRequest1 = JsonConvert.SerializeObject(request);
        ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

        ErrorLog.LoginfoToLogFile(strparam + ",FinalRequest=" + FinalRequest
            , "CreatePreDebitNoteNotification");


        var client = new RestClient(url);
        client.Timeout = -1;
        var reques11 = new RestRequest(Method.POST);
        reques11.AddParameter("enc_request", request.enc_request);
        reques11.AddParameter("access_code", request.access_code);
        reques11.AddParameter("request_type", request.request_type);
        reques11.AddParameter("response_type", request.response_type);
        reques11.AddParameter("command", request.command);
        reques11.AddParameter("si_sub_ref_no", request.si_sub_ref_no);
        reques11.AddParameter("si_mer_charge_ref_no", request.si_mer_charge_ref_no);
        reques11.AddParameter("si_currency", request.si_currency);
        reques11.AddParameter("si_amount", request.si_amount);
        reques11.AddParameter("version", request.version);

        ErrorLog.LoginfoToLogFile(strparam + ",FinalRequest="
            + request.enc_request, "CreatePreDebitNoteNotification");

        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        IRestResponse responseRest = client.Execute(reques11);
        var result = responseRest.Content;

        string[] restul = result.Split('&');
        string[] statusstr = restul[0].Split('=');
        string[] enc_responsestr = restul[1].Split('=');
        string status = statusstr[1];
        string enc_response = enc_responsestr[1].Replace("\r\n", "");

        ErrorLog.LoginfoToLogFile(strparam + ",FinalResponse="
            + enc_response, "CreatePreDebitNoteNotification");

        CCAvenueVariable.PreDebitNoteNotificationResponse response = new CCAvenueVariable.PreDebitNoteNotificationResponse();
        if (status == "0")
        {
            string enc_requestdec = ccaCrypto.Decrypt(enc_response, workingKey);

            ErrorLog.LoginfoToLogFile(strparam + ",FinalResponse="
                + enc_requestdec, "CreatePreDebitNoteNotification");

            response = new JavaScriptSerializer().Deserialize<CCAvenueVariable.PreDebitNoteNotificationResponse>(enc_requestdec);
        }
        else
        {
            response.si_error_desc = enc_response;
        }

        db.SaveCCAvenuePredebitNotificationStatus(BookingID, response);

        return returnstring;
    }

    //public string DeletePreDebitNoteNotification(int BookingID, string Totalcost)
    //{
    //    string returnstring = "", url = "", workingKey = "", strAccessCode = "", strmerchant_id = "";

    //    CCACrypto ccaCrypto = new CCACrypto();

    //    workingKey = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueWorkingKey");
    //    strAccessCode = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueAccessCode");
    //    strmerchant_id = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueMerchant_id");
    //    url = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueServiceURL");

    //    CCAvenueVariable.DeleteDebitNoteNotificationRequest request = new CCAvenueVariable.DeleteDebitNoteNotificationRequest();

    //    request.access_code = strAccessCode;
    //    request.request_type = "JSON";
    //    request.response_type = "JSON";
    //    request.command = "deletePreDebitNotification";
    //    request.si_sub_ref_no = BookingID.ToString(); //Unique HDFC sub reference number shared at setup SI.
    //    request.version = "1.2";

    //    string FinalRequest = JsonConvert.SerializeObject(request);

    //    request.enc_request = ccaCrypto.Encrypt(FinalRequest, workingKey);

    //    ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

    //    ErrorLog.LoginfoToLogFile("FinalRequest=" + FinalRequest, "DeletePreDebitNoteNotification");

    //    CCAvenueVariable.DeleteDebitNoteNotificationResponse response = new CCAvenueVariable.DeleteDebitNoteNotificationResponse();

    //    try
    //    {
    //        var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
    //        httpWebRequest.ContentType = "application/json";
    //        httpWebRequest.Method = "POST";
    //        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
    //        {
    //            string json = FinalRequest;
    //            streamWriter.Write(json);
    //            streamWriter.Flush();
    //            streamWriter.Close();
    //        }

    //        string objResponse = "";
    //        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
    //        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
    //        {
    //            var result = streamReader.ReadToEnd();
    //            objResponse = result.ToString();
    //        }


    //        string encResponse = ccaCrypto.Decrypt(objResponse, workingKey); //decrypt the details

    //        string FinalResponse = JsonConvert.DeserializeObject<string>(encResponse);

    //        ErrorLog.LoginfoToLogFile("FinalResponse=" + FinalResponse, "CreateDebitNoteNotification"); //logs in text file

    //        response = JsonConvert.DeserializeObject<CCAvenueVariable.DeleteDebitNoteNotificationResponse>(FinalResponse);

    //        returnstring = FinalResponse.ToString();

    //        //update DeletePredebitnotification status on success
    //    }
    //    catch (Exception ex)
    //    {
    //        returnstring = ex.ToString();
    //    }

    //    return returnstring;
    //}

    public string ChargeSI(int ClientCoIndivID, int BookingID, string Totalcost)
    {
        string strparam = "ClientCoIndivID:" + ClientCoIndivID
            + ",BookingID:" + BookingID + ",Totalcost:" + Totalcost;

        string returnstring = "", url = "", workingKey = ""
            , strAccessCode = "", strmerchant_id = "";

        CCACrypto ccaCrypto = new CCACrypto();

        workingKey = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueWorkingKey");
        strAccessCode = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueAccessCode");
        strmerchant_id = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueMerchant_id");
        url = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueServiceURL");

        JavaScriptSerializer js = new JavaScriptSerializer();

        CCAvenueVariable.ChargeSIRequest request = new CCAvenueVariable.ChargeSIRequest();
        CCAvenueVariable.ChargeSIEncRequest encrequest = new CCAvenueVariable.ChargeSIEncRequest();

        AmexCardCls objAmex = new AmexCardCls();
        DataSet ds = new DataSet();
        ds = objAmex.GetGuestDetailsForCCAvenueCharging(ClientCoIndivID, BookingID);
        string si_sub_ref_no = "", reference_no = "", ApprovalNo = "", notification_id = "";
        if (ds.Tables[0].Rows.Count > 0)
        {
            reference_no = ds.Tables[0].Rows[0]["reference_no"].ToString();
            si_sub_ref_no = ds.Tables[0].Rows[0]["si_sub_ref_no"].ToString();
            ApprovalNo = ds.Tables[0].Rows[0]["ApprovalNo"].ToString();
            notification_id = ds.Tables[0].Rows[0]["notification_id"].ToString();
        }

        request.access_code = strAccessCode;
        request.request_type = "JSON";
        request.response_type = "JSON";
        request.command = "chargeSI";
        request.si_sub_ref_no = si_sub_ref_no; //"SI2324410033532";//BookingID.ToString(); //Unique HDFC sub reference number shared at setup SI.
        request.si_mer_charge_ref_no = ApprovalNo; //BookingID.ToString(); //"312010264954";//"SIhub"; //Unique merchant reference number for the SI charge.
        request.si_currency = "INR";
        request.si_amount = Totalcost;
        request.si_mer_ref_no1 = BookingID.ToString();
        request.si_mer_ref_no2 = BookingID.ToString();
        request.version = "1.5";

        encrequest.si_sub_ref_no = request.si_sub_ref_no; //Unique HDFC sub reference number shared at setup SI.
        encrequest.si_mer_charge_ref_no = ApprovalNo; // BookingID.ToString(); //Unique merchant reference number for the SI charge.
        encrequest.si_amount = Totalcost;
        encrequest.si_currency = request.si_currency;
        encrequest.notification_id = notification_id;

        string FinalRequest = JsonConvert.SerializeObject(encrequest);

        request.enc_request = ccaCrypto.Encrypt(FinalRequest, workingKey);

        string FinalRequest1 = JsonConvert.SerializeObject(request);

        ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

        //var client = new RestClient("https://logintest.ccavenue.com/apis/servlet/DoWebTrans");
        var client = new RestClient(url);
        client.Timeout = -1;
        var reques11 = new RestRequest(Method.POST);
        reques11.AddParameter("enc_request", request.enc_request);
        reques11.AddParameter("access_code", request.access_code);
        reques11.AddParameter("request_type", request.request_type);
        reques11.AddParameter("response_type", request.response_type);
        reques11.AddParameter("command", request.command);
        reques11.AddParameter("si_sub_ref_no", request.si_sub_ref_no);
        reques11.AddParameter("si_mer_charge_ref_no", request.si_mer_charge_ref_no);
        reques11.AddParameter("si_currency", request.si_currency);
        reques11.AddParameter("si_amount", request.si_amount);
        reques11.AddParameter("notification_id", notification_id);
        reques11.AddParameter("version", request.version);

        ErrorLog.LoginfoToLogFile(strparam + ",FinalRequest="
            + FinalRequest, "ChargeSI");

        ErrorLog.LoginfoToLogFile(strparam + ",FinalRequest enc="
            + request.enc_request, "ChargeSI");

        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        IRestResponse responseRest = client.Execute(reques11);
        var result = responseRest.Content;

        string[] restul = result.Split('&');
        string[] statusstr = restul[0].Split('=');
        string[] enc_responsestr = restul[1].Split('=');
        string status = statusstr[1];
        string enc_response = enc_responsestr[1].Replace("\r\n", "");

        ErrorLog.LoginfoToLogFile(strparam + ",FinalResponse=" + enc_response, "ChargeSI");

        CCAvenueVariable.ChargeSIResponse response = new CCAvenueVariable.ChargeSIResponse();
        if (status == "0")
        {
            string enc_requestdec = ccaCrypto.Decrypt(enc_response, workingKey);

            ErrorLog.LoginfoToLogFile(strparam + ",FinalResponse=" + enc_requestdec, "ChargeSI");

            response = new JavaScriptSerializer().Deserialize<CCAvenueVariable.ChargeSIResponse>(enc_requestdec);
        }
        else
        {
            response.si_error_desc = enc_response;
        }

        DB db = new DB();

        db.SaveCCAvenueChargingStatus(BookingID, response);

        if (response.si_charge_status == "0"
            && response.si_charge_txn_status == "0")
        {
            returnstring = "Success";
            db.CloseBooking_UpdateStatus(BookingID, "C", "C");
        }
        else
        {
            returnstring = response.si_error_desc;
        }
        return returnstring;
    }

    public string GetSIStatus(int ClientCoIndivID)
    {
        ErrorLog.LoginfoToLogFile("ClientcoIndivid=" + ClientCoIndivID, "GetSIStatus");
        string returnstring = "";
        string url = "", workingKey = "", strAccessCode = "", strmerchant_id = "";

        CCACrypto ccaCrypto = new CCACrypto();

        AmexCardCls objAmex = new AmexCardCls();
        DataSet ds = new DataSet();
        ds = objAmex.GetGuestDetailsForCCAvenueFetchOnly(ClientCoIndivID);
        string si_sub_ref_no = "", reference_no = "";
        string CardName = "Amex";
        if (ds.Tables[0].Rows.Count > 0)
        {
            reference_no = ds.Tables[0].Rows[0]["reference_no"].ToString();
            si_sub_ref_no = ds.Tables[0].Rows[0]["si_sub_ref_no"].ToString();
            CardName = ds.Tables[0].Rows[0]["CardName"].ToString();
        }

        if (CardName == "Amex")
        {
            workingKey = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueWorkingKey");
            strAccessCode = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueAccessCode");
            strmerchant_id = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueMerchant_id");
            url = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueServiceURL");
        }
        else
        {
            workingKey = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueWorkingKeyMV");
            strAccessCode = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueAccessCodeMV");
            strmerchant_id = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueMVMerchant_id");
            url = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueServiceURL");
        }
        JavaScriptSerializer js = new JavaScriptSerializer();

        CCAvenueVariable.GetSIStatusRequest request = new CCAvenueVariable.GetSIStatusRequest();
        CCAvenueVariable.GetSIStatusEncRequest encrequest = new CCAvenueVariable.GetSIStatusEncRequest();
        request.access_code = strAccessCode;
        request.request_type = "JSON";
        request.response_type = "JSON";
        request.command = "getSIStatus";
        request.si_sub_ref_no = si_sub_ref_no; //"SI2324310033504"; //Unique HDFC sub reference number shared at setup SI.
        request.reference_no = reference_no; //"312010256949"; //Unique merchant reference number for the SI charge.
        request.version = "1.1";

        encrequest.si_sub_ref_no = request.si_sub_ref_no; //Unique HDFC sub reference number shared at setup SI.
        encrequest.reference_no = request.reference_no; //Unique merchant reference number for the SI charge.

        string FinalRequest = JsonConvert.SerializeObject(encrequest);

        ErrorLog.LoginfoToLogFile("request before enc ="
            + FinalRequest, "GetSIStatus");

        request.enc_request = ccaCrypto.Encrypt(FinalRequest, workingKey);

        ErrorLog.LoginfoToLogFile("request after enc ="
            + request.enc_request, "GetSIStatus");

        string FinalRequest1 = JsonConvert.SerializeObject(request);

        ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

        var client = new RestClient(url);
        client.Timeout = -1;
        var reques11 = new RestRequest(Method.POST);
        reques11.AddParameter("enc_request", request.enc_request);
        reques11.AddParameter("si_sub_ref_no", request.si_sub_ref_no);
        reques11.AddParameter("reference_no", request.reference_no);
        reques11.AddParameter("access_code", request.access_code);
        reques11.AddParameter("request_type", request.request_type);
        reques11.AddParameter("response_type", request.response_type);
        reques11.AddParameter("version", request.version);
        reques11.AddParameter("command", request.command);

        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        IRestResponse responseRest = client.Execute(reques11);
        var result = responseRest.Content;

        ErrorLog.LoginfoToLogFile("response =" + result, "GetSIStatus");

        string[] restul = result.Split('&');
        string[] statusstr = restul[0].Split('=');
        string[] enc_responsestr = restul[1].Split('=');
        string status = statusstr[1];
        string enc_response = enc_responsestr[1].Replace("\r\n", "");

        CCAvenueVariable.GetSIStatusResponse response = new CCAvenueVariable.GetSIStatusResponse();
        if (status == "0")
        {
            string enc_requestdec = ccaCrypto.Decrypt(enc_response, workingKey);
            //{"si_sub_ref_no":"SI2327011751527","si_mer_ref_no":"2023927172437397"
            //,"si_setup_amount":"1.00","si_start_date":"2023-09-27","si_end_date":"2025-09-27"
            //,"si_amount":"0.00","si_currency":"INR","si_frequency_type":"","si_frequency":"0"
            //,"si_billing_cycle":"0","si_create_date":"2023-09-27 17:25:18.92"
            //,"si_type":"ONDEMAND","si_amount_limit":"","si_status":"ACTI"
            //,"reference_no":"113024236405","si_nxt_bill_date":"","error_desc":""
            //,"error_code":""}
            ErrorLog.LoginfoToLogFile("response after decruption ="
                + enc_requestdec, "GetSIStatus");
            returnstring = enc_requestdec;
            response = new JavaScriptSerializer().Deserialize<CCAvenueVariable.GetSIStatusResponse>(enc_requestdec);
        }
        else
        {
            response.si_mer_ref_no = ClientCoIndivID.ToString();
            response.error_desc = enc_response;
            response.si_sub_ref_no = "";
            response.si_status = "";

        }
        int resultCode = objAmex.UpdateCCAvenueSaveCardDetails
            (response.si_mer_ref_no, reference_no, "", "", "", ""
            , response.error_desc
            , response.si_sub_ref_no, "", response.si_status
            , response.si_mer_ref_no);

        return returnstring;
    }

    public string GetHDFCChargingStatus(string OrderID, int bookingId)
    {
        string returnstatus = "";
        ErrorLog.LoginfoToLogFile("OrderID=" + OrderID
            + "bookingId=" + bookingId
            , "GetHDFCChargingStatus");

        string url = "", workingKey = "", strAccessCode = "", strmerchant_id = "";

        CCACrypto ccaCrypto = new CCACrypto();

        AmexCardCls objAmex = new AmexCardCls();
        //DataSet ds = new DataSet();
        //ds = objAmex.GetGuestDetailsForCCAvenueFetchOnly(OrderID);
        //string si_sub_ref_no = "", reference_no = "";
        //if (ds.Tables[0].Rows.Count > 0)
        //{
        //    order_no = ds.Tables[0].Rows[0]["order_no"].ToString();
        //    si_sub_ref_no = ds.Tables[0].Rows[0]["si_sub_ref_no"].ToString();
        //}

        workingKey = GuestRegistrationErrorDesc.GetConfigurationValue("HDFCCCAvenueWorkingKey");
        strAccessCode = GuestRegistrationErrorDesc.GetConfigurationValue("HDFCCCAvenueAccessCode");
        strmerchant_id = GuestRegistrationErrorDesc.GetConfigurationValue("HDFCCCAvenueMerchant_id");
        url = GuestRegistrationErrorDesc.GetConfigurationValue("HDFCServiceURL");

        JavaScriptSerializer js = new JavaScriptSerializer();

        CCAvenueVariable.GetHDFCStatusRequest request = new CCAvenueVariable.GetHDFCStatusRequest();
        CCAvenueVariable.GetHDFCStatusEncRequest encrequest = new CCAvenueVariable.GetHDFCStatusEncRequest();
        request.access_code = strAccessCode;
        request.request_type = "JSON";
        request.response_type = "JSON";
        request.command = "orderStatusTracker";
        request.order_no = OrderID; //"SI2324310033504"; //Unique HDFC sub reference number shared at setup SI.
        //request.reference_no = reference_no; //"312010256949"; //Unique merchant reference number for the SI charge.
        request.version = "1.1";

        //encrequest.si_sub_ref_no = request.si_sub_ref_no; //Unique HDFC sub reference number shared at setup SI.
        encrequest.order_no = request.order_no; //Unique merchant reference number for the SI charge.

        string FinalRequest = JsonConvert.SerializeObject(encrequest);

        ErrorLog.LoginfoToLogFile("request before enc ="
            + FinalRequest, "GetHDFCChargingStatus");

        request.enc_request = ccaCrypto.Encrypt(FinalRequest, workingKey);

        ErrorLog.LoginfoToLogFile("request after enc ="
            + request.enc_request, "GetHDFCChargingStatus");

        string FinalRequest1 = JsonConvert.SerializeObject(request);

        ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

        var client = new RestClient(url);
        client.Timeout = -1;
        var reques11 = new RestRequest(Method.POST);
        reques11.AddParameter("enc_request", request.enc_request);
        //reques11.AddParameter("si_sub_ref_no", request.si_sub_ref_no);
        reques11.AddParameter("order_no", request.order_no);
        reques11.AddParameter("access_code", request.access_code);
        reques11.AddParameter("request_type", request.request_type);
        reques11.AddParameter("response_type", request.response_type);
        reques11.AddParameter("version", request.version);
        reques11.AddParameter("command", request.command);

        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        IRestResponse responseRest = client.Execute(reques11);
        var result = responseRest.Content;

        ErrorLog.LoginfoToLogFile("response =" + result, "GetHDFCChargingStatus");

        string[] restul = result.Split('&');
        string[] statusstr = restul[0].Split('=');
        string[] enc_responsestr = restul[1].Split('=');
        string status = statusstr[1];
        string enc_response = enc_responsestr[1].Replace("\r\n", "");

        CCAvenueVariable.GetHDFCStatusResponse response = new CCAvenueVariable.GetHDFCStatusResponse();
        if (status == "0")
        {
            string enc_requestdec = ccaCrypto.Decrypt(enc_response, workingKey);
            ErrorLog.LoginfoToLogFile("response after decruption ="
                + enc_requestdec, "GetHDFCChargingStatus");

            returnstatus = enc_requestdec;
            response = new JavaScriptSerializer().Deserialize<CCAvenueVariable.GetHDFCStatusResponse>(enc_requestdec);
        }
        else
        {
            response.error_desc = enc_response;
        }

        bool ChargedYN = false;
        string chargingString = "";
        string AwaitedString = "";
        if (response.order_status == "Shipped"
            || response.order_status == "Successful")
        //&& response.order_bank_response == "Y")
        {
            ChargedYN = true;
        }
        else
        {
            if (response.order_status == "AWAITED")
            {
                AwaitedString = "Your transaction is pending at Bank end, please wait for 30 min before trying again.";
            }
            ChargedYN = false;
        }

        chargingString = "Order ID : " + OrderID
                + "<br/>" + "amount : " + response.order_amt
                + "<br/>" + "Order Status : " + response.order_status
                + "<br/>" + "BookingID : " + bookingId
                + "<br/>" + AwaitedString;

        int resultCode = objAmex.UpdateHDFCChargingDetails
                (bookingId, response.order_no, response.reference_no
                , response.order_bank_ref_no, response.order_status
                , "", response.order_card_name
                , response.order_bank_response, ChargedYN);


        return chargingString;
    }

    public string GetCCAvenueAmexChargingStatus(string OrderID
        , int bookingId)
    {
        string returnstatus = "";
        ErrorLog.LoginfoToLogFile("OrderID=" + OrderID
            + "bookingId=" + bookingId
            , "GetCCAvenueAmexChargingStatus");

        string url = "", workingKey = "", strAccessCode = "", strmerchant_id = "";

        CCACrypto ccaCrypto = new CCACrypto();

        AmexCardCls objAmex = new AmexCardCls();

        workingKey = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueAmexCCAvenueWorkingKey");
        strAccessCode = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueAmexCCAvenueAccessCode");
        strmerchant_id = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueAmexCCAvenueMerchant_id");
        url = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueAmexServiceURL");

        JavaScriptSerializer js = new JavaScriptSerializer();

        CCAvenueVariable.GetHDFCStatusRequest request = new CCAvenueVariable.GetHDFCStatusRequest();
        CCAvenueVariable.GetHDFCStatusEncRequest encrequest = new CCAvenueVariable.GetHDFCStatusEncRequest();
        request.access_code = strAccessCode;
        request.request_type = "JSON";
        request.response_type = "JSON";
        request.command = "orderStatusTracker";
        request.order_no = OrderID; //"SI2324310033504"; //Unique HDFC sub reference number shared at setup SI.
        //request.reference_no = reference_no; //"312010256949"; //Unique merchant reference number for the SI charge.
        request.version = "1.1";

        //encrequest.si_sub_ref_no = request.si_sub_ref_no; //Unique HDFC sub reference number shared at setup SI.
        encrequest.order_no = request.order_no; //Unique merchant reference number for the SI charge.

        string FinalRequest = JsonConvert.SerializeObject(encrequest);

        ErrorLog.LoginfoToLogFile("request before enc ="
            + FinalRequest, "GetCCAvenueAmexChargingStatus");

        request.enc_request = ccaCrypto.Encrypt(FinalRequest, workingKey);

        ErrorLog.LoginfoToLogFile("request after enc ="
            + request.enc_request, "GetCCAvenueAmexChargingStatus");

        string FinalRequest1 = JsonConvert.SerializeObject(request);

        ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

        var client = new RestClient(url);
        client.Timeout = -1;
        var reques11 = new RestRequest(Method.POST);
        reques11.AddParameter("enc_request", request.enc_request);
        //reques11.AddParameter("si_sub_ref_no", request.si_sub_ref_no);
        reques11.AddParameter("order_no", request.order_no);
        reques11.AddParameter("access_code", request.access_code);
        reques11.AddParameter("request_type", request.request_type);
        reques11.AddParameter("response_type", request.response_type);
        reques11.AddParameter("version", request.version);
        reques11.AddParameter("command", request.command);

        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        IRestResponse responseRest = client.Execute(reques11);
        var result = responseRest.Content;

        ErrorLog.LoginfoToLogFile("response =" + result, "GetCCAvenueAmexChargingStatus");

        string[] restul = result.Split('&');
        string[] statusstr = restul[0].Split('=');
        string[] enc_responsestr = restul[1].Split('=');
        string status = statusstr[1];
        string enc_response = enc_responsestr[1].Replace("\r\n", "");

        CCAvenueVariable.GetHDFCStatusResponse response = new CCAvenueVariable.GetHDFCStatusResponse();
        if (status == "0")
        {
            string enc_requestdec = ccaCrypto.Decrypt(enc_response, workingKey);
            ErrorLog.LoginfoToLogFile("response after decruption ="
                + enc_requestdec, "GetCCAvenueAmexChargingStatus");

            returnstatus = enc_requestdec;
            response = new JavaScriptSerializer().Deserialize<CCAvenueVariable.GetHDFCStatusResponse>(enc_requestdec);
        }
        else
        {
            response.error_desc = enc_response;
        }

        bool ChargedYN = false;
        string chargingString = "";
        string AwaitedString = "";
        if (response.order_status == "Shipped"
            || response.order_status == "Successful")
        {
            ChargedYN = true;
        }
        else
        {
            if (response.order_status == "AWAITED")
            {
                AwaitedString = "Your transaction is pending at Bank end, please wait for 30 min before trying again.";
            }
            ChargedYN = false;
        }

        chargingString = "Order ID : " + OrderID
                + "<br/>" + "amount : " + response.order_amt
                + "<br/>" + "Order Status : " + response.order_status
                + "<br/>" + "BookingID : " + bookingId
                + "<br/>" + AwaitedString;

        int resultCode = objAmex.UpdateCCAvenueAmexChargingDetails
                (bookingId, response.order_no, response.reference_no
                , response.order_bank_ref_no, response.order_status
                , "", response.order_card_name
                , response.order_bank_response, ChargedYN);


        return chargingString;
    }

    //public string CancelSI(int BookingID, string Totalcost)
    //{
    //    string returnstring = "", url = "", workingKey = "", strAccessCode = "", strmerchant_id = "";

    //    CCACrypto ccaCrypto = new CCACrypto();

    //    workingKey = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueWorkingKey");
    //    strAccessCode = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueAccessCode");
    //    strmerchant_id = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueMerchant_id");
    //    url = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueServiceURL");

    //    CCAvenueVariable.CancelSIRequest request = new CCAvenueVariable.CancelSIRequest();

    //    request.access_code = strAccessCode;
    //    request.request_type = "JSON";
    //    request.response_type = "JSON";
    //    request.command = "cancelSI";
    //    request.si_sub_ref_no = BookingID.ToString(); //Unique HDFC sub reference number shared at setup SI.
    //    request.version = "1.2";

    //    string FinalRequest = JsonConvert.SerializeObject(request);

    //    request.enc_request = ccaCrypto.Encrypt(FinalRequest, workingKey);

    //    ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

    //    ErrorLog.LoginfoToLogFile("FinalRequest=" + FinalRequest, "cancelSI");

    //    CCAvenueVariable.CancelSIResponse response = new CCAvenueVariable.CancelSIResponse();

    //    try
    //    {
    //        var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
    //        httpWebRequest.ContentType = "application/json";
    //        httpWebRequest.Method = "POST";
    //        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
    //        {
    //            string json = FinalRequest;
    //            streamWriter.Write(json);
    //            streamWriter.Flush();
    //            streamWriter.Close();
    //        }

    //        string objResponse = "";
    //        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
    //        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
    //        {
    //            var result = streamReader.ReadToEnd();
    //            objResponse = result.ToString();
    //        }


    //        string encResponse = ccaCrypto.Decrypt(objResponse, workingKey); //decrypt the details

    //        string FinalResponse = JsonConvert.DeserializeObject<string>(encResponse);

    //        ErrorLog.LoginfoToLogFile("FinalResponse=" + FinalResponse, "CreateDebitNoteNotification"); //logs in text file

    //        response = JsonConvert.DeserializeObject<CCAvenueVariable.CancelSIResponse>(FinalResponse);

    //        returnstring = FinalResponse.ToString();
    //    }
    //    catch (Exception ex)
    //    {
    //        returnstring = ex.ToString();
    //    }

    //    return returnstring;
    //}
}