﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AmexModal
/// </summary>
public class AmexModal
{
}
public class AmexSessionRequestBody
{
    public string apiOperation { get; set; }
    public AmexIntraction_OL interaction { get; set; }
    public AmexOrder_OL order { get; set; }

}
public class AmexDisplayControl_OL
{
    public string billingAddress { get; set; }
}
public class AmexIntraction_OL
{
    public string operation { get; set; }
    public AmexMerchant_OL merchant { get; set; }
    public string returnUrl { get; set; }
    public AmexDisplayControl_OL displayControl { get; set; }
}
public class AmexMerchant_OL
{
    public string name { get; set; }
}
public class AmexOrder_OL
{
    public string currency { get; set; }
    public string id { get; set; } //BookingId in carzonrent
    public string amount { get; set; }
    public string description { get; set; }

}

public class Session
{
    public string id { get; set; }
    public string updateStatus { get; set; }
    public string version { get; set; }
}

public class AmexSessionResponse
{
    public string checkoutMode { get; set; }
    public string merchant { get; set; }
    public string result { get; set; }
    public Session session { get; set; }
    public string successIndicator { get; set; }
}
public class BookingDetails
{
    public string bookingId { get; set; }
    public string clientName { get; set; }
    public string totalAmount { get; set; }
    public string approvalNo { get; set; }
}
public class OrderDetails
{
    public int bookingId { get; set; }
    public string ReceiptId { get; set; }
    public string transactionid { get; set; }
    public string paymentid { get; set; }
    public string preauthstatus { get; set; }
    public string resultdescription { get; set; }
    public DateTime createdate { get; set; }
    public string resultIndicator { get; set; }
    public bool chargedyn { get; set; }
    public string approvalNo { get; set; }
    public string totalCost { get; set; }
}

//Get Amex Order Status Api response clasess structure
public class AmexGetOrderStatusResponse
{
    [JsonProperty("3dsAcsEci")]
    public string _3dsAcsEci { get; set; }
    public decimal amount { get; set; }
    public Authentication authentication { get; set; }
    public string authenticationStatus { get; set; }
    public string authenticationVersion { get; set; }
    public Chargeback chargeback { get; set; }
    public DateTime creationTime { get; set; }
    public string currency { get; set; }
    public string description { get; set; }
    public MyDevice device { get; set; }
    public string id { get; set; }
    public DateTime lastUpdatedTime { get; set; }
    public string merchant { get; set; }
    public decimal merchantAmount { get; set; }
    public string merchantCategoryCode { get; set; }
    public string merchantCurrency { get; set; }
    public string result { get; set; }
    public Risk risk { get; set; }
    public SourceOfFunds sourceOfFunds { get; set; }
    public string status { get; set; }
    public decimal totalAuthorizedAmount { get; set; }
    public decimal totalCapturedAmount { get; set; }
    public decimal totalDisbursedAmount { get; set; }
    public decimal totalRefundedAmount { get; set; }
    public Transaction[] transaction { get; set; }
    public Error_OL error { get; set; }

}
public class Authentication
{
    [JsonProperty("3ds")]
    public _3DSAuthentication _3ds { get; set; }
}

public class _3DSAuthentication
{
    public string acsEci { get; set; }
    public string transactionId { get; set; }
}

public class Chargeback
{
    public decimal amount { get; set; }
    public string currency { get; set; }
}

public class MyDevice
{
    public string browser { get; set; }
    public string ipAddress { get; set; }
}

public class Risk
{
    public RiskResponse response { get; set; }
}

public class RiskResponse
{
    public string gatewayCode { get; set; }
    public RiskReview review { get; set; }
}

public class RiskReview
{
    public string decision { get; set; }
}

public class SourceOfFunds
{
    public ProvidedSourceOfFunds provided { get; set; }
    public string type { get; set; }
}

public class ProvidedSourceOfFunds
{
    public Card card { get; set; }
}

public class Card
{
    public string brand { get; set; }
    public CardExpiry expiry { get; set; }
    public string fundingMethod { get; set; }
    public string issuer { get; set; }
    public string nameOnCard { get; set; }
    public string number { get; set; }
    public string scheme { get; set; }
    public string storedOnFile { get; set; }
}

public class CardExpiry
{
    public string month { get; set; }
    public string year { get; set; }
}

public class Transaction
{
    public TransactionAuthentication authentication { get; set; }
    public MyDevice device { get; set; }
    public string merchant { get; set; }
    public Order order { get; set; }
    public TransactionResponse response { get; set; }
    public string result { get; set; }
    public SourceOfFunds sourceOfFunds { get; set; }
    public DateTime timeOfLastUpdate { get; set; }
    public DateTime timeOfRecord { get; set; }
    public TransactionDetails transaction { get; set; }
    public string version { get; set; }
}
public class TransactionDetails
{
    public Acquirer acquirer { get; set; }
    public decimal amount { get; set; }
    public string authenticationStatus { get; set; }
    public string currency { get; set; }
    public string id { get; set; }
    public string stan { get; set; }
    public string type { get; set; }
}
public class Acquirer
{
    public string merchantId { get; set; }
    public int batch { get; set; }
    public string id { get; set; }
}
public class TransactionAuthentication
{
    [JsonProperty("3ds")]
    public _3DSAuthentication ThreeDS { get; set; }
    [JsonProperty("3ds2")]
    public _3DS2Authentication ThreeDS2 { get; set; }
    public string acceptVersions { get; set; }
    public decimal amount { get; set; }
    public string channel { get; set; }
    public string method { get; set; }
    public string payerInteraction { get; set; }
    public string purpose { get; set; }
    public Redirect redirect { get; set; }
    public DateTime time { get; set; }
    public string version { get; set; }
}

public class _3DS2Authentication
{
    public string _3dsServerTransactionId { get; set; }
    public string acsTransactionId { get; set; }
    public string directoryServerId { get; set; }
    public string dsTransactionId { get; set; }
    public string methodSupported { get; set; }
    public string protocolVersion { get; set; }
    public string requestorId { get; set; }
    public string requestorName { get; set; }
    public string statusReasonCode { get; set; }
    public string transactionStatus { get; set; }
}

public class Redirect
{
    public string domainName { get; set; }
}
public class Order
{
    public decimal amount { get; set; }
    public string authenticationStatus { get; set; }
    public Chargeback chargeback { get; set; }
    public string creationTime { get; set; }
    public string currency { get; set; }
    public string description { get; set; }
    public string id { get; set; }
    public string lastUpdatedTime { get; set; }
    public decimal merchantAmount { get; set; }
    public string merchantCategoryCode { get; set; }
    public string merchantCurrency { get; set; }
    public string status { get; set; }
    public decimal totalAuthorizedAmount { get; set; }
    public decimal totalCapturedAmount { get; set; }
    public decimal totalDisbursedAmount { get; set; }
    public decimal totalRefundedAmount { get; set; }
}


public class TransactionResponse
{
    public string acquirerCode { get; set; }
    public string acquirerMessage { get; set; }
    public CardSecurityCode cardSecurityCode { get; set; }
    public string gatewayCode { get; set; }
    public string gatewayRecommendation { get; set; }
}

public class CardSecurityCode
{
    public string acquirerCode { get; set; }
    public string gatewayCode { get; set; }
}

public class AmexRefundRequestBody
{
    public string apiOperation { get; set; }
    public RefundTransactionDetails_OL transaction { get; set; }
}
public class RefundTransactionDetails_OL
{
    public string amount { get; set; }
    public string currency { get; set; }
}


//Refund Api response 


public class TransactionIdentifier
{
    public string posData { get; set; }
    public string transactionIdentifier { get; set; }
}


public class CardSecurity
{
    public string acquirerCode { get; set; }
    public string acquirerMessage { get; set; }
    public CardSecurityCode cardSecurityCode { get; set; }
    public string gatewayCode { get; set; }
}

public class RefundTransaction
{
    public Acquirer acquirer { get; set; }
    public decimal amount { get; set; }
    public string currency { get; set; }
    public string id { get; set; }
    public string receipt { get; set; }
    public string source { get; set; }
    public string stan { get; set; }
    public string terminal { get; set; }
    public string type { get; set; }
}


public class RefundResponse
{
    public TransactionIdentifier authorizationResponse { get; set; }
    public MyDevice device { get; set; }
    public string gatewayEntryPoint { get; set; }
    public string merchant { get; set; }
    public Order order { get; set; }
    public TransactionResponse response { get; set; }
    public string result { get; set; }
    public Risk risk { get; set; }
    public SourceOfFunds sourceOfFunds { get; set; }
    public string timeOfLastUpdate { get; set; }
    public string timeOfRecord { get; set; }
    public RefundTransaction transaction { get; set; }
    public string version { get; set; }
}

//Amex error refund response 

public class Error_OL
{
    public string cause { get; set; }
    public string explanation { get; set; }
    public string field { get; set; }
    public string validationType { get; set; }

}



