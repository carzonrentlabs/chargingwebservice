﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

public class CCAvenueVariable
{
    public class PredebitNotDetails
    {
        public int BookingID { get; set; }
        public int ClientCoIndivID { get; set; }
        public decimal TotalCost { get; set; }
    }
    public class CreateDebitNoteNotificationResponse
    {
        public string status { get; set; }
        public string enc_response { get; set; }
        public string si_mer_charge_ref_no { get; set; }
        public string si_sub_ref_no { get; set; }
        public string si_mer_ref_no1 { get; set; }
        public string si_mer_ref_no2 { get; set; }
        public string notification_id { get; set; }
        public string error_code { get; set; }
        public string si_error_desc { get; set; }
    }
    public class CreateDebitNoteNotificationRequest
    {
        public string enc_request { get; set; }
        public string access_code { get; set; }
        public string request_type { get; set; }
        public string response_type { get; set; }
        public string command { get; set; }
        public string si_sub_ref_no { get; set; }
        public string si_mer_charge_ref_no { get; set; }
        public string si_currency { get; set; }
        public string si_amount { get; set; }
        public string si_mer_ref_no1 { get; set; }
        public string si_mer_ref_no2 { get; set; }
        public string version { get; set; }
    }
    public class DeleteDebitNoteNotificationResponse
    {
        public string status { get; set; }
        public string enc_response { get; set; }
        public string delete_status { get; set; }
        public string error_code { get; set; }
        public string error_desc { get; set; }
        public string si_sub_ref_no { get; set; }
        public string notification_id { get; set; }
    }
    public class DeleteDebitNoteNotificationRequest
    {
        public string enc_request { get; set; }
        public string access_code { get; set; }
        public string request_type { get; set; }
        public string response_type { get; set; }
        public string command { get; set; }
        public string si_sub_ref_no { get; set; }
        public string notification_id { get; set; }
        public string version { get; set; }
    }
    public class PreDebitNoteNotificationResponse
    {
        public string status { get; set; }
        public string enc_response { get; set; }
        public string si_mer_charge_ref_no { get; set; }
        public string si_sub_ref_no { get; set; }
        public string si_mer_ref_no1 { get; set; }
        public string si_mer_ref_no2 { get; set; }
        public string notification_id { get; set; }
        public string error_code { get; set; }
        public string si_error_desc { get; set; }
    }
    public class ChargeSIResponse
    {
        public string si_charge_status { get; set; }
        public string si_mer_charge_ref_no { get; set; }
        public string si_charge_txn_status { get; set; }
        public string si_sub_ref_no { get; set; }
        public string si_error_desc { get; set; }
        public string reference_no { get; set; }
        public string error_code { get; set; }
        public string si_mer_ref_no1 { get; set; }
        public string si_mer_ref_no2 { get; set; }
        public string order_tid { get; set; }
        public string sub_acc_id { get; set; }
    }
    public class ChargeSIRequest
    {
        public string enc_request { get; set; }
        public string access_code { get; set; }
        public string request_type { get; set; }
        public string response_type { get; set; }
        public string command { get; set; }
        public string si_sub_ref_no { get; set; }
        public string si_mer_charge_ref_no { get; set; }
        public string si_currency { get; set; }
        public string si_amount { get; set; }
        public string si_mer_ref_no1 { get; set; }
        public string si_mer_ref_no2 { get; set; }
        public string version { get; set; }
    }
    public class PredebitNotificationSIRequest
    {
        public string enc_request { get; set; }
        public string access_code { get; set; }
        public string request_type { get; set; }
        public string response_type { get; set; }
        public string command { get; set; }
        public string si_sub_ref_no { get; set; }
        public string si_mer_charge_ref_no { get; set; }
        public string si_currency { get; set; }
        public string si_amount { get; set; }
        public string si_mer_ref_no1 { get; set; }
        public string si_mer_ref_no2 { get; set; }
        public string version { get; set; }
    }
    public class ChargeSIEncRequest
    {
        public string si_sub_ref_no { get; set; }
        public string si_mer_charge_ref_no { get; set; }
        public string si_amount { get; set; }
        public string si_currency { get; set; }
        public string notification_id { get; set; }
    }
    public class PredebitNotificationEncRequest
    {
        public string si_sub_ref_no { get; set; }
        public string si_mer_charge_ref_no { get; set; }
        public string si_amount { get; set; }
        public string si_currency { get; set; }
    }
    public class GetSIStatusEncRequest
    {
        public string reference_no { get; set; }
        public string si_sub_ref_no { get; set; }
    }
    public class GetHDFCStatusEncRequest
    {
        public string order_no { get; set; }
    }
    public class GetSIStatusRequest
    {
        public string enc_request { get; set; }
        public string access_code { get; set; }
        public string request_type { get; set; }
        public string response_type { get; set; }
        public string command { get; set; }
        public string reference_no { get; set; }
        public string si_sub_ref_no { get; set; }
        public string version { get; set; }
    }
    public class GetHDFCStatusRequest
    {
        public string enc_request { get; set; }
        public string access_code { get; set; }
        public string request_type { get; set; }
        public string response_type { get; set; }
        public string command { get; set; }
        public string reference_no { get; set; }
        public string order_no { get; set; }
        public string version { get; set; }
    }
    public class GetSIStatusResponse
    {
        public string si_sub_ref_no { get; set; }
        public string si_mer_ref_no { get; set; }
        public string si_setup_amount { get; set; }
        public string si_start_date { get; set; }
        public string si_end_date { get; set; }
        public string si_amount { get; set; }
        public string si_currency { get; set; }
        public string si_frequency_type { get; set; }
        public string si_frequency { get; set; }
        public string si_billing_cycle { get; set; }
        public string si_create_date { get; set; }
        public string si_type { get; set; }
        public string si_amount_limit { get; set; }
        public string si_status { get; set; }
        public string reference_no { get; set; }
        public string si_nxt_bill_date { get; set; }
        public string error_desc { get; set; }
        public string error_code { get; set; }
    }
    public class GetHDFCStatusResponse
    {
        public string status { get; set; }
        public string enc_response { get; set; }
        public string order_amt { get; set; }
        public string order_bill_address { get; set; }
        public string order_bill_city { get; set; }
        public string order_bill_country { get; set; }
        public string order_bill_email { get; set; }
        public string order_bill_name { get; set; }
        public string order_bill_state { get; set; }
        public string order_bill_tel { get; set; }
        public string order_bill_zip { get; set; }
        public string order_capt_amt { get; set; }
        public string order_curr { get; set; }
        public string order_date_time { get; set; }
        public string order_device_type { get; set; }
        public string order_discount { get; set; }
        public string order_fee_flat { get; set; }
        public string order_fee_perc { get; set; }
        public string order_fee_perc_value { get; set; }
        public string order_fraud_status { get; set; }
        public string order_gross_amt { get; set; }
        public string order_ip { get; set; }
        public string order_no { get; set; }
        public string order_notes { get; set; }
        public string order_ship_address { get; set; }
        public string order_ship_city { get; set; }

        public string order_ship_country { get; set; }
        public string order_ship_email { get; set; }
        public string order_ship_name { get; set; }
        public string order_ship_state { get; set; }
        public string order_ship_tel { get; set; }
        public string order_ship_zip { get; set; }
        public string order_status { get; set; }
        public string order_status_date_time { get; set; }
        public string order_TDS { get; set; }
        public string order_tax { get; set; }
        public string reference_no { get; set; }
        public string order_bank_ref_no { get; set; }
        public string order_bank_response { get; set; }
        public string order_gtw_id { get; set; }
        public string order_card_name { get; set; }
        public string order_option_type { get; set; }
        public string Merchant_param1 { get; set; }
        public string Merchant_param2 { get; set; }
        public string Merchant_param3 { get; set; }
        public string Merchant_param4 { get; set; }
        public string Merchant_param5 { get; set; }
        public string error_desc { get; set; }
        public string error_code { get; set; }
        public string page_count { get; set; }
        public string total_records { get; set; }
    }
    public class CancelSIResponse
    {
        public string status { get; set; }
        public string enc_response { get; set; }
        public string error_code { get; set; }
        public string si_error_desc { get; set; }
        public string si_cancel_status { get; set; }
        public string si_ref_no { get; set; }
        public string si_sub_ref_no { get; set; }
    }
    public class CancelSIRequest
    {
        public string enc_request { get; set; }
        public string access_code { get; set; }
        public string request_type { get; set; }
        public string response_type { get; set; }
        public string command { get; set; }
        public string si_sub_ref_no { get; set; }
        public string version { get; set; }
    }
}