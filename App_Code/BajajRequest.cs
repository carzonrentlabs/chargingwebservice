﻿public class OL_CheckLoginResponse
{
    public string LoginId { get; set; }
    public string Password { get; set; }
    public bool IsSucess { get; set; }
    public string Message { get; set; }
}

public class OL_CheckBookingAllowed
{
    public bool IsSucess { get; set; }
    public string Message { get; set; }
}