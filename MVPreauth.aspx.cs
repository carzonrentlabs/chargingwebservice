﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ChargingWebService;
using System.Data;
using System.Net;

public partial class MVPreauth : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string OrderID, PageID, OldBookingID, SendSMSLater, ClientCoID, AgentEmailId, InvoiceNo, RentalDate, RentalType, PickupCity, CustomerName, amount
            , SMSMobiles, SMSMobiles1, bitIsVIP, hdnAcceleration_No, SendMailLater, LimoStatus, IDMain, FYear, ClientCoIndivID, SysRegCode;

        IDMain = Request.Form["IDMain"].ToString();
        FYear = Request.Form["FYear"].ToString();

        amount = Request.Form["Amount"].ToString();
        OrderID = Request.Form["OrderID"].ToString();
        InvoiceNo = OrderID;
        PageID = Request.Form["PageID"].ToString();
        OldBookingID = Request.Form["OldBookingID"].ToString();
        SendSMSLater = Request.Form["SendSMSLater"].ToString();
        ClientCoID = Request.Form["ClientCoID"].ToString();
        SMSMobiles = Request.Form["SMSMobiles"].ToString();
        SMSMobiles1 = Request.Form["SMSMobiles1"].ToString();
        bitIsVIP = Request.Form["IsVIP"].ToString();
        hdnAcceleration_No = Request.Form["hdnAcceleration_No"].ToString();
        SendMailLater = Request.Form["SendMailLater"].ToString();
        LimoStatus = Request.Form["LimoStatus"].ToString();

        clsAdmin objadmin = new clsAdmin();

        DataSet ds = new DataSet();

        string EmailID, PhoneNo, StateCode;
        ds = objadmin.GetBookingSummary(OrderID);

        if (ds.Tables[0].Rows.Count > 0)
        {
            RentalDate = ds.Tables[0].Rows[0]["RentalDate"].ToString();
            PickupCity = ds.Tables[0].Rows[0]["CityName"].ToString();
            CustomerName = ds.Tables[0].Rows[0]["GuestName"].ToString();
            ClientCoIndivID = ds.Tables[0].Rows[0]["ClientCoIndivID"].ToString();
            SysRegCode = ds.Tables[0].Rows[0]["PaymateSysRegCode"].ToString();
            EmailID = ds.Tables[0].Rows[0]["EmailID"].ToString();
            PhoneNo = ds.Tables[0].Rows[0]["Phone1"].ToString();
            StateCode = ds.Tables[0].Rows[0]["StateCode"].ToString();
            
        }
        else
        {
            RentalDate = "24/06/2020";//Convert.ToString(System.DateTime.Now.ToString("dd/mmm/yyyy"));
            PickupCity = "Delhi";
            CustomerName = "test";
            ClientCoIndivID = "0";
            SysRegCode = "";

            EmailID = "";
            PhoneNo = "";
            StateCode = "";
        }

        AgentEmailId = "rahul.jain@carzonrent.com";
        RentalType = "self";
        //InvoiceNo = "123454";
        //OldBookingID = "";
        //SendSMSLater = "";
        //ClientCoID = "";
        //SMSMobiles = "";
        //SMSMobiles1 = "";
        //bitIsVIP = "";
        //hdnAcceleration_No = "";
        //SendMailLater = "";
        //LimoStatus = "";
        //amount = "100";
        //OrderID = "123454";
        //PageID = "CallTaker";
        //PickupCity , RentalDate, CustomerName --to    get from db

        ErrorLog.LoginfoToLogFile("MVPreAuthBeforeRedirect : " + "OrderID=" + OrderID + "&" + "amount=" + amount + "&" + "PageID=" + PageID + "&" + "OldBookingID=" + OldBookingID + "&"
            + "SendSMSLater=" + SendSMSLater + "&" + "ClientCoID=" + ClientCoID + "&" + "SMSMobiles=" + SMSMobiles + "&" + "SMSMobiles1=" + SMSMobiles1
            + "&" + "bitIsVIP=" + bitIsVIP + "&" + "hdnAcceleration_No=" + hdnAcceleration_No + "&" + "SendMailLater=" + SendMailLater + "&" + "LimoStatus=" + LimoStatus
            + "&" + "IDMain=" + IDMain + "&" + "FYear=" + FYear, "MVPreAuth"); //log file 

        objadmin.BeforePreAuth(OrderID, PageID, OldBookingID, SendSMSLater, ClientCoID, SMSMobiles, SMSMobiles1
            , bitIsVIP, hdnAcceleration_No, SendMailLater, LimoStatus, IDMain, FYear);

        //MVPreauth_ClassicAspIntegration(OrderID, AgentEmailId, InvoiceNo, Convert.ToDouble(amount), RentalDate, RentalType, PickupCity, CustomerName, SysRegCode);
        MVPreauth_NewIntegration(OrderID, AgentEmailId, InvoiceNo, Convert.ToDouble(amount), RentalDate, RentalType, PickupCity, CustomerName, SysRegCode
            , Convert.ToInt32(ClientCoIndivID), EmailID, PhoneNo, StateCode);
    }


    public void MVPreauth_ClassicAspIntegration(string ApprovalNo, string AgentEmailId, string InvoiceNo,
            double Amount, string RentalDate, string RentalType, string PickupCity, string CustomerName, string SysRegCode)
    {
        //string url = "https://uat.paymate.co.in/PMCorporate/ProcessAuth.aspx";

        string url = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateLiveURL");

        string EncryptedParameter = "";

        //SysRegCode = "dupZAk"; //Sysregcode for testing
        //SysRegCode = db.GetPaymateSysRegCodeForIndiv(ClientCoIndivid);  //Sysregcode for live

        string AuthUserName = "", AuthPassword = "", AuthCode = "";

        ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

        AuthUserName = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthUserName");
        AuthPassword = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthPassword");
        AuthCode = GuestRegistrationErrorDesc.GetConfigurationValue("LiveAuthCode");

        //UAT
        //AuthUserName = "67592538";
        //AuthPassword = "5BF24F3AA4CA";
        //AuthCode = "2511C6A40B4A";

        //AuthUserName=A1CBDF44&AuthPassword=50964C3DADA1&AuthCode=EF94F8D552B7&SysRegCode=TeDTVg&AgentEmailId=CZRAgt1@czr.com&InvoiceNo=INV00210&Amount=60&RentalDate=28/0
        //6/2020&RentalType=SELF&PickupCity=MUMBAI&ReturnCity=MUMBAI&CustomerName=Ajit Adhikari&CardTypes=Master,Visa&PartnerName=NA

        string CardTypes, PartnerName;
        CardTypes = "Master,Visa";
        PartnerName = "STL";


        ErrorLog.LoginfoToLogFile("MVPreauth_ClassicAspIntegration : " + "AuthUserName=" + AuthUserName + "&" + "AuthPassword=" + AuthPassword + "&" + "AuthCode=" + AuthCode + "&"
            + "SysRegCode=" + SysRegCode + "&" + "AgentEmailId=" + AgentEmailId + "&" + "InvoiceNo=" + ApprovalNo + "&" + "Amount=" + Amount
            + "&" + "RentalDate=" + RentalDate + "&" + "RentalType=" + RentalType + "&" + "PickupCity=" + PickupCity + "&" + "ReturnCity=" + PickupCity
            + "&" + "CustomerName=" + CustomerName + "&" + "CardTypes=" + CardTypes + "&" + "PartnerName=" + PartnerName, "SavePreauthdata"); //log file 


        EncryptedParameter = URLEncrypt("AuthUserName=" + AuthUserName + "&" + "AuthPassword=" + AuthPassword + "&" + "AuthCode=" + AuthCode + "&"
            + "SysRegCode=" + SysRegCode + "&" + "AgentEmailId=" + AgentEmailId + "&" + "InvoiceNo=" + ApprovalNo + "&" + "Amount=" + Amount
            + "&" + "RentalDate=" + RentalDate + "&" + "RentalType=" + RentalType + "&" + "PickupCity=" + PickupCity + "&" + "ReturnCity=" + PickupCity
            + "&" + "CustomerName=" + CustomerName + "&" + "CardTypes=" + CardTypes + "&" + "PartnerName=" + PartnerName);


        ErrorLog.LoginfoToLogFile("MVPreauth_ClassicAspIntegration : " + EncryptedParameter, "SavePreauthdata"); //log file 

        StringBuilder sb = new StringBuilder();
        sb.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
        sb.Append("<html><head></head><body>");
        sb.Append("<form name='form1' method='POST' action='" + url + "'>");
        sb.Append("<input type='hidden' name='EncData' value='" + EncryptedParameter + "'>");
        sb.Append("</form>");
        sb.Append("<script language='javascript'>document.form1.submit();</script>");
        sb.Append("</body></html>");
        Response.Write(sb.ToString());
        Response.End();
    }


    public void MVPreauth_NewIntegration(string ApprovalNo, string AgentEmailId, string InvoiceNo,
           double Amount, string RentalDate, string RentalType, string PickupCity, string CustomerName
        , string SysRegCode, int ClientCoIndivID, string EmailID, string PhoneNo, string StateCode)
    {
        string url = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessURL");

        string EncryptedParameter = "";

        string AuthUserName = "", AuthPassword = "", AuthCode = "";

        ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

        AuthUserName = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessAuthUserName");
        AuthPassword = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessAuthPassword");
        AuthCode = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessAuthCode");

        string CardTypes, PartnerName;
        CardTypes = "Master,Visa";
        PartnerName = "STL";
        string Address1 = "", Address2 = "";
        string ReturnURL = "http://insta.carzonrent.com/MasterPreauth/MVPreauthReturnNew.aspx";
        string TransactionMode = "A";

        ErrorLog.LoginfoToLogFile("MVPreauth_NewIntegration : " + "TravellerId=" + ClientCoIndivID + "&" + "TransactionMode=" + TransactionMode
            + "&" + "InvoiceNo=" + ApprovalNo + "&" + "Amount=" + Amount + "&" + "FirstName=" + CustomerName + "&" + "LastName=" + CustomerName
           + "&" + "EmailId=" + EmailID + "&" + "MobileNo=" + PhoneNo + "&" + "Address1=" + Address1 + "&" + "Address2=" + Address2
           + "&" + "StateCode=" + StateCode + "&" + "City=" + PickupCity + "&" + "PartnerName=" + PartnerName + "&" + "ReturnURL=" + ReturnURL
           , "SavePreauthdata"); //log file 

        EncryptedParameter = URLEncrypt("TravellerId=" + ClientCoIndivID + "&" + "TransactionMode=" + TransactionMode
            + "&" + "InvoiceNo=" + ApprovalNo + "&" + "Amount=" + Amount + "&" + "FirstName=" + CustomerName + "&" + "LastName=" + CustomerName
           + "&" + "EmailId=" + EmailID + "&" + "MobileNo=" + PhoneNo + "&" + "Address1=" + Address1 + "&" + "Address2=" + Address2
           + "&" + "StateCode=" + StateCode + "&" + "City=" + PickupCity + "&" + "PartnerName=" + PartnerName + "&" + "ReturnURL=" + ReturnURL);


        ErrorLog.LoginfoToLogFile("MVPreauth_NewIntegration : " + EncryptedParameter, "SavePreauthdata"); //log file 

        StringBuilder sb = new StringBuilder();
        sb.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
        sb.Append("<html><head></head><body>");
        sb.Append("<form name='form1' method='POST' action='" + url + "'>");
        sb.Append("<input type='hidden' name='EncData' value='" + EncryptedParameter + "'>");
        sb.Append("</form>");
        sb.Append("<script language='javascript'>document.form1.submit();</script>");
        sb.Append("</body></html>");
        Response.Write(sb.ToString());
        Response.End();
    }

    public string URLEncrypt(string strencrypt)
    {
        string cipher = "";
        DESCryptoServiceProvider des = new DESCryptoServiceProvider();
        byte[] key = ASCIIEncoding.ASCII.GetBytes("Paymate1");
        byte[] iv = ASCIIEncoding.ASCII.GetBytes("Paymate1");
        try
        {
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, iv),
            CryptoStreamMode.Write);
            StreamWriter sw = new StreamWriter(cs);
            sw.Write(strencrypt);
            sw.Flush();
            cs.FlushFinalBlock();
            sw.Flush();
            byte[] hash = ms.GetBuffer();
            for (int i = 0; i < ms.Length; i++)
                cipher += BitConverter.ToString(hash, i, 1);
            sw.Close();
            cs.Close();
            ms.Close();
        }
        catch (Exception ex)
        {
            cipher = ex.Message;
        }
        return cipher;
    }
}