﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using ChargingWebService;
using System.Data;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Linq;
using CCA.Util;
using System.Collections.Specialized;

public partial class HDFCRedirect : System.Web.UI.Page
{
    //CCACrypto ccaCrypto = new CCACrypto();
    //string workingKey = "281FE314EBBDEEA20CBA9539D61D2CB1"; //put in the 32bit alpha numeric key in the quotes provided here 	
    //string ccaRequest = "";
    //public string strEncRequest = "";
    //public string strAccessCode = "AVJX28JI73CE69XJEC";// put the access key in the quotes provided here.
    //public string strmerchant_id = "1381993"; //Put your merchant id here

    protected void Page_Load(object sender, EventArgs e)
    {
        //string workingKey = "281FE314EBBDEEA20CBA9539D61D2CB1";//put in the 32bit alpha numeric key in the quotes provided here
        string workingKey = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueWorkingKey"); //put in the 32bit alpha numeric key in the quotes provided here 	
        CCACrypto ccaCrypto = new CCACrypto();

        string encResponse = ccaCrypto.Decrypt(Request.Form["encResp"], workingKey);
        ErrorLog.LoginfoToLogFile(encResponse, "CCAvenueReturn");

        NameValueCollection Params = new NameValueCollection();
        string[] segments = encResponse.Split('&');
        foreach (string seg in segments)
        {
            string[] parts = seg.Split('=');
            if (parts.Length > 0)
            {
                string Key = parts[0].Trim();
                string Value = parts[1].Trim();
                Params.Add(Key, Value);
            }
        }

        for (int i = 0; i < Params.Count; i++)
        {
            Response.Write(Params.Keys[i] + " = " + Params[i] + "<br>");
        }
    }
}