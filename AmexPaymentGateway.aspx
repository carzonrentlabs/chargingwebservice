﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AmexPaymentGateway.aspx.cs" Inherits="AmexPaymentGateway" %>

<!DOCTYPE html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <script src="https://gateway.americanexpress.co.in/static/checkout/checkout.min.js" data-error="errorCallback" data-cancel="cancelCallback"></script>
        <script type="text/javascript">
            function errorCallback(error) {
                console.log(JSON.stringify(error));
            }
            function cancelCallback() {
                console.log('Payment cancelled');
            }
            //function completeCallback(response) {
            //    console.log("response: " , response);
            //    console.log("resultIndicator: ", response.resultIndicator);
            //    console.log("sessionVersion: ", response.sessionVersion);
            //}
            function showPaymentWithSessionId(sessionId) {
                Checkout.configure({
                    session: {
                        id: sessionId
                    }
                });
                Checkout.showPaymentPage();
            }

            // Automatically call showPaymentWithSessionId on page load
            window.addEventListener('load', function () {
                showPaymentWithSessionId('<%= SessionId %>');
            });
        </script>
    </head>
    <body>
        <!-- ... -->
        <div id="embed-target"> </div>
        <!-- ... -->
    </body>
</html>

