﻿using System;
using System.Linq;
using ChargingWebService;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;

public partial class MVChargingReturn : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //string EncData = "xt2ge/+0nulaLpTZxE3LSYmrJMsHHAkHIE3WiiLZgElJUEbhNKgkAeWXtB74/HAfp/WE1hoI+P8IzvqFMD+2NukpCmtAB3bnAp8ScV/6KElEzf40dI4yeAtS6pe0Rz1Mr1xhEUoLfMaTV1zXg2IUVDb36GmWbZpD2lh/G2uR6zQBBzdVuo+1f2zjBZswMKebn96RTX39J3Pm8z4Ixum7IekgSc6w+VZExsPp7PBxu2w7CiiE5DCBFO4zv5HQtEdFbbV+XENQc4Oo4/JPbIP9dkg8gc5jvmCd46eFs4QMjJTZgJlED6oOQfQS08JkgdnIzaXZXA8x3+RWwp/so8PGr2nPTVR8xzQmLSELsNxBPNrl9o6u/XelZK8jteg+/V3jL+Rtp3ZjdqlVjAY4xk92Rme2hJPv1qNVxPLS1RdPhjxrpcKbmyNgIZyrLBrwH68qafJopVr8KpH+M25w/bCboShuTawGBaF+Y0ihF6ztwqU4MBFDhO/NwRIYAQpUSrCVLy9t/mgiXESx5p8XbGreHnnVg+sGn7u95OPLBiCzhxHsFwwQ1N5tk6O2Wb864V9F5LS2kZyJx5XNxqxc7SYaDqqJT1Na6eBorNjlpaCrdH0=";
        string EncData = Request["EncData"];

        ErrorLog.LoginfoToLogFile("MVChargingReturn : " + EncData, ""); //log file 

        string HashKey, EncKey;
        HashKey = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessHashKey");
        EncKey = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessEncKey");

        NameValueCollection nvResponse = new NameValueCollection();

        bool status = Encryption.VerifySecureHash(HashKey, EncKey, EncData, out nvResponse);

        string ResponseCode = "", ResponseMessage = "", TransactionId = "", CardNo = "", CardBrand = ""
           , InvoiceNo = "", Amount = "", TransactionStatus = "", TravellerId = "", TransactionMode = "", PartnerName = "";

        if (status)
        {
            foreach (string key in nvResponse.AllKeys)
            {
                if (key == "Amount")
                {
                    string[] amt = nvResponse.GetValues(key);
                    Amount = amt[0]; //1600
                }
                if (key == "CardNo")
                {
                    string[] cars = nvResponse.GetValues(key);
                    CardNo = cars[0]; //NA
                }
                if (key == "InvoiceNo")
                {
                    string[] invno = nvResponse.GetValues(key);
                    InvoiceNo = invno[0]; //10437169
                }
                if (key == "PartnerName")
                {
                    string[] partn = nvResponse.GetValues(key);
                    PartnerName = partn[0]; //NA
                }
                if (key == "ResponseCode")
                {
                    string[] respcode = nvResponse.GetValues(key);
                    ResponseCode = respcode[0]; //E019
                }
                if (key == "ResponseMessage")
                {
                    string[] resstr = nvResponse.GetValues(key);
                    ResponseMessage = resstr[0]; //Transaction cancelled by user.
                }
                if (key == "TransactionMode")
                {
                    string[] tranm = nvResponse.GetValues(key);
                    TransactionMode = tranm[0]; //C
                }
                if (key == "TransactionRefNo")
                {
                    string[] transid = nvResponse.GetValues(key);
                    TransactionId = transid[0]; //
                }
                if (key == "TransactionStatus")
                {
                    string[] transstats = nvResponse.GetValues(key);
                    TransactionStatus = transstats[0]; //
                }
                if (key == "TravellerId")
                {
                    string[] travid = nvResponse.GetValues(key);
                    TravellerId = travid[0]; //
                }

                if (key == "CardBrand")
                {
                    string[] cardb = nvResponse.GetValues(key);
                    CardBrand = cardb[0]; //
                }
            }
        }
        clsAdmin objadmin = new clsAdmin();

        ErrorLog.LoginfoToLogFile("MVChargingReturn : Amount=" + Amount + ",CardNo=" + CardNo
            + ",InvoiceNo=" + InvoiceNo + ",PartnerName=" + PartnerName
            + ",ResponseCode=" + ResponseCode + ",ResponseMessage=" + ResponseMessage + ",TransactionMode=" + TransactionMode
            + ",TransactionId=" + TransactionId
            + ",TransactionStatus=" + TransactionStatus + ",TravellerId=" + TravellerId + ",CardBrand=" + CardBrand, ""); //log file 

        /*
        1. update invoice status
        2. sent invoice
        */

        if (!string.IsNullOrEmpty(ResponseCode))
        {
            if (TransactionMode == "C")
            {
                objadmin.UpdateInvoiceStatus(ResponseCode, ResponseMessage, TransactionId, TransactionStatus, TravellerId
                , TransactionMode, InvoiceNo, CardNo, CardBrand, PartnerName, Amount);
            }
            else
            {
                objadmin.UpdatePreauthStatusNew(ResponseCode, ResponseMessage, TransactionId, CardNo, CardBrand, InvoiceNo, Amount);
            }
        }
        if (string.IsNullOrEmpty(ResponseCode))
        {
            ResponseCode = "";
        }

        if (string.IsNullOrEmpty(TransactionStatus))
        {
            TransactionStatus = "";
        }

        if (ResponseCode == "E037" && TransactionStatus == "SUCCESSFUL")
        {
            if (TransactionMode == "C")
            {
                Session["ResponseText"] = "Charged Sucessfully.";
            }
            else
            {
                Session["ResponseText"] = "Preauth Sucessfully.";
            }
            //close and update status
            //send invoice
            //SMSMailRef.SendSMSSoapClient sms = new SMSMailRef.SendSMSSoapClient();
            //sms.SendInvoiceLink(Convert.ToInt32(InvoiceNo));

        }
        else
        {
            if (string.IsNullOrEmpty(ResponseMessage))
            {
                ResponseMessage = "Error in Paymate.";
            }

            Session["ResponseText"] = ResponseMessage;
        }

        ErrorLog.LoginfoToLogFile("MVChargingReturn : Before redirect", ""); //log file 

        Response.Redirect("Message.aspx");
    }
}