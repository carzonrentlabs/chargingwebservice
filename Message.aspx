﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Message.aspx.cs" Inherits="Message" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta content="Carzonrent India Pvt. Ltd." />
    <script language="javascript" type="text/javascript">

        function closeWindow() {
            window.close();
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="640" border="0" align="center" cellpadding="0" cellspacing="0" style="background-color: #000000;
            padding: 10px;">
            <tr>
                <td>
                    <table width="640" border="0" align="center" cellpadding="0" cellspacing="0" style="background-color: #FFFFFF;">
                        <tr>
                            <td>
                                <table width="640" border="0" align="center" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <table width="640" border="0" align="center" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="192" style="padding: 10px;">
                                                        <img src="http://insta.carzonrent.com/ICM/App_Themes/SD/cor_logo.png" alt="carzonrent" />
                                                    </td>
                                                    <td width="306" style="padding: 10px 0 0 122px;">
                                                        <img src="http://insta.carzonrent.com/ICM/App_Themes/SD/CallCenterNumber.png" alt="0888 222 2222" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-family: Arial, Helvetica, sans-serif; color: #000000; padding: 10px 0 6px 19px;
                                            line-height: 25px; font-size: 14px;" colspan="2">
                                            <font style="font-size: 14px;">
                                                <asp:Label ID="lbl_message" ForeColor="red" Font-Bold="true" runat="server" Text=""></asp:Label>
                                            </font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-family: Arial, Helvetica, sans-serif; color: #000000; padding: 10px 0 6px 19px;
                                            line-height: 25px; font-size: 14px;" colspan="2">
                                            <font style="font-size: 14px;">
                                                <asp:Button ID="btnClose" runat="server" Text="Close" OnClick="btnClose_Click" />
                                            </font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="bottom">
                                            <img alt="" src="http://insta.carzonrent.com/ICM/App_Themes/SD/connect-with-us.png"
                                                usemap="#Map" border="0" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
    <map name="Map" id="Map">
        <area alt="" shape="circle" coords="294,38,14" href="https://www.facebook.com/carzonrent"
            target="_blank" />
        <area alt="" shape="circle" coords="217,39,16" href="https://plus.google.com/+carzonrent/posts"
            target="_blank" />
        <area alt="" shape="circle" coords="254,38,15" href="https://twitter.com/CarzonrentIN"
            target="_blank" />
        <area alt="" shape="circle" coords="329,38,15" href="http://www.youtube.com/user/CarzonrentTv"
            target="_blank" />
    </map>
</body>
</html>
