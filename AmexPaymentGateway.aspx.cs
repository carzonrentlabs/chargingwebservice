﻿using ChargingWebService;
using Newtonsoft.Json;
using RestSharp.Extensions;
using System;
using System.Configuration;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

public partial class AmexPaymentGateway : Page
{
    protected string SessionId { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Redirect("https://corapi.carzonrent.com/chargingwebservice/CCAvenueAmexChargingLink.aspx?MTrackid=" + Request.QueryString["MTrackid"].ToString().Trim());

        //if (!IsPostBack)
        //{
        //    MakeApiRequest().Wait();
        //}
    }

    private async Task MakeApiRequest()
    {
        try
        {
            AmexCardCls objAmex = new AmexCardCls();

            var amex_session_api_url = ConfigurationManager.AppSettings["AmexSessionApiUrl"];
            var amex_gateway_host = ConfigurationManager.AppSettings["GatewayHost"];
            var amex_auth_value = ConfigurationManager.AppSettings["AmexAuthValue"];
            var return_url = ConfigurationManager.AppSettings["ReturnUrl"];
            var merchant_name = ConfigurationManager.AppSettings["MerchantName"];
            var intraction_operation = ConfigurationManager.AppSettings["IntractionOperationType"];
            var api_operation = ConfigurationManager.AppSettings["ApiOperation"];
            var username = ConfigurationManager.AppSettings["Username"];
            var password = ConfigurationManager.AppSettings["Password"];
            var showBillingAddress = ConfigurationManager.AppSettings["ShowBillingAddress"];

            var apiUrl = amex_gateway_host + amex_session_api_url;

            string encRequestParam = Request.QueryString["MTrackid"].ToString().Trim();

            if (!encRequestParam.Contains(":"))
            {
                Session["ResponseText"] = "Error:Invalid MTrackid parameter.";
                Response.Redirect("Message.aspx");
                return;
            }
            string[] words = encRequestParam.Split(':');
            string bookingId = words[0];
            string amount = words[1];

            if (string.IsNullOrEmpty(encRequestParam) || words.Length > 2)
            {
                // Handle the case where the query parameter is missing or empty
                //Response.Write("Error:Invalid MTrackid parameter.");
                Session["ResponseText"] = "Error:Invalid MTrackid parameter.";
                Response.Redirect("Message.aspx");
                return;
            }

            DataSet EncryptDecrypt = objAmex.EncryptDecryptNew(bookingId, amount, 0);
            if (EncryptDecrypt.Tables[0].Rows.Count > 0)
            {
                string decryptedParamStr = EncryptDecrypt.Tables[0].Rows[0]["BookingID"].ToString();
                string[] decryptedParamArray = decryptedParamStr.Split(':');
                bookingId = decryptedParamArray[0];
                amount = decryptedParamArray[1];
            }

            // Get and set booking details
            DataSet ds = objAmex.BookingDetailsAmexDirectCharge(Convert.ToInt32(bookingId));
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToBoolean(ds.Tables[0].Rows[0]["BookingChargedYN"]))
                {
                    Session["ResponseText"] = "Your booking " + bookingId + " is already charged.";
                    Response.Redirect("Message.aspx");
                    return;
                }
            }

            BookingDetails bookingDetails = new BookingDetails
            {
                bookingId = ds.Tables[0].Rows[0]["BookingId"].ToString(),
                totalAmount = ds.Tables[0].Rows[0]["TotalCost"].ToString(),
                clientName = ds.Tables[0].Rows[0]["ClientName"].ToString(),
                approvalNo = ds.Tables[0].Rows[0]["ApprovalNo"].ToString()
            };

            // Set the TLS version to TLS 1.2
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            var client = new HttpClient();
            var authValue = Convert.ToBase64String(Encoding.ASCII.GetBytes(username + ":" + password));// amex_auth_value;
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + authValue);

            var merchant_OL = new AmexMerchant_OL
            {
                name = merchant_name
            };

            var displayControl_OL = new AmexDisplayControl_OL
            {
                billingAddress = showBillingAddress
            };

            var interaction_OL = new AmexIntraction_OL
            {
                operation = intraction_operation,
                merchant = merchant_OL,
                returnUrl = return_url,
                displayControl = displayControl_OL
            };

            var order_OL = new AmexOrder_OL
            {
                currency = "INR",
                id = bookingDetails.approvalNo,
                amount = bookingDetails.totalAmount,
                description = bookingDetails.clientName
            };



            var requestBody = new AmexSessionRequestBody
            {
                apiOperation = api_operation,
                interaction = interaction_OL,
                order = order_OL
            };

            var jsonBody = Newtonsoft.Json.JsonConvert.SerializeObject(requestBody);

            var content = new StringContent(jsonBody, Encoding.UTF8, "application/json");
            var response = await client.PostAsync(apiUrl, content);

            if (response.IsSuccessStatusCode)
            {
                var responseContent = await response.Content.ReadAsStringAsync();
                AmexSessionResponse responseObj = JsonConvert.DeserializeObject<AmexSessionResponse>(responseContent);
                objAmex.SaveSessionIndicator(Convert.ToInt32(bookingId), responseObj.successIndicator, bookingDetails.approvalNo);
                SessionId = responseObj.session.id;
            }
            else
            {
                Response.Write("Error: API request failed.");
            }
        }
        catch (Exception ex)
        {
            Response.Write("Error: " + ex.Message);
        }
    }

    //private async Task MakeApiRequest()
    //{
    //    try
    //    {

    //        AmexCardCls objAmex = new AmexCardCls();

    //        var amex_session_api_url = ConfigurationManager.AppSettings["AmexSessionApiUrl"];
    //        var amex_gateway_host = ConfigurationManager.AppSettings["GatewayHost"];
    //        var amex_auth_value = ConfigurationManager.AppSettings["AmexAuthValue"];
    //        var return_url = ConfigurationManager.AppSettings["ReturnUrl"];
    //        var merchant_name = ConfigurationManager.AppSettings["MerchantName"];
    //        var intraction_operation = ConfigurationManager.AppSettings["IntractionOperationType"];
    //        var api_operation = ConfigurationManager.AppSettings["ApiOperation"];
    //        var username = ConfigurationManager.AppSettings["Username"];
    //        var password = ConfigurationManager.AppSettings["Password"];

    //        var apiUrl = amex_gateway_host + amex_session_api_url;

    //        //Start decrypt query params here.
    //        //  String bookingId = Request.QueryString["BookingId"].ToString();
    //        string encRequestParam = Request.QueryString["MTrackid"].ToString().Trim();
    //        string[] words = encRequestParam.Split(':');
    //        encRequestParam = words[0];
    //        string encTranAmount = words[1];
    //        string decryptedParamStr = "";
    //        string bookingId = "";
    //        string amount = "";

    //        DataSet EncryptDecrypt = objAmex.EncryptDecryptNew(encRequestParam, encTranAmount,0);
    //        if (EncryptDecrypt.Tables[0].Rows.Count > 0)
    //        {
    //            decryptedParamStr = EncryptDecrypt.Tables[0].Rows[0]["BookingID"].ToString();

    //            string[] decryptedParamArray = decryptedParamStr.Split(':');
    //            bookingId = words[0];
    //            amount = words[1];
    //        }
    //        //End decrypt query params here.

    //        //get and set booking details      
    //        DataSet ds = new DataSet();
    //        ds = objAmex.BookingDetailsAmexDirectCharge(Convert.ToInt32(bookingId));
    //        BookingDetails bookingDetails = new BookingDetails();
    //        if (ds.Tables[0].Rows.Count > 0)
    //        {
    //            if (Convert.ToBoolean(ds.Tables[0].Rows[0]["BookingChargedYN"]))
    //            {
    //                Session["ResponseText"] = "Your booking " + bookingId.ToString()
    //                    + " is already charged.";
    //                Response.Redirect("Message.aspx");
    //            }
    //            bookingDetails.bookingId = ds.Tables[0].Rows[0]["BookingId"].ToString();
    //            bookingDetails.totalAmount = ds.Tables[0].Rows[0]["TotalCost"].ToString();
    //            bookingDetails.clientName = ds.Tables[0].Rows[0]["ClientName"].ToString();
    //            bookingDetails.approvalNo = ds.Tables[0].Rows[0]["ApprovalNo"].ToString();
    //        }

    //        // Set the TLS version to TLS 1.2
    //        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

    //        var client = new HttpClient();
    //        var request = new HttpRequestMessage(HttpMethod.Post, apiUrl);

    //        // Set the Authorization header
    //        var authValue = Convert.ToBase64String(Encoding.ASCII.GetBytes(username + ":" + password));// amex_auth_value; 
    //        request.Headers.Add("Authorization", "Basic " + authValue);

    //        // Create the request body 
    //        var merchant_OL = new AmexMerchant_OL
    //        {
    //            name = merchant_name
    //        };

    //        var interaction_OL = new AmexIntraction_OL
    //        {
    //            operation = intraction_operation,
    //            merchant = merchant_OL,
    //            returnUrl = return_url
    //        };

    //        var order_OL = new AmexOrder_OL
    //        {
    //            currency = "INR",
    //            id = bookingDetails.approvalNo,  // "Amex67670",
    //            amount = "10.00", //bookingDetails.bookingId
    //            description = "TEST"//bookingDetails.clientName    // "TEST"
    //        };

    //        var requestBody = new AmexSessionRequestBody
    //        {
    //            apiOperation = api_operation,
    //            interaction = interaction_OL,
    //            order = order_OL
    //        };

    //        var jsonBody = Newtonsoft.Json.JsonConvert.SerializeObject(requestBody);

    //        // Set the Content-Type header
    //        request.Content = new StringContent(jsonBody, Encoding.UTF8, "application/json");

    //        var response = await client.SendAsync(request);
    //        var responseContent = await response.Content.ReadAsStringAsync();

    //        AmexSessionResponse responseObj = JsonConvert.DeserializeObject<AmexSessionResponse>(responseContent);
    //        objAmex.SaveSessionIndicator(Convert.ToInt32(bookingId), responseObj.successIndicator, bookingDetails.approvalNo);

    //        // Set the SessionId to your actual session ID
    //        SessionId = responseObj.session.id;
    //    }
    //    catch (Exception ex)
    //    {
    //        // Handle any exceptions
    //        Response.Write("Error: " + ex.Message);
    //    }
    //}
}
