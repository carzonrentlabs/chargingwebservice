﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using ChargingWebService;
using Jose;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;


public partial class AmexUserIntimationInvoice : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //createInvoice();
    }

    //static async Task
    public void createInvoice(string BookingID)
    {
        try
        {
            //string BookingID;
            var PreDebitUrl = ConfigurationManager.AppSettings["BilldeskPreDebitNoteUrl"];
            var merchantKey = ConfigurationManager.AppSettings["BilldeskMerchantID"];
            var clientID = ConfigurationManager.AppSettings["BilldeskClientID"];
            //BookingID = "10437123";//"10437536"; //"10437529";// Request.QueryString["ClientCoIndivID"].ToString();
            String BDInvoiceReqNum = DateTime.Now.ToString("HH:mm:ss").Replace(":", "") + BookingID;
            AmexCardCls objAmex = new AmexCardCls();
            //objRegistrationAmex_OL = new clsRegistration();
            CreateInvoice objCreateInvoice = new CreateInvoice();
            DataSet ds = new DataSet();
            ds = objAmex.BookingDetails(Convert.ToInt32(BookingID));
            if (ds.Tables[0].Rows.Count > 0)
            {

                objCreateInvoice.mercid = merchantKey;
                //objCreateInvoice.subscription_refid = "Sub" + ds.Tables[0].Rows[0]["ClientCoIndivID"].ToString();
                //objCreateInvoice.customer_refid = "Cus" + ds.Tables[0].Rows[0]["ClientCoIndivID"].ToString();

                objCreateInvoice.subscription_refid = "Sub" + ds.Tables[0].Rows[0]["TravellerId"].ToString();
                objCreateInvoice.customer_refid = "Cus" + ds.Tables[0].Rows[0]["TravellerId"].ToString();

                objCreateInvoice.additional_info = new AdditionalInfoInvoice();
                objCreateInvoice.additional_info.additional_info1 = "NA";
                objCreateInvoice.additional_info.additional_info2 = "NA";
                objCreateInvoice.additional_info.additional_info3 = "NA";
                objCreateInvoice.additional_info.additional_info4 = "NA";
                objCreateInvoice.additional_info.additional_info5 = "NA";
                objCreateInvoice.additional_info.additional_info6 = "NA";
                objCreateInvoice.additional_info.additional_info7 = "NA";
                objCreateInvoice.additional_info.additional_info8 = "NA";
                objCreateInvoice.additional_info.additional_info9 = "NA";
                objCreateInvoice.additional_info.additional_info10 = "NA";
                objCreateInvoice.invoice_number = BDInvoiceReqNum;//DateTime.Now.ToString("HH:mm:ss").Replace(":", "") + BookingID;
                objCreateInvoice.invoice_display_number = BookingID;
                objCreateInvoice.invoice_date = Convert.ToDateTime(ds.Tables[0].Rows[0]["PickUpDate"]).ToString("yyyy'-'MM'-'dd''");
                //objCreateInvoice.duedate = Convert.ToDateTime(ds.Tables[0].Rows[0]["debitDate"]).ToString("yyyy'-'MM'-'dd''");
                //objCreateInvoice.debit_date = Convert.ToDateTime(ds.Tables[0].Rows[0]["debitDate"]).ToString("yyyy'-'MM'-'dd''");
                objCreateInvoice.duedate = DateTime.Now.AddDays(1).ToString("yyyy'-'MM'-'dd''");
                objCreateInvoice.debit_date = DateTime.Now.AddDays(1).ToString("yyyy'-'MM'-'dd''");



                objCreateInvoice.amount = ds.Tables[0].Rows[0]["TotalCost"].ToString();
                //objCreateInvoice.early_payment_duedate = "";
                //objCreateInvoice.early_payment_discount = "";
                //objCreateInvoice.early_payment_amount = "";
                //objCreateInvoice.late_payment_charges = "";
                //objCreateInvoice.late_payment_amount = "";
                objCreateInvoice.net_amount = ds.Tables[0].Rows[0]["TotalCost"].ToString();
                objCreateInvoice.currency = "356";
                objCreateInvoice.mandateid = Convert.ToString(ds.Tables[0].Rows[0]["MandateID"]);
                objCreateInvoice.description = "Create Invoice before 24 hours.";


            }
            else
            {
                Session["ResponseText"] = "Plesae Provide valid Invoice Number.";
                Response.Redirect("Message.aspx");
            }

            string postData = JsonConvert.SerializeObject(objCreateInvoice);

            var payload = new
            {

                postData = JsonConvert.SerializeObject(objCreateInvoice)

            };
            Int32 unixTimestamp = (Int32)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            var headers = new Dictionary<string, object>()
            {
                { "alg", "HS256"},
                {"clientid", clientID},


            };
            var secretKey = ConfigurationManager.AppSettings["BilldeskEncryptionkey"];
            Jwk key = new Jwk(Encoding.UTF8.GetBytes(secretKey));
            var token = Jose.JWT.Encode(postData, key, JwsAlgorithm.HS256, headers);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var bdTimestamp = unixTimestamp.ToString();
            var traceid = Guid.NewGuid().ToString("N").Substring(0, 15);
            var client = new RestClient(PreDebitUrl);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Accept", "application/jose");
            request.AddHeader("Content-Type", "application/jose");
            request.AddHeader("BD-Traceid", traceid);
            request.AddHeader("BD-Timestamp", bdTimestamp);
            var body = token;
            request.AddParameter("application/jose", body, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            string result = response.Content;
            // Console.WriteLine(response.Content);
            var pgresponse = Jose.JWT.Decode(result, key, JwsAlgorithm.HS256);
            //ErrorLog.LoginfoToLogFile(pgresponse ," Invoice decryprted response ");

            ErrorLog.LoginfoToLogFile("***********************@Request AmexPreDebitNote*********************", "&&\n");
            ErrorLog.LoginfoToLogFile("For predebit Notification booking " + BookingID + "  TraceID " + traceid + " and timeStamp "
                + bdTimestamp + " ", token);
            ErrorLog.LoginfoToLogFile("&&", "&&\n");
            ErrorLog.LoginfoToLogFile("For predebit Notification booking " + BookingID + "  TraceID " + traceid + " and timeStamp "
                + bdTimestamp + " ", postData);
            ErrorLog.LoginfoToLogFile("@Response", "&&\n");
            ErrorLog.LoginfoToLogFile("For predebit Notification booking " + BookingID + "  TraceID " + traceid + " and timeStamp "
                + bdTimestamp + " ", result);
            ErrorLog.LoginfoToLogFile("&&", "&&\n");
            ErrorLog.LoginfoToLogFile("For predebit Notifacation booking " + BookingID + "  TraceID " + traceid + " and timeStamp "
                + bdTimestamp + " ", pgresponse);
            ErrorLog.LoginfoToLogFile("**********************************************************************", "&&\n");
            JObject jObj = JObject.Parse(pgresponse);
            string billDeskInvoiceID = (string)jObj["invoice_id"];
            string verificationErrorCode = string.Empty;
            string verificationErroDesc = string.Empty;
            string verificationErroType = string.Empty;
            string status = (string)jObj["status"];
            if (billDeskInvoiceID != null)
            {
                verificationErrorCode = (string)jObj["verification_error_code"];
                verificationErroDesc = (string)jObj["verification_error_desc"];
                verificationErroType = (string)jObj["verification_error_type"];
            }
            else
            {
                verificationErrorCode = (string)jObj["error_code"];
                verificationErroDesc = (string)jObj["message"];
                verificationErroType = (string)jObj["error_type"];
            }
            int resultCode = objAmex.UpdateBillDeskInvoice(Convert.ToInt32(BookingID), billDeskInvoiceID, BDInvoiceReqNum, status
                , verificationErrorCode, verificationErroType, verificationErroDesc);
        }
        catch (Exception ex)
        {
            ErrorLog.LoginfoToLogFile(ex.Message, "Error Message");
        }
    }
}