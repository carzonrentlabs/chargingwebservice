﻿using System;
using System.Linq;
using ChargingWebService;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
public partial class MVRegisterReturn : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //string EncData = "fZSl1bBpXnS8Hw0zbB4JryJZRGqRu1+54JCYHc2nFrj9twuAag+Yq0rM8KQHHwYMHOj2AusSIF1oQadm4GxrHpuYinTWmYWFWEN+HZFkcKVLTLvMM2xgIvt15uaWV+hR3VBT9TuyMeKXs6u19+kMJt8equJrNgA9CTm7C4UZR8NcoHjRBRtSRJ8GBoxTptZOIFwmqX+tJrn4AONbPdEDJAUJQZC0ouh6Vx+V1zxxLJ54WX5spSCNgDOr3fvLXbUka2tD97ZcBb7n/JX5Ak9SWzzLxIWdDmpQxZHEoJAPpvuiwAC+rZGXILm4tEsziKRh2Z0kulqyakTOt0MRwuVIzMLjbnSbmcQ8Z9Knt39V/eDX3SrlGyHS8coV+6BQyngx6BPI/RCg5lK7lmd6EloHP3qE0yO+OTJ5psY9q7TEv7fwCyM8NJOWtpDI3kyhQbW7fEa4nROvJoSxkrq5W/CoiQJN/9dpEtqmnT+WSyRFgjgSDOWeruKDij1lupKtEC4L9PapouMwMpB37ZQE9VBmnsxEyFFhimVOEWPW2m9uoa72cx865Y5voMEgigUj1G1L+sPuIw6WxEmtkomZQOSHnBmg+rdGdTC4oKlsJw0j2RmcG3l13dnJ4GzjqSVYPJ4/iIO+qrh9nImynAtHvTF5jR5eMz+yONGrFIBF7pHYWyGpCRAI8QSS0Oeg1f6PUxzLzDQMGQ8ueKXiLdPH00tXQHKEPsfvJ86VBvVK25jWjdDmQ/mojmgspmYSpiWTHIizrojiUfCKHqlf67OFxUwCpgXxvekcbWuQF7QSPJKZBftUg1BF1jM/4iH3rr0p6K+r2ssxa0o4fB1BWlv7sJNhU/my+fvnwtAtwb5R1Oeo/WU=";
        string EncData = Request["EncData"];

        ErrorLog.LoginfoToLogFile("MVRegisterReturn : " + EncData, ""); //log file 

        string HashKey, EncKey;
        HashKey = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessHashKey");
        EncKey = GuestRegistrationErrorDesc.GetConfigurationValue("PayMateNewProcessEncKey");

        NameValueCollection nvResponse = new NameValueCollection();

        bool status = Encryption.VerifySecureHash(HashKey, EncKey, EncData, out nvResponse);


        string ResponseCode = "", ResponseMessage = "", TravellerId = "", CardNo = "", CardBrand = ""
            , FirstName = "", LastName = "", EmailId = "", MobileNo = "", Action = "", PartnerName = "";

        if (status)
        {
            foreach (string key in nvResponse.AllKeys)
            {
                if (key == "ResponseCode")
                {
                    string[] respcode = nvResponse.GetValues(key);
                    ResponseCode = respcode[0]; //E019
                }
                if (key == "ResponseMessage")
                {
                    string[] resstr = nvResponse.GetValues(key);
                    ResponseMessage = resstr[0]; //Transaction cancelled by user.
                }
                if (key == "TravellerId")
                {
                    string[] travid = nvResponse.GetValues(key);
                    TravellerId = travid[0]; //
                }
                if (key == "CardNo")
                {
                    string[] cars = nvResponse.GetValues(key);
                    CardNo = cars[0]; //NA
                }
                if (key == "CardBrand")
                {
                    string[] cardb = nvResponse.GetValues(key);
                    CardBrand = cardb[0]; //
                }

                if (key == "FirstName")
                {
                    string[] fnm = nvResponse.GetValues(key);
                    FirstName = fnm[0]; //1600
                }

                if (key == "LastName")
                {
                    string[] lastnm = nvResponse.GetValues(key);
                    LastName = lastnm[0]; //10437169
                }
                if (key == "EmailId")
                {
                    string[] email = nvResponse.GetValues(key);
                    EmailId = email[0]; //C
                }
                if (key == "MobileNo")
                {
                    string[] mobno = nvResponse.GetValues(key);
                    MobileNo = mobno[0]; //
                }
                if (key == "Action")
                {
                    string[] act = nvResponse.GetValues(key);
                    Action = act[0]; //
                }
                if (key == "PartnerName")
                {
                    string[] partn = nvResponse.GetValues(key);
                    PartnerName = partn[0]; //NA
                }
            }
        }


        if (string.IsNullOrEmpty(ResponseCode))
        {
            Session["ResponseText"] = "Site is timeout, please try again later.";
        }
        else
        {
            //ResponseCode = Request["ResponseCode"];
            //ResponseMessage = Request["ResponseMessage"];
            //TravellerId = Request["TravellerId"];
            //CardNo = Request["CardNo"];
            //CardBrand = Request["CardBrand"];
            //FirstName = Request["FirstName"];
            //LastName = Request["LastName"];
            //EmailId = Request["EmailId"];
            //MobileNo = Request["MobileNo"];
            //Action = Request["Action"];
            //PartnerName = Request["PartnerName"];

            clsAdmin objadmin = new clsAdmin();

            ErrorLog.LoginfoToLogFile("SaveRegisterReturn : ResponseCode=" + ResponseCode + ",ResponseMessage=" + ResponseMessage
                + ",TravellerId=" + TravellerId
                + ",CardNo=" + CardNo + ",CardBrand=" + CardBrand + ",FirstName=" + FirstName + ",LastName=" + LastName
                + ",EmailId=" + EmailId + ",MobileNo=" + MobileNo + ",Action=" + Action + ",PartnerName=" + PartnerName
                , "RegisterCardDetail"); //log file 

            Preauth pp = new Preauth();

            objadmin.UpdateSavedCard(ResponseCode, ResponseMessage, TravellerId, CardNo, CardBrand, Action);
            Session["ResponseText"] = ResponseMessage;//"Card details sent for Approval.";
        }


        Response.Redirect("Message.aspx");
    }
}