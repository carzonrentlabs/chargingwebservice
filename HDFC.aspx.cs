﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using ChargingWebService;
using System.Data;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Linq;
using CCA.Util;
public partial class HDFC : System.Web.UI.Page
{
    CCACrypto ccaCrypto = new CCACrypto();

    string workingKey = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueWorkingKey"); //put in the 32bit alpha numeric key in the quotes provided here 	
    //string workingKey = "281FE314EBBDEEA20CBA9539D61D2CB1"; //put in the 32bit alpha numeric key in the quotes provided here 	
    string ccaRequest = "";
    public string strEncRequest = "";
    public string strAccessCode = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueAccessCode");// "AVJX28JI73CE69XJEC";// put the access key in the quotes provided here.
    public string strmerchant_id = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueMerchant_id"); //"1381993"; //Put your merchant id here
    public string strurl = GuestRegistrationErrorDesc.GetConfigurationValue("CCAvenueRegisterURL"); //"1381993"; //Put your merchant id here

    protected void Page_Load(object sender, EventArgs e)
    {
        //String merchant_id = "2193"; //Put your merchant id here
        //String access_code = "F94007DF1640D69A"; //Put access code here
        //String enc_key = "FABE114254BDBC7823534894FFFCCC1"; //Put encryption 

        if (!IsPostBack)
        {
            //foreach (string name in Request.Form)
            //{
            //    if (name != null)
            //    {
            //        if (!name.StartsWith("_"))
            //        {
            //            ccaRequest = ccaRequest + name + "=" + Request.Form[name] + "&";
            //            /* Response.Write(name + "=" + Request.Form[name]);
            //              Response.Write("</br>");*/
            //        }
            //    }
            //}

            string redirecturl = "", cancel_url = "", language = "", si_amount = ""
                    , si_start_date = "", billing_email = "", billing_tel = "", order_id = "", currency = "", amount = "", si_type = "", si_setup_amount = "";


            order_id = "1234586";
            currency = "INR";
            amount = "1";
            redirecturl = "http://instatest.carzonrent.com/chargingwebservice/HDFCRedirect.aspx";
            cancel_url = "http://instatest.carzonrent.com/chargingwebservice/HDFCRedirect.aspx";
            language = "EN";
            si_type = "Ondemand";
            si_amount = "15000";
            si_setup_amount = "Y";
            si_start_date = "30-09-2022";
            billing_email = "rahul.jain@carzonrent.com";
            billing_tel = "9818107670";


            ccaRequest = ccaRequest + "merchant_id" + "=" + strmerchant_id + "&";
            ccaRequest = ccaRequest + "order_id" + "=" + order_id + "&";
            ccaRequest = ccaRequest + "currency" + "=" + currency + "&";
            ccaRequest = ccaRequest + "amount" + "=" + amount + "&";
            ccaRequest = ccaRequest + "redirect_url" + "=" + redirecturl + "&";
            ccaRequest = ccaRequest + "cancel_url" + "=" + cancel_url + "&";
            ccaRequest = ccaRequest + "language" + "=" + language + "&";

            ccaRequest = ccaRequest + "si_type" + "=" + si_type + "&";
            ccaRequest = ccaRequest + "si_amount" + "=" + si_amount + "&";
            ccaRequest = ccaRequest + "si_setup_amount" + "=" + si_setup_amount + "&";
            ccaRequest = ccaRequest + "si_start_date" + "=" + si_start_date + "&";
            ccaRequest = ccaRequest + "billing_email" + "=" + billing_email + "&";
            ccaRequest = ccaRequest + "billing_tel" + "=" + billing_tel;

            ErrorLog.LoginfoToLogFile(ccaRequest, "HDFC");

            strEncRequest = ccaCrypto.Encrypt(ccaRequest, workingKey);
        }
        //if (!Request.QueryString.HasKeys())
        //{
        //    Session["ResponseText"] = "Unable to Process the Request.";
        //    Response.Redirect("Message.aspx");
        //}
        ////changes to do only bookingid required and then get all the details from the db
        //string merchant_id, access_code, enc_key;

        //merchant_id = "1381993";
        //access_code = "AVJX28JI73CE69XJEC";


        //HDFCRedirect(strAccessCode, strEncRequest);
    }

    public void HDFCRedirect(string access_code, string strEncRequest)
    {
        CCACrypto ccaCrypto = new CCACrypto();

        ErrorLog.LoginfoToLogFile("HDFCRedirect : access_code=" + access_code + "&strEncRequest=" + strEncRequest, ""); //log file 
        string EncryptedParameter = "";

        EncryptedParameter = "";

        ErrorLog.LoginfoToLogFile("HDFCCharging: " + EncryptedParameter, "ChargingTransaction"); //log file 

        StringBuilder sb = new StringBuilder();
        sb.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
        sb.Append("<html xmlns='http://www.w3.org/1999/xhtml'><head id='Head1' runat='server'>");
        sb.Append("<script type=text / javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>");
        sb.Append("<script type = 'text /javascript'>");
        sb.Append("$(document).ready(function() {");
        sb.Append("$('#nonseamless').submit();");
        sb.Append("});");
        sb.Append("</ script >");
        sb.Append("<title></title>");

        sb.Append("</head><body>");
        sb.Append("<form id='nonseamless' method='post' name='redirect' action='https://test.ccavenue.com/transaction.do?command=initiateTransaction'>");
        sb.Append("<input type='hidden' id='encRequest' name='encRequest' value='" + strEncRequest + "'>");
        sb.Append("<input type='hidden' id='access_code' name='access_code' value='" + access_code + "'>");
        sb.Append("</form>");
        //sb.Append("<script language='javascript'>document.nonseamless.submit();</script>");
        sb.Append("</body></html>");
        Response.Write(sb.ToString());
        Response.End();
    }
}