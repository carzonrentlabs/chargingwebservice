﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CreditCardRegister.aspx.cs" Inherits="CreditCardRegister" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head runat="server">
    <title>Carzonrent | Retail Booking</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,400;0,500;0,600;0,700;1,700&family=Quicksand:wght@400;500;600;700&display=swap" rel="stylesheet" />


    <script type="text/javascript">
        function callBilldesk() {
            if (document.getElementById("clientcoindivid").value != "") {
                //alert("https://corapi.carzonrent.com/chargingwebservice/AmexCardRegister.aspx?ClientCoIndivID=" + document.getElementById("clientcoindivid").value);
                //window.open("https://corapi.carzonrent.com/chargingwebservice/AmexCardRegister.aspx?ClientCoIndivID=" + document.getElementById("clientcoindivid").value, "_blank");
                location.href = "https://corapi.carzonrent.com/chargingwebservice/AmexCardRegister.aspx?ClientCoIndivID=" + document.getElementById("clientcoindivid").value;
            }
            else {
                alert("Invalid Request.");
            }
        }
        function callAmex() {
            if (document.getElementById("clientcoindivid").value != "") {
                //alert("https://corapi.carzonrent.com/chargingwebservice/CCAvenueCardRegister.aspx?ClientCoIndivID=" + document.getElementById("clientcoindivid").value);
                // window.open("https://corapi.carzonrent.com/chargingwebservice/CCAvenueCardRegister.aspx?ClientCoIndivID=" + document.getElementById("clientcoindivid").value);
                location.href = "https://corapi.carzonrent.com/chargingwebservice/CCAvenueCardRegister.aspx?ClientCoIndivID=" + document.getElementById("clientcoindivid").value;
            }
            else {
                alert("Invalid Request.");
            }
        }
    </script>
    <style>
        .btn-wrapper {
            padding-top: 50px;
        }

        .btn-inline.btn-primary.bgblack {
            background-color: #000;
        }

            .btn-inline.btn-primary.bgblack:hover {
                background: #ffd127;
                color: #fff;
            }

        .btn-inline {
            background-color: #ffd127;
            color: #fff;
            padding: 10px 30px;
            font-size: 16px;
            font-weight: 400;
            border-radius: 5px;
        }

        .d-flex.gap-20.groupbtn {
            gap: 20px;
        }

        .wrapper-main {
            padding-top: 100px;
        }

        a.btn-inline.btn-primary:hover {
            background-color: #000;
            text-decoration: none;
            color: #fff;
        }

        .section-heading-block {
            padding-bottom: 20px;
        }

        .heading {
            font-size: 22px;
            font-weight: 700;
            font-family: 'Poppins';
            margin: 0px;
        }

        .form-outline.mb-10 {
            padding-bottom: 10px;
            height: 36px;
        }

        .form-retail-booking {
            padding: 20px 0px;
        }

        .form-control.formfild:focus {
            outline: none;
            box-shadow: unset;
            border-color: #ffd227;
        }

        .bg-black {
            background-color: #000000;
        }

        .quicksand {
            font-family: 'Quicksand', sans-serif;
        }

        .pdtopbotoom10 {
            padding: 10px 0px;
        }

        .bggrey {
            padding: 6px 0px;
        }

        .form-field-wrapper.mb-10 {
            padding-bottom: 15px;
        }

            .form-field-wrapper.mb-10 .form-control.formfild {
                height: 34px;
                border-radius: 0px;
                font-size: 14px;
                line-height: normal;
                font-family: 'Quicksand';
                font-weight: 500;
            }

        .group-field.d-flex {
            gap: 10px;
        }

        .rightchevrolen {
            font-size: 12px;
            padding: 0 5px;
        }

        body {
            font-family: 'Poppins', sans-serif;
            font-family: 'Quicksand', sans-serif;
        }

        .bggrey {
            background-color: #EBEBEB;
        }

        .breadcrumb-nested-dropdown li {
            display: inline-block;
        }

            .breadcrumb-nested-dropdown li a {
                text-decoration: none;
                color: #000000;
                font-size: 14px;
                font-family: 'Poppins', sans-serif;
                line-height: normal;
            }

        .breadcrumb-nested-dropdown {
            list-style: none;
            padding: 0px;
            margin: 0px;
        }
    </style>
</head>

<body>
    <form runat="server">
        <header class="header-start">

            <div class="header-top bg-black text-white">
                <div class="container">
                    <div class="d-flex justify-content-between align-items-center header-top-flex pdtopbotoom10">
                        <div class="header-top-left">
                            <img src="https://www.carzonrent.com/images/carzonrent-logo.png" class="img-fluid carzonrent-logo" />
                        </div>
                        <div class="header-top-left">
                            <a href="#" class="text-white quicksand"><i class="fas fa-sign-out" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="breadcrum-wrapper bggrey">
                <div class="container">
                    <div class="d-flex gap10 breadcrum-inner-block">
                        <nav aria-label="breadcrumb">
                            <ul class="breadcrumb-nested-dropdown">
                                <li class="breadcrumb-items"><a href="#">Home  <i class="fa-solid fa-chevron-right rightchevrolen"></i></a></li>
                                <li class="breadcrumb-items"><a href="#">Tokenize Your Card</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <main class="page-content">
            <section class="wrapper-main">

                <div class="container">
                    <div class="section-heading-block">
                        <h4 class="heading">Please click on the below options to tokenize your card</h4>
                        <asp:HiddenField ID="clientcoindivid" runat="server" />
                        <asp:Label ID="lblderegister" runat="server" />
                    </div>
                    <div class="btn-wrapper">
                        <div class="d-flex gap-20 groupbtn">
                            <div class="btn-items">
                                <a href="#" class="btn-inline btn-primary" onclick="callAmex()">Amex Card</a>
                            </div>
                            <div class="btn-items">
                                <a href="#" class="btn-inline btn-primary bgblack" onclick="callBilldesk()">Master / Visa Card</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </main>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </form>
</body>
</html>
