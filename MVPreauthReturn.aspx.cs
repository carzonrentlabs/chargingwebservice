﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ChargingWebService;
using System.Data;
using System.Text;

public partial class MVPreauthReturn : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string ResponseCode, ResponseMessage, TransactionId, Card, CardType, InvoiceNo, Amount;

        ResponseCode = Request["ResponseCode"];
        ResponseMessage = Request["ResponseMessage"];
        TransactionId = Request["TransactionId"];
        Card = Request["Card"];
        CardType = Request["CardType"];
        InvoiceNo = Request["InvoiceNo"];
        Amount = Request["Amount"];

        clsAdmin objadmin = new clsAdmin();
        /*
        ResponseCode = "000";
        ResponseMessage = "Successful";
        TransactionId = "200629154313994";
        Card = "555555******4444";
        CardType = "MASTER";
        InvoiceNo = "10428090";
        Amount = "1730";
        */

        ErrorLog.LoginfoToLogFile("SavePreauthdata : " + ResponseCode + "," + ResponseMessage + "," + TransactionId
            + "," + Card + "," + CardType + "," + InvoiceNo + "," + Amount, "SavePreauthdata"); //log file 

        Preauth pp = new Preauth();

        objadmin.UpdatePreauthStatus(ResponseCode, ResponseMessage, TransactionId, Card, CardType, InvoiceNo, Amount);
        string LimoStatus1 = "";

        if (ResponseCode == "000")
        {
            LimoStatus1 = pp.LimoStatus;
            if (LimoStatus1 != "LimoStatus")
            {
                LimoStatus1 = "";
            }

            pp = objadmin.UpdateBookingPreauthStatus(ResponseCode, ResponseMessage, TransactionId, Card, CardType, InvoiceNo, Amount, LimoStatus1);

            pp.PreAuthStatus = InvoiceNo + "|" + ResponseCode + "|" + ResponseMessage;
            //pp = objadmin.GetPreauthByBookingID(InvoiceNo);

            if (!objadmin.IsBookingDispatched(InvoiceNo))
            {
                SMSMailRef.SendSMSSoapClient sms = new SMSMailRef.SendSMSSoapClient();
                if (pp.SendMailLater == "1")
                {
                    sms.ConfirmationMail(Convert.ToInt32(pp.BookingID), 0);
                }

                if (pp.SendSMSLater == "1")
                {
                    if (pp.bitIsVIP == "True")
                    {
                        if (!string.IsNullOrEmpty(pp.hdnAcceleration_No))
                        {
                            if (pp.hdnAcceleration_No.Contains(','))
                            {
                                string[] facilitatorMob = pp.hdnAcceleration_No.Split(',');

                                foreach (string facMob in facilitatorMob)
                                {
                                    if (facMob != "0" && facMob.Length >= 10)
                                    {
                                        sms.SendSMSDetails_smsdetails_GetDetails("Facilitator", "91" + facMob, pp.BookingID, 1);
                                    }
                                }

                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(pp.SMSMobiles))
                    {
                        if (pp.SMSMobiles.Length >= 12)
                        {
                            string smsstatus = sms.SendSMSDetails_smsdetails_GetDetails("Guest", "91" + pp.SMSMobiles, pp.BookingID, 1);

                            //update sms status
                            objadmin.Updatesmsstatus(pp.BookingID, smsstatus);
                        }
                    }

                    if (!string.IsNullOrEmpty(pp.SMSMobiles1))
                    {
                        if (pp.SMSMobiles != pp.SMSMobiles1)
                        {
                            if (pp.SMSMobiles1.Length >= 12)
                            {
                                sms.SendSMSDetails_smsdetails_GetDetails("Guest", "91" + pp.SMSMobiles1, pp.BookingID, 1);
                            }
                        }
                    }

                }
            }
            else
            {
                pp.BookingID = "0";
            }
        }
        string WebsiteURL = System.Configuration.ConfigurationManager.AppSettings["WebsiteURL"];
        pp.FrmIdentifyCode = "";
        //url
        if (ResponseCode == "000")
        {
            if (pp.PageID == "CORP")
            {
                //session id to put
                pp.PostURL = "../Message/ConfirmMessage.asp"; //url to change
                pp.FrmIdentifyCode = "BCS";
            }
            else if (pp.PageID == "NewCORP")
            {
                //session id to put
                pp.PostURL = "../DutySlips/MVPreauthStatus.asp"; //url to change
                pp.FrmIdentifyCode = "BCS";

            }
            else if (pp.PageID == "INDV")
            {
                //session id to put
                pp.PostURL = "../Message/ConfirmMessage.asp"; //url to change
                pp.FrmIdentifyCode = "BIS";
            }
            else if (pp.PageID == "VOID")
            {
                //session id to put
                pp.PostURL = "../Message/PreAuthVoidMessage.asp"; //url to change
            }
            else if (pp.PageID == "CWEB" || pp.PageID == "FWEB" || pp.PageID == "IWEB")
            {
                //session id to put
                pp.PostURL = "http://corporate.carzonrent.com/CorporateSite/CorpBookAddMessage.asp";
            }
            else if (pp.PageID == "CRDApps")
            {
                //session id to put
                //pp.PostURL = "https://crdapi.carzonrent.com/crd-booking-live/booking/payment/insta_clbk?status=" + pp.PreAuthStatus; //url to change
                pp.PostURL = WebsiteURL + "payment/insta_clbk?status=" + pp.PreAuthStatus; //url to change
                //pp.PostURL = "http://localhost:8080/booking/payment/insta_clbk?status=" + pp.PreAuthStatus; //url to change
            }
            else if (pp.PageID == "CRDAppsTest")
            {
                //session id to put
                //pp.PostURL = "https://crdapi.carzonrent.com/crd-booking-live/booking/payment/insta_clbk?status=" + pp.PreAuthStatus; //url to change
                pp.PostURL = WebsiteURL + "payment/insta_clbk?status=" + pp.PreAuthStatus; //url to change
            }
            else if (pp.PageID == "CallTaker")
            {
                //session id to put
                pp.PostURL = "http://insta.carzonrent.com/corcalltaker/Corporate/PreAuthMasterVISAStatus?PaymentStatus=" + pp.PreAuthStatus + "&BookingId=" + pp.BookingID; //url to change
            }
            else if (pp.PageID == "CallTakerTest")
            {
                //session id to put
                pp.PostURL = "http://localhost:1514/Corporate/PreAuthMasterVISAStatus?PaymentStatus=" + pp.PreAuthStatus + "&BookingId=" + pp.BookingID; //url to change
            }
            else
            {

            }
        }
        else
        {
            if (pp.PageID == "CRDApps")
            {
                //session id to put
                //pp.PostURL = "https://crdapi.carzonrent.com/crd-booking-live/booking/payment/insta_clbk?status=" + pp.PreAuthStatus; //url to change
                //pp.PostURL = "http://localhost:8080/booking/payment/insta_clbk?status=" + pp.PreAuthStatus; //url to change
                pp.PostURL = WebsiteURL + "payment/insta_clbk?status=" + pp.PreAuthStatus; //url to change
            }
            else if (pp.PageID == "CRDAppsTest")
            {
                //session id to put
                //pp.PostURL = "https://crdapi.carzonrent.com/crd-booking-live/booking/payment/insta_clbk?status=" + pp.PreAuthStatus; //url to change
                pp.PostURL = WebsiteURL + "payment/insta_clbk?status=" + pp.PreAuthStatus; //url to change
            }
            else if (pp.PageID == "CallTaker")
            {
                //session id to put
                pp.PostURL = "http://insta.carzonrent.com/corcalltaker/Corporate/PreAuthMasterVISAStatus?PaymentStatus=" + pp.PreAuthStatus + "&BookingId=" + pp.BookingID; //url to change
            }
            else if (pp.PageID == "CallTakerTest")
            {
                //session id to put
                pp.PostURL = "http://localhost:1514/Corporate/PreAuthMasterVISAStatus?PaymentStatus=" + pp.PreAuthStatus + "&BookingId=" + pp.BookingID; //url to change
            }
            else
            {
                pp.PostURL = WebsiteURL + "payment/insta_clbk?status=" + pp.PreAuthStatus; //url to change
            }
        }

        if (!string.IsNullOrEmpty(pp.PostURL))
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
            sb.Append("<html><head></head><body>");
            sb.Append("<form name='form1' method='POST' action='" + pp.PostURL + "'>");

            sb.Append("<input type='hidden' name='FrmIdentifyCode' value='" + pp.FrmIdentifyCode + "'>");
            sb.Append("<input type='hidden' name='BookingID' value='" + pp.BookingID + "'>");
            sb.Append("<input type='hidden' name='ServiceUnit' value='" + pp.UnitName + ", " + pp.CityName + "'>");
            sb.Append("<input type='hidden' name='TransID' value='" + TransactionId + "'>");
            sb.Append("<input type='hidden' name='OldBookingID' value='" + pp.BookingID + "'>");
            sb.Append("<input type='hidden' name='WSCorpID' value='" + pp.ClientCoID + "'>");
            sb.Append("<input type='hidden' name='WSCorpName' value='" + "" + "'>"); //limo id -- to check the logic
            sb.Append("<input type='hidden' name='PageID' value='" + pp.PageID + "'>");

            sb.Append("</form>");
            sb.Append("<script language='javascript'>document.form1.submit();</script>");
            sb.Append("</body></html>");
            //return sb.ToString();
            Response.Write(sb.ToString());
            Response.End();
        }
    }
}